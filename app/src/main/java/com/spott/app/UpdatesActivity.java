package com.spott.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;


/**
 * Created by Adimn on 10/22/2016.
 */
public class UpdatesActivity extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {


    ImageView iv_menu_icon;
    TextView tv_number, tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_num, iv_red_icon, tv_update_notif, markAsRead;
    Activity activity;
    DiamondImageView img_profile;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout rlMainFront, ll_groups;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    View shadowView = null, view = null;
    int height, width;
    boolean drawer_open = false;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    NavigationFragmentAdapter mAdapter;
    String string = "#ed3224";
    String string1 = "#ffffff";
    LayoutInflater layoutInflater = null;
    ViewPager pager;
    Context context;
    int count = 0;
    String has_more = "",feed_count="";
    ArrayList<HashMap<String, String>> mygrouplist;
    private BroadcastReceiver broadcastReceiver;
    GroupAdapter1 gAdapter;
    private PullToRefreshListView pulllist;
    private PullToRefreshListView list;
    SharedPreferences preferences;
    String firstname = "", lastname = "", gender = "";
    String encoded;
    String you_like = "";
    String star = "", group = "", post = "", notification = "";
    public static ArrayList<HashMap<String, String>> updateList;
    UpdateAdapter updateAdapter;
    private boolean isLoading = false,showProgressDialog=true;
    private ArrayList<ImageView> dotsNavigate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updates);
        this.activity = (Activity) UpdatesActivity.this;
        context = UpdatesActivity.this;
        mAdapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(mAdapter);

        layoutInflater = LayoutInflater.from(this);
        list = (PullToRefreshListView) findViewById(R.id.list);
        context = getApplicationContext();
        mygrouplist = new ArrayList<HashMap<String, String>>();
        updateList = new ArrayList<HashMap<String, String>>();
        initialise();
        clickables();

        addDotsNavigation();
        setAdapter();

        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        gender = preferences.getString("gender", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");
        notification = preferences.getString("notifications_id", "");

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                new UpdateData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            else
                new UpdateData().execute();

        }
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        view.setOnClickListener(this);

        list.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                {
                    if (!isLoading) {
                        if (has_more.equalsIgnoreCase("true")) {
                            isLoading = true;
                            if (!connDec.isConnectingToInternet()) {
                                alert.showNetAlert();
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new UpdateData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                                } else {
                                    new UpdateData().execute("loadmore");
                                }
                            }
                        } else {
                            Toast.makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                        }

                    }
                }

            }
        });
        list.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                count = 0;
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new UpdateData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new UpdateData().execute();
                    }

                    updateList.clear();
                }
            }
        });

    }

    private void initialise() {
        connDec = new ConnectionDetector(UpdatesActivity.this);
        alert = new Alert_Dialog_Manager(context);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_update_notif = (TextView) findViewById(R.id.tv_update_notif);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        markAsRead = (TextView) findViewById(R.id.markAsRead);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);
                    if (i == 0) {

                        iv_red_icon.setVisibility(View.GONE);
                        feed_count=i + "";
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        feed_count=i + "";
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        feed_count="99+";
                        iv_red_icon.setText("99+");
                    }
                    mAdapter.notifyDataSetChanged();
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();
        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }

    private void clickables() {

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();

                    drawer_open = true;
                    pager.setAdapter(mAdapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);

                } else if (drawer_open == true) {

                    StopDrawerAnim();

                    drawer_open = false;
                }

            }
        });

        markAsRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new MarkAllAsReadService().execute();
            }
        });

    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:

                Intent intent4 = new Intent(UpdatesActivity.this, MyFeedActivity.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent4);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(UpdatesActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(UpdatesActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(UpdatesActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    @Override
    public void onResume() {

        super.onResume();

        getAllData();

        final int pos = 1;
        pager.postDelayed(new Runnable() {

            @Override
            public void run() {
                pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
            }
        }, 100);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ((UTCAPP) getApplication()).pagerPos = position;

                selectDotNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void getAllData() {
        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GroupData().execute();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

// For Navigation Animation

    private class NavigationFragmentAdapter extends PagerAdapter {


        TextView tv_browsing;

        GroupAdapter1 gAdapter;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront, rll;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_profile_name, tv_profile_username
                ,tv_notification_count,tv_feed, tv_profile, tv_update, tv_groups, tv_staff_dir, tv_logout;

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            if (feed_count.equals("0"))
                tv_notification_count.setVisibility(View.GONE);

            else {
                tv_notification_count.setVisibility(View.VISIBLE);

                tv_notification_count.setText(feed_count);
            }
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);
                rll = (RelativeLayout) view.findViewById(R.id.rll);
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(UpdatesActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                    }
                });
                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupData().execute();
                    }
                }
                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {

                    }

                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;
                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new GroupData().execute();
                            }

                            mygrouplist.clear();
                        }
                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(UpdatesActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_logout = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(CommonUtilities.decodeUTF8(firstname) + " " + CommonUtilities.decodeUTF8(lastname));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));

                if (feed_count.equals("0"))
                    tv_notification_count.setVisibility(View.GONE);

                else {
                    tv_notification_count.setVisibility(View.VISIBLE);

                    tv_notification_count.setText(feed_count);
                }

                if (encoded.equalsIgnoreCase("")) {

                    img_profile.setImageResource(R.drawable.app);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }
                tv_feed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        Intent intent = new Intent(UpdatesActivity.this, MyFeedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);

                    }
                });

                tv_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        Intent intent = new Intent(UpdatesActivity.this, MyProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);

                    }
                });
                img_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(UpdatesActivity.this, MyProfileActivity.class);
                        intent1.putExtra("myProfile", "");
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);
                    }
                });
                tv_groups.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(UpdatesActivity.this, MyCrewsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(UpdatesActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_staff_dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(UpdatesActivity.this, MyDirectoryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });

                tv_logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        customShareLogOutPopup();
                    }
                });
                container.addView(view, 0);
            }
            //   ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = updateList.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new UpdateData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new UpdateData().execute();
                    }
                }
            } else {
            }
        }
    }

    public class GroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {
                        mygrouplist.clear();
                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");

                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);

                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygrouplist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(UpdatesActivity.this, mygrouplist);

                        pulllist.setAdapter(gAdapter);

                        gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygrouplist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygrouplist) {
            this.context = context;
            this.mygrouplist = mygrouplist;
        }

        @Override
        public int getCount() {
            return mygrouplist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            ViewHolder holder = new ViewHolder();
            String value = mygrouplist.get(i).get("unread");
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygrouplist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygrouplist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygrouplist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygrouplist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygrouplist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygrouplist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygrouplist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygrouplist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygrouplist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygrouplist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygrouplist.get(i).get("you_group"));

                    intent.putExtra("from_update", "");// for show list last position if this intent found

                    intent.putExtra("scrollTobottom", "TRUE");

                    startActivity(intent);
                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygrouplist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygrouplist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygrouplist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygrouplist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygrouplist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygrouplist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygrouplist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygrouplist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygrouplist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygrouplist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygrouplist.get(i).get("you_group"));

                    intent.putExtra("from_update", "");

                    intent.putExtra("scrollTobottom", "TRUE");


                    startActivity(intent);
                }
            });


            if (value == "0") {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygrouplist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygrouplist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setText(value);
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    //Log Out the App

    public class LogOutData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    {

                        preferences.edit().clear().commit();
                        ((UTCAPP) getApplication()).joingroupList.clear();

                        Intent i = new Intent(activity, SplashActivity.class);
                        startActivity(i);
                        finishAffinity();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(UpdatesActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(UpdatesActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LogOutData().execute();
                    }
                }
            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    public class UpdateData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            if(showProgressDialog)
            LoaderClass.ShowProgressWheel(UpdatesActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();
                if (strings.length > 0) {
                    if (strings[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }
                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "notification_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                if (!isLoadMore) {
                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", updateList.size() + "");
                }

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            if(showProgressDialog)
            LoaderClass.HideProgressWheel(UpdatesActivity.this);
            showProgressDialog=true;
            list.onRefreshComplete();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    String list = resultObj.getString("list");

                    if (list == null) {

                        Toast.makeText(getApplicationContext(), "No Updates Available", Toast.LENGTH_SHORT).show();
                    } else {

                        JSONArray jArray = resultObj.getJSONArray("list");

                        if (!isLoadMore) {
                            updateList.clear();
                        }

                        for (int i = 0; i < jArray.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject user_dataObj = jArray.getJSONObject(i);
                            String notifications_id = user_dataObj.getString("notifications_id");
                            String noti_read = user_dataObj.getString("noti_read");
                            String survey_added = user_dataObj.getString("survey_added");
                            String uid = user_dataObj.getString("uid");
                            String notifications_message = user_dataObj.getString("notifications_message");
                            String action = user_dataObj.getString("action");
                            String value = user_dataObj.getString("value");
                            String notifications_create_date = user_dataObj.getString("notifications_create_date");

                            hmap.put("notifications_id", notifications_id);
                            hmap.put("noti_read", noti_read);
                            hmap.put("survey_added", survey_added);
                            hmap.put("uid", uid);
                            hmap.put("notifications_message", notifications_message);
                            hmap.put("action", action);
                            hmap.put("value", value);
                            hmap.put("notifications_create_date", notifications_create_date);

                            preferences.edit().putString("notifications_id", notifications_id).commit();
                            preferences.edit().putString("action", action).commit();
                            preferences.edit().putString("value", value).commit();
                            updateList.add(hmap);

                        }

                        setAdapter();

                    }
                }
                else if (errCode.equals("300")) {

                    tv_update_notif.setVisibility(View.VISIBLE);
                    pulllist.setVisibility(View.GONE);

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


            isLoading = false;
        }
    }

    private void setAdapter() {
        if (updateAdapter == null) {
            updateAdapter = new UpdateAdapter(UpdatesActivity.this, updateList);

            list.setAdapter(updateAdapter);
        } else {
            updateAdapter.notifyDataSetChanged();
        }

    }

    public class UpdateNotificationData extends AsyncTask<String, String, String> {


        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";



        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "read_notifications");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("notifications_id", strings[0]);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            showProgressDialog = false;
                            new UpdateData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                         else
                            new UpdateData().execute();

                    }

                } else if (errCode.equals("700"))
                {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class UpdateAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> updateList;

        public UpdateAdapter(Context context, ArrayList<HashMap<String, String>> updateList) {
            this.context = context;
            this.updateList = updateList;
        }

        @Override
        public int getCount() {
            return updateList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.update_adapter, null);
            final ViewHolder holder = new ViewHolder();
            holder.tv_paragp = (TextView) convertView.findViewById(R.id.tv_paragp);
            holder.tv_paragp.setTypeface(Typeface.createFromAsset(getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_update_time = (TextView) convertView.findViewById(R.id.tv_update_time);
            holder.tv_paragp.setText(CommonUtilities.decodeUTF8(updateList.get(i).get("notifications_message")));
            if (updateList == null) {

                tv_update_notif.setVisibility(View.VISIBLE);
                pulllist.setVisibility(View.GONE);

            } else {
                pulllist.setVisibility(View.VISIBLE);
                tv_update_notif.setVisibility(View.GONE);
            }
            if (updateList.get(i).get("noti_read").equals("0")) {

                holder.tv_paragp.setTextColor(Color.parseColor("#F15A51"));

            } else if (updateList.get(i).get("noti_read").equals("1")) {

                holder.tv_paragp.setTextColor(Color.parseColor("#000000"));
            }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        new UpdateNotificationData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, updateList.get(i).get("notifications_id"));
//                    } else {
//                        new UpdateNotificationData().execute(updateList.get(i).get("notifications_id"));
//                    }

                    if (updateList.get(i).get("survey_added").equals("0")) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                            new UpdateNotificationData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, updateList.get(i).get("notifications_id"));
                        else
                            new UpdateNotificationData().execute(updateList.get(i).get("notifications_id"));


                        if (updateList.get(i).get("action").equals("survey")) {
                            Intent intent = new Intent(UpdatesActivity.this, SurveyScreenActivity.class);
                            intent.putExtra("fromupdate", "");
                            intent.putExtra("pos", i);
                            intent.putExtra("value", updateList.get(i).get("value"));
                            startActivity(intent);
                        }
                        else if (updateList.get(i).get("action").equals("group") || updateList.get(i).get("action").equals("group_request") )
                        {
                            Intent intent = new Intent(UpdatesActivity.this, CrewActivity.class);
                            intent.putExtra("group_id", updateList.get(i).get("value"));
                            intent.putExtra("key", updateList.get(i).get("uid"));
                            intent.putExtra("from_update", "");
                            if (updateList.get(i).get("action").equals("group"))
                            {
                                intent.putExtra("scrollTobottom", "TRUE");
                            }
                            else
                            {
                                if(updateList.get(i).get("noti_read").equals("0"))
                                    intent.putExtra("isUnreadNotification","yes");
                            }
                            startActivity(intent);
                        }
                        else if ( updateList.get(i).get("action").equals("invite_group"))
                        {
                            Intent intent = new Intent(UpdatesActivity.this, InviteActivity.class);
                            startActivity(intent);
                        }
                        else  if (updateList.get(i).get("action").equals("post")) {

                            Intent intent = new Intent(UpdatesActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", updateList.get(i).get("value"));
                            intent.putExtra("scrollTobottom", "TRUE");
                            startActivity(intent);
                        }
                        else if (updateList.get(i).get("action").equals("common_push")) {

                            holder.tv_paragp.setTextColor(Color.parseColor("#000000"));
                        }
                    }
                    else if (updateList.get(i).get("survey_added").equals("1"))
                    {
                        if (updateList.get(i).get("action").equals("survey")) {

                            customShareDetailsPopup();
                        }
                        else if (updateList.get(i).get("action").equals("group") || updateList.get(i).get("action").equals("group_request")  ) {

                            Intent intent = new Intent(UpdatesActivity.this, CrewActivity.class);
                            intent.putExtra("group_id", updateList.get(i).get("value"));

                            if (updateList.get(i).get("action").equals("group"))
                            {
                                intent.putExtra("scrollTobottom", "TRUE");
                            }

                            intent.putExtra("from_update", "");
                            startActivity(intent);
                        }

                        else if (updateList.get(i).get("action").equals("post")) {

                            Intent intent = new Intent(UpdatesActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", updateList.get(i).get("value"));
                            startActivity(intent);
                        }
                        else if ( updateList.get(i).get("action").equals("invite_group"))
                        {
                            Intent intent = new Intent(UpdatesActivity.this, InviteActivity.class);
                            startActivity(intent);
                        }
                    }
                }


            });


            try {
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

                String FinalDate = (String) updateList.get(i).get("notifications_create_date");

                Date date1;
                Date date2;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH:mm");


                //Setting dates
                date1 = dates.parse(date);
                date2 = dates.parse(FinalDate);

                //Comparing dates
               /* long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);*/

                long diff = date1.getTime() - date2.getTime();
                long seconds = diff / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;

                //Convert long to String
                //String dayDifference = Long.toString(differenceDates);

               /* System.out.println("HERE Kubs " + seconds);

                System.out.println("HERE Kubs " + minutes);

                System.out.println("HERE Kubs " + hours);

                System.out.println("HERE Kubs " + days);
*/
                // holder.tv_update_time.setText(dayDifference + "d");

                if (days > 0) {
                    holder.tv_update_time.setText(days + " days");
                } else if (minutes >= 0 && minutes <= 60) {
                    holder.tv_update_time.setText(minutes + " mins");
                } else {
                    holder.tv_update_time.setText(hours + " hours");
                }


            } catch (Exception exception) {

                System.out.println("Work " + exception);
            }

            return convertView;
        }

        class ViewHolder {
            TextView tv_paragp, tv_update_time;

        }

        private void customShareDetailsPopup() {
            final Dialog dialog = new Dialog(UpdatesActivity.this);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.update_popup);
            final TextView tv_title_survey, ok_btn, tv_title_participate, tv_title_banned;
            ok_btn = (TextView) dialog.findViewById(R.id.ok_btn);
            tv_title_survey = (TextView) dialog.findViewById(R.id.tv_title_survey);
            tv_title_participate = (TextView) dialog.findViewById(R.id.tv_title_participate);
            tv_title_banned = (TextView) dialog.findViewById(R.id.tv_title_banned);
            tv_title_participate.setVisibility(View.GONE);
            tv_title_banned.setVisibility(View.GONE);
            tv_title_survey.setVisibility(View.VISIBLE);
            ok_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                }
            });

        }

    }

    public class MarkAllAsReadService extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            LoaderClass.ShowProgressWheel(UpdatesActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "all_read");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
                LoaderClass.HideProgressWheel(UpdatesActivity.this);

            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            LoaderClass.HideProgressWheel(UpdatesActivity.this);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0"))
                {
                    new UpdateData().execute();
                }
                else if (errCode.equals("700"))
                {
                    CommonUtilities.showSessionExpirePopup(activity);
                }

            }
            catch (Exception e)
            {
                LoaderClass.HideProgressWheel(UpdatesActivity.this);

                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Updates Screen");

        FlurryAgent.onStartSession(UpdatesActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(UpdatesActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    // drawer navigate functionality
    public void addDotsNavigation() {
        dotsNavigate = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.circle_indicator);

        for (int i = 0; i < 2; i++) {
            ImageView dot = new ImageView(this);

            dot.setImageDrawable(getResources().getDrawable(R.drawable.under_slide_black_out));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            final int pos = i;

            dot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pager.setCurrentItem(pos);
                }
            });

            dotsLayout.addView(dot, params);

            dotsNavigate.add(dot);
        }
    }

    public void selectDotNavigation(int idx) {

        Resources res = getResources();
        for (int i = 0; i < 2; i++) {
            int drawableId = (i == idx) ? (R.drawable.under_slide_black) : (R.drawable.under_slide_black_out);
            Drawable drawable = res.getDrawable(drawableId);
            dotsNavigate.get(i).setImageDrawable(drawable);
        }
    }
}
