package com.spott.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ArticleDetailsActivity extends AppCompatActivity {

    TextView _articleTitle;
    TextView _articleText;
    TextView _articleLikes;
    TextView _articleComments;
    ImageView _articleImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);

        _articleTitle = (TextView) findViewById(R.id.tv_article_title);
        _articleText = (TextView) findViewById(R.id.tv_article_text);
        _articleLikes = (TextView) findViewById(R.id.tv_article_likes);
        _articleComments = (TextView) findViewById(R.id.tv_article_comments);
        _articleImage = (ImageView) findViewById(R.id.iv_article_image);


        // init
        _articleTitle.setText("Long article title Loremm ipsum dolor sit amet, here");
        _articleText.setText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pretium congue ex, eget egestas elit convallis nec. Sed sed consectetur libero, eget interdum arcu. Phasellus sit amet diam neque. Nulla ut elementum libero. Nulla tempor venenatis libero, eget cursus nisl dictum porttitor. Mauris imperdiet, nulla eget tincidunt semper, tellus turpis efficitur massa, et ornare risus orci at velit. Vivamus porta quis ipsum vel vulputate. Integer imperdiet non lectus nec dictum. Vivamus placerat ligula enim, iaculis lobortis odio mattis sed. Curabitur in tellus auctor urna convallis vestibulum. \n" +
                "Aliquam eros sapien, euismod ut ex at, congue pulvinar justo. Quisque gravida urna lacus, eu viverra elit vestibulum non. In nec justo efficitur, bibendum dolor eget, placerat est. Mauris bibendum justo id venenatis porta. Donec justo eros, dictum a diam eu, sollicitudin bibendum lectus. Praesent euismod fringilla ultricies. Praesent ullamcorper lacus sodales egestas volutpat. Nam imperdiet erat vel eros aliquet, at ultrices ipsum finibus. Suspendisse commodo eros in leo vulputate tincidunt. Nam tristique quis ligula posuere fringilla. Curabitur tincidunt gravida dolor. Aliquam convallis dolor non quam scelerisque, vitae elementum massa placerat. ");
        _articleLikes.setText("18");
        _articleComments.setText("200");
        Picasso.with(this).load("http://varlamov.me/img/grec_cig/00s.jpg").into(_articleImage);
    }
}
