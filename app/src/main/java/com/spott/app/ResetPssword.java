package com.spott.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.spott.app.common.LoaderClass;
import com.spott.app.services.api.ResetPasswordRequest;
import com.spott.app.services.api.ResetPasswordResponse;
import com.spott.app.services.api.RetrofitBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPssword extends Activity {

    EditText et_old_password, et_new_password, et_confirm_password;
    String oldStr = "", newStr = "", confirmStr = "";
    ConnectionDetector connDec;
    Activity activity;
    ImageView iv_back_icon, iv_email;
    Alert_Dialog_Manager alert;
    SharedPreferences preferences;
    TextView btn_reset;
    RelativeLayout rl_bck;
    View views;
    String firstName = "", lastName = "", userImage = "", userName = "", gender = "";
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        context = ResetPssword.this;
        initialise();
        Intent intent = getIntent();

        firstName = intent.getStringExtra("first_name");
        lastName = intent.getStringExtra("last_name");
        userImage = intent.getStringExtra("user_image");
        userName = intent.getStringExtra("username");
        gender = intent.getStringExtra("gender");

        if (intent.hasExtra("values")) {
            rl_bck.setVisibility(View.GONE);
        } else {
            rl_bck.setVisibility(View.VISIBLE);
        }
        if (intent.hasExtra("tick")) {
            iv_email.setVisibility(View.VISIBLE);
            et_old_password.setVisibility(View.VISIBLE);
            views.setVisibility(View.VISIBLE);
        } else {
            iv_email.setVisibility(View.GONE);
            et_old_password.setVisibility(View.GONE);
            views.setVisibility(View.GONE);
        }
        clickables();
    }

    private void initialise() {
        activity = ResetPssword.this;
        connDec = new ConnectionDetector(ResetPssword.this);
        alert = new Alert_Dialog_Manager(context);
        btn_reset = (TextView) findViewById(R.id.btn_reset);
        et_old_password = (EditText) findViewById(R.id.et_old_password);
        et_new_password = (EditText) findViewById(R.id.et_new_password);
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_email = (ImageView) findViewById(R.id.iv_email);
        rl_bck = (RelativeLayout) findViewById(R.id.rl_bck);
        views = findViewById(R.id.views);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
    }

    private void clickables() {
        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResetPssword.this, MyProfileActivity.class);
                startActivity(intent);
            }
        });
        btn_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldStr = et_old_password.getText().toString().trim();
                newStr = et_new_password.getText().toString().trim();
                confirmStr = et_confirm_password.getText().toString().trim();
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else if (!(newStr.length() > 0)) {
                    Toast.makeText(getApplicationContext(), R.string.new_pass, Toast.LENGTH_SHORT).show();
                } else if (!(confirmStr.length() > 0)) {
                    Toast.makeText(getApplicationContext(), R.string.confirm_pass, Toast.LENGTH_SHORT).show();
                } else if (newStr.equals(confirmStr)) {
                    if (!getIntent().hasExtra("tick")){
                        oldStr = preferences.getString("password", "");
                    }

                    RetrofitBuilder.getSpottApi()
                            .resetPassword(new ResetPasswordRequest(
                                    ResetPssword.this,
                                    newStr, oldStr))
                            .enqueue(new Callback<ResetPasswordResponse>() {
                                @Override
                                public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                                    processResponse(response.body());
                                }
                                @Override
                                public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {

                                }
                            });
                } else if (!(newStr.equals(confirmStr))) {
                    Toast.makeText(getApplicationContext(), "Passwords entered do not match.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        et_new_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    et_new_password.setHint("");
                    et_confirm_password.setHint("CONFIRM PASSWORD");
                    et_old_password.setHint("OLD PASSWORD");
                }
            }
        });
        et_confirm_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    et_confirm_password.setHint("");
                    et_new_password.setHint("NEW PASSWORD");
                    et_old_password.setHint("OLD PASSWORD");
                }
            }
        });
        et_old_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    et_old_password.setHint("");
                    et_new_password.setHint("NEW PASSWORD");
                    et_confirm_password.setHint("CONFIRM PASSWORD");
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Reset Password Screen");

        FlurryAgent.onStartSession(ResetPssword.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(ResetPssword.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    private void processResponse(ResetPasswordResponse response){
        LoaderClass.HideProgressWheel(ResetPssword.this);

        if (response.isSuccessfulCode()){
            preferences.edit().putString("password_change", "no").apply();
            Toast.makeText(activity, "Password change successfully.", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ResetPssword.this, MyFeedActivity.class);
            intent.putExtra("first_name", firstName);
            intent.putExtra("last_name", lastName);
            intent.putExtra("user_image", userImage);
            intent.putExtra("username", userName);
            intent.putExtra("gender", gender);
            startActivity(intent);
        } else if (response.isMessageCode()) {
            Toast.makeText(activity, response.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (response.isTokenErrorCode()){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ResetPssword.this);
            alertDialogBuilder.setTitle(R.string.app_name);
            alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            preferences.edit().clear().apply();
                            ((UTCAPP) getApplication()).joingroupList.clear();
                            Intent i = new Intent(ResetPssword.this, LoginScreenActivity.class);
                            startActivity(i);
                            finish();
                        }
                    });

            final AlertDialog alertDialog = alertDialogBuilder.create();

            runOnUiThread(new Runnable() {
                public void run() {
                    alertDialog.show();
                }
            });
        }
    }
}
