package com.spott.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.plus.model.people.Person;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.DrawableHelper;
import com.spott.app.common.LoaderClass;
import com.spott.app.data_models.CalendarEventsDay;
import com.spott.app.data_models.CalendarEventsDayEvent;
import com.squareup.picasso.Picasso;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;
import com.stacktips.view.DayDecorator;
import com.stacktips.view.DayView;
import com.stacktips.view.utils.CalendarUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Acer on 9/21/2016.
 */
public class CalendarActivity extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {

    TextView iv_red_icon, tv_num;
    Activity activity;
    DiamondImageView img_profile;
    LinearLayout ll_feed, ll_profile, ll_calendar;
    RelativeLayout rlMainFront, ll_groups;
    DirectAdapter mAdapter;
    EditText iv_serach_view;
    GroupAdapter1 gAdapter;
    DirectorPagingAdapter directAdapter;
    NavigationFragmentAdapter nadapter;
    String string = "#ed3224";
    String string1 = "#ffffff";
    private BroadcastReceiver broadcastReceiver;
    LayoutInflater layoutInflater = null;
    ViewPager pager;
    ArrayList<HashMap<String, String>> directoryList;
    ArrayList<HashMap<String, Object>> featuredList;
    Context context;
    ListView listview;
    View shadowView = null, view = null;
    int height, width;
    boolean drawer_open = false;
    ViewPager viewpager;
    SharedPreferences preferences;
    RelativeLayout fl_view_pager;
    int count = 0;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    ImageView iv_menu_icon;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, btn_invite;
    long last_text_edit = 0;
    long idle_min = 1500; // 4 seconds after user stops typing
    ArrayAdapter<Person> adapter;
    Handler h = new Handler();
    boolean showDropDown = true;
    boolean already_queried = false;
    String previousText = "";
    ArrayList<HashMap<String, String>> searchlist;
    ArrayList<HashMap<String, String>> mygroupfavlist;
    Person[] people;
    String firstname = "", lastname = "", star = "", group = "", post = "", method = "";
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "", feed_count = "";
    ArrayList<HashMap<String, String>> mygrouplist;
    //    GroupAdapter1 gAdapter;
    String encoded;
    String et_mailtr = "", gender = "", gender1 = "";
    private PullToRefreshListView pulllist;
    private int currentPage = 0, maxPages = 0;
    private List<ImageView> dots, dotsNavigate;

    private boolean isLoading = false;
    private EventsAdapter eventsAdapter;

    private TextView titleDay;

    private RecyclerView recyclerView;






    CustomCalendarView calendarView;
    private JSONObject mEvents;
    private Calendar currentCalendar;
    private ArrayList<CalendarEventsDay> mEventsDays;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.directory_activity);
        setContentView(R.layout.activity_calendar);
        this.activity = (Activity) CalendarActivity.this;
        context = CalendarActivity.this;
        nadapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(nadapter);
        mygrouplist = new ArrayList<HashMap<String, String>>();
        listview = (ListView) findViewById(R.id.lists);
        context = getApplicationContext();
        directoryList = new ArrayList<HashMap<String, String>>();
        featuredList = new ArrayList<HashMap<String, Object>>();
        mygroupfavlist = new ArrayList<HashMap<String, String>>();
        DirectAdapter adapter = new DirectAdapter(CalendarActivity.this, directoryList);
        //       listview.setAdapter(adapter);
        directAdapter = new DirectorPagingAdapter(CalendarActivity.this, featuredList);
        searchlist = new ArrayList<HashMap<String, String>>();
        titleDay = (TextView) findViewById(R.id.titleDay);

        viewpager = (ViewPager) activity.findViewById(R.id.viewpager);
        recyclerView = (RecyclerView) findViewById(R.id.adapterData);


        //       viewpager.setAdapter(directAdapter);






        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);

        //Initialize calendar with date
        currentCalendar = Calendar.getInstance(Locale.getDefault());
        @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("EEEE d MMMM, yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        titleDay.setText("TODAY - "+date);
        titleDay.setAllCaps(true);




//        titleDay.setText(Calendar.getInstance(Locale.getDefault()).toString());


        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);



        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String selectDay = df.format(date);
                @SuppressLint("SimpleDateFormat") DateFormat df2 = new SimpleDateFormat("EEEE d MMMM, yyyy");
                String date2 = df2.format(date);
                titleDay.setText(date2);
                titleDay.setAllCaps(true);
                if (mEvents!=null){
                    try {
                        JSONObject eventDay = mEvents.getJSONObject(selectDay);

                        if (eventDay!=null){
                            recyclerView.setVisibility(View.VISIBLE);

                            mEventsDays.clear();

                            JSONArray nameArray = mEvents.names();
                            int arrayLength = nameArray.length();
                            for (int minimumIndex = 0;minimumIndex<arrayLength;minimumIndex++){
                                if (selectDay.equals(nameArray.get(minimumIndex))){
                                    for (int indexDays = minimumIndex;indexDays<arrayLength;indexDays++){

                                        JSONObject eventsDay = mEvents.getJSONObject(nameArray.getString(indexDays));

                                        JSONArray eventsJSONArray = eventsDay.getJSONArray("events");
                                        ArrayList <CalendarEventsDayEvent> eventsList = new ArrayList<>();

                                        for (int i = 0; i < eventsJSONArray.length(); i++) {
                                            JSONObject iJSONObject = eventsJSONArray.getJSONObject(i);

                                            eventsList.add(new CalendarEventsDayEvent(iJSONObject.getString("title"), iJSONObject.getString("start_timing"), iJSONObject.getString("end_timing")));
                                        }
                                        String data = nameArray.getString(indexDays);

                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                        Date newDate = format.parse(data);

                                        format = new SimpleDateFormat("EEEE d MMMM, yyyy");
                                        String datos = format.format(newDate);

                                        mEventsDays.add(new CalendarEventsDay(eventDay.getBoolean("today"), datos, eventsList));
                                    }
                                }
                            }

                            eventsAdapter = new EventsAdapter(CalendarActivity.this, eventDay);
                            LinearLayoutManager layoutManager
                                    = new LinearLayoutManager(CalendarActivity.this, LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(layoutManager);
                            recyclerView.setAdapter(eventsAdapter);
                        }else {
                            recyclerView.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        recyclerView.setVisibility(View.GONE);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onMonthChanged(Date date) {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(CalendarActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });

        addDotsNavigation();

        layoutInflater = LayoutInflater.from(this);

        initialise();

        new EventsForCalendarData().execute();

        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");
        gender = preferences.getString("gender", "");

        clickables();
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            new FeaturedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        /*if (!connDec.isConnectingToInternet()) {
            //    alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new FeedCountData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new FeedCountData().execute();
            }
        }
        if (!connDec.isConnectingToInternet()) {
            //    alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new MessageData().execute();
            }
        }*/
        setting();

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_calendar.setOnClickListener(this);
        view.setOnClickListener(this);

        //listview.setScrollingWhileRefreshingEnabled(false);

/*        listview.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (!isLoading) {
                    if (has_more.equalsIgnoreCase("true")) {
                        isLoading = true;

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                        } else {
                            new DirectoryListData().execute("loadmore");
                        }
                    } else {
                        Toast.makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        listview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                count = 0;

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                    new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                } else {
//                    new DirectoryListData().execute();
//                }

            }
        });*/


//        listview.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
//                    if (!isLoading) {
//                        if (has_more.equalsIgnoreCase("true")) {
//                            isLoading = true;
//                            int length = iv_serach_view.getText().toString().length();
//
//                            if (length == 1) {
//
//                                method = "alumni_by_alpha";
//
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//
//                                    new search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, iv_serach_view.getText().toString().trim());
//                                } else {
//                                    new search_help().execute(iv_serach_view.getText().toString().trim());
//                                }
//                            } else if (length > 1) {
//                                method = "alumni_by_name";
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//
//                                    new search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, iv_serach_view.getText().toString().trim());
//                                } else {
//                                    new search_help().execute(iv_serach_view.getText().toString().trim());
//                                }
//                            } else {
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                                    new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
//                                } else {
//                                    new DirectoryListData().execute("loadmore");
//                                }
//                            }
//                        } else {
//
//                        }
//                    }
//                }
//
//            }
//        });
//
//        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//
//                System.out.println("LALALA : " + position);
//
//                if (position == 0) {
//                    selectDot(0);
//                } else if (position == featuredList.size() - 1) {
//                    selectDot(2);
//                } else {
//                    selectDot(1);
//                }
//                //selectDot(position);
//
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });

    }



    private class DisabledColorDecorator implements DayDecorator {

        @Override

        public void decorate(DayView dayView) {

            /*Log.d("date", String.valueOf(dayView.getDate()));*/

            SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy-MM-dd");


            String strDt = simpleDate.format(dayView.getDate());
            String date = simpleDate.format(Calendar.getInstance().getTime());

            try {
                JSONObject eventDay = mEvents.getJSONObject(strDt);

                if (eventDay != null) {
                    dayView.setBackgroundResource(R.drawable.calendar_two2);
                    dayView.setWidth(30);
                    dayView.setHeight(150);
                }
                if (strDt.equals(date)){
                    dayView.setBackgroundResource(R.drawable.calendar_one);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("LongLogTag")
    public void allEventsOnlick (View v) {

        Intent allEventsDays = new Intent(CalendarActivity.this,AllEventsDaysActivity.class);

        allEventsDays.putParcelableArrayListExtra("extraextra", mEventsDays);
        startActivity(allEventsDays);
        Log.d("CalendarActivity...allEventsOnlick", "!");

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_calendar.setBackgroundColor(Color.parseColor("#FF7F60"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#ffffff"));

        DrawableHelper.setBottomNavigationBackground(ll_calendar);
    }


    private void initialise() {
        alert = new Alert_Dialog_Manager(context);
        connDec = new ConnectionDetector(CalendarActivity.this);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_calendar = (LinearLayout) findViewById(R.id.ll_calendar);
        iv_serach_view = (EditText) findViewById(R.id.iv_serach_view);
        btn_invite = (TextView) findViewById(R.id.btn_invite);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        fl_view_pager = (RelativeLayout) findViewById(R.id.fl_view_pager);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

//        broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.hasExtra("feed_count")) {
//                    String new_feed = intent.getStringExtra("feed_count");
//                    Integer i = Integer.parseInt(new_feed);
//
//                    if (i == 0) {
//                        iv_red_icon.setVisibility(View.GONE);
//                        feed_count = i + "";
//                        iv_red_icon.setText(i + "");
//                    } else if (i < 99) {
//                        iv_red_icon.setVisibility(View.VISIBLE);
//                        feed_count = i + "";
//                        iv_red_icon.setText(i + "");
//                    } else {
//                        iv_red_icon.setVisibility(View.VISIBLE);
//                        feed_count = "99+";
//                        iv_red_icon.setText("99+");
//                    }
//                    nadapter.notifyDataSetChanged();
//                }
//                if (intent.hasExtra("message_count")) {
//                    String new_message = intent.getStringExtra("message_count");
//                    Integer i = Integer.parseInt(new_message);
//                    if (i == 0) {
//                        tv_num.setVisibility(View.GONE);
//                    } else if (i < 99) {
//                        tv_num.setVisibility(View.VISIBLE);
//                        tv_num.setText(new_message);
//                    } else {
//                        tv_num.setVisibility(View.VISIBLE);
//                        tv_num.setText("99+");
//                    }
//                }
//            }
//        };
//
//        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onResume() {

        super.onResume();
//        if (!connDec.isConnectingToInternet()) {
//            //    alert.showNetAlert();
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            } else {
//                new GroupFavData().execute();
//            }
//        }
//        final int pos = 1;
        pager.postDelayed(new Runnable() {

            @Override
            public void run() {
                pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
            }
        }, 100);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ((UTCAPP) getApplication()).pagerPos = position;

                selectDotNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public class GroupFavData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            pulllist.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        if (mygroupfavlist != null)
                            mygroupfavlist.clear();
                        else
                            mygroupfavlist = new ArrayList<>();


                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");
                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);
                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygroupfavlist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(CalendarActivity.this, mygroupfavlist);

                        pulllist.setAdapter(gAdapter);

                        gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

// For Navigation Animation

    private class NavigationFragmentAdapter extends PagerAdapter {

        TextView tv_grp1, tv_grp2, tv_grp3, tv_grp4, tv_grp5, tv_grp6, tv_browsing;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_profile_name, tv_profile_username, tv_notification_count, tv_update, tv_feed, tv_profile, tv_groups, tv_staff_dir, tv_log_out;

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            if (feed_count.equals("0"))
                tv_notification_count.setVisibility(View.GONE);

            else {
                tv_notification_count.setVisibility(View.VISIBLE);

                tv_notification_count.setText(feed_count);
            }

        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CalendarActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {

                    }

                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;

                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                            mygroupfavlist.clear();
                        }

                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(CalendarActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
                tv_log_out = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(CommonUtilities.decodeUTF8(preferences.getString("first_name", "")) + " " + CommonUtilities.decodeUTF8(preferences.getString("last_name", "")));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                if (feed_count.equals("0"))
                    tv_notification_count.setVisibility(View.GONE);

                else {
                    tv_notification_count.setVisibility(View.VISIBLE);

                    tv_notification_count.setText(feed_count);
                }
                if (encoded.equalsIgnoreCase("")) {

                    img_profile.setImageResource(R.drawable.avatar);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }
                tv_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(CalendarActivity.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);

                    }
                });

                tv_notification_count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(CalendarActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                white_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(CalendarActivity.this, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);

                    }
                });
                tv_groupss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(CalendarActivity.this, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                    }
                });
                orange_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonUtilities.showStarsPopup(activity);
                    }
                });

                img_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(CalendarActivity.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);
                    }
                });
                tv_groups.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(CalendarActivity.this, Calendar.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(CalendarActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_feed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(CalendarActivity.this, MyFeedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_log_out.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        customShareLogOutPopup();
                    }
                });

                container.addView(view, 0);
            }
//               ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }


    }

    public class EventsForCalendarData extends AsyncTask<String, String, String> {




        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "get_events_for_calendar");

                jo.put("login_token", preferences.getString("login_token", ""));

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                return String.valueOf(content);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("artman","result -> " + result);

            try {
                JSONObject calendar = new JSONObject(result);

                if (calendar.getString("err-code").equals("0")) {
                    mEvents = calendar.getJSONObject("events");

                    mEventsDays = new ArrayList<>();

                    List decorators = new ArrayList<>();
                    decorators.add(new DisabledColorDecorator());

                    @SuppressLint("SimpleDateFormat") SimpleDateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
                    String date2 = df2.format(Calendar.getInstance().getTime());
                    if (mEvents!=null){
                        try {
                            JSONObject eventDay = mEvents.getJSONObject(date2);

                            if (eventDay!=null){
                                recyclerView.setVisibility(View.VISIBLE);
                                mEventsDays.clear();

                                JSONArray nameArray = mEvents.names();
                                int arrayLength = nameArray.length();
                                for (int minimumIndex = 0;minimumIndex<arrayLength;minimumIndex++){
                                    if (date2.equals(nameArray.get(minimumIndex))){
                                        for (int indexDays = minimumIndex;indexDays<arrayLength;indexDays++){

                                            JSONObject eventsDay = mEvents.getJSONObject(nameArray.getString(indexDays));

                                            JSONArray eventsJSONArray = eventsDay.getJSONArray("events");
                                            ArrayList <CalendarEventsDayEvent> eventsList = new ArrayList<>();

                                            for (int i = 0; i < eventsJSONArray.length(); i++) {
                                                JSONObject iJSONObject = eventsJSONArray.getJSONObject(i);

                                                eventsList.add(new CalendarEventsDayEvent(iJSONObject.getString("title"), iJSONObject.getString("start_timing"), iJSONObject.getString("end_timing")));
                                            }
                                            String data = nameArray.getString(indexDays);

                                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                            Date newDate = format.parse(data);

                                            format = new SimpleDateFormat("EEEE d MMMM, yyyy");
                                            String datos = format.format(newDate);

                                            mEventsDays.add(new CalendarEventsDay(eventDay.getBoolean("today"), datos, eventsList));
                                        }
                                    }
                                }

                                eventsAdapter = new EventsAdapter(CalendarActivity.this, eventDay);
                                LinearLayoutManager layoutManager
                                        = new LinearLayoutManager(CalendarActivity.this, LinearLayoutManager.VERTICAL, false);
                                recyclerView.setLayoutManager(layoutManager);
                                recyclerView.setAdapter(eventsAdapter);
                            }else {
                                recyclerView.setVisibility(View.GONE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            recyclerView.setVisibility(View.GONE);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    calendarView.setDecorators(decorators);
                    calendarView.refreshCalendar(currentCalendar);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygroupfavlist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygroupfavlist) {
            this.context = context;
            this.mygroupfavlist = mygroupfavlist;
        }

        @Override
        public int getCount() {
            return mygroupfavlist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);
                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);
                }
            });
            if (mygroupfavlist.get(i).get("unread").equals("0")) {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setText(mygroupfavlist.get(i).get("unread"));
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }


    private void clickables() {

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();

                    drawer_open = true;
                    pager.setAdapter(nadapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);

                    mAdapter.notifyDataSetChanged();
                    if (!(preferences.getString("user_image", "").equals(""))) {
                        Picasso.with(getApplicationContext()).load(preferences.getString("user_image", "")).into(img_profile);
                        img_profile.setBorderColor(Color.parseColor("#F15A51"));
                        img_profile.setBorderWidth(2);
                    }

                } else {
                    StopDrawerAnim();
                }

            }
        });


//        iv_serach_view.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//                /*if (iv_serach_view.getText().toString().equals("")) {
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    } else {
//                        new DirectoryListData().execute();
//                    }
//
//                    fl_view_pager.setVisibility(View.VISIBLE);
//                }*/
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable arg0) {
//                last_text_edit = System.currentTimeMillis();
//
//                h.postDelayed(input_finish_checker, idle_min);
//
//            }
//        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CalendarActivity.this, UpdatesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

//        iv_serach_view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//
//                    //performSearch();
//
//                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//
//                    imm.hideSoftInputFromWindow(iv_serach_view.getWindowToken(), 0);
//
//                    iv_serach_view.setCursorVisible(false);
//
//                    //  setSearchLayout();
//
//                    callAPI();
//
//
//                    return true;
//                }
//
//
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//
//                imm.hideSoftInputFromWindow(iv_serach_view.getWindowToken(), 0);
//
//                iv_serach_view.setCursorVisible(false);
//
//                return false;
//            }
//        });
//        btn_invite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDialogInvite();
//            }
//        });
    }

    private void callAPI() {
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new search_help().execute();
            }
        }

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        //this.isScrollCompleted();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    private void showDialogInvite() {
        final Dialog dialog = new Dialog(CalendarActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.invite_popup);
        final EditText et_email;
        TextView send_button, cancel_button;
        et_email = (EditText) dialog.findViewById(R.id.et_email);
        et_email.setText("@ttutc.com");
        send_button = (TextView) dialog.findViewById(R.id.send_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);

        //CommonUtilities.appendCode(et_email);

        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_mailtr = et_email.getText().toString().trim();
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else if (!(et_mailtr.length() > 0)) {

                    Toast.makeText(getApplicationContext(), R.string.email, Toast.LENGTH_SHORT).show();
                } else if (!(et_mailtr.contains("@dikonia.com") || et_mailtr.contains("@dikonia.in") || et_mailtr.contains("@ttutc.com") || et_mailtr.contains("@simplyintense.com"))) {

                    Toast.makeText(getApplicationContext(), R.string.validemail, Toast.LENGTH_SHORT).show();
                } else if (!CommonUtilities.isValidEmail(et_mailtr)) {
                    Toast.makeText(getApplicationContext(), R.string.validemail, Toast.LENGTH_SHORT).show();
                } else {
                    if (!connDec.isConnectingToInternet()) {
                        //    alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new InviteData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new InviteData().execute();
                        }
                    }
                    dialog.dismiss();
                }
            }
        });
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    @Override
    public void onClick(View view) {


        switch (view.getId()) {


            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:
                Intent intent = new Intent(CalendarActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(CalendarActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(CalendarActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:



                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));

                break;
        }
    }

    public static void setListViewHeightBasedOnItems(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private class DirectAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> directoryList;

        public DirectAdapter(Context context, ArrayList<HashMap<String, String>> directoryList) {
            this.context = context;
            this.directoryList = directoryList;
        }

        @Override
        public int getCount() {
            return directoryList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_directory_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.iv_profile_alumni = (DiamondImageView) convertView.findViewById(R.id.iv_profile_alumni);
            holder.tv_name_alumni = (TextView) convertView.findViewById(R.id.tv_name_alumni);
            holder.tv_name_alumni.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_date_alumni = (TextView) convertView.findViewById(R.id.tv_date_alumni);
            holder.tv_date_alumni.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.rl_direct = (RelativeLayout) convertView.findViewById(R.id.rl_direct);
            String image = directoryList.get(i).get("user_image");
            gender1 = directoryList.get(i).get("gender");
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (directoryList.get(i).get("uid").equals(preferences.getString("uid", ""))) {
                        Intent intent1 = new Intent(CalendarActivity.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.putExtra("myProfile", "");
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);

                    } else {
                        Intent intent = new Intent(CalendarActivity.this, MyProfile.class);
                        intent.putExtra("uid", directoryList.get(i).get("uid"));
                        startActivity(intent);
                    }
                }
            });
            holder.rl_direct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (directoryList.get(i).get("uid").equals(preferences.getString("uid", ""))) {
                        Intent intent1 = new Intent(CalendarActivity.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.putExtra("myProfile", "");
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);

                    } else {
                        Intent intent = new Intent(CalendarActivity.this, MyProfile.class);
                        intent.putExtra("uid", directoryList.get(i).get("uid"));
                        startActivity(intent);
                    }
                }
            });
            holder.tv_name_alumni.setText(CommonUtilities.decodeUTF8(directoryList.get(i).get("first_name")) + " "
                    + CommonUtilities.decodeUTF8(directoryList.get(i).get("last_name")));
            holder.tv_date_alumni.setText(CommonUtilities.decodeUTF8(directoryList.get(i).get("job_company")));

            if (image.isEmpty()) {
                if (gender1.equals("Male")) {
                    holder.iv_profile_alumni.setImageResource(R.drawable.avatar);
                    holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_profile_alumni.setBorderWidth(1);

                } else if (gender1.equals("Female")) {
                    holder.iv_profile_alumni.setImageResource(R.drawable.avatar);
                    holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_profile_alumni.setBorderWidth(1);

                } else if (gender1.equals("")) {
                    holder.iv_profile_alumni.setImageResource(R.drawable.avatar);
                    holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_profile_alumni.setBorderWidth(1);

                }
            } else {
                Picasso.with(getApplicationContext()).load(image).into(holder.iv_profile_alumni);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(1);

            }

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_profile_alumni;
            TextView tv_name_alumni, tv_date_alumni, tv_view_btngrp;
            RelativeLayout rl_direct;
        }
    }

    private class DirectorPagingAdapter extends PagerAdapter {


        DiamondImageView iv_alumni_profile;
        TextView tv_featured, tv_alumni_name, tv_features_descr, tv_time_alumni;
        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, Object>> featuredList;

        public DirectorPagingAdapter(Context context, ArrayList<HashMap<String, Object>> featuredList) {

            this.featuredList = featuredList;
            this.context = context;
            System.out.println("dataList=" + this.featuredList);
        }

        @Override
        public int getCount() {
            return featuredList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewGroup) container).removeView((View) object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {

            try {
                view = layoutInflater.inflate(R.layout.directory_paging_adapter, container, false);
                iv_alumni_profile = (DiamondImageView) view.findViewById(R.id.iv_alumni_profile);
                tv_featured = (TextView) view.findViewById(R.id.tv_featured);
                tv_featured.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
                tv_alumni_name = (TextView) view.findViewById(R.id.tv_alumni_name);
                tv_alumni_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_features_descr = (TextView) view.findViewById(R.id.tv_features_descr);
                tv_time_alumni = (TextView) view.findViewById(R.id.tv_time_alumni);
                String image1 = (String) featuredList.get(position).get("user_image");
                gender1 = (String) featuredList.get(position).get("gender");
                if (image1.isEmpty()) {
                    if (gender1.equals("Male") || gender1 == null || gender1.equals("")) {
                        iv_alumni_profile.setImageResource(R.drawable.avatar);
                        iv_alumni_profile.setBorderColor(Color.parseColor("#F15A51"));
                        iv_alumni_profile.setBorderWidth(2);

                    } else if (gender1.equals("Female")) {
                        iv_alumni_profile.setImageResource(R.drawable.avatar);
                        iv_alumni_profile.setBorderColor(Color.parseColor("#F15A51"));
                        iv_alumni_profile.setBorderWidth(2);

                    }
                } else {
                    Picasso.with(getApplicationContext()).load(image1).into(iv_alumni_profile);
                    iv_alumni_profile.setBorderColor(Color.parseColor("#F15A51"));
                    iv_alumni_profile.setBorderWidth(2);
                }

                tv_alumni_name.setText(CommonUtilities.decodeUTF8(featuredList.get(position).get("first_name")+"") + " "
                        + CommonUtilities.decodeUTF8(featuredList.get(position).get("last_name")+""));

                System.out.println("NAVNEET=" + featuredList.get(position).get("first_name").toString() + " " + featuredList.get(position).get("last_name").toString());

                tv_features_descr.setText(featuredList.get(position).get("designation").toString());
                tv_time_alumni.setText(featuredList.get(position).get("job_company").toString());

                ((ViewPager) container).addView(view, 0);
            } catch (InflateException e) {
                e.printStackTrace();
            }
            return view;
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    public class DirectoryListData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            LoaderClass.ShowProgressWheel(CalendarActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                if (strings.length > 0) {
                    if (strings[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }

                //Create JSONObject here


                JSONObject jo = new JSONObject();

                jo.put("method", "alumni_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                if (!isLoadMore) {
                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", directoryList.size() + "");
                }
                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }

                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            // listview.onRefreshComplete();

            try {
                JSONObject resultObj = new JSONObject(getData);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    if (!isLoadMore) {
                        directoryList.clear();
                    }

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

//                        JSONObject jO = user_dataObj.getJSONObject("list");
                        String profile_id = user_dataObj.getString("profile_id");
                        String uid = user_dataObj.getString("uid");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String hobbies = user_dataObj.getString("hobbies");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String username = user_dataObj.getString("username");
                        String email = user_dataObj.getString("email");
                        String password = user_dataObj.getString("password");
                        String user_status = user_dataObj.getString("user_status");
                        String login_token = preferences.getString("login_token", "");
                        String device_type = preferences.getString("device_type", "");
                        String device_token = preferences.getString("device_token", "");
                        String latitude = preferences.getString("latitude", "");
                        String longitude = preferences.getString("longitude", "");
                        String user_create_date = user_dataObj.getString("user_create_date");
                        String badge = user_dataObj.getString("badge");
                        String facebook_id = user_dataObj.getString("facebook_id");
                        String survey_alert = user_dataObj.getString("survey_alert");
                        String password_token = user_dataObj.getString("password_token");
                        String register_check = user_dataObj.getString("register_check");
                        String batch = user_dataObj.getString("batch");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String job_company = user_dataObj.getString("job_company");
                        String designation = user_dataObj.getString("designation");

                        hmap.put("profile_id", profile_id);
                        hmap.put("uid", uid);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("username", username);
                        hmap.put("email", email);
                        hmap.put("password", password);
                        hmap.put("user_status", user_status);
                        hmap.put("login_token", login_token);
                        hmap.put("device_type", device_type);
                        hmap.put("device_token", device_token);
                        hmap.put("latitude", latitude);
                        hmap.put("longitude", longitude);
                        hmap.put("user_create_date", user_create_date);
                        hmap.put("badge", badge);
                        hmap.put("facebook_id", facebook_id);
                        hmap.put("survey_alert", survey_alert);
                        hmap.put("password_token", password_token);
                        hmap.put("register_check", register_check);
                        hmap.put("batch", batch);
                        hmap.put("year_of_passing", year_of_passing);
                        hmap.put("job_company", job_company);
                        hmap.put("designation", designation);


                        preferences.edit().putString("profile_id", profile_id).commit();
                        //  preferences.edit().putString("uid", uid).commit();
                        preferences.edit().putString("gender", gender).commit();


                        directoryList.add(hmap);

                    }

                    setAdapter();

                   /* if(isLoadMore)
                    {
                        listview.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                scrollMyListViewToBottom(listview.getRefreshableView());
                            }
                        },500);

                    }*/

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            LoaderClass.HideProgressWheel(CalendarActivity.this);

            isLoading = false;

        }
    }

    private void setAdapter() {
        if (mAdapter == null) {
            mAdapter = new DirectAdapter(CalendarActivity.this, directoryList);

            listview.setAdapter(mAdapter);
        } else {
            mAdapter.notifyDataSetChanged();
        }


    }

    private void scrollMyListViewToBottom(final ListView list) {

        if (list != null && adapter != null) {
            list.post(new Runnable() {
                public void run() {
                    list.setSelection(adapter.getCount() - 1);
                }
            });
        }

    }

    public class FeaturedData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "featured_user");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Recomm data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    featuredList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, Object> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String feature_id = user_dataObj.getString("feature_id");
                        String uid = preferences.getString("uid", "");
                        String feature_status = user_dataObj.getString("feature_status");
                        String feature_create_date = user_dataObj.getString("feature_create_date");
                        String profile_id = preferences.getString("profile_id", "");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String hobbies = user_dataObj.getString("hobbies");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String quotes = preferences.getString("quotes", "");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String achievements = preferences.getString("achievements", "");
                        String skills = preferences.getString("skills", "");
                        String education = user_dataObj.getString("education");
                        String job_company = user_dataObj.getString("job_company");
                        if (job_company.equals("null"))
                        {
                            job_company="";
                        }
                        String designation = user_dataObj.getString("designation");
                        if (designation.equals("null"))
                        {
                            designation="";
                        }

                        if (!education.equals("")) {
                            JSONObject user_dataObj1 = new JSONObject(education);
                            String eid = user_dataObj1.getString("eid");
                            String uid1 = user_dataObj1.getString("uid");
                            String course = user_dataObj1.getString("course");
                            String batch = user_dataObj1.getString("batch");
                            String year_of_passing = user_dataObj1.getString("year_of_passing");
                            String education_create_date = user_dataObj1.getString("education_create_date");
                            String education_status = user_dataObj1.getString("education_status");
                            preferences.edit().putString("course", course).commit();
                            preferences.edit().putString("batch", batch).commit();
                            preferences.edit().putString("year_of_passing", year_of_passing).commit();


                        }

                        hmap.put("group_id", feature_id);
                        hmap.put("uid", uid);
                        hmap.put("feature_status", feature_status);
                        hmap.put("feature_create_date", feature_create_date);
                        hmap.put("profile_id", profile_id);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("achievements", achievements);
                        hmap.put("skills", skills);
                        hmap.put("education", education);
                        hmap.put("job_company", job_company);
                        hmap.put("designation", designation);

                        featuredList.add(hmap);

                    }
                    if (featuredList.size() <2)
                        maxPages = featuredList.size();

                    else
                        maxPages = 3;

//                    addDots();

//                    selectDot(0);

                    directAdapter = new DirectorPagingAdapter(CalendarActivity.this, featuredList);

                    viewpager.setAdapter(directAdapter);

                    viewpager.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            playPager(featuredList.size());

                        }
                    }, 500);

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void playPager(final int NUM_PAGES) {
        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewpager.setCurrentItem(currentPage++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 3000);
    }

    public class InviteData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "invite_user");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("email_id", et_mailtr);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            //LoaderClass.HideProgressWheel(MyDirectoryActivity.this);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CalendarActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Staff has been invited successfully.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CalendarActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(CalendarActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class search_help extends AsyncTask<String, String, String> {


        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(CalendarActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();
                if (params.length > 0) {
                    if (params[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "" + method);

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("post_name", params[0]);         // pass empty string

                if (!isLoadMore) {
                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", directoryList.size() + "");
                }
                //  jo.put("search_for", MainActivity.str_search_for);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");
                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {
                        if (!isLoadMore) {
                            directoryList.clear();
                        }

                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);

                            String profile_id = helpObject.getString("profile_id");
                            String uid = helpObject.getString("uid");
                            String first_name = helpObject.getString("first_name");
                            String last_name = helpObject.getString("last_name");
                            String user_image = helpObject.getString("user_image");
                            String user_thumbnail = helpObject.getString("user_thumbnail");
                            String hobbies = helpObject.getString("hobbies");
                            String phone = helpObject.getString("phone");
                            String dob = helpObject.getString("dob");
                            String quotes = helpObject.getString("quotes");
                            String profile_create_date = helpObject.getString("profile_create_date");
                            String profile_modify_date = helpObject.getString("profile_modify_date");
                            String gender = helpObject.getString("gender");
                            String batch = helpObject.getString("batch");
                            String year_of_passing = helpObject.getString("year_of_passing");
                            String job_company = helpObject.getString("job_company");
                            String designation = helpObject.getString("designation");

                            hmap.put("profile_id", profile_id);
                            hmap.put("uid", uid);
                            hmap.put("first_name", first_name);
                            hmap.put("last_name", last_name);
                            hmap.put("user_image", user_image);
                            hmap.put("user_thumbnail", user_thumbnail);
                            hmap.put("hobbies", hobbies);
                            hmap.put("phone", phone);
                            hmap.put("dob", dob);
                            hmap.put("quotes", quotes);
                            hmap.put("profile_create_date", profile_create_date);
                            hmap.put("profile_modify_date", profile_modify_date);
                            hmap.put("gender", gender);
                            hmap.put("batch", batch);
                            hmap.put("year_of_passing", year_of_passing);
                            hmap.put("job_company", job_company);
                            hmap.put("designation", designation);

                            directoryList.add(hmap);
                        }

                        setAdapter();

                        showDropDown = true;

                        showDrop();

                    }
                } else if (errCode.equals("300")) {
                    directoryList.clear();
                    setAdapter();
                } else if (errCode.equals("700")) {
                    String message = resultObj.getString("message");

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CalendarActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(CalendarActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(CalendarActivity.this);
            isLoading = false;
        }
    }

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                //System.out.println("a
                // lready_queried>>>>> "+already_queried);
                String textCalled = iv_serach_view.getText().toString().trim();

                if (!already_queried) {
                    if (!textCalled.equals("")) {
                        if (!previousText.equals(iv_serach_view.getText().toString().trim())) {
                            previousText = textCalled;

                            String search = previousText.replace(",", "");

                            search = search.trim();

                            if (!search.equals("")) {
                                if (search.length() == 1) {
                                    method = "alumni_by_alpha";
                                } else {
                                    method = "alumni_by_name";
                                }
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                                    new search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, search);
                                } else {
                                    new search_help().execute(search);
                                }
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    new DirectoryListData().execute();
                                }
                            }
                        }
                    } else {
                        if (isLoading == false) {
                            isLoading = true;
                            if (!connDec.isConnectingToInternet()) {
                                //    alert.showNetAlert();
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    new DirectoryListData().execute();
                                }
                            }
                            if (!connDec.isConnectingToInternet()) {
                                //    alert.showNetAlert();
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new FeaturedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    new FeaturedData().execute();
                                }
                            }
                            showDrop();
                        }
                        // updateAdapter();


                    }
                }
            }
        }
    };

    private void showDrop() {
        try {
            if (showDropDown == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!CalendarActivity.this.isFinishing()) {
                            // iv_serach_view.showDropDown();
                        }
                    }
                }, 500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class LogOutData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);
                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    {
                        preferences.edit().clear().commit();
                        ((UTCAPP) getApplication()).joingroupList.clear();

                        Intent i = new Intent(activity, SplashActivity.class);
                        startActivity(i);
                        finishAffinity();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CalendarActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(CalendarActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();

                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LogOutData().execute();
                    }
                }
            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("My Directory Screen");

        FlurryAgent.onStartSession(CalendarActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(CalendarActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    // add dots dynamically
    public void addDots() {
        dots = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.indicator);

        for (int i = 0; i < maxPages; i++) {
            ImageView dot = new ImageView(this);

            dot.setImageDrawable(getResources().getDrawable(R.drawable.under_login_white));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
//            dotsLayout.addView(dot, params);

            dots.add(dot);
        }
    }

    public void selectDot(int idx) {

        Resources res = getResources();
        for (int i = 0; i < maxPages; i++) {
            int drawableId = (i == idx) ? (R.drawable.under_login) : (R.drawable.under_login_white);
            Drawable drawable = res.getDrawable(drawableId);
//            dots.get(i).setImageDrawable(drawable);
        }
    }

    // drawer navigate functionality
    public void addDotsNavigation() {
        dotsNavigate = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.circle_indicator);

        for (int i = 0; i < 2; i++) {
            ImageView dot = new ImageView(this);

            dot.setImageDrawable(getResources().getDrawable(R.drawable.under_slide_black_out));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            final int pos = i;

            dot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pager.setCurrentItem(pos);
                }
            });

//            dotsLayout.addView(dot, params);

            dotsNavigate.add(dot);
        }
    }

    public void selectDotNavigation(int idx) {

        Resources res = getResources();
        for (int i = 0; i < 2; i++) {
            int drawableId = (i == idx) ? (R.drawable.under_slide_black) : (R.drawable.under_slide_black_out);
            Drawable drawable = res.getDrawable(drawableId);
            dotsNavigate.get(i).setImageDrawable(drawable);
        }
    }

}