package com.spott.app;

/**
 * Created by Adimn on 9/28/2016.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.provider.Settings;

public class Alert_Dialog_Manager
{
    Context context;
    Resources recR;

    public Alert_Dialog_Manager(Context ctx)
    {
        context = ctx;

        recR = context.getResources();
    }

    public void showMessageAlert(String message, boolean cancelable)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setCancelable(cancelable);

        alert.setTitle(recR.getString(R.string.app_name));

        alert.setMessage(message);

        alert.setPositiveButton(recR.getString(R.string.okay), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();
            }
        });

        //alert.show();
    }


    public void showMessageAlert(String title, String message, boolean cancelable, final boolean activityClose)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setCancelable(cancelable);

        alert.setTitle(title);

        alert.setMessage(message);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();

				/*if(activityClose==true)
				{
					context.finish();
				}*/

            }
        });

        //alert.show();

    }
    public void selectSocietyType(String message, boolean cancelable)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setCancelable(cancelable);

        alert.setTitle(recR.getString(R.string.app_name));

        alert.setMessage(message);

        alert.setPositiveButton(recR.getString(R.string.okay), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();
            }
        });

       // alert.show();
    }
    public void notSameAlert(String message, boolean cancelable)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setCancelable(cancelable);

        alert.setTitle(recR.getString(R.string.app_name));

        alert.setMessage(message);

        alert.setPositiveButton(recR.getString(R.string.okay), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();
            }
        });

       // alert.show();
    }



    public void showEmptyAlert(String message, boolean cancelable)
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setCancelable(cancelable);

        alert.setTitle(recR.getString(R.string.app_name));

        alert.setMessage(message);

        alert.setPositiveButton(recR.getString(R.string.okay), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int whichButton)
            {
                dialog.cancel();
            }
        });

       // alert.show();
    }



    public void showNetAlert()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle(recR.getString(R.string.app_name));

        alert.setMessage(recR.getString(R.string.no_connection));

        alert.setPositiveButton(recR.getString(R.string.alert_settings),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int whichButton)
                    {
                        context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                    }
                });

        alert.setNegativeButton(recR.getString(R.string.cancel), new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog,	int whichButton)
            {
                dialog.dismiss();

            }
        });

        alert.show();
    }

}