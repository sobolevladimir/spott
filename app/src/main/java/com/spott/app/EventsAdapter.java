package com.spott.app;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Artem on 07.11.2017.
 */

public class EventsAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private Context context;
    private JSONObject eventsObject;

    private JSONArray events;

    EventsAdapter(Context context, JSONObject eventsObject){
        this.context = context;
        this.eventsObject = eventsObject;
        try {
            events = eventsObject.getJSONArray("events");
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_events, parent, false);

        return new ItemView(itemView);
    }
    private class ItemView extends RecyclerView.ViewHolder{
        View rootView;
        TextView startTiming,endTiming,titel;
        private ItemView(View itemView) {
            super(itemView);
            rootView = itemView;
            startTiming = (TextView) rootView.findViewById(R.id.start_timing);
            endTiming = (TextView) rootView.findViewById(R.id.end_timing);
            titel = (TextView) rootView.findViewById(R.id.title);



        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            Log.d("artman",eventsObject.toString());
            JSONObject currentJSONObject = (JSONObject) events.getJSONObject(position);
            String startTiming = currentJSONObject.optString("start_timing");
            String endTiming = currentJSONObject.optString("end_timing");
            String title = currentJSONObject.optString("title");
            ((ItemView) holder).startTiming.setText(startTiming);
            ((ItemView) holder).endTiming.setText(endTiming);
            ((ItemView) holder).titel.setText(title);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return events.length();
    }
}
