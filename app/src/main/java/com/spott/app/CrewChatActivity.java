package com.spott.app;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.github.rockerhieu.emojicon.EmojiconEditText;

public class CrewChatActivity extends FragmentActivity {

    private FrameLayout backFrameLayout;
    private TextView titleTextView;
    private TextView sendTextView;
    private EmojiconEditText messageEditText;
    private RecyclerView messagesRecyclerView;
    private MessagesAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crew_chat);

        backFrameLayout = (FrameLayout) findViewById(R.id.fl_back);
        titleTextView = (TextView) findViewById(R.id.tv_title);
        messagesRecyclerView = (RecyclerView) findViewById(R.id.rv_messages);
        sendTextView = (TextView) findViewById(R.id.tv_send);
        messageEditText = (EmojiconEditText) findViewById(R.id.et_leave_comment);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        messagesRecyclerView.setLayoutManager(manager);
        _adapter = new MessagesAdapter();
        messagesRecyclerView.setAdapter(_adapter);

        _adapter.add(new Message("Johnny Cage", "Hey everybody look this!", "http://varlamov.me/img/grec_cig/00s.jpg"));
        _adapter.add(new Message("Mariana Stephens", "Great!"));
        _adapter.add(new Message("See you there!"));


        backFrameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sendTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = messageEditText.getText().toString();
                messageEditText.setText("");
                _adapter.add(new Message(text));
            }
        });
    }

    class MessageHolder extends RecyclerView.ViewHolder {

        TextView username;
        TextView message;
        ImageView image;
        LinearLayout container;
        LinearLayout messageHolder;


        public MessageHolder(View itemView) {
            super(itemView);

            username = (TextView) itemView.findViewById(R.id.message_username);
            message = (TextView) itemView.findViewById(R.id.message_message);
            image = (ImageView) itemView.findViewById(R.id.message_image);
            container = (LinearLayout) itemView;
            messageHolder = (LinearLayout) itemView.findViewById(R.id.message_container);
        }

        void update(Message m){
            username.setText(m.author);
            message.setText(m.message);
            if (m.image == null || m.image.length() == 0){
                image.setVisibility(View.GONE);
            } else {
                image.setVisibility(View.VISIBLE);
                Picasso.with(image.getContext()).load(m.image).into(image);
            }

            if (m.isMe){
                messageHolder.setBackground(ContextCompat.getDrawable(container.getContext(), R.drawable.message_from));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                params.gravity = Gravity.CENTER;
                container.setLayoutParams(params);
                username.setVisibility(View.GONE);
                message.setTextColor(Color.WHITE);
            } else {
                messageHolder.setBackground(ContextCompat.getDrawable(container.getContext(), R.drawable.message_to));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                params.gravity = Gravity.LEFT;
                container.setLayoutParams(params);
                username.setVisibility(View.VISIBLE);
                message.setTextColor(Color.BLACK);
            }
        }
    }

    class MessagesAdapter extends RecyclerView.Adapter<MessageHolder>{
        private List<Message> _messages = new ArrayList<>();

        @Override
        public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = viewType == 0
                ? getLayoutInflater().inflate(R.layout.message, parent, false)
                : getLayoutInflater().inflate(R.layout.message_right, parent, false);

            return new MessageHolder(view);
        }

        @Override
        public void onBindViewHolder(MessageHolder holder, int position) {
            holder.update(_messages.get(position));
        }

        @Override
        public int getItemCount() {
            return _messages.size();
        }

        @Override
        public int getItemViewType(int position) {
            if (_messages.get(position).isMe){
                return 1;
            }
            return 0;
        }

        public void add(Message message){
            _messages.add(0, message);
            notifyDataSetChanged();
        }
    }

    class Message {
        private String author;
        private String message;
        private String image;
        private boolean isMe;

        public Message(String message) {
            this.message = message;
            isMe = true;
        }

        public Message(String author, String message) {
            this.author = author;
            this.message = message;
            isMe = false;
        }

        public Message(String author, String message, String image) {
            this.author = author;
            this.message = message;
            this.image = image;
            isMe = false;
        }
    }
}
