package com.spott.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Adimn on 10/8/2016.
 */
public class
UTCAPP extends Application
{
    public  ArrayList<HashMap<String, String>> joingroupList = new ArrayList<>();

    public  ArrayList<String> bannedWordsList = new ArrayList<>();

    public  ArrayList<HashMap<String, Object>> bannedWordsDetailList = new ArrayList<>();

    public int pagerPos =1;

    Tracker tracker = null;

    @Override
    public void onCreate()
    {
        super.onCreate();

        new FlurryAgent.Builder().withLogEnabled(true).build(this, "DVKWRXXXJHMRT6QTVGMB");
    }

    synchronized public Tracker getDefaultTracker()
    {
        if (tracker == null)
        {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);

            tracker = analytics.newTracker(R.xml.global_tracker);
        }

        return tracker;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }
}
