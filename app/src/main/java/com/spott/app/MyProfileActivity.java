package com.spott.app;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.renderscript.Allocation;

import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

import com.spott.app.cropiimage.CropImage;

import jp.wasabeef.blurry.Blurry;

/**
 * Created by Acer on 9/21/2016.
 */
public class MyProfileActivity extends FragmentActivity implements View.OnClickListener, AbsListView.OnScrollListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    ImageView iv_profile_photo, iv_back_icon, iv_edit, iv_done, iv_done_education, iv_setting, iv_edit_education, iv_editexp, iv_edit_icon1,
            iv_background, iv_edit_profile_img, iv_emojibtn_name, iv_keyboard_name, iv_edit_bio, iv_emojibtn_subjects, iv_keyboard_subjects, iv_emojibtn_occupation, iv_keyboard_occupation;
    EditText et_mail, et_text, et_profile_name, et_profile_lastname, et_bio, et_interestText;
    RelativeLayout rl_interestView, rlMainFront, ll_groups, rl_action_bar, rl_emo_buttons_name, rl_emo_buttons_subjects, rl_emo_buttons_occupation;
    EditText et_subjects, et_occupation, et_skills;
    ContactsCompletionView et_interst11;
    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    ImageView iv_menu_icon;
    TextView tv_fed, tv_profile_txt, tv_bio, tv_crews, tv_events, tv_my_group, tv_staf_directory, tv_setting_done, white_box1, white_box, orange_box;
    private File mFileTemp;
    DiamondImageView img_photos;
    public static final int REQUEST_CODE_GALLERY = 0x1,
            REQUEST_CODE_TAKE_PICTURE = 0x2, REQUEST_CODE_CROP_IMAGE = 0x3;
    TextView iv_red_icon, tv_num, tv_profile_name, tv_profile_username, tv_subjects, tv_occupation;
    Activity activity;
    Context context;
    LinearLayout ll_feed, ll_profile, ll_directory, emoLay;
    View shadowView = null, view = null;
    int height, width;
    boolean drawer_open = false;
    SharedPreferences preferences;
    TextView tv_done_txt1, tv_done_txt2, tv_done_txt3, tv_done_txt4;
    Bitmap bitmap;
    DiamondImageView img_profile;
    String feed_count = "", profilenamStr = "", lastnamStr = "", usernameStr = "", subjectsStr = "", yearStr = "", batchStr = "", occupStr = "", emplStr = "", monthStr = "", endStr = "", pelicanStr = "",
            skillStr = "", interstStr = "", awardStr = "", honorStr = "";
    ImageView iv_done_icon_exp, iv_done_icon1;
    String quote = "", thumb = "", course = "", batch = "", yearpas = "", comp = "", desig = "", datestart = "", skill = "", interst = "", award = "", honor = "";
    NavigationFragmentAdapter mAdapter;
    String string = "#ed3224";
    String string1 = "#ffffff";
    LayoutInflater layoutInflater = null;
    ViewPager pager;
    int count = 0;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "";
    ArrayList<HashMap<String, String>> mygrouplist;
    GroupAdapter1 gAdapter;
    String encoded = "", base64 = "";
    PullToRefreshListView pulllist;
    String firstname = "", lastname = "", myProfile = "", phone = "", star = "", group = "", post = "", gender = "", uid = "", uid1 = "";
    public static int api_level = 0;
    Button btn_reset_pasword;
    ArrayList<HashMap<String, String>> mygroupfavlist;
    BroadcastReceiver broadcastReceiver;
    private ArrayList<ImageView> dotsNavigate;
    private AdView mAdView;

    EditText et_phone;

    String phone_number = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        MobileAds.initialize(this, String.valueOf(R.string.MobileAds_initialize));
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        setContentView(com.spott.app.R.layout.activity_my_profile);

        et_phone = (EditText) findViewById(R.id.et_phone);

        this.activity = (Activity) MyProfileActivity.this;

        context = getApplicationContext();
        mAdapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(mAdapter);

        addDotsNavigation();
        layoutInflater = LayoutInflater.from(this);
        mygroupfavlist = new ArrayList<HashMap<String, String>>();
        initialise();
        Intent intent = getIntent();
        uid = intent.getStringExtra("uid");
        quote = preferences.getString("quote", "");
        thumb = preferences.getString("thumnail", "");
        course = preferences.getString("course", "");
        batch = preferences.getString("batch", "");
        yearpas = preferences.getString("year_of_passing", "");
        comp = preferences.getString("job_company", "");
        desig = preferences.getString("designation", "");
        datestart = preferences.getString("date_of_start", "");
        interst = preferences.getString("interests", "");
        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        phone = preferences.getString("phone", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");
        gender = preferences.getString("gender", "");
        uid1 = preferences.getString("uid", "");

        if (intent.hasExtra("myProfile")) {

            iv_back_icon.setVisibility(View.VISIBLE);
            iv_red_icon.setVisibility(View.VISIBLE);
            iv_menu_icon.setVisibility(View.GONE);

        } else {
            iv_back_icon.setVisibility(View.GONE);
            iv_red_icon.setVisibility(View.GONE);
            iv_menu_icon.setVisibility(View.VISIBLE);
        }


        iv_edit.setEnabled(false);
        iv_edit_education.setEnabled(false);
        iv_editexp.setEnabled(false);
        iv_edit_icon1.setEnabled(false);
        iv_edit.setVisibility(View.GONE);
        iv_edit_education.setVisibility(View.GONE);
        iv_editexp.setVisibility(View.GONE);
        iv_edit_icon1.setVisibility(View.GONE);
        tv_done_txt1.setVisibility(View.GONE);
        tv_done_txt2.setVisibility(View.GONE);
        tv_done_txt3.setVisibility(View.GONE);
        tv_done_txt4.setVisibility(View.GONE);
        et_profile_name.setEnabled(false);
        et_profile_lastname.setEnabled(false);
        img_photos.setEnabled(false);
        et_mail.setEnabled(false);
        et_phone.setEnabled(false);
        et_subjects.setEnabled(false);
        et_occupation.setEnabled(false);
        et_interst11.setEnabled(false);

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        view.setOnClickListener(this);
        ll_feed.setVisibility(View.VISIBLE);
        setting();
        clickables();

        et_interst11.allowCollapse(false);

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GetEventsIAttend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                new GetCrewsIBelongTo().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GetEventsIAttend().execute();
                new GetCrewsIBelongTo().execute();
            }
        }
    }

    private void initialise() {
        connDec = new ConnectionDetector(MyProfileActivity.this);
        alert = new Alert_Dialog_Manager(context);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        rl_action_bar = (RelativeLayout) findViewById(R.id.rl_action_bar);
        rl_emo_buttons_name = (RelativeLayout) findViewById(R.id.rl_emo_buttons_name);
        rl_emo_buttons_subjects = (RelativeLayout) findViewById(R.id.rl_emo_buttons_subjects);
        rl_emo_buttons_occupation = (RelativeLayout) findViewById(R.id.rl_emo_buttons_occupation);
        et_interestText = (EditText) findViewById(R.id.et_interestText);
        et_interestText.setEnabled(false);
        iv_setting = (ImageView) findViewById(R.id.iv_setting);
        tv_setting_done = (TextView) findViewById(R.id.tv_setting_done);
        iv_profile_photo = (ImageView) findViewById(R.id.iv_profile_photo);
        img_photos = (DiamondImageView) findViewById(R.id.img_photos);
        iv_background = (ImageView) findViewById(R.id.iv_background);
        iv_done = (ImageView) findViewById(R.id.iv_done);
        iv_done_icon_exp = (ImageView) findViewById(R.id.iv_done_icon_exp);
        iv_done_icon1 = (ImageView) findViewById(R.id.iv_done_icon1);
        iv_done_education = (ImageView) findViewById(R.id.iv_done_education);
        iv_edit = (ImageView) findViewById(R.id.iv_edit);
        iv_edit_profile_img = (ImageView) findViewById(R.id.iv_edit_profile_img);
        iv_emojibtn_name = (ImageView) findViewById(R.id.iv_emojibtn_name);
        iv_keyboard_name = (ImageView) findViewById(R.id.iv_keyboard_name);
        iv_edit_bio = (ImageView) findViewById(R.id.iv_edit_bio);
        iv_emojibtn_subjects = (ImageView) findViewById(R.id.iv_emojibtn_subjects);
        iv_keyboard_subjects = (ImageView) findViewById(R.id.iv_keyboard_subjects);
        iv_emojibtn_occupation = (ImageView) findViewById(R.id.iv_emojibtn_occupation);
        iv_keyboard_occupation = (ImageView) findViewById(R.id.iv_keyboard_occupation);
        iv_edit_education = (ImageView) findViewById(R.id.iv_edit_education);
        iv_editexp = (ImageView) findViewById(R.id.iv_editexp);
        iv_edit_icon1 = (ImageView) findViewById(R.id.iv_edit_icon1);
        et_mail = (EditText) findViewById(R.id.et_mail);
        et_mail.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_text = (EditText) findViewById(R.id.et_text);
        et_profile_name = (EditText) findViewById(R.id.et_profile_name);
        et_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_profile_lastname = (EditText) findViewById(R.id.et_profile_lastname);
        et_profile_lastname.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_bio = (EditText) findViewById(R.id.et_bio);
        et_bio.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_subjects = (EditText) findViewById(R.id.et_subjects);
        et_subjects.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_occupation = (EditText) findViewById(R.id.et_occupation);
        et_occupation.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_skills = (EditText) findViewById(R.id.et_skills);
        tv_subjects = (TextView) findViewById(R.id.tv_subjects);
        tv_occupation = (TextView) findViewById(R.id.tv_occupation);
        tv_subjects.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_interst11 = (ContactsCompletionView) findViewById(R.id.et_interest);
        et_interst11.setEllipsize(null);
        rl_interestView = (RelativeLayout) findViewById(R.id.rl_interestView);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        tv_num = (TextView) findViewById(R.id.tv_num);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        white_box1 = (TextView) findViewById(R.id.white_box1);
        white_box = (TextView) findViewById(R.id.white_box);
        orange_box = (TextView) findViewById(R.id.orange_box);
        tv_bio = (TextView) findViewById(R.id.tv_bio);
        tv_events = (TextView) findViewById(R.id.tv_events);
        tv_crews = (TextView) findViewById(R.id.tv_crews);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        tv_done_txt1 = (TextView) findViewById(R.id.tv_done_txt1);
        tv_done_txt2 = (TextView) findViewById(R.id.tv_done_txt2);
        tv_done_txt3 = (TextView) findViewById(R.id.tv_done_txt3);
        tv_done_txt4 = (TextView) findViewById(R.id.tv_done_txt4);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        emoLay = (LinearLayout) findViewById(R.id.emoLay);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        et_mail.setTextColor(Color.parseColor("#000000"));
        et_subjects.setTextColor(Color.parseColor("#D3D3D3"));
        et_occupation.setTextColor(Color.parseColor("#D3D3D3"));
        et_interst11.setTextColor(Color.parseColor("#D3D3D3"));
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        btn_reset_pasword = (Button) findViewById(R.id.btn_reset_pasword);

        et_interst11.allowCollapse(false);

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new ProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ProfileData().execute();
            }
        }

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);
                    if (!getIntent().hasExtra("myProfile")) {
                        if (i == 0) {

                            iv_red_icon.setVisibility(View.GONE);
                            feed_count = i + "";
                            iv_red_icon.setText(i + "");
                        } else if (i < 99) {
                            iv_red_icon.setVisibility(View.VISIBLE);
                            feed_count = i + "";
                            iv_red_icon.setText(i + "");
                        } else {
                            iv_red_icon.setVisibility(View.VISIBLE);
                            feed_count = "99+";
                            iv_red_icon.setText("99+");
                        }
                        mAdapter.notifyDataSetChanged();
                    }
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    int i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#B4BC35"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#ffffff"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_profile);
    }

    private void clickables() {

        rl_interestView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_interst11.isEnabled()) {
                    et_interst11.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(et_interst11, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });


        //////////////////////////////////////////////profile name clicks//////////////////////////////////////////////////////
        iv_emojibtn_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_name.setVisibility(View.VISIBLE);
                iv_emojibtn_name.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                }, 200);


            }
        });

        iv_keyboard_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard_name.setVisibility(View.GONE);
                iv_emojibtn_name.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                if (et_profile_name.hasFocus())
                    CommonUtilities.showSoftKeyboard(activity, et_profile_name);
                else
                    CommonUtilities.showSoftKeyboard(activity, et_profile_lastname);
            }
        });

        et_profile_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons_name.setVisibility(View.VISIBLE);
                } else {
                    rl_emo_buttons_name.setVisibility(View.GONE);
                    iv_emojibtn_name.setVisibility(View.VISIBLE);
                    iv_keyboard_name.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_profile_name.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_profile_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_name.setVisibility(View.VISIBLE);
                iv_keyboard_name.setVisibility(View.GONE);
            }
        });


        et_profile_lastname.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons_name.setVisibility(View.VISIBLE);
                } else {
                    rl_emo_buttons_name.setVisibility(View.GONE);
                    iv_emojibtn_name.setVisibility(View.VISIBLE);
                    iv_keyboard_name.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_profile_lastname.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_profile_lastname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_name.setVisibility(View.VISIBLE);
                iv_keyboard_name.setVisibility(View.GONE);
            }
        });

        iv_edit_bio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_bio.setVisibility(View.GONE);
                et_bio.setVisibility(View.VISIBLE);
            }
        });

        //////////////////////////////////////////////////////et subjects clicks//////////////////////////////////////////////////////////

        iv_emojibtn_subjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_subjects.setVisibility(View.VISIBLE);
                iv_emojibtn_subjects.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                }, 200);
            }
        });

        iv_keyboard_subjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_subjects.setVisibility(View.GONE);
                iv_emojibtn_subjects.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity, et_subjects);
            }
        });

        et_subjects.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    rl_emo_buttons_subjects.setVisibility(View.VISIBLE);
                } else {
                    rl_emo_buttons_subjects.setVisibility(View.GONE);
                    iv_emojibtn_subjects.setVisibility(View.VISIBLE);
                    iv_keyboard_subjects.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_subjects.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);
                return false;
            }
        });

        et_subjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_subjects.setVisibility(View.VISIBLE);
                iv_keyboard_subjects.setVisibility(View.GONE);
            }
        });


        //////////////////////////////////////////////////Occupation Clicks////////////////////////////////////

        iv_emojibtn_occupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_occupation.setVisibility(View.VISIBLE);
                iv_emojibtn_occupation.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                }, 200);
            }
        });

        iv_keyboard_occupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_occupation.setVisibility(View.GONE);
                iv_emojibtn_occupation.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity, et_occupation);
            }
        });

        et_occupation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    rl_emo_buttons_occupation.setVisibility(View.VISIBLE);
                } else {
                    rl_emo_buttons_occupation.setVisibility(View.GONE);
                    iv_emojibtn_occupation.setVisibility(View.VISIBLE);
                    iv_keyboard_occupation.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_occupation.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);
                return false;
            }
        });

        et_occupation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_occupation.setVisibility(View.VISIBLE);
                iv_keyboard_occupation.setVisibility(View.GONE);
            }
        });


        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();
                    drawer_open = true;
                    pager.setAdapter(mAdapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);

                    mAdapter.notifyDataSetChanged();
//                    if (!(preferences.getString("user_image", "").equals(""))||!(preferences.getString("user_image", "").equals("null"))) {
//                        Picasso.with(getApplicationContext()).load(preferences.getString("user_image", "")).into(img_profile);
//                        img_profile.setBorderColor(Color.parseColor("#F15A51"));
//                        img_profile.setBorderWidth(2);
//                    }

                } else {
                    StopDrawerAnim();
                }

            }
        });


        btn_reset_pasword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfileActivity.this, ResetPssword.class);
                intent.putExtra("tick", "");
                startActivity(intent);
            }

        });
        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_profile_name.setEnabled(true);
                et_profile_lastname.setEnabled(true);
                et_text.setEnabled(true);
                img_photos.setEnabled(true);
                iv_done.setVisibility(View.VISIBLE);
                tv_done_txt1.setVisibility(View.VISIBLE);
                iv_edit.setVisibility(View.GONE);
                et_profile_name.setTextColor(Color.parseColor("#000000"));
                et_profile_lastname.setTextColor(Color.parseColor("#000000"));
                et_mail.setTextColor(Color.parseColor("#000000"));
                et_text.setTextColor(Color.parseColor("#000000"));

                et_profile_name.setCursorVisible(true);
                et_profile_name.requestFocus();
                et_profile_name.setSelection(et_profile_name.getText().toString().length());
                et_phone.setEnabled(true);
            }
        });

        iv_edit_education.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                et_subjects.setVisibility(View.VISIBLE);
                tv_subjects.setVisibility(View.GONE);
                et_subjects.setEnabled(true);
                et_subjects.setTextColor(Color.parseColor("#D3D3D3"));
                iv_done_education.setVisibility(View.VISIBLE);
                tv_done_txt2.setVisibility(View.VISIBLE);
                iv_edit_education.setVisibility(View.GONE);
                et_subjects.setCursorVisible(true);
                et_subjects.requestFocus();
                et_subjects.setSelection(et_subjects.getText().toString().length());

            }
        });

        iv_editexp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_occupation.setVisibility(View.GONE);
                et_occupation.setVisibility(View.VISIBLE);
                et_occupation.setEnabled(true);
                iv_done_icon_exp.setVisibility(View.VISIBLE);
                tv_done_txt3.setVisibility(View.VISIBLE);
                iv_editexp.setVisibility(View.GONE);
                et_occupation.setTextColor(Color.parseColor("#D3D3D3"));
                et_occupation.setCursorVisible(true);
                et_occupation.requestFocus();
                et_occupation.setSelection(et_occupation.getText().toString().length());
            }
        });

        iv_edit_icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_interst11.setEnabled(true);
                et_interestText.setVisibility(View.GONE);
                rl_interestView.setVisibility(View.VISIBLE);

                System.out.println("LIST SIZE EDIT : " + et_interst11.getObjects().size());

                iv_done_icon1.setVisibility(View.VISIBLE);
                tv_done_txt4.setVisibility(View.VISIBLE);
                iv_edit_icon1.setVisibility(View.GONE);
                et_interst11.setTextColor(Color.parseColor("#D3D3D3"));
                et_interst11.setCursorVisible(true);
                et_interst11.requestFocus();
                // et_interst11.setSelection(et_interst11.getText().toString().length());
            }
        });
        iv_done_icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<Person> persons = et_interst11.getObjects();
                rl_interestView.setVisibility(View.GONE);
                et_interst11.setEnabled(false);

                System.out.println("LIST SIZE : " + persons.size());
                String interestsToShow = "";
                for (int i = 0; i < persons.size(); i++) {
                    if (i == persons.size() - 1)
                        interestsToShow = interestsToShow + persons.get(i).getEmail();
                    else
                        interestsToShow = interestsToShow + persons.get(i).getEmail() + ",";
                }

                et_interestText.setText("" + interestsToShow);
                et_interestText.setVisibility(View.VISIBLE);
                iv_edit_icon1.setVisibility(View.VISIBLE);
                iv_done_icon1.setVisibility(View.GONE);
                tv_done_txt4.setVisibility(View.GONE);
                hideKeyBoard();


            }
        });
        iv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv_edit.setVisibility(View.VISIBLE);
                iv_done.setVisibility(View.GONE);
                tv_done_txt1.setVisibility(View.GONE);
                et_profile_name.setEnabled(false);
                et_profile_lastname.setEnabled(false);
                et_mail.setEnabled(false);
                et_phone.setEnabled(false);
                et_text.setEnabled(false);
                img_photos.setEnabled(false);
                profilenamStr = CommonUtilities.encodeUTF8(et_profile_name.getText().toString().trim());
                lastnamStr = CommonUtilities.encodeUTF8(et_profile_lastname.getText().toString().trim());
                usernameStr = et_mail.getText().toString().trim();
                rl_emo_buttons_name.setVisibility(View.GONE);
                iv_emojibtn_name.setVisibility(View.VISIBLE);
                iv_keyboard_name.setVisibility(View.GONE);
                emoLay.setVisibility(View.GONE);
                hideKeyBoard();

            }
        });
        iv_done_education.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_subjects.setVisibility(View.VISIBLE);
                et_subjects.setVisibility(View.GONE);
                iv_edit_education.setVisibility(View.VISIBLE);
                iv_done_education.setVisibility(View.GONE);
                tv_done_txt2.setVisibility(View.GONE);
                et_subjects.setEnabled(false);
                subjectsStr = CommonUtilities.encodeUTF8(et_subjects.getText().toString().trim());
                rl_emo_buttons_subjects.setVisibility(View.GONE);
                iv_emojibtn_subjects.setVisibility(View.VISIBLE);
                iv_keyboard_subjects.setVisibility(View.GONE);
                emoLay.setVisibility(View.GONE);
                hideKeyBoard();
                setVisivility();
            }
        });

        iv_done_icon_exp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_occupation.setVisibility(View.VISIBLE);
                et_occupation.setVisibility(View.GONE);
                iv_editexp.setVisibility(View.VISIBLE);
                iv_done_icon_exp.setVisibility(View.GONE);
                tv_done_txt3.setVisibility(View.GONE);
                et_occupation.setEnabled(false);
                rl_emo_buttons_occupation.setVisibility(View.GONE);
                iv_emojibtn_occupation.setVisibility(View.VISIBLE);
                iv_keyboard_occupation.setVisibility(View.GONE);
                emoLay.setVisibility(View.GONE);
                setVisivility();
                hideKeyBoard();

            }
        });

        img_photos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showCustomMessagephoto("UTC", "Profile Pic");

            }
        });

        iv_edit_profile_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMessagephoto("UTC", "Profile Pic");
            }
        });

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }


        iv_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv_setting.setVisibility(View.GONE);
                iv_edit.setVisibility(View.VISIBLE);
                iv_edit_bio.setVisibility(View.VISIBLE);
                iv_edit_education.setVisibility(View.VISIBLE);
                iv_editexp.setVisibility(View.VISIBLE);
                iv_edit_icon1.setVisibility(View.VISIBLE);
                iv_edit_profile_img.setVisibility(View.VISIBLE);
                tv_setting_done.setVisibility(View.VISIBLE);
                iv_edit.setEnabled(true);
                iv_edit_education.setEnabled(true);
                iv_editexp.setEnabled(true);
                img_photos.setEnabled(true);
                iv_edit_icon1.setEnabled(true);
                iv_setting.setEnabled(true);
            }
        });

        tv_setting_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                profilenamStr = CommonUtilities.encodeUTF8(et_profile_name.getText().toString().trim());
                lastnamStr = CommonUtilities.encodeUTF8(et_profile_lastname.getText().toString().trim());
                if (profilenamStr.equals("") || lastnamStr.equals("")) {
                    Toast.makeText(activity, "First name and last name should not be empty.", Toast.LENGTH_SHORT).show();
                } else {
                    iv_edit_bio.setVisibility(View.GONE);
                    iv_edit.setVisibility(View.GONE);
                    iv_edit_education.setVisibility(View.GONE);
                    iv_editexp.setVisibility(View.GONE);
                    iv_edit_icon1.setVisibility(View.GONE);
                    iv_setting.setVisibility(View.VISIBLE);
                    iv_done_education.setVisibility(View.GONE);
                    iv_done_icon_exp.setVisibility(View.GONE);
                    tv_setting_done.setVisibility(View.GONE);
                    iv_done_icon1.setVisibility(View.GONE);
                    iv_edit_profile_img.setVisibility(View.GONE);
                    iv_done.setVisibility(View.GONE);
                    tv_done_txt1.setVisibility(View.GONE);
                    tv_done_txt2.setVisibility(View.GONE);
                    tv_done_txt3.setVisibility(View.GONE);
                    tv_done_txt4.setVisibility(View.GONE);
                    img_photos.setEnabled(false);
                    iv_edit.setEnabled(false);
                    iv_edit_education.setEnabled(false);
                    iv_editexp.setEnabled(false);
                    iv_edit_icon1.setEnabled(false);
                    et_profile_name.setEnabled(false);
                    et_profile_lastname.setEnabled(false);
                    et_mail.setEnabled(false);
                    et_subjects.setEnabled(false);
                    et_occupation.setEnabled(false);
                    et_interst11.setEnabled(false);
                    et_phone.setEnabled(false);
                    hideKeyBoard();

                    phone_number = et_phone.getText().toString().trim();

                    usernameStr = et_mail.getText().toString().trim();
                    interstStr = et_interestText.getText().toString().trim();
                    occupStr = CommonUtilities.encodeUTF8(et_occupation.getText().toString().trim());
                    subjectsStr = CommonUtilities.encodeUTF8(et_subjects.getText().toString().trim());

                    if (!connDec.isConnectingToInternet()) {
                        //alert.showNetAlert();
                    } else {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new UserProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, base64, profilenamStr, lastnamStr, usernameStr);
                        } else {
                            new UserProfileData().execute(base64, profilenamStr, lastnamStr, usernameStr);
                        }

                    }

                    ///
                    /*
                    if (!connDec.isConnectingToInternet()) {
                        //alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new UserAcheivementData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new UserAcheivementData().execute();
                        }
                    }

                    if (!connDec.isConnectingToInternet()) {
                        //alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new UserJobData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, occupStr, subjectsStr);
                        } else {
                            new UserJobData().execute(occupStr, subjectsStr);
                        }
                    }*/

                    if (!connDec.isConnectingToInternet()) {
                        //alert.showNetAlert();
                    } else {
                        String updatedBio = String.valueOf(et_bio.getText());

                        if (!updatedBio.equals("")) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new UpdateBio().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(et_bio.getText()));
                            } else {
                                new UpdateBio().execute(String.valueOf(et_bio.getText()));
                            }
                        }
                    }
                }
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().hasExtra("myProfile")) {
                    finish();
                } else {
                    Intent intent = new Intent(MyProfileActivity.this, UpdatesActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);
                }
            }
        });
    }

    private class GetEventsIAttend extends AsyncTask<String, String, String> {
        String getData = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "map_events");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            //TODO GetEventsIAttend

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {

                    JSONArray campusJSONArray = new JSONObject(result).optJSONArray("campus");

                    String eventsText = "";

                    int campusJSONArrayLength = campusJSONArray.length();

                    int campusJSONArrayLengthMinusOne = campusJSONArrayLength - 1;

                    for (int i = 0; i < campusJSONArrayLength; i++) {
                        String eventName = campusJSONArray.optJSONObject(i).optString("title");
                        String statusEvent  =campusJSONArray.optJSONObject(i).optString("status");
                        if (statusEvent.equals("Attending")||statusEvent.equals("Has Ticket")) {
                            if (i != campusJSONArrayLengthMinusOne)
                                eventsText += eventName + ", ";
                            else
                                eventsText += eventName;
                        }
                    }

                    if (eventsText.equals("")) {
                        eventsText = "No events attended";
                    }

                    tv_events.setText(eventsText);

                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new ProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new ProfileData().execute();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetCrewsIBelongTo extends AsyncTask<String, String, String> {

        String getData = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "my_groups");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {
                    JSONArray listJSONArray = new JSONObject(result).optJSONArray("list");

                    String crewsText = "";

                    int listJSONArrayLength = listJSONArray.length();

                    int listJSONArrayLengthMinusOne = listJSONArrayLength - 1;

                    for (int i = 0; i < listJSONArrayLength; i++) {
                        String crewName = listJSONArray.optJSONObject(i).optString("group_name");
                        if (i != listJSONArrayLengthMinusOne)
                            crewsText += crewName + ", ";
                        else
                            crewsText += crewName;
                    }

                    if (crewsText.equals("")) {
                        crewsText = "No Crews joined";
                    }

                    tv_crews.setText(crewsText);

                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new ProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new ProfileData().execute();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class UpdateBio extends AsyncTask<String, String, String> {

        String getData = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "update_bio");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("bio", strings[0]);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {
                    et_bio.setVisibility(View.GONE);

                    tv_bio.setVisibility(View.VISIBLE);

                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new ProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new ProfileData().execute();
                        }
                    }
                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onResume() {

        super.onResume();
        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GroupFavData().execute();
            }
        }
        final int pos = 1;
        pager.postDelayed(new Runnable() {

            @Override
            public void run() {
                pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
            }
        }, 100);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ((UTCAPP) getApplication()).pagerPos = position;

                selectDotNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        //LoaderClass.HideProgressWheel(MyProfileActivity.this);
    }

    @Override
    public void onBackPressed() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(et_interst11.getWindowToken(), 0);

        if (emoLay.getVisibility() == View.VISIBLE)
            emoLay.setVisibility(View.GONE);
        else
            finish();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        if (et_profile_name.hasFocus()) {
            EmojiconsFragment.input(et_profile_name, emojicon);
        } else if (et_profile_lastname.hasFocus()) {
            EmojiconsFragment.input(et_profile_lastname, emojicon);
        } else if (et_subjects.hasFocus()) {
            EmojiconsFragment.input(et_subjects, emojicon);
        } else if (et_occupation.hasFocus()) {
            EmojiconsFragment.input(et_occupation, emojicon);
        }
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        if (et_profile_name.hasFocus()) {
            EmojiconsFragment.backspace(et_profile_name);
        } else if (et_profile_lastname.hasFocus()) {
            EmojiconsFragment.backspace(et_profile_lastname);
        } else if (et_subjects.hasFocus()) {
            EmojiconsFragment.backspace(et_subjects);
        } else if (et_occupation.hasFocus()) {
            EmojiconsFragment.backspace(et_occupation);
        }
    }

    private void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


// For Navigation Animation

    private class NavigationFragmentAdapter extends PagerAdapter {


        TextView tv_browsing;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_notification_count, tv_update, tv_feed, tv_profile, tv_groups, tv_staff_dir, tv_log_out;


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            if (feed_count.equals("0"))
                tv_notification_count.setVisibility(View.GONE);

            else {
                tv_notification_count.setVisibility(View.VISIBLE);

                tv_notification_count.setText(feed_count);
            }
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupFavData().execute();
                    }
                }
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyProfileActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {

                    }

                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;

                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new GroupFavData().execute();
                            }

                            mygroupfavlist.clear();
                        }

                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyProfileActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
                tv_log_out = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(preferences.getString("first_name", "") + " " + preferences.getString("last_name", ""));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                if (feed_count.equals("0"))
                    tv_notification_count.setVisibility(View.GONE);

                else {
                    tv_notification_count.setVisibility(View.VISIBLE);

                    tv_notification_count.setText(feed_count);
                }
                if (encoded.equalsIgnoreCase("")||encoded.equalsIgnoreCase("null")) {
                    img_profile.setImageResource(R.drawable.avatar);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }

                white_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(activity, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                        finish();

                    }
                });
                tv_groupss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(activity, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                        finish();
                    }
                });
                orange_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonUtilities.showStarsPopup(activity);
                    }
                });

                tv_feed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        Intent intent = new Intent(MyProfileActivity.this, MyFeedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();

                    }
                });

                tv_notification_count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyProfileActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_groups.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyProfileActivity.this, JoinGroupActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyProfileActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }
                });
                tv_staff_dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyProfileActivity.this, MyDirectoryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }
                });
                tv_log_out.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        customShareLogOutPopup();
                    }
                });

                container.addView(view, 0);
            }
            //   ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = mygrouplist.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    //new GroupListData().execute();
                }
            } else {
            }
        }
    }

    public class GroupFavData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            pulllist.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        mygroupfavlist.clear();

                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");
                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);
                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygroupfavlist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(MyProfileActivity.this, mygroupfavlist);

                        pulllist.setAdapter(gAdapter);

                        gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygroupfavlist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygroupfavlist) {
            this.context = context;
            this.mygroupfavlist = mygroupfavlist;
        }

        @Override
        public int getCount() {
            return mygroupfavlist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);

                    finish();
                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);

                    finish();
                }
            });
            if (mygroupfavlist.get(i).get("unread").equals("0")) {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setText(mygroupfavlist.get(i).get("unread"));
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }

    @Override
    public void onClick(View view) {

        Handler handler = new Handler();

        switch (view.getId()) {
            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:
                Intent intent = new Intent(MyProfileActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                finish();

                break;

            case R.id.ll_groups:

                Intent intent2 = new Intent(MyProfileActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);
                finish();

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(MyProfileActivity.this, CalendarActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);
                finish();

                break;
        }
    }

    private void showCustomMessagephoto(String string, String string2) {
        final Dialog dialogphoto = new Dialog(MyProfileActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        api_level = Build.VERSION.SDK_INT;
                        if (api_level >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequest2();
                            } else {
                                OpenGallery();
                            }
                        } else {
                            OpenGallery();
                        }
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        api_level = Build.VERSION.SDK_INT;
                        if (api_level >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            int permission3 = ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.CAMERA);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED
                                    || permission3 != PackageManager.PERMISSION_GRANTED) {
                                makeRequest1();
                            } else {
                                takePicture();
                            }
                        } else {
                            takePicture();
                        }

                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();
    }

    public void makeRequest1() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.CAMERA"}, 1);
    }

    @SuppressLint("Override")
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(MyProfileActivity.this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();

                } else {
                    takePicture();
                }
                break;

            case 2:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(MyProfileActivity.this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();

                } else {
                    OpenGallery();
                }
                break;
        }
    }

    public void makeRequest2() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 2);
    }

    public class ProfileData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;

        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //LoaderClass.ShowProgressWheel(MyProfileActivity.this);
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "user_profile_data");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("uid", uid1);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is PROFILE  : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONObject user_dataObject = resultObj.getJSONObject("profile");

                    String profile_id = user_dataObject.getString("profile_id");
                    profilenamStr = user_dataObject.getString("first_name");
                    lastnamStr = user_dataObject.getString("last_name");
                    encoded = user_dataObject.getString("user_image");
                    String user_thumbnail = user_dataObject.getString("user_thumbnail");
                    String hobbies = user_dataObject.getString("hobbies");
                    if (user_dataObject.has("phone_no")) {
                        String phone = user_dataObject.getString("phone_no");

                        et_phone.setText(phone);
                    }
                    String dob = user_dataObject.getString("dob");
                    //quoteStr = user_dataObject.getString("quotes");
                    String profile_create_date = user_dataObject.getString("profile_create_date");
                    String profile_modify_date = user_dataObject.getString("profile_modify_date");
                    String gender = user_dataObject.getString("gender");
                    usernameStr = user_dataObject.getString("username");

                    JSONObject jtotal_stars = resultObj.getJSONObject("total_stars");

                    String pelican = jtotal_stars.getString("pelican");

                    preferences.edit().putString("pelican", pelican).commit();

                    JSONObject jtotalgroups = resultObj.getJSONObject("total_group");

                    String total_grp = jtotalgroups.getString("total_group");

                    preferences.edit().putString("total_group", total_grp).commit();

                    JSONObject jtotalpost = resultObj.getJSONObject("total_post");

                    String total_posst = jtotalpost.getString("total_post");

                    preferences.edit().putString("total_post", total_posst).commit();

                    preferences.edit().putString("username", usernameStr).commit();

                    preferences.edit().putString("quotes", user_dataObject.getString("quotes")).commit();

                    System.out.println("IMAGE=" + encoded);

                    et_profile_name.setText(CommonUtilities.decodeUTF8(profilenamStr));
                    et_profile_lastname.setText(CommonUtilities.decodeUTF8(lastnamStr));
                    et_mail.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                    et_text.setText("@" + preferences.getString("username", ""));
                    white_box1.setText(total_posst);
                    white_box.setText(total_grp);
                    orange_box.setText(preferences.getString("pelican", "0"));

                    if (encoded.equalsIgnoreCase("")) {

                        img_photos.setImageResource(R.drawable.avatar);
//                        Bitmap blurredBitmap = BlurBuilder.blur( getApplicationContext(), encoded );
//
//                        view.setBackgroundDrawable( new BitmapDrawable( getResources(), blurredBitmap ) );

                        iv_background.setImageResource(R.drawable.avatar);
                        Blurry.with(MyProfileActivity.this)
                                .radius(10)
                                .sampling(8)
                                .async()
                                .capture(findViewById(R.id.iv_background))
                                .into((ImageView) findViewById(R.id.iv_background));
                        img_photos.setBorderColor(Color.parseColor("#F15A51"));
                        img_photos.setBorderWidth(2);
                    } else {

                        Target target = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                img_photos.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        };

                        Picasso.with(getApplicationContext()).load(encoded).into(target);

                        Target target1 = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                /*Bitmap bm = blur(bitmap);*/

                                iv_background.setImageBitmap(bitmap);

                                Blurry.with(MyProfileActivity.this)
                                        .radius(10)
                                        .sampling(8)
                                        .async()
                                        .capture(findViewById(R.id.iv_background))
                                        .into((ImageView) findViewById(R.id.iv_background));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                iv_background.setImageResource(R.drawable.avatar);

                                Blurry.with(MyProfileActivity.this)
                                        .radius(10)
                                        .sampling(8)
                                        .async()
                                        .capture(findViewById(R.id.iv_background))
                                        .into((ImageView) findViewById(R.id.iv_background));
                                img_photos.setImageResource(R.drawable.avatar);
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        };

                        Picasso.with(getApplicationContext()).load(encoded).into(target1);


                        // Picasso.with(getApplicationContext()).load(encoded).into(iv_background);
                        img_photos.setBorderColor(Color.parseColor("#F15A51"));
                        img_photos.setBorderWidth(2);
                    }

                    String bio = resultObj.optJSONObject("profile").optString("bio");
                    if (!bio.equals("null")) {
                        tv_bio.setText(bio);
                        et_bio.setText(bio);
                    }

                    String education = resultObj.getString("education");
                    if (!education.equals("")) {
                        JSONObject user_dataObj = resultObj.getJSONObject("education");
                        String course = user_dataObj.getString("course");
                        String batch = user_dataObj.getString("batch");
                        String eid = user_dataObj.getString("eid");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String education_create_date = user_dataObj.getString("education_create_date");
                        String education_status = user_dataObj.getString("education_status");

                        preferences.edit().putString("course", course).commit();

                        preferences.edit().putString("batch", batch).commit();

                        preferences.edit().putString("year_of_passing", year_of_passing).commit();
                        //   et_subjects.setText(course);
                    }

                    String job = resultObj.getString("job");
                    if (!job.equals("")) {
                        JSONObject user_dataObj1 = resultObj.getJSONObject("job");
                        String job_id = user_dataObj1.getString("job_id");
                        String job_company = user_dataObj1.getString("job_company");
                        String designation = user_dataObj1.getString("designation");
                        String date_of_start = user_dataObj1.getString("date_of_start");
                        String date_of_end = user_dataObj1.getString("date_of_end");
                        String ispresent = user_dataObj1.getString("ispresent");
                        String job_create_date = user_dataObj1.getString("job_create_date");
                        String status = user_dataObj1.getString("status");
                        preferences.edit().putString("job_company", job_company).commit();
                        preferences.edit().putString("designation", designation).commit();
                        preferences.edit().putString("date_of_start", date_of_start).commit();
                        preferences.edit().putString("date_of_end", date_of_end).commit();
                        et_subjects.setText(CommonUtilities.decodeUTF8(designation));
                        tv_subjects.setText(CommonUtilities.decodeUTF8(designation));
                        et_occupation.setText(CommonUtilities.decodeUTF8(job_company));
                        tv_occupation.setText(CommonUtilities.decodeUTF8(job_company));
                        setVisivility();

                    }
                    String achievements = resultObj.getString("achievements");
                    if (!achievements.equals("")) {
                        JSONObject user_dataObj2 = resultObj.getJSONObject("achievements");
                        //JSONObject user_dataObj2= new JSONObject("achievements");
                        String achievement_id = user_dataObj2.getString("achievement_id");
                        String skills = user_dataObj2.getString("skills");
                        String interests = user_dataObj2.getString("interests");
                        String awards = user_dataObj2.getString("awards");
                        String honors = user_dataObj2.getString("honors");
                        String achievement_create_date = user_dataObj2.getString("achievement_create_date");
                        String achievement_modify_date = user_dataObj2.getString("achievement_modify_date");

                        preferences.edit().putString("interests", interests).commit();

                        if (interests.substring(interests.length() - 1).equals(",")) {
                            interests = interests.substring(0, interests.length() - 1);
                        }

                        et_interestText.setText(interests);

                        et_interst11.clear();

                        String[] intrestArray = interests.split(",");
                        for (int i = 0; i < intrestArray.length; i++)
                            et_interst11.addObject(new Person(intrestArray[i], intrestArray[i]));

                        rl_interestView.setVisibility(View.GONE);

                        et_interestText.setVisibility(View.VISIBLE);

//                        String[] tagsArray = interests.split(",");
//
//                        for (int i = 0; i < tagsArray.length; i++) {
//                            et_interst.addObject(new Person(tagsArray[i], tagsArray[i]));
//                        }
                        // et_interst.setText(interests);
                    }

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public class UserProfileData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        String photo = "", firstname = "", lastname = "", quote = "", updatedusername = "";

        @Override
        protected void onPreExecute() {

//            String usernameL = et_mail.getText().toString().trim();
//
//            updatedusername = usernameL.substring(1, usernameL.length());

            LoaderClass.ShowProgressWheel(MyProfileActivity.this);

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "user_quote");

                jo.put("login_token", preferences.getString("login_token", ""));

                // String usernameL = et_mail.getText().toString().trim();

                jo.put("quotes", "");

                jo.put("first_name", profilenamStr);

                jo.put("last_name", lastnamStr);

                preferences.edit().putString("phone", phone_number).commit();

                jo.put("contact", phone_number);

                //jo.put("profile_image", base64);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.
                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is QUOTE : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                base64 = "";

                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    // preferences.edit().putString("username", username).commit();

                    usernameStr = resultObj.getString("quote");

                    String getImageUrl = resultObj.getString("profile_image");

                    String thumnail = resultObj.getString("thumnail");
                    preferences.edit().putString("quote", usernameStr).commit();

                    preferences.edit().putString("user_image", getImageUrl).commit();

                    preferences.edit().putString("first_name", CommonUtilities.decodeUTF8(profilenamStr)).commit();

                    preferences.edit().putString("last_name", CommonUtilities.decodeUTF8(lastnamStr)).commit();
                    if (getImageUrl.isEmpty()) {

                        if (gender.equals("Male")) {
                            img_profile.setImageResource(R.drawable.avatar);
                            img_photos.setImageResource(R.drawable.avatar);
                            iv_background.setImageResource(R.drawable.avatar);
                            Blurry.with(MyProfileActivity.this)
                                    .radius(10)
                                    .sampling(8)
                                    .async()
                                    .capture(findViewById(R.id.iv_background))
                                    .into((ImageView) findViewById(R.id.iv_background));

                        } else if (gender.equals("Female")) {
                            img_profile.setImageResource(R.drawable.avatar);
                            img_photos.setImageResource(R.drawable.avatar);
                            iv_background.setImageResource(R.drawable.avatar);
                            Blurry.with(MyProfileActivity.this)
                                    .radius(10)
                                    .sampling(8)
                                    .async()
                                    .capture(findViewById(R.id.iv_background))
                                    .into((ImageView) findViewById(R.id.iv_background));

                        }
                    } else {
                        Target target = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                img_photos.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        };

                        Target target1 = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                // Bitmap bm = blur(bitmap);

                                // iv_background.setImageBitmap(bm);

                                iv_background.setImageBitmap(bitmap);

                                Blurry.with(MyProfileActivity.this)
                                        .radius(10)
                                        .sampling(8)
                                        .async()
                                        .capture(findViewById(R.id.iv_background))
                                        .into((ImageView) findViewById(R.id.iv_background));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        };

                        Picasso.with(getApplicationContext()).load(getImageUrl).into(target);

                        Picasso.with(getApplicationContext()).load(getImageUrl).into(target1);

                        img_photos.setBorderColor(Color.parseColor("#F15A51"));
                        img_photos.setBorderWidth(2);
                    }

                    et_profile_name.setText(CommonUtilities.decodeUTF8(profilenamStr));
                    et_profile_lastname.setText(CommonUtilities.decodeUTF8(lastnamStr));
                    et_mail.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                    et_text.setText("@" + preferences.getString("username", ""));
                    tv_profile_name.setText(profilenamStr + " " + lastnamStr);
                    tv_profile_username.setText(CommonUtilities.decodeUTF8(usernameStr));
                    Picasso.with(getApplicationContext()).load(getImageUrl).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);

                } else if (errCode.equals("700")) {
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            LoaderClass.HideProgressWheel(MyProfileActivity.this);

        }
    }

    public class UserProfileImage extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        String photo = "", firstname = "", lastname = "", quote = "", updatedusername = "";

        @Override
        protected void onPreExecute() {

//            String usernameL = et_mail.getText().toString().trim();
//
//            updatedusername = usernameL.substring(1, usernameL.length());

            LoaderClass.ShowProgressWheel(MyProfileActivity.this);

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "update_pic");

                jo.put("login_token", preferences.getString("login_token", ""));

                // String usernameL = et_mail.getText().toString().trim();

                jo.put("profile_image", base64);
                ;

                System.out.println("JSON DATA = " + jo);

                // Send POST output.
                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is QUOTE : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                base64 = "";

                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    String getImageUrl = resultObj.getString("profile_image");

                    String thumnail = resultObj.getString("thumnail");

                    preferences.edit().putString("user_image", getImageUrl).commit();
                    if (getImageUrl.isEmpty()) {

                        if (gender.equals("Male")) {
                            img_profile.setImageResource(R.drawable.avatar);
                            img_photos.setImageResource(R.drawable.avatar);
                            iv_background.setImageResource(R.drawable.avatar);
                            Blurry.with(MyProfileActivity.this)
                                    .radius(10)
                                    .sampling(8)
                                    .async()
                                    .capture(findViewById(R.id.iv_background))
                                    .into((ImageView) findViewById(R.id.iv_background));

                        } else if (gender.equals("Female")) {
                            img_profile.setImageResource(R.drawable.avatar);
                            img_photos.setImageResource(R.drawable.avatar);
                            iv_background.setImageResource(R.drawable.avatar);
                            Blurry.with(MyProfileActivity.this)
                                    .radius(10)
                                    .sampling(8)
                                    .async()
                                    .capture(findViewById(R.id.iv_background))
                                    .into((ImageView) findViewById(R.id.iv_background));

                        }
                    } else {
                        Target target = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                img_photos.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                                img_profile.setImageResource(R.drawable.avatar);
                                img_photos.setImageResource(R.drawable.avatar);
                                iv_background.setImageResource(R.drawable.avatar);
                                Blurry.with(MyProfileActivity.this)
                                        .radius(10)
                                        .sampling(8)
                                        .async()
                                        .capture(findViewById(R.id.iv_background))
                                        .into((ImageView) findViewById(R.id.iv_background));

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        };

                        Target target1 = new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

                                // Bitmap bm = blur(bitmap);

                                // iv_background.setImageBitmap(bm);

                                iv_background.setImageBitmap(bitmap);

                                Blurry.with(MyProfileActivity.this)
                                        .radius(10)
                                        .sampling(8)
                                        .async()
                                        .capture(findViewById(R.id.iv_background))
                                        .into((ImageView) findViewById(R.id.iv_background));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                                img_profile.setImageResource(R.drawable.avatar);
                                img_photos.setImageResource(R.drawable.avatar);
                                iv_background.setImageResource(R.drawable.avatar);
                                Blurry.with(MyProfileActivity.this)
                                        .radius(10)
                                        .sampling(8)
                                        .async()
                                        .capture(findViewById(R.id.iv_background))
                                        .into((ImageView) findViewById(R.id.iv_background));

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        };

                        Picasso.with(getApplicationContext()).load(getImageUrl).into(target);

                        Picasso.with(getApplicationContext()).load(thumnail).into(target1);

                        img_photos.setBorderColor(Color.parseColor("#F15A51"));
                        img_photos.setBorderWidth(2);
                    }

                    Picasso.with(getApplicationContext()).load(getImageUrl).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);

                } else if (errCode.equals("700")) {
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            LoaderClass.HideProgressWheel(MyProfileActivity.this);
        }
    }

    public class UserEducationData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        String courses = "", batches = "", passyear = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdDialog = new Dialog(MyProfileActivity.this);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            //  LoaderClass.ShowProgressWheel(MyProfileActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;
//            courses = strings[0];
//
//            batches = strings[1];
//
//            passyear = strings[2];
            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "update_education");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("course", subjectsStr);

                jo.put("batch", batchStr);

                jo.put("year_of_passing", yearStr);


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //   LoaderClass.HideProgressWheel(MyProfileActivity.this);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

//                    courses = resultObj.getString("course");
//                    batchStr = resultObj.getString("batch");
//                    yearStr = resultObj.getString("year_of_passing");
                    preferences.edit().putString("course", subjectsStr).commit();
                    preferences.edit().putString("batch", batchStr).commit();
                    preferences.edit().putString("year_of_passing", passyear).commit();

                    et_subjects.setText(CommonUtilities.decodeUTF8(batchStr));
                    tv_subjects.setText(CommonUtilities.decodeUTF8(batchStr));
                    setVisivility();


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class UserJobData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        String company, design, startdate;

        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "update_job");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("company", occupStr);

                jo.put("designation", subjectsStr);

                jo.put("date_of_start", datestart);

                jo.put("date_of_end", preferences.getString("date_of_end", ""));

                jo.put("ispresent", preferences.getString("ispresent", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {


                    preferences.edit().putString("company", occupStr).commit();
                    preferences.edit().putString("designation", subjectsStr).commit();
                    preferences.edit().putString("date_of_start", startdate).commit();

                    et_occupation.setText(CommonUtilities.decodeUTF8(occupStr));
                    tv_occupation.setText(CommonUtilities.decodeUTF8(occupStr));
                    et_subjects.setText(CommonUtilities.decodeUTF8(subjectsStr));
                    tv_subjects.setText(CommonUtilities.decodeUTF8(subjectsStr));
                    setVisivility();

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private void setVisivility() {
        if (et_occupation.getText().toString().equals("")) {
            et_occupation.setVisibility(View.VISIBLE);
            tv_occupation.setVisibility(View.GONE);
        } else {
            et_occupation.setVisibility(View.GONE);
            tv_occupation.setText(et_occupation.getText().toString());
        }

        if (et_subjects.getText().toString().equals("")) {
            et_subjects.setVisibility(View.VISIBLE);
            tv_subjects.setVisibility(View.GONE);
        } else {
            et_subjects.setVisibility(View.GONE);
            tv_subjects.setText(et_subjects.getText().toString());
        }
    }

    public class UserAcheivementData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "update_achievement");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("skills", "");

//                List<Person> array = et_interst11.getObjects();
//
//                StringBuilder stringBuilder = new StringBuilder(9999999);
//
//                for (int i = 0; i < array.size(); i++) {
//
//                    stringBuilder.append(array.get(i).getName() + ",");
//                }

                jo.put("interests", interstStr);

                jo.put("awards", "");

                jo.put("honors", "");

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new ProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new ProfileData().execute();
                        }
                    }
                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //////////////////////////////////////////////////


    private String base64frombitmap(Bitmap bitmap) {
        String base64 = "";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return base64;
    }


    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_GALLERY);
    }

    static ArrayList<Bitmap> bitmaps_arrayList = new ArrayList<Bitmap>();

    File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Bitmap bitmap;

            byte[] b;
            switch (requestCode) {
                case REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                        copyStream(inputStream, fileOutputStream);

                        fileOutputStream.close();

                        inputStream.close();

                        startCropImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case REQUEST_CODE_TAKE_PICTURE:
                    //System.out.println("onact camera");
                    startCropImage();
                    break;
                case REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                    // bitmap2 = BitmapFactory.decodeFile(mFileTemp.getPath());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                    b = baos.toByteArray();

                    //String baseimg = Base64.encodeToString(b, Base64.DEFAULT);

                    System.out.println("bitmap in return : " + bitmap);

                    bitmaps_arrayList.add(bitmap);

                    if (mFileTemp.exists()) {
                        mFileTemp.delete();
                    }

                    base64 = base64frombitmap(bitmap);

                    int bwidth = bitmap.getWidth();

                    int bheight = bitmap.getHeight();

                    int swidth = img_photos.getWidth();

                    int sheight = img_photos.getHeight();

                    int new_width = swidth;

                    img_photos.setImageBitmap(bitmap);


                    if (!connDec.isConnectingToInternet()) {
                        //alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new UserProfileImage().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new UserProfileImage().execute();
                        }
                    }

                    //  bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                    //Bitmap blurredBitmap = BlurBuilder.blur( MyProfileActivity.this, bitmap );

                    //  view.setBackgroundDrawable( new BitmapDrawable( getResources(), blurredBitmap ) );

                    //  iv_background.setImageBitmap(blurredBitmap);

            }
        } else {
            if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                startCropImage();
            }
        }
    }

    private void startCropImage() {

        //System.out.println("cropimage");

        Intent intent = new Intent(MyProfileActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //System.out.println("takepict inner");
        try {
            Uri mImageCaptureUri = null;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public class LogOutData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);
                String errCode = resultObj.getString("err-code");
                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    {
                        preferences.edit().clear().commit();
                        ((UTCAPP) getApplication()).joingroupList.clear();

                        Intent i = new Intent(activity, SplashActivity.class);
                        startActivity(i);
                        finishAffinity();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyProfileActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(MyProfileActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                    //  finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LogOutData().execute();
                    }
                }
            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("My Profile Screen");

        FlurryAgent.onStartSession(MyProfileActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(MyProfileActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    // drawer navigate functionality
    public void addDotsNavigation() {
        dotsNavigate = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.circle_indicator);

        for (int i = 0; i < 2; i++) {
            ImageView dot = new ImageView(this);

            dot.setImageDrawable(getResources().getDrawable(R.drawable.under_slide_black_out));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            final int pos = i;

            dot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pager.setCurrentItem(pos);
                }
            });

            dotsLayout.addView(dot, params);

            dotsNavigate.add(dot);
        }
    }

    public void selectDotNavigation(int idx) {

        Resources res = getResources();
        for (int i = 0; i < 2; i++) {
            int drawableId = (i == idx) ? (R.drawable.under_slide_black) : (R.drawable.under_slide_black_out);
            Drawable drawable = res.getDrawable(drawableId);
            dotsNavigate.get(i).setImageDrawable(drawable);
        }
    }

    //Set the radius of the Blur. Supported range 0 < radius <= 25
    private static final float BLUR_RADIUS = 25f;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public Bitmap blur(Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(this);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

        //Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

}
