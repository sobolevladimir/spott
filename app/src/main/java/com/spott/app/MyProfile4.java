package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

import jp.wasabeef.blurry.Blurry;

/**
 * Created by Acer on 9/21/2016.
 */
public class MyProfile4 extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {

    ImageView iv_profile_photo, iv_background;
    EditText et_mail, et_text, et_profile_name, et_profile_lastname;
    EditText et_subjects, et_occupation, et_skills, et_interst;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_setting_done, white_box1, white_box, orange_box;
    TextView tv_number, tv_num;
    Activity activity;
    Context context;
    DiamondImageView img_photos;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout rlMainFront, ll_groups, rl_action_bar;
    View shadowView = null, view = null;
    int height, width;
    boolean drawer_open = false;
    SharedPreferences preferences;
    Bitmap bitmap;
    String  usernameStr ="";
    PageIndicator mIndicator;
    String string = "#ed3224";
    String string1 = "#ffffff";
    LayoutInflater layoutInflater = null;
    ViewPager pager;
    private BroadcastReceiver broadcastReceiver;
    ImageView iv_back_icon,iv_red_icon;
    int count = 0;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "";
    ArrayList<HashMap<String, String>> mygrouplist;
    String encoded;
    private PullToRefreshListView pulllist;
    private AdView mAdView;
    String firstname = "", lastname = "", username = "", star = "", group = "", post = "", uid ="";
    String group_id = "", groupname = "", grpdetail = "", grpimg = "", grptype = "", grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_profile);
        this.activity = (Activity) MyProfile4.this;
        context = MyProfile4.this;
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.circle_indicator);
        mIndicator = indicator;
        indicator.setViewPager(pager);
        indicator.setPageColor(Color.parseColor(string1));
        indicator.setFillColor(Color.parseColor(string));
        indicator.setStrokeColor(Color.parseColor(string));
        layoutInflater = LayoutInflater.from(this);
        mygrouplist = new ArrayList<HashMap<String, String>>();
        initialise();
        clickables();
        Intent intent = getIntent();
        uid = intent.getStringExtra("UID");
        owner_id = intent.getStringExtra("owner_id");
        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new ProfileData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ProfileData().execute();
            }
        }
        et_profile_name.setEnabled(false);
        et_profile_lastname.setEnabled(false);
        img_photos.setEnabled(false);
        et_mail.setEnabled(false);
        et_subjects.setEnabled(false);
        et_occupation.setEnabled(false);
        et_interst.setEnabled(false);
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        view.setOnClickListener(this);
        setting();
        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        owner_id = intent.getStringExtra("owner_id");
        owner_type = intent.getStringExtra("owner_type");
        you_grp = intent.getStringExtra("you_group");

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.btm_myfeed,0,0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#B4BC35"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.sel_btm_myprofile,0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#ffffff"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.btm_mygroups,0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0,R.drawable.btm_staff,0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_profile);

    }


    private void initialise() {
        connDec = new ConnectionDetector(MyProfile4.this);
        alert = new Alert_Dialog_Manager(context);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        rl_action_bar = (RelativeLayout) findViewById(R.id.rl_action_bar);
        tv_setting_done = (TextView) findViewById(R.id.tv_setting_done);
        iv_profile_photo = (ImageView) findViewById(R.id.iv_profile_photo);
        img_photos = (DiamondImageView) findViewById(R.id.img_photos);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_background = (ImageView) findViewById(R.id.iv_background);
        et_mail = (EditText) findViewById(R.id.et_mail);
        et_mail.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_text = (EditText) findViewById(R.id.et_text);
        et_profile_name = (EditText) findViewById(R.id.et_profile_name);
        et_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_profile_lastname = (EditText) findViewById(R.id.et_profile_lastname);
        et_profile_lastname.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
        et_subjects = (EditText) findViewById(R.id.et_subjects);
        et_subjects.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_occupation = (EditText) findViewById(R.id.et_occupation);
        et_occupation.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_skills = (EditText) findViewById(R.id.et_skills);
        et_interst = (EditText) findViewById(R.id.et_interest);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_num = (TextView) findViewById(R.id.tv_num);
        white_box1 = (TextView) findViewById(R.id.white_box1);
        white_box = (TextView) findViewById(R.id.white_box);
        orange_box = (TextView) findViewById(R.id.orange_box);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        et_profile_name.setTextColor(Color.parseColor("#000000"));
        et_profile_lastname.setTextColor(Color.parseColor("#000000"));
        et_mail.setTextColor(Color.parseColor("#000000"));
        et_subjects.setTextColor(Color.parseColor("#D3D3D3"));
        et_occupation.setTextColor(Color.parseColor("#D3D3D3"));
        et_interst.setTextColor(Color.parseColor("#D3D3D3"));
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i=Integer.parseInt(new_message);
                    if(i==0){
                        tv_num.setVisibility(View.GONE);
                    }
                    else if(i>0) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }

    @Override
    public void onResume() {

        super.onResume();
        final int pos = 1;

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = mygrouplist.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    //new GroupListData().execute();
                }
            } else {
            }
        }
    }


    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    private void clickables() {
        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MyProfile4.this,CrewActivity.class);
                intent.putExtra("uid",uid);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("group_image", grpimg);
                intent.putExtra("group_type", grptype);
                intent.putExtra("group_tags", grptag);
                intent.putExtra("group_create_date", groupcreatdate);
                intent.putExtra("group_status", grpstatus);
                intent.putExtra("owner_id", owner_id);
                intent.putExtra("owner_type", owner_type);
                intent.putExtra("you_group", you_grp);
                startActivity(intent);

            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(MyProfile4.this,CrewActivity.class);
                intent.putExtra("uid",uid);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("group_image", grpimg);
                intent.putExtra("group_type", grptype);
                intent.putExtra("group_tags", grptag);
                intent.putExtra("group_create_date", groupcreatdate);
                intent.putExtra("group_status", grpstatus);
                intent.putExtra("owner_id", owner_id);
                intent.putExtra("owner_type", owner_type);
                intent.putExtra("you_group", you_grp);
                startActivity(intent);

            }
        });

    }

    @Override
    public void onClick(View view) {

        Handler handler = new Handler();

        switch (view.getId()) {

            case R.id.ll_feed:
                Intent intent = new Intent(MyProfile4.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(MyProfile4.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(MyProfile4.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class ProfileData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData  = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdDialog = new Dialog(MyProfile4.this);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            LoaderClass.ShowProgressWheel(MyProfile4.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "user_profile_data");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("uid", uid);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            LoaderClass.HideProgressWheel(MyProfile4.this);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONObject user_dataObject = resultObj.getJSONObject("profile");

                    String profile_id = user_dataObject.getString("profile_id");
                    String first_name = user_dataObject.getString("first_name");
                    String last_name = user_dataObject.getString("last_name");
                    String image = user_dataObject.getString("user_image");
                    String user_thumbnail = user_dataObject.getString("user_thumbnail");
                    String hobbies = user_dataObject.getString("hobbies");
                    String phone = user_dataObject.getString("phone");
                    String dob = user_dataObject.getString("dob");
                    String quotes = user_dataObject.getString("quotes");
                    String profile_create_date = user_dataObject.getString("profile_create_date");
                    String profile_modify_date = user_dataObject.getString("profile_modify_date");
                    String gender = user_dataObject.getString("gender");
                    usernameStr = user_dataObject.getString("username");

                    JSONObject jtotal_stars = resultObj.getJSONObject("total_stars");

                    String pelican = jtotal_stars.getString("pelican");

                    JSONObject jtotalgroups = resultObj.getJSONObject("total_group");

                    String total_grp = jtotalgroups.getString("total_group");

                    JSONObject jtotalpost = resultObj.getJSONObject("total_post");

                    String total_posst = jtotalpost.getString("total_post");

                    System.out.println("IMAGE=" + encoded);

                    et_profile_name.setText(CommonUtilities.decodeUTF8(first_name));
                    et_profile_lastname.setText(CommonUtilities.decodeUTF8(last_name));

                    preferences.edit().putString("quotes",quotes).commit();


                    preferences.edit().putString("username",usernameStr).commit();

                    et_mail.setText("@"+CommonUtilities.decodeUTF8(preferences.getString("username","")));
                    et_text.setText("@"+preferences.getString("username",""));

                    white_box1.setText(total_posst);
                    white_box.setText(total_grp);
                    orange_box.setText(preferences.getString("pelican","0"));
                    if (image.equalsIgnoreCase("")) {

                        img_photos.setImageResource(R.drawable.app);
                        img_photos.setImageResource(R.drawable.app);
                        img_photos.setBorderColor(Color.parseColor("#F15A51") );
                        iv_background.setImageResource(R.drawable.app);
                        Blurry.with(MyProfile4.this)
                                .radius(10)
                                .sampling(8)
                                .async()
                                .capture(findViewById(R.id.iv_background))
                                .into((ImageView) findViewById(R.id.iv_background));
                        img_photos.setBorderWidth(2);
                    }
                    else {
                        Picasso.with(getApplicationContext()).load(image).into(img_photos);
                        Picasso.with(getApplicationContext()).load(image).into(iv_background);
                        Blurry.with(MyProfile4.this)
                                .radius(10)
                                .sampling(8)
                                .async()
                                .capture(findViewById(R.id.iv_background))
                                .into((ImageView) findViewById(R.id.iv_background));
                        img_photos.setBorderColor(Color.parseColor("#F15A51"));
                        img_photos.setBorderWidth(2);
                    }
                    String education = resultObj.getString("education");
                    if (!education.equals("")) {
                        JSONObject user_dataObj = resultObj.getJSONObject("education");
                        String course = user_dataObj.getString("course");
                        String batch = user_dataObj.getString("batch");
                        String eid = user_dataObj.getString("eid");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String education_create_date = user_dataObj.getString("education_create_date");
                        String education_status = user_dataObj.getString("education_status");
                     //   et_subjects.setText(course);

                    }

                    String job = resultObj.getString("job");
                    if (!job.equals("")) {
                        JSONObject user_dataObj1 = resultObj.getJSONObject("job");

                        String job_id = user_dataObj1.getString("job_id");
                        String job_company = user_dataObj1.getString("job_company");
                        String designation = user_dataObj1.getString("designation");
                        String date_of_start = user_dataObj1.getString("date_of_start");
                        String date_of_end = user_dataObj1.getString("date_of_end");
                        String ispresent = user_dataObj1.getString("ispresent");
                        String job_create_date = user_dataObj1.getString("job_create_date");
                        String status = user_dataObj1.getString("status");
                        et_subjects.setText(CommonUtilities.decodeUTF8(designation));
                        et_occupation.setText(CommonUtilities.decodeUTF8(job_company));

                    }
                    String achievements = resultObj.getString("achievements");
                    if (!achievements.equals("")) {
                        JSONObject user_dataObj2 = resultObj.getJSONObject("achievements");

                        String achievement_id = user_dataObj2.getString("achievement_id");
                        String skills = user_dataObj2.getString("skills");
                        String interests = user_dataObj2.getString("interests");
                        String awards = user_dataObj2.getString("awards");
                        String honors = user_dataObj2.getString("honors");
                        String achievement_create_date = user_dataObj2.getString("achievement_create_date");
                        String achievement_modify_date = user_dataObj2.getString("achievement_modify_date");

                        et_interst.setText(interests);

                    }

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Profile Screen 4");

        FlurryAgent.onStartSession(MyProfile4.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(MyProfile4.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}
