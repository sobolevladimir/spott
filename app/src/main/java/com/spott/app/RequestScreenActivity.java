package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Acer on 9/22/2016.
 */
public class RequestScreenActivity extends Activity implements View.OnClickListener {

    ImageView iv_red_icon, iv_back_icon, iv_red_cube;
    Activity activity;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront, ll_groups;
    ListView listview;
    Context context;
    RelativeLayout rl_request;
    String member_id,status;
    private BroadcastReceiver broadcastReceiver;
    ArrayList<HashMap<String, String>> requestList;
    SharedPreferences preferences;
    RequestAdapter adapter;
    TextView tv_num,tv_no_request;
    String group_id = "",member="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_activity);
        this.activity = (Activity) RequestScreenActivity.this;
        listview = (ListView) findViewById(R.id.listview);
        context = getApplicationContext();
        requestList = new ArrayList<HashMap<String, String>>();
        adapter = new RequestAdapter(context, requestList);
        listview.setAdapter(adapter);
        initialise();
        clickables();
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else
        {
            new MessageData().execute();
        }*/
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            new RequestMemberData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else
        {
            new RequestMemberData().execute();
        }
    }

    private void initialise() {
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_red_cube = (ImageView) findViewById(R.id.iv_red_cube);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_no_request = (TextView) findViewById(R.id.tv_no_request);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    String new_feed = intent.getStringExtra("feed_count");
                    /*Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    }
                    else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }

    private void clickables() {
        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestScreenActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });
        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RequestScreenActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {
        Handler handler = new Handler();

        switch (view.getId()) {
            case R.id.ll_feed:

                ll_feed.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent = new Intent(RequestScreenActivity.this, MyFeedActivity.class);
                startActivity(intent);

                break;

            case R.id.ll_profile:

                ll_profile.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent1 = new Intent(RequestScreenActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                startActivity(intent1);

                break;

            case R.id.ll_groups:

                ll_groups.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent2 = new Intent(RequestScreenActivity.this, JoinGroupActivity.class);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:

                ll_directory.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent3 = new Intent(RequestScreenActivity.this, MyDirectoryActivity.class);
                startActivity(intent3);

                break;
        }
    }

    private class RequestAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> groupList;

        public RequestAdapter(Context context, ArrayList<HashMap<String, String>> groupList) {
            this.context = context;
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return requestList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.request_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.iv_group_profile = (DiamondImageView) convertView.findViewById(R.id.iv_group_profile);
            holder.tv_name_request = (TextView) convertView.findViewById(R.id.tv_name_request);
            holder.tv_name_request.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_accept = (TextView) convertView.findViewById(R.id.tv_accept);
            holder.tv_decline = (TextView) convertView.findViewById(R.id.tv_decline);
            holder.tv_pending_txt = (TextView) convertView.findViewById(R.id.tv_pending_txt);
            rl_request=(RelativeLayout)convertView.findViewById(R.id.rl_request);

            String gender = requestList.get(i).get("gender");

            if(requestList.equals("") || requestList == null){
                rl_request.setVisibility(View.GONE);
                holder.tv_pending_txt.setVisibility(View.VISIBLE);
            }
            else {
                rl_request.setVisibility(View.VISIBLE);
                holder.tv_pending_txt.setVisibility(View.GONE);
                if (requestList.get(i).get("user_image").toString().equals("") || requestList.get(i).get("user_image").toString() == null) {
                    holder.iv_group_profile.setImageResource(R.drawable.app);
                    holder.iv_group_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_profile.setBorderWidth(2);
                }
                else
                {
                    Picasso.with(getApplicationContext()).load(requestList.get(i).get("user_image")).into(holder.iv_group_profile);
                    holder.iv_group_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_profile.setBorderWidth(2);
                }
                if (requestList.get(i).get("gender").toString().equals("") || requestList.get(i).get("gender").toString() == null) {
                    holder.iv_group_profile.setImageResource(R.drawable.app);
                    holder.iv_group_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_profile.setBorderWidth(2);
                }

                }
                holder.tv_name_request.setText(CommonUtilities.decodeUTF8(requestList.get(i).get("first_name")));

                holder.tv_accept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            member_id=requestList.get(i).get("member_id");
                            status="Accept";
                            new RequestStatusData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new RequestStatusData().execute();
                        }

                        requestList.remove(i);
                        adapter.notifyDataSetChanged();
                    }
                });
                holder.tv_decline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            member_id=requestList.get(i).get("member_id");
                            status="Decline";
                            new RequestStatusData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new RequestStatusData().execute();
                        }
                        requestList.remove(i);
                        adapter.notifyDataSetChanged();
                    }
                });

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_group_profile;
            TextView tv_accept, tv_decline, tv_name_request,tv_pending_txt;

        }
    }

    public class RequestMemberData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(RequestScreenActivity.this);
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "request_member");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");
                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String uid1 = "";
                        String member_id = user_dataObj.getString("member_id");
                        String group_id = user_dataObj.getString("group_id");
                        uid1 = user_dataObj.getString("uid");
                        String member_status = user_dataObj.getString("member_status");
                        String mute_group = user_dataObj.getString("mute_group");
                        String member_create_date = user_dataObj.getString("member_create_date");
                        String profile_id = user_dataObj.getString("profile_id");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");

                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String hobbies = user_dataObj.getString("hobbies");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        hmap.put("member_id", member_id);
                        hmap.put("uid", uid1);
                        hmap.put("profile_id", profile_id);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("member_create_date", member_create_date);
                        hmap.put("mute_group", mute_group);

                        hmap.put("member_status", member_status);
                        hmap.put("group_id", group_id);

                        hmap.put("member_id", member_id);

                       // preferences.edit().putString("uid", uid1).commit();

                       preferences.edit().putString("member_id",member_id).commit();

                        requestList.add(hmap);

                    }

                    adapter = new RequestAdapter(context, requestList);
                    listview.setAdapter(adapter);

                }
                else if(errCode.equals("300")){

                    listview.setVisibility(View.GONE);
                    tv_no_request.setVisibility(View.VISIBLE);


                }else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(RequestScreenActivity.this);
        }
    }

    public class RequestStatusData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            LoaderClass.ShowProgressWheel(RequestScreenActivity.this);

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "request_status");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("member_id",member_id);

                jo.put("member_status", status);

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            LoaderClass.HideProgressWheel(RequestScreenActivity.this);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {
                    if(requestList.size()==0)
                    {
                        listview.setVisibility(View.GONE);
                        tv_no_request.setVisibility(View.VISIBLE);
                    }


                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Request Screen");

        FlurryAgent.onStartSession(RequestScreenActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(RequestScreenActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}

