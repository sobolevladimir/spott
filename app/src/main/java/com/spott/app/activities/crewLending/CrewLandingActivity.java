package com.spott.app.activities.crewLending;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.spott.app.CrewChatActivity;
import com.spott.app.R;
import com.spott.app.services.api.GroupDetailRequest;
import com.spott.app.services.api.GroupDetailResponse;
import com.spott.app.services.api.GroupMembersRequest;
import com.spott.app.services.api.GroupMembersResponse;
import com.spott.app.services.api.RetrofitBuilder;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by worker on 15.09.2017.
 */

public class CrewLandingActivity extends FragmentActivity {

    public static final String ARG_GROUP_ID = "group_id";

    @BindView(R.id.tv_group_name)
    TextView tvGroupName;

    @BindView(R.id.tv_group_detail)
    TextView tvGroupDetail;

    @BindView(R.id.tv_likes_count)
    TextView tvLikesCount;

    @BindView(R.id.tv_comments_count)
    TextView tvCommentsCount;

    @BindView(R.id.rv_photos)
    RecyclerView mRecViewPhotos;

    @BindView(R.id.rv_members)
    RecyclerView mRecViewMembers;

    @BindView(R.id.iv_group_image)
    ImageView ivGroupImage;

    @BindView(R.id.progress)
    ProgressBar progressBar;

    @BindView(R.id.layout_data)
    ViewGroup layData;

    private String mGroupId;

    private MembersAdapter membersAdapter;
    private MembersPhotoAdapter membersPhotoAdapter;

    private int totalAsyncTasks;
    private int completeAsyncTasks;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crew_landing);
        ButterKnife.bind(this);

        loadIntent();
        initAdapters();

        loadDataAsync();
    }

    private void loadDataAsync() {

        layData.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        totalAsyncTasks = 2;
        loadDetailsAsync();
        loadMembersAsync();
    }

    private void loadIntent() {
        mGroupId = getIntent().getStringExtra(ARG_GROUP_ID);
    }

    private void loadDetailsAsync() {
        RetrofitBuilder
                .getSpottApi()
                .getGroupDetail(new GroupDetailRequest(this, mGroupId))
                .enqueue(new Callback<GroupDetailResponse>() {
                    @Override
                    public void onResponse(Call<GroupDetailResponse> call, Response<GroupDetailResponse> response) {
                        refreshGroupDetails(response.body());
                        checkAsyncEnd();
                    }

                    @Override
                    public void onFailure(Call<GroupDetailResponse> call, Throwable t) {
                        Toast.makeText(CrewLandingActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        checkAsyncEnd();
                    }
                });
    }

    private void checkAsyncEnd() {

        completeAsyncTasks++;

        if (completeAsyncTasks == totalAsyncTasks) {
            progressBar.setVisibility(View.GONE);
            layData.setVisibility(View.VISIBLE);
        }
    }

    private void loadMembersAsync() {
        RetrofitBuilder.getSpottApi()
                .getGroupMembers(new GroupMembersRequest(this, mGroupId))
                .enqueue(new Callback<GroupMembersResponse>() {
                    @Override
                    public void onResponse(Call<GroupMembersResponse> call, Response<GroupMembersResponse> response) {
                        membersPhotoAdapter.addItems(response.body().list);
                        membersAdapter.addItems(response.body().list);
                        checkAsyncEnd();
                    }

                    @Override
                    public void onFailure(Call<GroupMembersResponse> call, Throwable t) {
                        Toast.makeText(CrewLandingActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        checkAsyncEnd();
                    }
                });
    }

    private void refreshGroupDetails(GroupDetailResponse response) {
        tvGroupName.setText(response.detail.groupName);
        tvGroupDetail.setText(response.detail.groupDetail);
        tvLikesCount.setText(response.detail.totalLikes);
        tvCommentsCount.setText(response.totalComment);
        Picasso.with(this)
                .load(response.detail.groupImage)
                .error(R.drawable.avatar)
                .placeholder(R.drawable.avatar)
                .into(ivGroupImage);
    }

    @OnClick(R.id.iv_chat)
    void onBtnChatClick() {
        startActivity(new Intent(CrewLandingActivity.this, CrewChatActivity.class));
    }

    private void initAdapters() {
        mRecViewMembers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecViewMembers.setAdapter(membersAdapter = new MembersAdapter());

        mRecViewPhotos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecViewPhotos.setAdapter(membersPhotoAdapter = new MembersPhotoAdapter());
    }

}
