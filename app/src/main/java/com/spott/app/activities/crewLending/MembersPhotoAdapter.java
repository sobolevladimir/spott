package com.spott.app.activities.crewLending;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.spott.app.R;
import com.spott.app.common.BaseRecyclerViewAdapter;
import com.spott.app.common.DiamondImageView;
import com.spott.app.services.api.GroupMembersResponse;
import com.squareup.picasso.Picasso;

/**
 * Created by oleg on 08.10.2017.
 */

public class MembersPhotoAdapter extends BaseRecyclerViewAdapter<GroupMembersResponse.Member, MembersPhotoAdapter.PhotosViewHolder> {

    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        ImageView view = new DiamondImageView(viewGroup.getContext());
        Picasso.with(viewGroup.getContext())
                .load(R.drawable.avatar)
                .into(view);
        ViewGroup.MarginLayoutParams margin = new ViewGroup.MarginLayoutParams(96, 96);
        margin.setMargins(8, 8, 8, 8);
        view.setLayoutParams(margin);

        return new PhotosViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PhotosViewHolder photosViewHolder, final int i) {

        final GroupMembersResponse.Member member = items.get(i);

        Picasso.with(photosViewHolder.itemView.getContext())
                .load(member.userThumbnail)
                .error(R.drawable.avatar)
                .placeholder(R.drawable.avatar)
                .into((ImageView) photosViewHolder.itemView);

        photosViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(photosViewHolder.itemView.getContext(), "Clicked user id: " + member.profileId, Toast.LENGTH_SHORT).show();
            }
        });
    }

    class PhotosViewHolder extends RecyclerView.ViewHolder {

        PhotosViewHolder(View itemView) {
            super(itemView);
        }
    }
}

