package com.spott.app.activities.crewLending;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.spott.app.R;
import com.spott.app.common.BaseRecyclerViewAdapter;
import com.spott.app.services.api.GroupMembersResponse;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by oleg on 08.10.2017.
 */

public class MembersAdapter extends BaseRecyclerViewAdapter<GroupMembersResponse.Member, MembersAdapter.MembersViewHolder> {

    @Override
    public MembersViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_crew_landing_member, null, false);
        return new MembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MembersViewHolder membersViewHolder, final int i) {

        GroupMembersResponse.Member item = items.get(i);

        Picasso.with(membersViewHolder.itemView.getContext())
                .load(item.userThumbnail)
                .error(R.drawable.avatar)
                .placeholder(R.drawable.avatar)
                .into(membersViewHolder.ivMemberPhoto);
        membersViewHolder.ivMemberName.setText(item.firstName + " " + item.lastName);

        membersViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(membersViewHolder.itemView.getContext(), "Clicked member " + String.valueOf(i), Toast.LENGTH_SHORT).show();
            }
        });
    }

    class MembersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_member_photo)
        ImageView ivMemberPhoto;

        @BindView(R.id.tv_member_name)
        TextView ivMemberName;

        MembersViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

