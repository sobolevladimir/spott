package com.spott.app;

/**
 * Created by Vlad on 13.10.2017.
 */

public class Constants {
    public static final int REQUEST_CODE_GALLERY = 2;
    public static final int REQUEST_CODE_TAKE_PICTURE = 3;
    public static final int REQUEST_CODE_CROP_IMAGE = 4;

    public static final String ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE = "android.permission.WRITE_EXTERNAL_STORAGE";
    public static final String ANDROID_PERMISSION_READ_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";
    public static final String PERMISSION_HAS_BEEN_DENIED_BY_USER = "Permission has been denied by user";

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
}
