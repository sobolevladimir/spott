package com.spott.app;

/**
 * Created by Adimn on 9/30/2016.
 */

import java.io.Serializable;

public class Person implements Serializable {
    private String name;
    private String email;

    public Person(String n, String e) {
        name = n;
        email = e;
    }

    public String getName() { return name; }
    public String getEmail() { return email; }

    @Override
    public String toString() { return name; }


}

