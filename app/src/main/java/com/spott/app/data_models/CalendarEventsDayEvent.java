package com.spott.app.data_models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artem on 08.11.2017.
 */


public class CalendarEventsDayEvent implements Parcelable {
    private String mName;

    private String mStartTime;

    private String mEndTime;

    public CalendarEventsDayEvent(String name, String startTime, String endTime) {
        CalendarEventsDayEvent.this.mName = name;
        CalendarEventsDayEvent.this.mStartTime = startTime;
        CalendarEventsDayEvent.this.mEndTime = endTime;
    }

    protected CalendarEventsDayEvent(Parcel in) {
        mName = in.readString();
        mStartTime = in.readString();
        mEndTime = in.readString();
    }

    public static final Creator<CalendarEventsDayEvent> CREATOR = new Creator<CalendarEventsDayEvent>() {
        @Override
        public CalendarEventsDayEvent createFromParcel(Parcel in) {
            return new CalendarEventsDayEvent(in);
        }

        @Override
        public CalendarEventsDayEvent[] newArray(int size) {
            return new CalendarEventsDayEvent[size];
        }
    };

    public String getName() {
        return CalendarEventsDayEvent.this.mName;
    }

    public String getStartTime() {
        return CalendarEventsDayEvent.this.mStartTime;
    }

    public String getEndTime() {
        return CalendarEventsDayEvent.this.mEndTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeString(mStartTime);
        parcel.writeString(mEndTime);
    }
}
