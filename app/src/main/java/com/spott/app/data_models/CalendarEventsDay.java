package com.spott.app.data_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.spott.app.CalendarActivity;

import java.util.ArrayList;

/**
 * Created by Artem on 08.11.2017.
 */

public class CalendarEventsDay implements Parcelable {
    private boolean mIsToday;

    private String mDate;

    private ArrayList<CalendarEventsDayEvent> mEvents = new ArrayList<>();

    public CalendarEventsDay(boolean isToday, String date, ArrayList<CalendarEventsDayEvent> events) {
        CalendarEventsDay.this.mIsToday = isToday;
        CalendarEventsDay.this.mDate = date;
        CalendarEventsDay.this.mEvents = events;
    }

    private CalendarEventsDay(Parcel in) {
        mIsToday = in.readByte() != 0;
        mDate = in.readString();
        in.readTypedList(mEvents, CalendarEventsDayEvent.CREATOR);
    }

    public static final Creator<CalendarEventsDay> CREATOR = new Creator<CalendarEventsDay>() {
        @Override
        public CalendarEventsDay createFromParcel(Parcel in) {
            return new CalendarEventsDay(in);
        }

        @Override
        public CalendarEventsDay[] newArray(int size) {
            return new CalendarEventsDay[size];
        }
    };

    public boolean getIsToday() {
        return CalendarEventsDay.this.mIsToday;
    }

    public String getDate() {
        return CalendarEventsDay.this.mDate;
    }

    public ArrayList<CalendarEventsDayEvent> getEvents() {
        return CalendarEventsDay.this.mEvents;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (mIsToday ? 1 : 0));
        parcel.writeString(mDate);
        parcel.writeTypedList(mEvents);
    }
}
