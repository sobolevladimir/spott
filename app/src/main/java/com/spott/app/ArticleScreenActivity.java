package com.spott.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

import com.spott.app.common.DrawableHelper;
import com.spott.app.cropiimage.CropImage;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DetectHtml;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;
import com.spott.app.common.SwipeLayout;

/**
 * Created by Acer on 9/24/2016.
 */
public class ArticleScreenActivity extends FragmentActivity implements View.OnClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener

{

    ImageView iv_back_icon, iv_full_img, iv_news, iv_heart_fill, iv_msg;
    TextView tv_title, tv_bydefine, tv_notif, tv_msg_noti, tv_date, tv_send, tv_cal_date, tv_clock_time, tv_location, tv_load_more;
    ImageView iv_menu_icon, iv_red_icon,iv_keyboard;
    ImageView iv_smiley1, iv_smiley2, iv_smiley3, iv_smiley4, iv_smiley5, iv_iconheart;
    TextView tvTotalGlobal, tv_number, tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_num, tv_noti, tv_description, tv_heart;
    RelativeLayout ll_groups, rl_sel_smileys, rl_background,rl_emo_buttons;
    LinearLayout ll_feed, ll_profile, ll_directory, ll_staff_info, emoLay;
    ImageView iv_heart_smile,happy_smile;
    Activity activity;
    LayoutInflater layoutInflater = null;
    ArrayList<HashMap<String, String>> pingList;
    Context context;
    private WebView web_description;
    ListView listis;
    private View mCustomView;
    private int mOriginalSystemUiVisibility;
    private int mOriginalOrientation;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    EditText et_leave_comment;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    RecyclerView rvImages;
    ViewPager pager;
    Timer timer;
    private HorizontalAdapter horizontalAdapter;
    private ArrayList<Bitmap> imagesArray = new ArrayList<>();
    ArrayList<HashMap<String, String>> imageList;
    String post_id = "", you_like = "", video_link = "";
    ArrayList<HashMap<String, Object>> commentList;
    ArrayList<HashMap<String, Object>> commentLists;
    ArrayList<HashMap<String, Object>> mainMapList = new ArrayList<>();
    HashMap<String, Object> mainMap;
    ArrayList<HashMap<String, String>> moodList;
    int l_position = 0;
    ArticleListingAdapter adapter = null;
    String commentStr = "";
    int count = 0;
    String rating_selected = "", moodStr = "", VideoURL = "", primary_image = "";

    boolean has_more;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    WebView webview;
    ScrollView scrollView;
    int tempPos = 0;
    private boolean isLoading = false;
    private String your_like = "";
    private JSONArray jArrayy;
    String messageId = "";
    private int scrolledPosition = 0;
    TextView tv_no_comn;
    LinearLayout ll_delete;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    ArrayList<HashMap<String, String>> moodList1;
    ArrayList<HashMap<String, Object>> groupList;
    private BroadcastReceiver broadcastReceiver;
    ArrayList<String> checkIfDataPresent = new ArrayList<>();

    boolean isFirst = true, hideView = false;

    ImageView iv_full_popup_img, iv_emojibtn;

    RelativeLayout rlPhoto;

    boolean boolScroolToBottom = false;

    private int apiLevel;

    private File tempImageFile;

    private String encodedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_screen_activity);
        scrollView = (ScrollView) findViewById(R.id.scroll);
        this.activity = (Activity) ArticleScreenActivity.this;
        listis = (ListView) findViewById(R.id.listis);
        pager = (ViewPager) findViewById(R.id.pager);
        context = ArticleScreenActivity.this;
        pingList = new ArrayList<HashMap<String, String>>();
        commentList = new ArrayList<HashMap<String, Object>>();
        commentLists = new ArrayList<HashMap<String, Object>>();
        moodList = new ArrayList<HashMap<String, String>>();
        imageList = new ArrayList<HashMap<String, String>>();
        moodList1 = new ArrayList<HashMap<String, String>>();
        groupList = new ArrayList<HashMap<String, Object>>();
        layoutInflater = LayoutInflater.from(this);

        Intent intent = getIntent();

        if (intent.hasExtra("post_id")) {
            post_id = intent.getStringExtra("post_id");
            //isFirst = false;
        }


        you_like = intent.getStringExtra("you_like");
        video_link = intent.getStringExtra("video_link");
        initialise();
        clickables();
        /*if (!connDec.isConnectingToInternet()) {
            // alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new MessageData().execute();
            }
        }*/
        setting();

        callMainApis();

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);

        timer.scheduleAtFixedRate(new RemindTask(), 0, 7000);
        rlPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rlPhoto.setVisibility(View.GONE);
                iv_full_popup_img.setImageBitmap(null);
                try {
                    final GlideDrawable gifDrawable = (GlideDrawable) iv_full_img.getDrawable();
                    gifDrawable.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (getIntent().hasExtra("scrollTobottom")) {
            if (getIntent().getStringExtra("scrollTobottom").equals("TRUE")) {
                boolScroolToBottom = true;
            } else {
                boolScroolToBottom = false;
            }
        } else {
            boolScroolToBottom = false;
        }

    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(et_leave_comment, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(et_leave_comment);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pingServices();
                }
            });
        }
    }

    private void pingServices() {
        if (!connDec.isConnectingToInternet()) {

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new PingServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new PingServiceData().execute();
            }
        }
    }

    private void callMainApis() {
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new ArticleScreenData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ArticleScreenData().execute();
            }
        }

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#FFAD3F"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#ffffff"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(tv_fed);
    }


    private void setAdapter() {
        if (adapter == null) {
            adapter = new ArticleListingAdapter();

            listis.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

        setListViewHeightBasedOnChildren(listis);

       /* listis.postDelayed(new Runnable() {
            @Override
            public void run() {
                setListViewHeightBasedOnChildren(listis);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        tv_msg_noti.setText("" + commentList.size());

                        scrollView.post(new Runnable() {
                            @Override
                            public void run()
                            {
                                if(isFirst)
                                {
                                    scrollView.fullScroll(View.FOCUS_DOWN);

                                    LoaderClass.HideProgressWheel(ArticleScreenActivity.this);

                                    isFirst = true;
                                }
                                //listis.setSelection(adapter.getCount()-1);
                            }
                        });
                    }
                });

            }
        }, 500);*/

        listis.postDelayed(new Runnable() {
            @Override
            public void run() {


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //tv_msg_noti.setText("" + commentList.size());

                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {

                                if (boolScroolToBottom == true) {
                                    scrollView.fullScroll(View.FOCUS_DOWN);
                                }

                                System.out.println("Input data is PING SCROLL TO BOTTOM " + boolScroolToBottom);
                            }
                        });
                    }
                });

            }
        }, 10);

    }

    private void hideView() {
        // 1. Remove the custom view
        FrameLayout decor = (FrameLayout) getWindow().getDecorView();
        decor.removeView(mCustomView);
        mCustomView = null;

        // 2. Restore the state to it's original form
        getWindow().getDecorView().setSystemUiVisibility(mOriginalSystemUiVisibility);
        setRequestedOrientation(mOriginalOrientation);

        // 3. Call the custom view callback
        mCustomViewCallback.onCustomViewHidden();
        mCustomViewCallback = null;
    }


    @Override
    public void onBackPressed() {
        if (hideView) {
            hideView();
            hideView = false;
        } else if (emoLay.getVisibility() == View.VISIBLE)
            emoLay.setVisibility(View.GONE);
        else
            finish();
    }

    private void initialise() {
        timer = new Timer();
        webview = (WebView) findViewById(R.id.rl_video);
        setUpWebViewDefaults(webview);
        webview.setWebChromeClient(new WebChromeClient() {


            @Override
            public Bitmap getDefaultVideoPoster() {
                if (ArticleScreenActivity.this == null) {
                    return null;
                }

                return BitmapFactory.decodeResource(getApplicationContext().getResources(),
                        R.drawable.app);
            }

            @Override
            public void onShowCustomView(final View view, final WebChromeClient.CustomViewCallback callback) {
                // if a view already exists then immediately terminate the new one
                if (mCustomView != null) {
                    onHideCustomView();
                    return;
                }
                hideView = true;
                // 1. Stash the current state
                mCustomView = view;
                mCustomView.setBackgroundColor(Color.parseColor("#000000"));
                mOriginalSystemUiVisibility = getWindow().getDecorView().getSystemUiVisibility();
                mOriginalOrientation = getRequestedOrientation();

                // 2. Stash the custom view callback
                mCustomViewCallback = callback;

                // 3. Add the custom view to the view hierarchy
                FrameLayout decor = (FrameLayout) getWindow().getDecorView();
                decor.addView(mCustomView, new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));


                // 4. Change the state of the window
                getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                                View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                                View.SYSTEM_UI_FLAG_FULLSCREEN |
                                View.SYSTEM_UI_FLAG_IMMERSIVE);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

                if (view instanceof FrameLayout) {
                    FrameLayout frame = (FrameLayout) view;
                    if (frame.getFocusedChild() instanceof VideoView) {
                        VideoView video = (VideoView) frame.getFocusedChild();
                        frame.removeView(video);
                        setContentView(video);
                        video.start();
                    }
                }
            }

            @Override
            public void onHideCustomView() {
                hideView();
                hideView = false;
            }

        });

        rl_background = (RelativeLayout) findViewById(R.id.rl_background);
        rl_emo_buttons=(RelativeLayout)findViewById(R.id.rl_emo_buttons);
        iv_keyboard=(ImageView)findViewById(R.id.iv_keyboard);
        connDec = new ConnectionDetector(ArticleScreenActivity.this);
        alert = new Alert_Dialog_Manager(context);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_full_img = (ImageView) findViewById(R.id.iv_full_img);
        iv_news = (ImageView) findViewById(R.id.iv_news);
        tv_heart = (TextView) findViewById(R.id.iv_heart);
        tv_no_comn = (TextView) findViewById(R.id.tv_no_comn);
        iv_heart_fill = (ImageView) findViewById(R.id.iv_heart_fill);
        iv_msg = (ImageView) findViewById(R.id.iv_msg);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_load_more = (TextView) findViewById(R.id.tv_load_more);
        tv_bydefine = (TextView) findViewById(R.id.tv_bydefine);
        tv_send = (TextView) findViewById(R.id.tv_send);
        tv_noti = (TextView) findViewById(R.id.tv_noti);
        tv_cal_date = (TextView) findViewById(R.id.tv_cal_date);
        tv_clock_time = (TextView) findViewById(R.id.tv_clock_time);
        tv_location = (TextView) findViewById(R.id.tv_location);
        et_leave_comment = (EditText) findViewById(R.id.et_leave_comment);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_description = (TextView) findViewById(R.id.tv_description);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_msg_noti = (TextView) findViewById(R.id.tv_msg_noti);
        iv_smiley1 = (ImageView) findViewById(R.id.iv_smiley1);
        iv_smiley2 = (ImageView) findViewById(R.id.iv_smiley2);
        iv_smiley3 = (ImageView) findViewById(R.id.iv_smiley3);
        iv_smiley4 = (ImageView) findViewById(R.id.iv_smiley4);
        iv_smiley5 = (ImageView) findViewById(R.id.iv_smiley5);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        rl_sel_smileys = (RelativeLayout) findViewById(R.id.rl_sel_smileys);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        ll_staff_info = (LinearLayout) findViewById(R.id.ll_staff_info);
        emoLay = (LinearLayout) findViewById(R.id.emoLay);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        editor = preferences.edit();
        rvImages = (RecyclerView) findViewById(R.id.rvImages);
        web_description = (WebView) findViewById(R.id.web_description);
        iv_full_popup_img = (ImageView) findViewById(R.id.iv_full_popup_img);
        iv_emojibtn = (ImageView) findViewById(R.id.iv_emojibtn);

        rlPhoto = (RelativeLayout) findViewById(R.id.rlPhoto);

        horizontalAdapter = new HorizontalAdapter(ArticleScreenActivity.this, imageList);
        rvImages.setAdapter(horizontalAdapter);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(ArticleScreenActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(horizontalLayoutManagaer);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    String new_feed = intent.getStringExtra("feed_count");
                    /*Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);

    }

    /**
     * Convenience method to set some generic defaults for a
     * given WebView
     *
     * @param webView
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setUpWebViewDefaults(WebView webView) {
        WebSettings settings = webView.getSettings();

        // Enable Javascript
        settings.setJavaScriptEnabled(true);

        // Use WideViewport and Zoom out if there is no viewport defined
        settings.setUseWideViewPort(false);
        settings.setLoadWithOverviewMode(true);

        // Enable pinch to zoom without the zoom buttons
        settings.setBuiltInZoomControls(false);

        // Allow use of Local Storage
        settings.setDomStorageEnabled(true);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            // Hide the zoom controls for HONEYCOMB+
            settings.setDisplayZoomControls(false);
        }

        // Enable remote debugging via chrome://inspect
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        webView.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.endsWith(".mp4") || url.endsWith("some other supported type")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i); //warning no error handling will cause force close if no media player on phone.
                    return true;
                } else return false;
            }
        });
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if (listItem != null) {
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + 100 + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void clickables() {

        iv_emojibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard.setVisibility(View.VISIBLE);
                iv_emojibtn.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);


            }
        });

        iv_keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard.setVisibility(View.GONE);
                iv_emojibtn.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_leave_comment);
            }
        });

        et_leave_comment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons.setVisibility(View.GONE);
                    iv_emojibtn.setVisibility(View.VISIBLE);
                    iv_keyboard.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_leave_comment.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_leave_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn.setVisibility(View.VISIBLE);
                iv_keyboard.setVisibility(View.GONE);
            }
        });

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArticleScreenActivity.this, MyFeedActivity.class);
                startActivity(intent);
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ArticleScreenActivity.this, MyFeedActivity.class);
                startActivity(intent);
            }
        });


        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_send.setEnabled(false);

                commentStr = CommonUtilities.encodeUTF8(et_leave_comment.getText().toString().trim());

                ArrayList<String> bannedList = new ArrayList<String>();

                bannedList = ((UTCAPP) getApplication()).bannedWordsList;

                if (!commentStr.equals("")) {
                    boolean banWord = false;

                    String[] commentedWordsArray;

                    commentedWordsArray = commentStr.split(" ");

                    for (int i = 0; i < commentedWordsArray.length; i++) {
                        String name = commentedWordsArray[i];

                        if (bannedList.contains(name.toLowerCase()) || bannedList.contains(name) || bannedList.contains(name.toUpperCase())) {
                            banWord = true;

                            commentStr = commentStr.replaceAll(name, "<font color='red'>" + name + "</font>");

                            et_leave_comment.setText(Html.fromHtml(commentStr));

                            et_leave_comment.setSelection(Integer.parseInt(String.valueOf(et_leave_comment.getText().length())));

                            /// break;

                        }
                    }

                    if (banWord == false) {
                        if (!connDec.isConnectingToInternet()) {
                            alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new PostCommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new PostCommentData().execute();
                            }
                        }

                        et_leave_comment.setText("");
                    } else {
                        customShareBannedPopup();
                    }

                } else {
                    Toast.makeText(activity, "Please write some comment to post.", Toast.LENGTH_SHORT).show();
                }
            }
        });



        tv_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                count = commentList.size();

                if (!connDec.isConnectingToInternet()) {

                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new CommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new CommentData().execute();
                    }
                }
            }
        });

    }


    private void customShareBannedPopup() {
        final Dialog dialog = new Dialog(ArticleScreenActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.update_popup);
        final TextView tv_title_survey, ok_btn, tv_title_participate, tv_title_banned;
        ok_btn = (TextView) dialog.findViewById(R.id.ok_btn);
        tv_title_survey = (TextView) dialog.findViewById(R.id.tv_title_survey);
        tv_title_participate = (TextView) dialog.findViewById(R.id.tv_title_participate);
        tv_title_banned = (TextView) dialog.findViewById(R.id.tv_title_banned);
        tv_title_participate.setVisibility(View.GONE);
        tv_title_banned.setVisibility(View.VISIBLE);
        tv_title_survey.setVisibility(View.GONE);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }


    private void customShareDetailsPopup1(String primary_image) {

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.image_popup);

        ImageView iv_full_popup_img = (ImageView) dialog.findViewById(R.id.iv_full_popup_img);

        Picasso.with(getApplicationContext()).load(primary_image).into(iv_full_popup_img);

        dialog.show();
    }

    private void hideAllSmileys() {
        iv_smiley1.setVisibility(View.GONE);

        iv_smiley2.setVisibility(View.GONE);

        iv_smiley3.setVisibility(View.GONE);

        iv_smiley4.setVisibility(View.GONE);

        iv_smiley5.setVisibility(View.GONE);
    }

    private void inflateSmileys() {
        hideAllSmileys();

        for (int x = 0; x < mainMapList.size(); x++) {

            final HashMap<String, Object> mapHash = mainMapList.get(x);

            String moods = mapHash.get("moods").toString();

            String count = mapHash.get("count").toString();

            if (moods.equalsIgnoreCase("happy")) {
                iv_smiley1.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley1.setImageResource(R.drawable.happy);
                } else {
                    iv_smiley1.setImageResource(R.drawable.happy);
                }
            } else if (moods.equalsIgnoreCase("indifferen")) {
                iv_smiley2.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley2.setImageResource(R.drawable.indifferent);
                } else {
                    iv_smiley2.setImageResource(R.drawable.indifferent);
                }
            } else if (moods.equalsIgnoreCase("amazing")) {
                iv_smiley3.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley3.setImageResource(R.drawable.amazing);
                } else {
                    iv_smiley3.setImageResource(R.drawable.amazing);
                }
            } else if (moods.equalsIgnoreCase("love")) {
                iv_smiley4.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley4.setImageResource(R.drawable.love);
                } else {
                    iv_smiley4.setImageResource(R.drawable.love);
                }
            } else if (moods.equalsIgnoreCase("sad")) {
                iv_smiley5.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley5.setImageResource(R.drawable.sad);
                } else {
                    iv_smiley5.setImageResource(R.drawable.sad);
                }
            }
        }
    }

    public void showSecondDialogSmiley(String your_like, final String val) {

        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.moods_activity_one);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        for (int x = 0; x < mainMapList.size(); x++) {

            final HashMap<String, Object> mapHash = mainMapList.get(x);

            String moods = mapHash.get("moods").toString();

            String count = mapHash.get("count").toString();

            tempPos = x;

            /*your_like = "";

            your_like = "" + mapHash.get("your_like");*/

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");

                    break;

                case "indifferen":

                    count_indiff.setText("(" + count + ")");

                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");

                    break;
                case "love":
                    count_love.setText("(" + count + ")");

                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");

                    break;

            }
        }
        if (!your_like.equals("")) {

            if (your_like.equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }
        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);

                if (moodStr == "happy") {
                    iv_smiley1.setVisibility(View.VISIBLE);
                }
                moodStr = "happy";

                callAPI(val);

                dialog.dismiss();


            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "indifferen") {
                    iv_smiley2.setVisibility(View.VISIBLE);
                }

                moodStr = "indifferen";

                callAPI(val);

                dialog.dismiss();


            }

        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "amazing") {
                    iv_smiley3.setVisibility(View.VISIBLE);
                }
                moodStr = "amazing";

                callAPI(val);

                dialog.dismiss();


            }

        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "love") {
                    iv_smiley4.setVisibility(View.VISIBLE);
                }
                moodStr = "love";

                callAPI(val);

                dialog.dismiss();


            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "sad") {
                    iv_smiley5.setVisibility(View.VISIBLE);
                }
                moodStr = "sad";

                callAPI(val);

                dialog.dismiss();


            }
        });

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    private void callAPI(String val) {
        if (val.equals("1")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new LikeServiceData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, post_id, "1", "" + tempPos);
            } else {
                new LikeServiceData1().execute(moodStr, post_id, "1", "" + tempPos);
            }
        } else if (val.equals("2")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, post_id, "1", "" + tempPos);
            } else {
                new ChangePostData().execute(moodStr, post_id, "1", "" + tempPos);
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ll_feed:

                Intent intent = new Intent(ArticleScreenActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:

                Intent intent1 = new Intent(ArticleScreenActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:

                Intent intent2 = new Intent(ArticleScreenActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:

                Intent intent3 = new Intent(ArticleScreenActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }

    }

    public class ChangePostData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "change_post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "post");

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    callMainApis();


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class ChangePostData1 extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "change_comment_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "comment");

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    /*commentList.get(position).put("mood_like",moodString);

                    adapter.notifyDataSetChanged();*/

                    scrolledPosition = scrollView.getScrollY();

                    callMainApis();


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class CommentData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_comment_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", post_id);

                jo.put("source", "post");

                jo.put("post_value", count);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            System.out.println("Result : " + result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    tv_no_comn.setVisibility(View.GONE);
                    listis.setVisibility(View.VISIBLE);

                    has_more = resultObj.getBoolean("has_more");

                    if (has_more)
                        tv_load_more.setVisibility(View.VISIBLE);

                    else
                        tv_load_more.setVisibility(View.GONE);


                    JSONArray jArray = resultObj.getJSONArray("comments");

                    System.out.println("FIRST" + jArray);

                    if (count == 0) {
                        commentList.clear();

                        commentList = new ArrayList<>();
                    }

                    checkIfDataPresent.clear();

                    for (int i = 0; i < jArray.length(); i++) {
                        HashMap<String, Object> hmap = new HashMap<>();

                        HashMap<String, String> hmap1;

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String message_id = user_dataObj.getString("message_id");
                        String ref_id = user_dataObj.getString("ref_id");
                        String content = user_dataObj.getString("content");
                        String source = user_dataObj.getString("source");
                        String uid = user_dataObj.getString("uid");
                        String message_status = user_dataObj.getString("message_status");
                        String user_type = user_dataObj.getString("user_type");
                        String message_create_date = user_dataObj.getString("message_create_date");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String gender = user_dataObj.getString("gender");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String you_flag = user_dataObj.getString("you_flag");
                        String you_like = user_dataObj.getString("you_like");
                        String comment_like = user_dataObj.getString("comment_like");
                        String post_image = user_dataObj.getString("post_image");

                        JSONObject you_moods = user_dataObj.optJSONObject("you_moods"); /// кореневий JSON Object з моїми смайликами до цього коментаря

                        String mood = null;

                        String moo = null;

                        if (you_moods != null) { /// якщо JSON Object моїх смайликів не null
                            mood = you_moods.getString("moods"); /// вид (-и) моїх смайликів до цбого коментаря

                            moo = you_moods.getString("moo"); /// кількість млїх смайликів до цього коментаря
                        }

                        try {
                            String moods = "";

                            String count = "";

                            JSONArray jArrayy = user_dataObj.getJSONArray("moods"); /// JSON Array зі смайликами від інших користувачів, які були поставлені до цього коментаря
                            // final int numberOfItemsInResp = jArrayy.length();

                            moodList = new ArrayList<>();

                            for (int j = 0; j < jArrayy.length(); j++) {
                                hmap1 = new HashMap<>();

                                JSONObject user_dataObj1 = jArrayy.getJSONObject(j);
                                moods = user_dataObj1.getString("moods"); /// конкретний вид смайлика від іншого користувача, який був поставлений до цього коментаря

                                count = user_dataObj1.getString("con"); /// кількість цих смайликів

                                hmap1.put("con", count);
                                hmap1.put("moods", moods);
                                moodList.add(hmap1);
                            }

                            JSONObject total_moods = user_dataObj.getJSONObject("total_moods");
                            String moo1 = total_moods.getString("moo");

                            hmap.put("message_id", message_id);
                            hmap.put("ref_id", ref_id);
                            hmap.put("content", content);
                            hmap.put("source", source);
                            hmap.put("uid", uid);
                            hmap.put("message_status", message_status);
                            hmap.put("user_type", user_type);
                            hmap.put("message_create_date", message_create_date);
                            hmap.put("first_name", first_name);
                            hmap.put("last_name", last_name);
                            hmap.put("gender", gender);
                            hmap.put("user_image", user_image);
                            hmap.put("user_thumbnail", user_thumbnail);
                            hmap.put("you_flag", you_flag);
                            hmap.put("you_like", you_like);
                            hmap.put("total_moods", moo1);
                            hmap.put("you_moods", you_moods);
                            hmap.put("comment_like", comment_like);

                            hmap.put("moods", moodList);
                            hmap.put("mood_like", mood);
                            hmap.put("moo", moo);
                            hmap.put("post_image", post_image);

                            preferences.edit().putString("message_id", message_id).commit();

                            if (!checkIfDataPresent.contains(message_id)) {
                                commentList.add(hmap);
                                checkIfDataPresent.add(message_id);
                            }
                            //updateSmiley1(mood, false);

                            /*String time_stamp = resultObj.getString("time_stamp");
                            String has_more = resultObj.getString("has_more");
                            preferences.edit().putString("time_stamp", time_stamp).commit();*/

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    Collections.reverse(commentList);
                    setAdapter();
                    System.out.println("COMLIST:jj" + commentList);
                } else if (errCode.equals("300")) {
                    tv_no_comn.setVisibility(View.VISIBLE);
                    listis.setVisibility(View.GONE);

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (scrolledPosition > 0) {
                scrollView.scrollTo(0, scrolledPosition);
            }
            LoaderClass.HideProgressWheel(ArticleScreenActivity.this);
        }
    }

    private class ArticleListingAdapter extends BaseAdapter {

        @Override
        public int getCount() {

            return commentList.size();

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {

            l_position = i;

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_artcle_comment, null);

            System.out.println("GET VIEW=" + i);

            final ViewHolder holder = new ViewHolder();
            holder.iv_iconheart = (ImageView) convertView.findViewById(R.id.iv_iconheart);
            holder.iv_list_img = (DiamondImageView) convertView.findViewById(R.id.iv_list_img);
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_message = (TextView) convertView.findViewById(R.id.tv_message);
            holder.tv_message.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_count_days = (TextView) convertView.findViewById(R.id.tv_count_days);
            holder.ll_heart = (LinearLayout) convertView.findViewById(R.id.ll_heart);
            ll_delete = (LinearLayout) convertView.findViewById(R.id.ll_delete);
            holder.ll_heart_smiley = (LinearLayout) convertView.findViewById(R.id.ll_heart_smiley);
            holder.iv_smiley1 = (ImageView) convertView.findViewById(R.id.iv_smiley1);
            holder.iv_heart = (ImageView) convertView.findViewById(R.id.iv_heart);
            holder.iv_smiley2 = (ImageView) convertView.findViewById(R.id.iv_smiley2);
            holder.iv_smiley3 = (ImageView) convertView.findViewById(R.id.iv_smiley3);
            holder.iv_smiley4 = (ImageView) convertView.findViewById(R.id.iv_smiley4);
            holder.iv_smiley5 = (ImageView) convertView.findViewById(R.id.iv_smiley5);
            holder.iv_image = (ImageView) convertView.findViewById(R.id.imgv_event_comment_image);
            holder.swipeLayout = (SwipeLayout) convertView.findViewById(R.id.swipe);
            tv_notif = (TextView) convertView.findViewById(R.id.tv_notif);
            holder.rl_sel_smileys = (RelativeLayout) convertView.findViewById(R.id.rl_sel_smileys);
            iv_iconheart = (ImageView) convertView.findViewById(R.id.iv_iconheart);
            happy_smile = (ImageView) convertView.findViewById(R.id.happy_smile);
            iv_heart_smile = (ImageView) convertView.findViewById(R.id.iv_heart_smile);
            holder.tv_total_rating = (TextView) convertView.findViewById(R.id.tv_total_rating);
            String image1 = (String) commentList.get(i).get("user_thumbnail");
            String genderr = (String) commentList.get(i).get("gender");

            System.out.println("GENDER" + genderr);

            holder.tv_total_rating.setText((String) commentList.get(i).get("comment_like"));

            if (commentList != null) {
                if (commentList.get(i).containsKey("total_moods")) {
                    if (!commentList.get(i).get("total_moods").equals("") || commentList.get(i).get("total_moods") != null) {
                        iv_heart_smile.setVisibility(View.GONE);
                        happy_smile.setVisibility(View.VISIBLE);
                        tv_notif.setText(commentList.get(i).get("total_moods").toString());
                    }
                }
                if (commentList.get(i).containsKey("user_type")) {
                    if (commentList.get(i).get("user_type").equals("admin")) {

                        holder.iv_list_img.setClickable(false);
                        holder.tv_message.setClickable(false);
                        holder.tv_items_grp.setClickable(false);
                        ll_delete.setVisibility(View.GONE);
                        holder.tv_items_grp.setText("Administrator");
                        holder.iv_list_img.setImageResource(R.drawable.app);
                        holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                        holder.iv_list_img.setBorderWidth(2);
                    } else if (commentList.get(i).get("user_type").equals("user")) {

                        holder.iv_list_img.setClickable(true);
                        holder.tv_message.setClickable(true);
                        holder.tv_items_grp.setClickable(true);
                        ll_delete.setVisibility(View.VISIBLE);
                        ll_delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String msgId = commentList.get(i).get("message_id").toString();

                                if (connDec.isConnectingToInternet()) {

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                                        new FlagData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msgId);
                                    else
                                        new FlagData().execute(msgId);

                                }
                            }
                        });
                        if (commentList.get(i).get("user_thumbnail").equals("")) {

                            holder.iv_list_img.setImageResource(R.drawable.app);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);


                        } else {
                            Picasso.with(getApplicationContext()).load(commentList.get(i).get("user_thumbnail").toString()).into(holder.iv_list_img);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);
                        }
                    }
                }

                if (commentList.get(i).containsKey("moods")) {
                    if (commentList.get(i).get("moods").toString().contains("happy")) {

                        holder.iv_smiley1.setVisibility(View.VISIBLE);

                    } else {
                        holder.iv_smiley1.setVisibility(View.GONE);
                    }
                    if (commentList.get(i).get("moods").toString().contains("indifferen")) {

                        holder.iv_smiley2.setVisibility(View.VISIBLE);

                    } else {
                        holder.iv_smiley2.setVisibility(View.GONE);
                    }
                    if (commentList.get(i).get("moods").toString().contains("amazing")) {

                        holder.iv_smiley3.setVisibility(View.VISIBLE);

                    } else {
                        holder.iv_smiley3.setVisibility(View.GONE);
                    }
                    if (commentList.get(i).get("moods").toString().contains("love")) {

                        holder.iv_smiley4.setVisibility(View.VISIBLE);

                    } else {
                        holder.iv_smiley4.setVisibility(View.GONE);
                    }
                    if (commentList.get(i).get("moods").toString().contains("sad")) {

                        holder.iv_smiley5.setVisibility(View.VISIBLE);
                    } else {
                        holder.iv_smiley5.setVisibility(View.GONE);
                    }
                }

                if (commentList.get(i).containsKey("mood_like")) {
                    Object mySmiley = commentList.get(i).get("mood_like");

                    if (mySmiley != null) {
                        if (mySmiley.equals("happy")) {
                            holder.iv_smiley1.setImageResource(R.drawable.happy);
                            holder.iv_smiley1.setVisibility(View.VISIBLE);
                        } else if (mySmiley.equals("indifferen")) {
                            holder.iv_smiley2.setImageResource(R.drawable.indifferent);
                            holder.iv_smiley2.setVisibility(View.VISIBLE);
                        } else if (mySmiley.equals("amazing")) {
                            holder.iv_smiley3.setImageResource(R.drawable.amazing);
                            holder.iv_smiley3.setVisibility(View.VISIBLE);
                        } else if (mySmiley.equals("love")) {
                            holder.iv_smiley4.setImageResource(R.drawable.love);
                            holder.iv_smiley4.setVisibility(View.VISIBLE);
                        } else if (mySmiley.equals("sad")) {
                            holder.iv_smiley5.setImageResource(R.drawable.sad);
                            holder.iv_smiley5.setVisibility(View.VISIBLE);
                        }
                    }
                }
                if (commentList.get(i).containsKey("moo")) {
                    Object mySmileysCount = commentList.get(i).get("moo");

                    if (mySmileysCount != null) {
                        if (mySmileysCount.equals("0")) { /// якщо я НЕ "смайликував" цей комент

                            holder.ll_heart_smiley.setVisibility(View.VISIBLE);
                            holder.ll_heart_smiley.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showDialogSmiley(i, holder.iv_heart);

                                }
                            });
                        } else { /// якщо я "смайликував" цей комент

                            holder.ll_heart_smiley.setVisibility(View.GONE);

                            holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDialogSmiley1(i);
                                }
                            });
                        }
                    }
                    else {
                        holder.ll_heart_smiley.setVisibility(View.VISIBLE);
                        holder.ll_heart_smiley.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                showDialogSmiley(i, holder.iv_heart);

                            }
                        });
                    }
                }
                if (commentList.get(i).containsKey("uid")) {
                    if (commentList.get(i).get("uid").equals(preferences.getString("uid", ""))) {

                        holder.swipeLayout.setRightSwipeEnabled(false);
                        holder.swipeLayout.setLeftSwipeEnabled(false);
                        if (commentList.get(i).containsKey("comment_like")) {
                            if (commentList.get(i).get("comment_like").equals("")) {
                                holder.ll_heart_smiley.setVisibility(View.GONE);
                            }
                        } else {
                            holder.ll_heart_smiley.setVisibility(View.VISIBLE);
                        }
                        holder.ll_heart_smiley.setClickable(false);
                        ll_delete.setVisibility(View.GONE);
                    }
                }


                if (commentList.get(i).containsKey("comment_like")) {

                    String commentLike = (String) commentList.get(i).get("comment_like");

                    if (commentLike == null) {
                        commentLike = "";
                    }
                    if (commentLike.equals("0")) {
                        if (!(commentList.get(i).get("uid")).equals(preferences.getString("uid", ""))) {
                            holder.ll_heart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    System.out.println("MSG ID=" + (String) commentList.get(i).get("message_id"));

                                    customShareDetailsPopup((String) commentList.get(i).get("message_id"), i, holder.tv_total_rating);

                                }
                            });
                        }

                    } else {

                        if (!((String) commentList.get(i).get("uid")).equals(preferences.getString("uid", ""))) {
                            holder.tv_total_rating.setText((String) commentList.get(i).get("comment_like"));

                            holder.iv_iconheart.setImageResource(R.drawable.sel_star);
                        } else {
                            holder.tv_total_rating.setText((String) commentList.get(i).get("comment_like"));

                            holder.iv_iconheart.setImageResource(R.drawable.unsel_star);
                        }

                        /*holder.tv_total_rating.setText((String) commentList.get(i).get("comment_like"));

                        holder.iv_iconheart.setImageResource(R.drawable.sel_star);*/

                    }
                }

            }
            holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(commentList.get(i).get("first_name")+"") + " " + CommonUtilities.decodeUTF8(commentList.get(i).get("last_name")+""));

            System.out.println("COMMENT DATA=" + commentList.get(i).get("first_name") + " " + commentList.get(i).get("last_name") + "");

            holder.tv_message.setText(CommonUtilities.decodeUTF8(""+commentList.get(i).get("content")));

            String commentImage = String.valueOf(commentList.get(i).get("post_image"));

            if (commentImage != null) {
                String commentImageFullURL = CommonUtilities.LOGIN_URL1 + commentImage;

                Picasso.with(ArticleScreenActivity.this).load(commentImageFullURL).into(holder.iv_image);
            }

            {

                if (commentList.get(i).get("user_thumbnail") == null) {
                    commentList.get(i).put("user_thumbnail", "");
                }

                if (commentList.get(i).get("user_thumbnail").equals("")) {

                    holder.iv_list_img.setImageResource(R.drawable.app);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);


                } else {
                    Picasso.with(getApplicationContext()).load(commentList.get(i).get("user_thumbnail").toString()).into(holder.iv_list_img);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);

                }
            }


            try {
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

                String FinalDate = (String) commentList.get(i).get("message_create_date");

                Date date1;
                Date date2;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                date1 = dates.parse(date);
                date2 = dates.parse(FinalDate);

                //Comparing dates
                /*long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);

                //Convert long to String
                String dayDifference = Long.toString(differenceDates);

                System.out.println("HERE " + dayDifference);
                holder.tv_count_days.setText(dayDifference + "d");*/

                long diff = date1.getTime() - date2.getTime();

                holder.tv_count_days.setText(CommonUtilities.getDaysMonths(diff));
                /*long seconds = diff / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;
                long months=days/30;

                if(months>0)
                {
                    holder.tv_count_days.setText(months + " mons");
                }
                if (days > 0) {
                    holder.tv_count_days.setText(days + " days");
                } else if (minutes >= 0 && minutes <= 60) {
                    holder.tv_count_days.setText(minutes + " mins");
                } else {
                    holder.tv_count_days.setText(hours + " hours");
                }*/

            } catch (Exception exception) {

                System.out.println("Work " + exception);
            }
            if (getIntent().hasExtra("is_comment")) {
                listis.getParent().requestChildFocus(listis, listis);
            }

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_list_img;
            ImageView iv_iconheart, iv_heart;
            ImageView iv_smiley1, iv_smiley2, iv_smiley3, iv_smiley4, iv_smiley5;
            ImageView iv_image;
            TextView tv_items_grp, tv_message, tv_count_days, tv_total_rating;
            LinearLayout ll_heart, ll_heart_smiley;
            RelativeLayout rl_sel_smileys;
            SwipeLayout swipeLayout = null;
        }
    }

    public void showDialogSmiley(final int position, final ImageView iv_heart) {
        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.moods_activity_one);
        dialog.getWindow().setDimAmount(0f);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ArrayList<HashMap<String, String>> lis = (ArrayList<HashMap<String, String>>) commentList.get(position).get("moods");

        for (int x = 0; x < lis.size(); x++) {

            String moods = lis.get(x).get("moods");

            String count = lis.get(x).get("con");

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");
                    break;

                case "indifferen":
                    count_indiff.setText("(" + count + ")");
                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");
                    break;
                case "love":
                    count_love.setText("(" + count + ")");
                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");
                    break;

            }

        }

        Object mySmiley = commentList.get(position).get("mood_like");

        if (mySmiley != null) {
            if (mySmiley.equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }

        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("happy")) {
                    happyCount = "" + commentList.get(position).get("happy");
                }
                moodStr = "happy";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                counting++;

                commentList.get(position).put("happy", "" + count);

                commentList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_happy.setImageResource(R.drawable.happy);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new LikeServiceData().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }

                dialog.dismiss();

            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("indifferen")) {
                    happyCount = "" + commentList.get(position).get("indifferen");
                }
                moodStr = "indifferen";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                counting++;

                commentList.get(position).put("indifferen", "" + count);

                commentList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.indifferent);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new LikeServiceData().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();
            }


        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("amazing")) {
                    happyCount = "" + commentList.get(position).get("amazing");
                }
                moodStr = "amazing";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                counting++;

                commentList.get(position).put("amazing", "" + count);

                commentList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_amazing.setImageResource(R.drawable.amazing);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new LikeServiceData().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();
            }


        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("love")) {
                    happyCount = "" + commentList.get(position).get("love");
                }
                moodStr = "love";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                counting++;

                commentList.get(position).put("love", "" + count);

                commentList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_love.setImageResource(R.drawable.love);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new LikeServiceData().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();

            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("sad")) {
                    happyCount = "" + commentList.get(position).get("sad");
                }
                moodStr = "sad";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                counting++;

                commentList.get(position).put("sad", "" + count);

                commentList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_sad.setImageResource(R.drawable.sad);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new LikeServiceData().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();

            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    public void showDialogSmiley1(final int position) {

        rl_background.setVisibility(View.VISIBLE);
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.moods_activity_one);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setDimAmount(0f);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ArrayList<HashMap<String, String>> lis = (ArrayList<HashMap<String, String>>) commentList.get(position).get("moods");

        for (int x = 0; x < lis.size(); x++) {

            String moods = lis.get(x).get("moods");

            String count = lis.get(x).get("con");

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");
                    break;

                case "indifferen":
                    count_indiff.setText("(" + count + ")");
                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");
                    break;
                case "love":
                    count_love.setText("(" + count + ")");
                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");


            }

        }


        if (commentList.get(position).get("mood_like").equals("happy")) {

            iv_happy.setVisibility(View.GONE);
            iv_happy_sel.setVisibility(View.VISIBLE);

        } else if (commentList.get(position).get("mood_like").equals("indifferen")) {

            iv_indiff.setVisibility(View.GONE);
            iv_indiff_sel.setVisibility(View.VISIBLE);

        } else if (commentList.get(position).get("mood_like").equals("amazing")) {

            iv_amazing.setVisibility(View.GONE);
            iv_amazing_sel.setVisibility(View.VISIBLE);

        } else if (commentList.get(position).get("mood_like").equals("love")) {

            iv_love.setVisibility(View.GONE);
            iv_love_sel.setVisibility(View.VISIBLE);

        } else if (commentList.get(position).get("mood_like").equals("sad")) {

            iv_sad.setVisibility(View.GONE);
            iv_sad_sel.setVisibility(View.VISIBLE);
        }

        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("happy")) {
                    happyCount = "" + commentList.get(position).get("happy");
                }
                moodStr = "happy";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                // counting++;

                commentList.get(position).put("happy", "" + count);

                // commentList.get(position).put("total_moods", "" + counting);

                // adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_happy.setImageResource(R.drawable.happy);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new ChangePostData1().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();

            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("indifferen")) {
                    happyCount = "" + commentList.get(position).get("indifferen");
                }
                moodStr = "indifferen";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                //  counting++;

                commentList.get(position).put("indifferen", "" + count);

                //  commentList.get(position).put("total_moods", "" + counting);

                //adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.indifferent);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new ChangePostData1().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();
            }


        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("amazing")) {
                    happyCount = "" + commentList.get(position).get("amazing");
                }
                moodStr = "amazing";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                // counting++;

                commentList.get(position).put("amazing", "" + count);

                // commentList.get(position).put("total_moods", "" + counting);

                // adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_amazing.setImageResource(R.drawable.amazing);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new ChangePostData1().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();
            }


        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("love")) {
                    happyCount = "" + commentList.get(position).get("love");
                }
                moodStr = "love";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                // counting++;

                commentList.get(position).put("love", "" + count);

                // commentList.get(position).put("total_moods", "" + counting);

                //  adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_love.setImageResource(R.drawable.love);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new ChangePostData1().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();

            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                commentList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (commentList.get(position).containsKey("sad")) {
                    happyCount = "" + commentList.get(position).get("sad");
                }
                moodStr = "sad";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(commentList.get(position).get("total_moods") + "");

                //  counting++;

                commentList.get(position).put("sad", "" + count);

                //  commentList.get(position).put("total_moods", "" + counting);

                //adapter.notifyDataSetChanged();

                tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_sad.setImageResource(R.drawable.sad);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                } else {
                    new ChangePostData1().execute(moodStr, commentList.get(position).get("message_id").toString(), "1", "" + position);
                }
                dialog.dismiss();

            }
        });

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    private void customShareDetailsPopup(final String msg_id, final int k, final TextView tvTotalGlobal1) {

        final Dialog dialog = new Dialog(activity);
        rl_background.setVisibility(View.VISIBLE);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.rating_star);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setDimAmount(0f);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);




        messageId = msg_id;

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {


            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating_selected = rating + "";
                tvTotalGlobal = tvTotalGlobal1;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new RatingData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msg_id);
                } else {
                    new RatingData().execute(msg_id);
                }

                dialog.dismiss();
                commentList.get(k).put("comment_like", "1");


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                rl_background.setVisibility(View.GONE);
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });
        dialog.show();
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
        private ArrayList<HashMap<String, String>> imageList;

        public HorizontalAdapter(ArticleScreenActivity articleScreenActivity, ArrayList<HashMap<String, String>> imageList) {

            this.imageList = imageList;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView ivImage;

            public MyViewHolder(View view) {
                super(view);
                ivImage = (ImageView) view.findViewById(R.id.ivImage);
            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.inflate_rv_one_click_prescription, parent, false);
            MyViewHolder holder = new MyViewHolder(itemView);
            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            System.out.println("IMAGELIST" + imageList);

            if (!imageList.get(position).get("additional_image").toString().equals("")) {
                Picasso.with(getApplicationContext()).load(imageList.get(position).get("additional_image").toString())
                        .into(holder.ivImage);
            } else {
                System.out.println("EMPTY LIST IMAGE additional " + imageList.get(position));
            }

            if (!VideoURL.equals("")) {
                holder.ivImage.setVisibility(View.VISIBLE);

                if (!imageList.get(position).get("additional_image").toString().equals("")) {
                    Picasso.with(getApplicationContext()).load(imageList.get(position).get("additional_image").toString())
                            .into(holder.ivImage);
                } else {
                    System.out.println("EMPTY LIST IMAGE additional " + imageList.get(position));
                }


            } else {
                if (!imageList.get(position).get("additional_image").toString().equals("")) {
                    Picasso.with(getApplicationContext()).load(imageList.get(position).get("additional_image").toString()).into(holder.ivImage);

                } else {
                    System.out.println("EMPTY LIST IMAGE VIDEO EMPTY " + imageList.get(position));
                }
            }


            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (position == 0) {
//                        customShareDetailsPopup2(primary_image);
//                    }
//                    else{

                    rlPhoto.setVisibility(View.VISIBLE);

                    Picasso.with(ArticleScreenActivity.this).load(imageList.get(position).get("additional_image")).into(iv_full_popup_img);

                    //customShareDetailsPopup2(imageList.get(position).get("additional_image"));
                    // }
                }
            });
        }

        @Override
        public int getItemCount() {

            return imageList.size();

        }
    }

    private void customShareDetailsPopup2(String additional_image) {

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.image_popup);

        ImageView iv_full_popup_img = (ImageView) dialog.findViewById(R.id.iv_full_popup_img);

        Picasso.with(getApplicationContext()).load(additional_image).into(iv_full_popup_img);

        dialog.show();
    }
// For Listing of Articles

    public class ArticleScreenData extends AsyncTask<String, String, String> {


        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //      LoaderClass.ShowProgressWheel(ArticleScreenActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "feed_detail");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("post_id", post_id);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new CommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new CommentData().execute();
            }

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONObject user_dataObj = resultObj.getJSONObject("post_detail");
                    final String post_id = user_dataObj.getString("post_id");
                    String type = user_dataObj.getString("type");
                    String title = user_dataObj.getString("title");
                    String detail = user_dataObj.getString("detail");
                    VideoURL = user_dataObj.getString("video_link");
                    String post_create_date = user_dataObj.getString("post_create_date");
                    String date_of_start = user_dataObj.getString("date_of_start");
                    String date_of_end = user_dataObj.getString("date_of_end");
                    String start_timing = user_dataObj.getString("start_timing");
                    String end_timing = user_dataObj.getString("end_timing");
                    String venue = user_dataObj.getString("venue");
                    String latitude = user_dataObj.getString("latitude");
                    String longitude = user_dataObj.getString("longitude");
                    String location_address = user_dataObj.getString("location_address");
                    String post_status = user_dataObj.getString("post_status");
                    String admin_id = user_dataObj.getString("admin_id");
                    String visibility = user_dataObj.getString("visibility");
                    String publish_date = user_dataObj.getString("publish_date");
                    String archive = user_dataObj.getString("archive");
                    String image_big = user_dataObj.getString("image_big");
                    String facebook = user_dataObj.getString("facebook");
                    String facebook_id = user_dataObj.getString("facebook_id");
                    String facebook_url = user_dataObj.getString("facebook_url");
                    String post_hide = user_dataObj.getString("post_hide");
                    String total_likes = user_dataObj.getString("total_likes");
                    String total_comments = user_dataObj.getString("total_comments");
                    final String you_like = user_dataObj.getString("you_like");
                    your_like = user_dataObj.getString("your_like");
                    primary_image = user_dataObj.getString("primary_image");
                    preferences.edit().putString("type", type);
                    preferences.edit().putString("video_link", VideoURL);
                    tv_noti.setText(total_likes);
                    tv_msg_noti.setText(total_comments);
                    if (VideoURL != null && !VideoURL.equals("")) {
                        webview.setVisibility(View.VISIBLE);
                        iv_full_img.setVisibility(View.GONE);
                        String[] separated = VideoURL.split("src='");
                        String[] c = separated[1].split("'");
                        String Link = c[0];
                        WebSettings webSettings = webview.getSettings();

                        webSettings.setJavaScriptEnabled(true);
                        webview.getSettings().setLoadsImagesAutomatically(true);
                        webview.loadUrl(Link);
                        System.out.println("hfggg " + " " + Link);
                    } else {
                        webview.setVisibility(View.GONE);
                        iv_full_img.setVisibility(View.VISIBLE);
                    }

                    updateHeartVisibility(you_like);


                    if (type.equals("artical")) {

                        ll_staff_info.setVisibility(View.GONE);
                        tv_date.setVisibility(View.VISIBLE);
                        tv_title.setText(title);
                        WebSettings webSettings = web_description.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        web_description.getSettings().setLoadsImagesAutomatically(true);
                        webSettings.setPluginState(WebSettings.PluginState.ON);

                        boolean getHtml = DetectHtml.isHtml(detail);
                        if (getHtml == true) {
                            detail = detail.replace("span", "font");
                            web_description.setVisibility(View.VISIBLE);
                            tv_description.setVisibility(View.GONE);
                            String htmlBody = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + detail;
                            web_description.loadDataWithBaseURL("file:///android_asset/", htmlBody, "text/html", "utf-8", null);

                        } else {
                            tv_description.setVisibility(View.VISIBLE);
                            detail = detail.replace("\n", "<br>");
                            web_description.setVisibility(View.GONE);
                            tv_description.setText(Html.fromHtml(detail));
                        }

                        tv_date.setText(ChangeDateFormat1(publish_date, "yyyy-MM-dd", "MMMM dd,yyyy"));
                        tv_bydefine.setVisibility(View.VISIBLE);
                    } else if (type.equals("event")) {

                        ll_staff_info.setVisibility(View.VISIBLE);
                        tv_title.setText(title);
                        WebSettings webSettings = web_description.getSettings();
                        webSettings.setJavaScriptEnabled(true);
                        web_description.getSettings().setLoadsImagesAutomatically(true);
                        webSettings.setPluginState(WebSettings.PluginState.ON);
                        boolean getHtml = DetectHtml.isHtml(detail);
                        if (getHtml == true) {
                            detail = detail.replace("span", "font");
                            web_description.setVisibility(View.VISIBLE);
                            tv_description.setVisibility(View.GONE);
                            //web_description.loadData(detail,"text/html; charset=UTF-8", null);

                            String htmlBody = "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\" />" + detail;
                            web_description.loadDataWithBaseURL("file:///android_asset/", htmlBody, "text/html", "utf-8", null);

                        } else {
                            tv_description.setVisibility(View.VISIBLE);
                            detail = detail.replace("\n", "<br>");
                            web_description.setVisibility(View.GONE);
                            tv_description.setText(Html.fromHtml(detail));
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat("h:mm");

                        Date testDate = null;

                        try {
                            testDate = sdf.parse(start_timing);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        SimpleDateFormat formatter = new SimpleDateFormat("h:mm a");

                        String newFormat = formatter.format(testDate);

                        SimpleDateFormat sdf1 = new SimpleDateFormat("h:mm");

                        Date testDate1 = null;

                        try {
                            testDate1 = sdf1.parse(end_timing);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        SimpleDateFormat formatter1 = new SimpleDateFormat("h:mm a");

                        String newFormat1 = formatter.format(testDate1);
                        tv_cal_date.setText(ChangeDateFormat1(date_of_start, "yyyy-MM-dd", "MMMM dd,yyyy") + " / " + newFormat);
                        tv_clock_time.setText(ChangeDateFormat1(date_of_end, "yyyy-MM-dd", "MMMM dd,yyyy") + " / " + newFormat1);
                        tv_location.setText(location_address);
                        tv_date.setVisibility(View.GONE);
                        tv_bydefine.setVisibility(View.GONE);
                    }


                    try {

                        jArrayy = new JSONArray();

                        jArrayy = user_dataObj.getJSONArray("moods");

                        updateSmiley(your_like, false);
                        //setAdapter();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!primary_image.isEmpty()) {
                        Glide.with(getApplicationContext())
                                .load(primary_image)
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .into(new GlideDrawableImageViewTarget(iv_full_img) {
                                    @Override
                                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                                        super.onResourceReady(resource, animation);
                                        resource.start();
                                    }

                                    @Override
                                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                        super.onLoadFailed(e, errorDrawable);
                                    }
                                });
                    }
                    iv_full_img.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            // customShareDetailsPopup1(primary_image);

                            rlPhoto.setVisibility(View.VISIBLE);

                            Glide.with(getApplicationContext())
                                    .load(primary_image)
                                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                    .into(new GlideDrawableImageViewTarget(iv_full_popup_img) {
                                        @Override
                                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                                            super.onResourceReady(resource, animation);
                                            resource.start();
                                        }

                                        @Override
                                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                            super.onLoadFailed(e, errorDrawable);
                                        }
                                    });

                            try {
                                final GlideDrawable gifDrawable = (GlideDrawable) iv_full_img.getDrawable();
                                gifDrawable.stop();
                            } catch (Exception e) {

                            }

                        }
                    });
                    tv_date.setText(ChangeDateFormat1(publish_date, "yyyy-MM-dd", "MMMM dd,yyyy"));
                    tv_title.setText(title);
                    WebSettings webSettings = webview.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    webview.getSettings().setLoadsImagesAutomatically(true);
                    webSettings.setPluginState(WebSettings.PluginState.ON);

                    detail = detail.replace("span", "font");
                    //tv_description.setVisibility(View.VISIBLE);
                    //tv_description.setText(Html.fromHtml(detail));

                    try {

                        JSONArray jArray = user_dataObj.getJSONArray("additional_image");
                        if (jArray != null) {

                            for (int i = 0; i < jArray.length(); i++) {
                                String additional_image = jArray.getString(i);

                                HashMap<String, String> hmap = new HashMap<>();
                                //  JSONObject user_dataObj1 = jArray.getJSONObject(i);
                                hmap.put("additional_image", additional_image);
                                imageList.add(hmap);
                            }
                            if (!VideoURL.equals("")) {
                                /*if (!primary_image.equals("")) {
                                    HashMap<String, String> hmap = new HashMap<>();
                                    //  JSONObject user_dataObj1 = jArray.getJSONObject(i);
                                    hmap.put("additional_image", primary_image);
                                    imageList.add(0, hmap);
                                    System.out.println("LISTT " + imageList);
                                }*/
                            }
                            horizontalAdapter = new HorizontalAdapter(ArticleScreenActivity.this, imageList);
                            rvImages.setAdapter(horizontalAdapter);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();


                    }


                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    private void updateHeartVisibility(final String you_like) {
        if (you_like != null) {
            if (you_like.equals("1")) {
                iv_heart_fill.setVisibility(View.GONE);
                tv_heart.setVisibility(View.GONE);
                rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSecondDialogSmiley(your_like, "2");
                    }
                });

            } else if (you_like.equals("0")) {
                iv_heart_fill.setVisibility(View.GONE);
                tv_heart.setVisibility(View.VISIBLE);
                tv_heart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSecondDialogSmiley(you_like, "1");
                    }
                });
            }

        }
    }

    private void updateSmiley(String your_like, boolean update) {

        if (jArrayy != null) {
            updateSmileyArray(your_like);

            updateOtherSmileys();

            inflateSmileys();
        }

        if (update == true && jArrayy.length() > 0) {
            int count = Integer.valueOf(tv_noti.getText().toString().trim());
            count++;
            tv_noti.setText(count + "");
        }
    }

    private void updateSmileyArray(String your_like1) {
        boolean likecontains = false;

        try {
            for (int k = 0; k < jArrayy.length(); k++) {
                try {
                    JSONObject curntObj = jArrayy.getJSONObject(k);

                    if (curntObj.get("moods").equals(this.your_like)) {
                        curntObj.remove("isYour");

                        if (curntObj.has("isNew")) {
                            ArrayList<String> list = new ArrayList<String>();
                            int len = jArrayy.length();
                            if (jArrayy != null) {
                                for (int i = 0; i < len; i++) {
                                    list.add(jArrayy.get(i).toString());
                                }
                            }
                            //Remove the element from arraylist
                            list.remove(k);

                            jArrayy = new JSONArray(list);

                            //jArrayy.remove(k);

                            curntObj = null;
                        }
                    }

                    if (curntObj != null) {
                        if (curntObj.get("moods").equals(your_like1)) {
                            curntObj.put("isYour", "true");

                            likecontains = true;
                        }

                        jArrayy.put(k, curntObj);
                    }

                    int countArray = jArrayy.length();

                    if (countArray > 0) {
                        countArray = countArray - 1;
                    }
                    if (k == countArray) {
                        if (!likecontains) {
                            JSONObject newObj1 = new JSONObject();

                            newObj1.put("isYour", "true");

                            newObj1.put("isNew", "true");

                            newObj1.put("moods", your_like1);

                            newObj1.put("modcount", 1);

                            jArrayy.put(newObj1);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            this.your_like = your_like1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateOtherSmileys() {
        mainMapList.clear();

        final int numberOfItemsInResp = jArrayy.length();

        for (int j = 0; j < numberOfItemsInResp; j++) {
            try {
                mainMap = new HashMap<>();

                JSONObject user_dataObj1 = null;

                if (jArrayy.get(j) instanceof String) {
                    user_dataObj1 = new JSONObject(jArrayy.getString(j));
                } else
                    user_dataObj1 = jArrayy.getJSONObject(j);

                String moods = user_dataObj1.getString("moods");

                String count = user_dataObj1.getString("modcount");

                HashMap<String, Object> hmap = new HashMap<>();

                hmap.put(moods, count);

                mainMap.put("moods", moods);

                mainMap.put("count", count);

                if (your_like.equalsIgnoreCase(moods)) {
                    mainMap.put("isYour", "true");
                    user_dataObj1.put("isYour", "true");

                    if (count.equals("1")) {
                        mainMap.put("isNew", "true");

                        user_dataObj1.put("isNew", "true");
                    }

                    jArrayy.put(j, user_dataObj1);
                }


                mainMapList.add(mainMap);

                commentLists.add(hmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private String ChangeDateFormat1(String dateString, String fromFormat,
                                     String toFormat) {
        try {
            Date paramString1 = new SimpleDateFormat(fromFormat)
                    .parse(dateString);
            String formatedDate = new SimpleDateFormat(toFormat)
                    .format(paramString1);
            System.out.println("formattedDate : " + paramString1);
            return formatedDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public class PostCommentData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(ArticleScreenActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_comments");

                jo.put("content", commentStr);

                if (encodedImage != null) ///
                    jo.put("image", encodedImage);

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", post_id);

                jo.put("source", "post");

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {

                LoaderClass.HideProgressWheel(ArticleScreenActivity.this);

                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    boolScroolToBottom = true;

                    String time_stamp = resultObj.getString("time_stamp");

                    preferences.edit().putString("time_stamp", time_stamp).commit();

                    JSONObject user_dataObj = resultObj.getJSONObject("comment");

                    String message_id = user_dataObj.getString("message_id");
                    String ref_id = user_dataObj.getString("ref_id");
                    String content = user_dataObj.getString("content");
                    String source = user_dataObj.getString("source");
                    String uid = user_dataObj.getString("uid");
                    String message_status = user_dataObj.getString("message_status");
                    String message_create_date = user_dataObj.getString("message_create_date");
                    String first_name = user_dataObj.getString("first_name");
                    String last_name = user_dataObj.getString("last_name");
                    String user_image = user_dataObj.getString("user_image");
                    String user_thumbnail = user_dataObj.getString("user_thumbnail");
                    String post_image = user_dataObj.getString("post_image");

                    HashMap<String, Object> hm2 = new HashMap<>();
                    hm2.put("content", content);
                    hm2.put("message_id", message_id);
                    hm2.put("first_name", first_name);
                    hm2.put("last_name", last_name);
                    hm2.put("user_image", user_image);
                    hm2.put("uid", uid);
                    hm2.put("message_create_date", message_create_date);
                    hm2.put("user_thumbnail", user_thumbnail);
                    hm2.put("post_image", post_image);

                    commentList.add(commentList.size(), hm2);

                    setAdapter();

                    if (encodedImage != null)
                        removeAttachedPicture(null);

                    int total_commmments = Integer.parseInt(tv_msg_noti.getText().toString().trim()) + 1;

                    tv_msg_noti.setText("" + total_commmments);

                    if (!connDec.isConnectingToInternet()) {
                        //alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new PushNotificationData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new PushNotificationData().execute();
                        }
                    }

                } else if (errCode.equals("700")) {
                    boolScroolToBottom = false;
                } else {
                    boolScroolToBottom = false;
                }

                tv_send.setEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class PushNotificationData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "push_notification");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", post_id);

                jo.put("source", "post");


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class RatingData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_comment_rating");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", messageId);

                jo.put("group_id", post_id);

                jo.put("type", "post");

                jo.put("like_star", rating_selected);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    //tvTotalGlobal.setText(rating_selected);
                    callMainApis();
                }

                //  setAdapter();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class FlagData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(ArticleScreenActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "comment_flag");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[0]);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ArticleScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Flag added successfully.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    setAdapter();

                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ArticleScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(ArticleScreenActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            LoaderClass.HideProgressWheel(ArticleScreenActivity.this);
        }
    }

    public class LikeServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "", moodString = "";
        int position = 0;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "comment_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "comment");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    scrolledPosition = scrollView.getScrollY();

                    callMainApis();

                    /*commentList.get(2).put("mood_like",moodString);

                    adapter.notifyDataSetChanged();*/

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class PingServiceData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "ping_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", post_id);

                jo.put("source", "post");

                jo.put("time_stamp", preferences.getString("time_stamp", ""));

                Log.e("JSON DATA ARTICLE= ", jo.toString());

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                String time_stamp = resultObj.getString("time_stamp");

                preferences.edit().putString("time_stamp", time_stamp).commit();

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("message");

                    pingList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, Object> hmap = new HashMap<>();
                        HashMap<String, String> hmap1;
                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String message_id = user_dataObj.getString("message_id");
                        String ref_id = user_dataObj.getString("ref_id");
                        String content = user_dataObj.getString("content");
                        String source = user_dataObj.getString("source");
                        String uid = user_dataObj.getString("uid");
                        String message_status = user_dataObj.getString("message_status");
                        String user_type = user_dataObj.getString("user_type");
                        String message_create_date = user_dataObj.getString("message_create_date");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String gender = user_dataObj.getString("gender");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String you_flag = user_dataObj.getString("you_flag");
                        String you_like = user_dataObj.getString("you_like");
                        String comment_like = user_dataObj.getString("comment_like");

                        try {

                            String moods = "";

                            String count = "";

                            JSONArray jArrayy = user_dataObj.getJSONArray("moods");
                            moodList1 = new ArrayList<>();
                            for (int j = 0; j < jArrayy.length(); j++) {
                                hmap1 = new HashMap<>();

                                JSONObject user_dataObj1 = jArrayy.getJSONObject(j);
                                moods = user_dataObj1.getString("moods");

                                count = user_dataObj1.getString("con");

                                hmap1.put("con", count);
                                hmap1.put("moods", moods);
                                moodList1.add(hmap1);
                            }

                            JSONObject total_moods = user_dataObj.getJSONObject("total_moods");
                            String moo1 = total_moods.getString("moo");
                            JSONObject you_moods = user_dataObj.getJSONObject("you_moods");

                            String moo = you_moods.getString("moo");
                            String mood = you_moods.getString("moods");
                            hmap.put("message_id", message_id);
                            hmap.put("ref_id", ref_id);
                            hmap.put("content", content);
                            hmap.put("source", source);
                            hmap.put("uid", uid);
                            hmap.put("message_status", message_status);
                            hmap.put("user_type", user_type);
                            hmap.put("message_create_date", message_create_date);
                            hmap.put("first_name", first_name);
                            hmap.put("last_name", last_name);
                            hmap.put("user_image", user_image);
                            hmap.put("gender", gender);
                            hmap.put("user_thumbnail", user_thumbnail);
                            hmap.put("you_flag", you_flag);
                            hmap.put("you_like", you_like);
                            hmap.put("moo", moo1);
                            hmap.put("you_moods", you_moods);
                            hmap.put("comment_like", comment_like);
                            hmap.put("total_moods", moo1);
                            preferences.edit().putString("message_id", message_id).commit();
                            hmap.put("moods", moodList1);
                            hmap.put("mood_like", mood);
                            hmap.put("moo", moo);

                            if (!checkIfDataPresent.contains(message_id)) {
                                commentList.add(hmap);
                                checkIfDataPresent.add(message_id);
                                if (!isFirst)
                                    boolScroolToBottom = true;
                            }

                            // String has_more = resultObj.getString("has_more");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (isFirst)
                        boolScroolToBottom = true;
                    isFirst = false;
                    setAdapter();

                } else if (errCode.equals("300")) {
                    boolScroolToBottom = false;
                    AlertDialog.Builder alert = new AlertDialog.Builder(ArticleScreenActivity.this);

                    alert.setCancelable(false);

                    alert.setTitle("");

                    alert.setMessage("Try Again");

                    alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();

                            startActivity(new Intent(ArticleScreenActivity.this, ArticleScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                            finish();
                        }
                    });

                    //alert.show();
                } else if (errCode.equals("700")) {
                    boolScroolToBottom = false;
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class LikeServiceData1 extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "post");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {
                    callMainApis();

                    /*if(tv_heart.getVisibility()==View.VISIBLE)
                    {
                        updateHeartVisibility("1");

                        try {
                            JSONObject user_dataObj1 = new JSONObject();

                            user_dataObj1.put("moods", moodString);

                            user_dataObj1.put("modcount", "1");

                            user_dataObj1.put("isYour", "true");

                            user_dataObj1.put("isNew", "true");

                            int pos=0;

                            if(jArrayy.length()>0)
                            {
                               pos = jArrayy.length();
                            }

                            jArrayy.put(pos, user_dataObj1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    updateSmiley(moodString, false);*/


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {

        super.onStart();

        getAnalyticTracker().setScreenName("Article Detail Screen");

        FlurryAgent.onStartSession(ArticleScreenActivity.this);
    }

    public void showCustomMessagephoto(View v) {
        final Dialog dialogphoto = new Dialog(ArticleScreenActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();

                        if (apiLevel == 0)
                            apiLevel = Build.VERSION.SDK_INT;

                        if (apiLevel >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(ArticleScreenActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(ArticleScreenActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequestForGallery();
                            } else {
                                OpenGallery();
                            }
                        } else {
                            OpenGallery();
                        }
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();

                        if (apiLevel == 0)
                            apiLevel = Build.VERSION.SDK_INT;

                        if (apiLevel >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(ArticleScreenActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(ArticleScreenActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequestForCamera();
                            } else {
                                takePicture();
                            }
                        } else {
                            takePicture();
                        }

                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();
    }

    private void makeRequestForGallery() {
        ActivityCompat
                .requestPermissions
                        (ArticleScreenActivity.this,
                                new String[]{Constants.ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE, Constants.ANDROID_PERMISSION_READ_EXTERNAL_STORAGE},
                                Constants.REQUEST_CODE_GALLERY);
    }

    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.REQUEST_CODE_GALLERY);
    }

    private void makeRequestForCamera() {
        ActivityCompat
                .requestPermissions
                        (ArticleScreenActivity.this,
                                new String[]{android.Manifest.permission.CAMERA, Constants.ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE},
                                Constants.REQUEST_CODE_TAKE_PICTURE);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            checkExternalStorageAndCreateTempFile();

            Uri mImageCaptureUri;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(tempImageFile);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, Constants.REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkExternalStorageAndCreateTempFile() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            tempImageFile = new File(Environment.getExternalStorageDirectory(),
                    Constants.TEMP_PHOTO_FILE_NAME);
        } else {
            tempImageFile = new File(getFilesDir(), Constants.TEMP_PHOTO_FILE_NAME);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Bitmap bitmap;

            switch (requestCode) {
                case Constants.REQUEST_CODE_GALLERY:
                    try {
                        checkExternalStorageAndCreateTempFile();

                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        FileOutputStream fileOutputStream = new FileOutputStream(tempImageFile);

                        copyStream(inputStream, fileOutputStream);

                        fileOutputStream.close();

                        inputStream.close();

                        startCropImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case Constants.REQUEST_CODE_TAKE_PICTURE:
                    startCropImage();
                    break;
                case Constants.REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(tempImageFile.getPath());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                    ImageView attached = (ImageView) findViewById(R.id.iv_attached_image);
                    attached.setImageBitmap(bitmap);

                    findViewById(R.id.rellay_attachment).setVisibility(View.VISIBLE);

                    if (tempImageFile.exists()) {
                        tempImageFile.delete();
                    }

                    base64frombitmap(bitmap);

                    /*iv_signup_img.setImageBitmap(bitmap);
                    iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
                    iv_signup_img.setBorderWidth(2);*/

            }
        } else {
            if (requestCode == Constants.REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                startCropImage();
            }
        }
    }

    private void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encodedImage;
    }

    private void startCropImage() {

        //System.out.println("cropimage");

        Intent intent = new Intent(ArticleScreenActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, tempImageFile.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, Constants.REQUEST_CODE_CROP_IMAGE);
    }

    public void removeAttachedPicture(View v) {
        findViewById(R.id.rellay_attachment).setVisibility(View.GONE);

        ImageView thumbmailImageView = (ImageView) findViewById(R.id.iv_attached_image);

        thumbmailImageView.setImageDrawable(null);

        encodedImage = null;
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(ArticleScreenActivity.this);
        timer.cancel();
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

}
