package com.spott.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;

import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;
import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;
import com.spott.app.common.SwipeLayout;
import com.spott.app.cropiimage.CropImage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Acer on 9/19/2016.
 */
public class CrewActivity extends FragmentActivity implements View.OnClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    ArrayList<HashMap<String, Object>> groupList;
    Context context;
    ListView group_list;
    ImageView iv_smiley1, iv_smiley2, iv_smiley3, iv_smiley4, iv_smiley5, iv_emojibtn, iv_attach; ///
    ImageView iv_back_icon, tv_heart, iv_heart_fill, iv_msg, iv_red_icon, iv_keyboard;
    DiamondImageView iv_groupImage;
    TextView tv_load_more, tv_title_group, tv_mygroup, tv_subheading, tv_count, tv_msg_count, tv_send, tv_num, tv_notif, tvTotalGlobal, tv_no_comment;
    EditText et_leave_comment;
    Activity activity;
    LinearLayout ll_feed, ll_profile, ll_directory, emoLay;
    RelativeLayout ll_groups, rl_send, rl_background, rl_emo_buttons;
    HorizontalAdapter horizontalAdapter;
    RecyclerView rvImages;
    Timer timer;
    private BroadcastReceiver broadcastReceiver;
    Intent intent;
    private boolean has_more;
    String isUnreadNotification = "", group_id = "", groupname = "", member_status = "", mute_group = "", grpdetail = "", grpimg = "", grptype = "", grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", is_fav = "", owner_type = "", member_id = "", you_grp = "";
    SharedPreferences preferences;
    ArrayList<HashMap<String, String>> viewlist;
    ImageView iv_menu_icon, iv_menu_icon1;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_number;
    static ArrayList<HashMap<String, String>> joingroupList;
    ArrayList<HashMap<String, String>> memberlist, moodList1;
    String post_id = "", you_like = "";
    GroupAdapter adapter;
    String commentStr = "";
    int count = 0;
    String moodStr = "", uid = "", value = "";
    String rating_selected = "";
    String msg = "";
    ArrayList<HashMap<String, Object>> pingList;

    ArrayList<HashMap<String, Object>> mainMapList = new ArrayList<>();
    HashMap<String, Object> mainMap;
    RelativeLayout rl_sel_smileys;
    int tempPos = 0;
    private String your_like = "";
    private JSONArray jArrayy;
    private int scrolledPosition = 0;
    ScrollView scrollView;
    boolean isFirst = true;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    private int apiLevel = Build.VERSION.SDK_INT; ///
    private File tempImageFile; ///
    private String encodedImage; ///

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_crew);

        ///
        if (savedInstanceState != null) {
            File savedTempImageFile = (File) savedInstanceState.getSerializable("tempImageFile");

            if (savedTempImageFile != null) {
                tempImageFile = savedTempImageFile;
            }
        }

        this.activity = (Activity) CrewActivity.this;
        context = CrewActivity.this;

        initialise();


        horizontalAdapter = new HorizontalAdapter(CrewActivity.this, memberlist);
        rvImages.setAdapter(horizontalAdapter);
        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        member_id = intent.getStringExtra("member_id");
        owner_type = intent.getStringExtra("owner_type");
        you_grp = intent.getStringExtra("you_group");
        uid = intent.getStringExtra("imp_id");
        value = intent.getStringExtra("key");
        if (intent.hasExtra("post_id")) {
            post_id = intent.getStringExtra("post_id");
            isFirst = false;
        }
        if (intent.hasExtra("isUnreadNotification")) {
            isUnreadNotification = intent.getStringExtra("isUnreadNotification");
        }

        you_like = preferences.getString("YOU", "");

        if (intent.hasExtra("options")) // join Group Activity intent
        {
            group_list.setVisibility(View.GONE);
            rl_send.setVisibility(View.GONE);
            rvImages.setVisibility(View.GONE);
            iv_menu_icon.setVisibility(View.GONE);
            iv_menu_icon1.setVisibility(View.VISIBLE);
        } else if (intent.hasExtra("options_next"))//My Group Activity intents
        {
            group_list.setVisibility(View.GONE);
            rl_send.setVisibility(View.GONE);
            iv_menu_icon.setVisibility(View.GONE);
            rvImages.setVisibility(View.GONE);
            iv_menu_icon1.setVisibility(View.VISIBLE);

            iv_menu_icon.setEnabled(false);
            iv_menu_icon1.setEnabled(true);

        } else// else case handle everything
        {
            group_list.setVisibility(View.VISIBLE);
            rl_send.setVisibility(View.VISIBLE);
            iv_menu_icon.setVisibility(View.VISIBLE);
            iv_menu_icon1.setVisibility(View.GONE);

        }

        clickables();

        setting();

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);

       /* if (connDec.isConnectingToInternet()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

            else
                new MessageData().execute();

        }*/

        timer.scheduleAtFixedRate(new RemindTask(), 0, 7000); // delay

        if (getIntent().hasExtra("scrollTobottom")) {
            if (getIntent().getStringExtra("scrollTobottom").equals("TRUE")) {
                boolScroolToBottom = true;
            } else {
                boolScroolToBottom = false;
            }
        } else {
            boolScroolToBottom = false;
        }

    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(et_leave_comment, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(et_leave_comment);
    }

    @Override
    public void onBackPressed() {
        if (emoLay.getVisibility() == View.VISIBLE)
            emoLay.setVisibility(View.GONE);
        else
            finish();
    }

    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {

            System.out.println("TIMER TIMER ::::::::");
            // As the TimerTask run on a separate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {

                    pingServices();

                }
            });

        }
    }

    boolean boolScroolToBottom = false;

    private void callMainApis() {

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new ViewGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new ViewGroupData().execute();
            }
        }

        if (!connDec.isConnectingToInternet()) {
            //    alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GroupmemberData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GroupmemberData().execute();
            }
        }
    }

    private void setAdapter() {
        if (adapter == null) {
            adapter = new GroupAdapter();

            group_list.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
            setListViewHeightBasedOnChildren(group_list);

            /*scrollView.post(new Runnable() {
                @Override
                public void run() {

                    if(!isFirst)
                    {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                }
            });*/
        }

        group_list.postDelayed(new Runnable() {
            @Override
            public void run() {
                setListViewHeightBasedOnChildren(group_list);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //tv_msg_count.setText("" + groupList.size());

                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {

                                if (boolScroolToBottom == true) {
                                    scrollView.fullScroll(View.FOCUS_DOWN);
                                }

                                System.out.println("Input data is PING SCROLL TO BOTTOM " + boolScroolToBottom);
                            }
                        });
                    }
                });

            }
        }, 10);

    }


    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);
    }

    private void initialise() {
        timer = new Timer();
        connDec = new ConnectionDetector(CrewActivity.this);
        alert = new Alert_Dialog_Manager(context);
        iv_smiley1 = (ImageView) findViewById(R.id.iv_smiley1);
        iv_smiley2 = (ImageView) findViewById(R.id.iv_smiley2);
        iv_smiley3 = (ImageView) findViewById(R.id.iv_smiley3);
        iv_smiley4 = (ImageView) findViewById(R.id.iv_smiley4);
        iv_smiley5 = (ImageView) findViewById(R.id.iv_smiley5);
        iv_emojibtn = (ImageView) findViewById(R.id.iv_emojibtn);
        iv_attach = (ImageView) findViewById(R.id.iv_attach); ///
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        rl_emo_buttons = (RelativeLayout) findViewById(R.id.rl_emo_buttons);
        iv_keyboard = (ImageView) findViewById(R.id.iv_keyboard);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_menu_icon1 = (ImageView) findViewById(R.id.iv_menu_icon1);
        iv_groupImage = (DiamondImageView) findViewById(R.id.iv_groupImage);
        tv_heart = (ImageView) findViewById(R.id.tv_heart);
        iv_heart_fill = (ImageView) findViewById(R.id.iv_heart_fill);
        iv_msg = (ImageView) findViewById(R.id.iv_msg);
        tv_title_group = (TextView) findViewById(R.id.tv_title_group);
        tv_load_more = (TextView) findViewById(R.id.tv_load_more);
        tv_no_comment = (TextView) findViewById(R.id.tv_no_comment);
        tv_mygroup = (TextView) findViewById(R.id.tv_mygroup);
        tv_mygroup.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
        tv_subheading = (TextView) findViewById(R.id.tv_subheading);
        tv_subheading.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        tv_count = (TextView) findViewById(R.id.tv_count);
        tv_msg_count = (TextView) findViewById(R.id.tv_msg_count);
        tv_send = (TextView) findViewById(R.id.tv_send);
        tv_num = (TextView) findViewById(R.id.tv_num);
        et_leave_comment = (EditText) findViewById(R.id.et_leave_comment);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        emoLay = (LinearLayout) findViewById(R.id.emoLay);
        tv_number = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        rl_sel_smileys = (RelativeLayout) findViewById(R.id.rl_sel_smileys);
        rl_send = (RelativeLayout) findViewById(R.id.rl_send);
        rl_background = (RelativeLayout) findViewById(R.id.rl_background);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        rvImages = (RecyclerView) findViewById(R.id.rvImages);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(CrewActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(horizontalLayoutManagaer);
        group_list = (ListView) findViewById(R.id.group_list);

        groupList = new ArrayList<HashMap<String, Object>>();
        viewlist = new ArrayList<HashMap<String, String>>();
        memberlist = new ArrayList<HashMap<String, String>>();
        moodList1 = new ArrayList<HashMap<String, String>>();
        pingList = new ArrayList<>();
        scrollView = (ScrollView) findViewById(R.id.scroll);
        rvImages = (RecyclerView) findViewById(R.id.rvImages);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);
    }


    private void clickables() {

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
 /*               Intent intent = new Intent(CrewActivity.this, MyCrewsActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("group_image", grpimg);
                intent.putExtra("group_type", grptype);
                intent.putExtra("group_tags", grptag);
                intent.putExtra("group_create_date", groupcreatdate);
                intent.putExtra("group_status", grpstatus);
                intent.putExtra("owner_id", owner_id);
                intent.putExtra("owner_type", owner_type);
                intent.putExtra("you_group", you_grp);

                if ( getIntent().hasExtra("member_status"))
                intent.putExtra("member_status", getIntent().getStringExtra("member_status"));

                startActivity(intent);*/

                finish();
            }
        });


        iv_emojibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard.setVisibility(View.VISIBLE);
                iv_emojibtn.setVisibility(View.GONE);
                iv_attach.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                }, 200);


            }
        });

        ///
        iv_attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMessagephoto();
            }
        });

        iv_keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard.setVisibility(View.GONE);
                iv_emojibtn.setVisibility(View.VISIBLE);
                iv_attach.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity, et_leave_comment);
            }
        });

        et_leave_comment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons.setVisibility(View.VISIBLE);
                } else {
                    rl_emo_buttons.setVisibility(View.GONE);
                    iv_emojibtn.setVisibility(View.VISIBLE);
                    iv_attach.setVisibility(View.VISIBLE);
                    iv_keyboard.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_leave_comment.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_leave_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn.setVisibility(View.VISIBLE);
                iv_attach.setVisibility(View.VISIBLE);
                iv_keyboard.setVisibility(View.GONE);
            }
        });


        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //       Intent intent = new Intent(CrewActivity.this, MyCrewsActivity.class);
                //     startActivity(intent);

                finish();
            }
        });

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setIntentData();
                startActivityForResult(intent, 1);
            }
        });

        //different drawer button
        iv_menu_icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setIntentData();
                intent.putExtra("options_next", "");
                startActivityForResult(intent, 1);
            }
        });

        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                commentStr = et_leave_comment.getText().toString().trim();

                ArrayList<String> bannedList = new ArrayList<String>();

                bannedList = ((UTCAPP) getApplication()).bannedWordsList;

                if (!commentStr.equals("")) {
                    boolean banWord = false;

                    String[] commentedWordsArray;

                    commentedWordsArray = commentStr.split(" ");

                    for (int i = 0; i < commentedWordsArray.length; i++) {
                        String name = commentedWordsArray[i];

                        if (bannedList.contains(name.toLowerCase()) || bannedList.contains(name) || bannedList.contains(name.toUpperCase())) {
                            banWord = true;

                            commentStr = commentStr.replaceAll(name, "<font color='red'>" + name + "</font>");

                            et_leave_comment.setText(Html.fromHtml(commentStr));

                            et_leave_comment.setSelection(Integer.parseInt(String.valueOf(et_leave_comment.getText().length())));

                            ///break;
                        }
                    }
                    if (banWord == false) {
                        if (!connDec.isConnectingToInternet()) {
                            alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new PostCommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new PostCommentData().execute();
                            }
                        }
                        et_leave_comment.setText("");
                    } else {
                        customShareBannedPopup();
                    }

                } else {
                    Toast.makeText(activity, "Please write some comment to post.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tv_load_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (connDec.isConnectingToInternet()) {

                    count = groupList.size();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        new CommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                    else
                        new CommentData().execute();

                }
            }
        });

    }


    private void setIntentData() {
        intent = new Intent(CrewActivity.this, GroupMemberOptions.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intent.putExtra("group_id", group_id);
        intent.putExtra("group_name", groupname);
        intent.putExtra("group_detail", grpdetail);
        intent.putExtra("group_image", grpimg);
        intent.putExtra("group_type", grptype);
        intent.putExtra("group_tags", grptag);
        intent.putExtra("group_create_date", groupcreatdate);
        intent.putExtra("group_status", grpstatus);
        intent.putExtra("owner_id", owner_id);
        intent.putExtra("owner_type", owner_type);
        intent.putExtra("you_group", you_grp);
        intent.putExtra("member_id", preferences.getString("member_id", "").toString());
        intent.putExtra("imp_id", uid);
        intent.putExtra("key", value);
        intent.putExtra("is_fav", is_fav);
        intent.putExtra("mute_group", mute_group);
        //intent.putExtra("options_next", "");
        intent.putExtra("member_status", member_status);


    }

    private void startCropImage() {

        //System.out.println("cropimage");

        Intent intent = new Intent(CrewActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, tempImageFile.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, Constants.REQUEST_CODE_CROP_IMAGE);
    }

    private void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encodedImage;
    }

    private void customShareBannedPopup() {
        final Dialog dialog = new Dialog(CrewActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.update_popup);
        final TextView tv_title_survey, ok_btn, tv_title_participate, tv_title_banned;
        ok_btn = (TextView) dialog.findViewById(R.id.ok_btn);
        tv_title_survey = (TextView) dialog.findViewById(R.id.tv_title_survey);
        tv_title_participate = (TextView) dialog.findViewById(R.id.tv_title_participate);
        tv_title_banned = (TextView) dialog.findViewById(R.id.tv_title_banned);
        tv_title_participate.setVisibility(View.GONE);
        tv_title_banned.setVisibility(View.VISIBLE);
        tv_title_survey.setVisibility(View.GONE);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    public class LikeServiceData1 extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "group");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    callMainApis();

                } else if (errCode.equals("700")) {

                    //  alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

//    public void setListViewHeightBasedOnItems(ListView listView) {
//        ListAdapter listAdapter = listView.getAdapter();
//        if (listAdapter == null) {
//            // pre-condition
//            return;
//        }
//        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
//        for (int i = 0; i < listAdapter.getCount(); i++) {
//            View listItem = listAdapter.getView(i, null, listView);
//            if (listItem instanceof ViewGroup) {
//                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//            }
//            listItem.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
//            totalHeight += listItem.getMeasuredHeight();
//
//        }
//
//        ViewGroup.LayoutParams params = listView.getLayoutParams();
//        params.height = (totalHeight + (listAdapter.getCount() - 1));
//        System.out.println("Height is:" + params.height);
//        listView.setLayoutParams(params);
//        listView.requestLayout();
//    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);

            if (listItem != null) {
                // This next line is needed before you call measure or else you won't get measured height at all. The listitem needs to be drawn first to know the height.
                listItem.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();

            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + 100 + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:
                Intent intent = new Intent(CrewActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(CrewActivity.this, CalendarActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }

    }


    public void pingServices() {

        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new PingServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new PingServiceData().execute();
            }
        }
    }

    private class GroupAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return groupList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.group_page_adapter, null);

            final ViewHolder holder = new ViewHolder();
            holder.iv_iconheart = (ImageView) convertView.findViewById(R.id.iv_iconheart);
            holder.iv_list_img = (DiamondImageView) convertView.findViewById(R.id.iv_list_img);
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_message = (TextView) convertView.findViewById(R.id.tv_message);
            holder.tv_message.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_count_days = (TextView) convertView.findViewById(R.id.tv_count_days);
            tv_notif = (TextView) convertView.findViewById(R.id.tv_notif);
            holder.tv_total_rating = (TextView) convertView.findViewById(R.id.tv_total_rating);
            holder.ll_heart = (LinearLayout) convertView.findViewById(R.id.ll_heart);
            holder.ll_delete = (LinearLayout) convertView.findViewById(R.id.ll_delete);
            holder.iv_smiley1 = (ImageView) convertView.findViewById(R.id.iv_smiley1);
            holder.iv_smiley2 = (ImageView) convertView.findViewById(R.id.iv_smiley2);
            holder.iv_smiley3 = (ImageView) convertView.findViewById(R.id.iv_smiley3);
            holder.iv_smiley4 = (ImageView) convertView.findViewById(R.id.iv_smiley4);
            holder.iv_smiley5 = (ImageView) convertView.findViewById(R.id.iv_smiley5);
            holder.iv_heart_smile = (ImageView) convertView.findViewById(R.id.iv_heart_smile);
            holder.ll_heart_smiley = (LinearLayout) convertView.findViewById(R.id.ll_heart_smiley);
            holder.rl_sel_smileys = (RelativeLayout) convertView.findViewById(R.id.rl_sel_smileys);
            holder.swipeLayout = (SwipeLayout) convertView.findViewById(R.id.swipe);
            String image = (String) groupList.get(i).get("user_thumbnail");
            String genderr = (String) groupList.get(i).get("gender");

            if (!member_status.equalsIgnoreCase("Active")) {
                holder.ll_heart_smiley.setEnabled(false);
                holder.iv_iconheart.setEnabled(false);
                holder.swipeLayout.setLeftSwipeEnabled(false);
                holder.swipeLayout.setRightSwipeEnabled(false);
            }

            if ((groupList == null) || groupList.equals("")) {

                tv_no_comment.setVisibility(View.VISIBLE);
                group_list.setVisibility(View.GONE);
            } else {
                tv_no_comment.setVisibility(View.GONE);
            }
            if (image == null) {
                image = "";
            }

            holder.tv_total_rating.setText((String) groupList.get(i).get("comment_like"));

            if (groupList.get(i).containsKey("comment_like")) {
                if (((String) groupList.get(i).get("comment_like")).equals("0")) {
                    if (!((String) groupList.get(i).get("uid")).equals(preferences.getString("uid", ""))) {
                        holder.ll_heart.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                System.out.println("MSG ID=" + (String) groupList.get(i).get("message_id"));

                                customShareDetailsPopup((String) groupList.get(i).get("message_id"), i, holder.tv_total_rating);

                            }
                        });
                    }
                } else {
                    if (!((String) groupList.get(i).get("uid")).equals(preferences.getString("uid", ""))) {
                        holder.tv_total_rating.setText((String) groupList.get(i).get("comment_like"));

                        holder.iv_iconheart.setImageResource(R.drawable.sel_star);
                    } else {
                        holder.tv_total_rating.setText((String) groupList.get(i).get("comment_like"));

                        holder.iv_iconheart.setImageResource(R.drawable.unsel_star);
                    }
                }
            }
            msg = (String) groupList.get(i).get("message_id");
            if (groupList != null) {

                if (image.equalsIgnoreCase("")) {

                    if (groupList.get(i).containsKey("gender")) {

                        if (groupList.get(i).get("gender").equals("Male")) {

                            holder.iv_list_img.setImageResource(R.drawable.avatar);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);

                        } else if (groupList.get(i).get("gender").equals("Female")) {

                            holder.iv_list_img.setImageResource(R.drawable.avatar);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);
                        } else if (groupList.get(i).get("gender").equals("")) {
                            holder.iv_list_img.setImageResource(R.drawable.avatar);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);
                        }
                    }
                } else {
                    Picasso.with(getApplicationContext()).load(image).into(holder.iv_list_img);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);

                }
            }

            String comment_like = "" + groupList.get(i).get("comment_like");

            if (comment_like == null) {
                comment_like = "";
            }
            if (groupList.size() != 0) {

                if (groupList.get(i).containsKey("total_moods")) {
                    if (!groupList.get(i).get("total_moods").equals("") || groupList.get(i).get("total_moods") != null) {
                        tv_notif.setText(groupList.get(i).get("total_moods").toString());
                    }
                }
                if (groupList.get(i).containsKey("user_type")) {

                    if (groupList.get(i).get("user_type").equals("admin")) {

                        holder.iv_list_img.setClickable(false);
                        holder.tv_message.setClickable(false);
                        holder.tv_items_grp.setClickable(false);
                        holder.ll_delete.setVisibility(View.GONE);
                        holder.tv_items_grp.setText("Administrator");

                        if (groupList.get(i).get("user_thumbnail").equals("null")) {
                            groupList.get(i).put("user_thumbnail", "");
                            holder.iv_list_img.setImageResource(R.drawable.avatar);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);
                        } else if (groupList.get(i).get("user_thumbnail").equals("")) {

                            holder.iv_list_img.setImageResource(R.drawable.avatar);
                            holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                            holder.iv_list_img.setBorderWidth(2);


                        }
                    } else if (groupList.get(i).get("user_type").equals("user")) {

                        holder.iv_list_img.setClickable(true);
                        holder.tv_message.setClickable(true);
                        holder.tv_items_grp.setClickable(true);
                        holder.ll_delete.setVisibility(View.VISIBLE);
                        holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(groupList.get(i).get("first_name") + "") + " " + CommonUtilities.decodeUTF8(groupList.get(i).get("last_name") + ""));
                        {

                            if (groupList.get(i).get("user_thumbnail") == null) {
                                groupList.get(i).put("user_thumbnail", "");
                                holder.iv_list_img.setImageResource(R.drawable.avatar);
                                holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                                holder.iv_list_img.setBorderWidth(2);
                            }

                            if (groupList.get(i).get("user_thumbnail").equals("")) {

                                holder.iv_list_img.setImageResource(R.drawable.avatar);
                                holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                                holder.iv_list_img.setBorderWidth(2);


                            } else {
                                Picasso.with(getApplicationContext()).load(groupList.get(i).get("user_thumbnail").toString()).into(holder.iv_list_img);
                                holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                                holder.iv_list_img.setBorderWidth(2);

                            }
                        }
                        System.out.println("COMMENT DATA=" + groupList.get(i).get("first_name") + groupList.get(i).get("last_name") + "");

                        holder.iv_list_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (groupList.get(i).get("uid").equals(preferences.getString("uid", ""))) {
                                    Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                    intent1.putExtra("uid", preferences.getString("uid", ""));
                                    intent1.putExtra("myProfile", "");
                                    intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent1);

                                } else {
                                    Intent intent = new Intent(CrewActivity.this, MyProfile2.class);
                                    intent.putExtra("uid", groupList.get(i).get("uid").toString());
                                    intent.putExtra("group_id", group_id);
                                    intent.putExtra("group_name", groupname);
                                    intent.putExtra("group_detail", grpdetail);
                                    intent.putExtra("group_image", grpimg);
                                    intent.putExtra("group_type", grptype);
                                    intent.putExtra("group_tags", grptag);
                                    intent.putExtra("group_create_date", groupcreatdate);
                                    intent.putExtra("group_status", grpstatus);
                                    intent.putExtra("owner_id", owner_id);
                                    intent.putExtra("owner_type", owner_type);
                                    intent.putExtra("you_group", you_grp);
                                    startActivity(intent);
                                }
                            }
                        });

                        holder.tv_items_grp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (groupList.get(i).get("uid").equals(preferences.getString("uid", ""))) {
                                    Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                    intent1.putExtra("uid", preferences.getString("uid", ""));
                                    intent1.putExtra("myProfile", "");
                                    intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent1);

                                } else {
                                    Intent intent = new Intent(CrewActivity.this, MyProfile2.class);
                                    intent.putExtra("uid", groupList.get(i).get("uid").toString());
                                    intent.putExtra("group_id", group_id);
                                    intent.putExtra("group_name", groupname);
                                    intent.putExtra("group_detail", grpdetail);
                                    intent.putExtra("group_image", grpimg);
                                    intent.putExtra("group_type", grptype);
                                    intent.putExtra("group_tags", grptag);
                                    intent.putExtra("group_create_date", groupcreatdate);
                                    intent.putExtra("group_status", grpstatus);
                                    intent.putExtra("owner_id", owner_id);
                                    intent.putExtra("owner_type", owner_type);
                                    intent.putExtra("you_group", you_grp);
                                    startActivity(intent);
                                }
                            }
                        });
                        holder.tv_message.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (groupList.get(i).get("uid").equals(preferences.getString("uid", ""))) {
                                    Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                    intent1.putExtra("uid", preferences.getString("uid", ""));
                                    intent1.putExtra("myProfile", "");
                                    intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                    startActivity(intent1);

                                } else {
                                    Intent intent = new Intent(CrewActivity.this, MyProfile2.class);
                                    intent.putExtra("uid", groupList.get(i).get("uid").toString());
                                    intent.putExtra("group_id", group_id);
                                    intent.putExtra("group_name", groupname);
                                    intent.putExtra("group_detail", grpdetail);
                                    intent.putExtra("group_image", grpimg);
                                    intent.putExtra("group_type", grptype);
                                    intent.putExtra("group_tags", grptag);
                                    intent.putExtra("group_create_date", groupcreatdate);
                                    intent.putExtra("group_status", grpstatus);
                                    intent.putExtra("owner_id", owner_id);
                                    intent.putExtra("owner_type", owner_type);
                                    intent.putExtra("you_group", you_grp);
                                    startActivity(intent);
                                }
                            }
                        });
                        holder.ll_delete.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                String mesage_id = groupList.get(i).get("message_id").toString();

                                if (connDec.isConnectingToInternet()) {

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                                        new FlagData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mesage_id);
                                    else
                                        new FlagData().execute(mesage_id);

                                }
                            }
                        });

                    }
                }

                if (groupList.get(i).containsKey("moo")) {

                    if (groupList.get(i).get("moo").equals("0")) {

                        holder.ll_heart_smiley.setVisibility(View.VISIBLE);

                        holder.ll_heart_smiley.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                showDialogSmiley(i, holder.iv_heart_smile);
                            }
                        });
                    } else {

                        holder.ll_heart_smiley.setVisibility(View.GONE);

                        holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDialogSmiley1(i);
                            }
                        });
                    }
                }
                if (groupList.get(i).containsKey("uid")) {
                    if (groupList.get(i).get("uid").equals(preferences.getString("uid", ""))) {

                        //   holder.ll_heart_smiley.setVisibility(View.GONE);
                        holder.ll_delete.setVisibility(View.GONE);
                        if (groupList.get(i).containsKey("comment_like")) {
                            holder.ll_heart_smiley.setVisibility(View.GONE);
                        } else {
                            holder.ll_heart_smiley.setVisibility(View.VISIBLE);
                        }
                        holder.ll_heart_smiley.setClickable(false);

                    }
                }
            }

            if (!groupList.equals("")) {

                holder.tv_message.setText(CommonUtilities.decodeUTF8(groupList.get(i).get("content") + ""));

            }
            {

                if (groupList.get(i).get("user_thumbnail") == null) {
                    groupList.get(i).put("user_thumbnail", "");
                    holder.iv_list_img.setImageResource(R.drawable.avatar);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);
                }

                if (groupList.get(i).get("user_thumbnail").equals("")) {

                    holder.iv_list_img.setImageResource(R.drawable.avatar);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);


                } else {
                    Picasso.with(getApplicationContext()).load(groupList.get(i).get("user_thumbnail").toString()).into(holder.iv_list_img);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);

                }
            }


            if (groupList.get(i).containsKey("moods")) {

                if (groupList.get(i).get("moods").toString().contains("happy")) {

                    holder.iv_smiley1.setVisibility(View.VISIBLE);

                } else {
                    holder.iv_smiley1.setVisibility(View.GONE);
                }
                if (groupList.get(i).get("moods").toString().contains("indifferen")) {

                    holder.iv_smiley2.setVisibility(View.VISIBLE);

                } else {
                    holder.iv_smiley2.setVisibility(View.GONE);
                }
                if (groupList.get(i).get("moods").toString().contains("amazing")) {

                    holder.iv_smiley3.setVisibility(View.VISIBLE);

                } else {
                    holder.iv_smiley3.setVisibility(View.GONE);
                }
                if (groupList.get(i).get("moods").toString().contains("love")) {

                    holder.iv_smiley4.setVisibility(View.VISIBLE);

                } else {
                    holder.iv_smiley4.setVisibility(View.GONE);
                }
                if (groupList.get(i).get("moods").toString().contains("sad")) {

                    holder.iv_smiley5.setVisibility(View.VISIBLE);
                } else {
                    holder.iv_smiley5.setVisibility(View.GONE);
                }
            }

            if (groupList.get(i).containsKey("mood_like")) {
                if (groupList.get(i).get("mood_like").equals("happy")) {

                    holder.iv_smiley1.setImageResource(R.drawable.happy);
                    holder.iv_smiley1.setVisibility(View.VISIBLE);
                } else if (groupList.get(i).get("mood_like").equals("indifferen")) {

                    holder.iv_smiley2.setImageResource(R.drawable.indifferent);
                    holder.iv_smiley2.setVisibility(View.VISIBLE);
                } else if (groupList.get(i).get("mood_like").equals("amazing")) {

                    holder.iv_smiley3.setImageResource(R.drawable.amazing);
                    holder.iv_smiley3.setVisibility(View.VISIBLE);
                } else if (groupList.get(i).get("mood_like").equals("love")) {

                    holder.iv_smiley4.setImageResource(R.drawable.love);
                    holder.iv_smiley4.setVisibility(View.VISIBLE);
                } else if (groupList.get(i).get("mood_like").equals("sad")) {

                    holder.iv_smiley5.setImageResource(R.drawable.sad);
                    holder.iv_smiley5.setVisibility(View.VISIBLE);
                }
            }

            try {
                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

                String FinalDate = (String) groupList.get(i).get("message_create_date");

                Date date1;
                Date date2;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                //Setting dates
                date1 = dates.parse(date);
                date2 = dates.parse(FinalDate);

                //Comparing dates
                /*long difference = Math.abs(date1.getTime() - date2.getTime());
                long differenceDates = difference / (24 * 60 * 60 * 1000);

                //Convert long to String
                String dayDifference = Long.toString(differenceDates);*/
                long diff = date1.getTime() - date2.getTime();

                holder.tv_count_days.setText(CommonUtilities.getDaysMonths(diff));


                //System.out.println("HERE " + dayDifference);
                //holder.tv_count_days.setText(dayDifference + "d");

            } catch (Exception exception) {

                System.out.println("Work " + exception);
            }

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_list_img;

            ImageView iv_iconheart;
            TextView tv_items_grp, tv_message, tv_count_days, tv_total_rating;
            LinearLayout ll_heart, ll_delete, ll_heart_smiley;
            ImageView iv_smiley1, iv_smiley2, iv_smiley3, iv_smiley4, iv_smiley5, iv_heart_smile;
            RelativeLayout rl_sel_smileys;
            SwipeLayout swipeLayout;
        }
    }

    public void showDialogSmiley1(final int position) {
        rl_background.setVisibility(View.VISIBLE);
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0f);
        dialog.setContentView(R.layout.moods_activity_one);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ArrayList<HashMap<String, String>> lis = (ArrayList<HashMap<String, String>>) groupList.get(position).get("moods");

        for (int x = 0; x < lis.size(); x++) {

            String moods = lis.get(x).get("moods");

            String count = lis.get(x).get("con");

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");

                    break;

                case "indifferen":
                    count_indiff.setText("(" + count + ")");

                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");

                    break;
                case "love":
                    count_love.setText("(" + count + ")");

                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");

                    break;

            }

        }


        if (groupList.get(position).get("mood_like").equals("happy")) {

            iv_happy.setVisibility(View.GONE);
            iv_happy_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("indifferen")) {

            iv_indiff.setVisibility(View.GONE);
            iv_indiff_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("amazing")) {

            iv_amazing.setVisibility(View.GONE);
            iv_amazing_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("love")) {

            iv_love.setVisibility(View.GONE);
            iv_love_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("sad")) {

            iv_sad.setVisibility(View.GONE);
            iv_sad_sel.setVisibility(View.VISIBLE);
        }

        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("happy")) {
                    happyCount = "" + groupList.get(position).get("happy");
                }
                moodStr = "happy";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                //  counting++;

                groupList.get(position).put("happy", "" + count);

                //  groupList.get(position).put("total_moods", "" + counting);

                //adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_happy.setImageResource(R.drawable.happy);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData1().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();

            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("indifferen")) {
                    happyCount = "" + groupList.get(position).get("indifferen");
                }
                moodStr = "indifferen";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                //  int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                // counting++;

                groupList.get(position).put("indifferen", "" + count);

                //   groupList.get(position).put("total_moods", "" + counting);

                // adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.indifferent);
                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                        new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData1().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();
            }


        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("amazing")) {
                    happyCount = "" + groupList.get(position).get("amazing");
                }
                moodStr = "amazing";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                // int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                //  counting++;

                groupList.get(position).put("amazing", "" + count);

                //  groupList.get(position).put("total_moods", "" + counting);

                // adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_amazing.setImageResource(R.drawable.amazing);
                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData1().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();
            }


        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("love")) {
                    happyCount = "" + groupList.get(position).get("love");
                }
                moodStr = "love";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                //  int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                //  counting++;

                groupList.get(position).put("love", "" + count);

                // groupList.get(position).put("total_moods", "" + counting);

                //adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_love.setImageResource(R.drawable.love);
                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData1().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();

            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("sad")) {
                    happyCount = "" + groupList.get(position).get("sad");
                }
                moodStr = "sad";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                //   int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                //   counting++;

                groupList.get(position).put("sad", "" + count);

                //   groupList.get(position).put("total_moods", "" + counting);

                // adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_sad.setImageResource(R.drawable.sad);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData1().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();

            }
        });

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });
    }

    public class ChangePostData1 extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "change_comment_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "comment");

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    /*commentList.get(position).put("mood_like",moodString);

                    adapter.notifyDataSetChanged();*/

                    scrolledPosition = scrollView.getScrollY();

                    callMainApis();


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void showDialogSmiley(final int position, final ImageView iv_heart_smile) {
        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0f);
        dialog.setContentView(R.layout.moods_activity_one);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ArrayList<HashMap<String, String>> lis = (ArrayList<HashMap<String, String>>) groupList.get(position).get("moods");

        for (int x = 0; x < lis.size(); x++) {

            String moods = lis.get(x).get("moods");

            String count = lis.get(x).get("con");

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");

                    break;

                case "indifferen":
                    count_indiff.setText("(" + count + ")");

                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");

                    break;
                case "love":
                    count_love.setText("(" + count + ")");

                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");

                    break;

            }

        }

        if (groupList.get(position).get("mood_like").equals("happy")) {

            iv_happy.setVisibility(View.GONE);
            iv_happy_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("indifferen")) {

            iv_indiff.setVisibility(View.GONE);
            iv_indiff_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("amazing")) {

            iv_amazing.setVisibility(View.GONE);
            iv_amazing_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("love")) {

            iv_love.setVisibility(View.GONE);
            iv_love_sel.setVisibility(View.VISIBLE);

        } else if (groupList.get(position).get("mood_like").equals("sad")) {

            iv_sad.setVisibility(View.GONE);
            iv_sad_sel.setVisibility(View.VISIBLE);
        }

        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("happy")) {
                    happyCount = "" + groupList.get(position).get("happy");
                }
                moodStr = "happy";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                counting++;

                groupList.get(position).put("happy", "" + count);

                groupList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_happy.setImageResource(R.drawable.happy);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();

            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("indifferen")) {
                    happyCount = "" + groupList.get(position).get("indifferen");
                }
                moodStr = "indifferen";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                counting++;

                groupList.get(position).put("indifferen", "" + count);

                groupList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.indifferent);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();
            }


        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("amazing")) {
                    happyCount = "" + groupList.get(position).get("amazing");
                }
                moodStr = "amazing";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                counting++;

                groupList.get(position).put("amazing", "" + count);

                groupList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_amazing.setImageResource(R.drawable.amazing);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();
            }


        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("love")) {
                    happyCount = "" + groupList.get(position).get("love");
                }
                moodStr = "love";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                counting++;

                groupList.get(position).put("love", "" + count);

                groupList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_love.setImageResource(R.drawable.love);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();

            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_heart.setVisibility(View.GONE);

                groupList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (groupList.get(position).containsKey("sad")) {
                    happyCount = "" + groupList.get(position).get("sad");
                }
                moodStr = "sad";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(groupList.get(position).get("total_moods") + "");

                counting++;

                groupList.get(position).put("sad", "" + count);

                groupList.get(position).put("total_moods", "" + counting);

                adapter.notifyDataSetChanged();

                tv_count.setText(groupList.get(position).get("total_moods").toString());

                iv_sad.setImageResource(R.drawable.sad);

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, groupList.get(position).get("message_id").toString(), "1", "" + position);
                    }
                }
                dialog.dismiss();

            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    private void customShareDetailsPopup(final String msg_id, final int k, final TextView tvTotalGlobal1) {

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.rating_star);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {


            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating_selected = rating + "";
                tvTotalGlobal = tvTotalGlobal1;

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new RatingData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msg_id);
                    } else {
                        new RatingData().execute(msg_id);
                    }
                }
                dialog.dismiss();
                groupList.get(k).put("comment_like", "1");

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
        private ArrayList<HashMap<String, String>> memberlist;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public DiamondImageView ivImage, ivImage1;

            public MyViewHolder(View view) {
                super(view);
                ivImage = (DiamondImageView) view.findViewById(R.id.ivImage);
                ivImage1 = (DiamondImageView) view.findViewById(R.id.ivImage1);
            }
        }

        public HorizontalAdapter(CrewActivity crewActivity, ArrayList<HashMap<String, String>> memberlist) {
            this.memberlist = memberlist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_xyz, parent, false);
            MyViewHolder holder = new MyViewHolder(itemView);

            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            String image = memberlist.get(position).get("user_image");
            String gender = preferences.getString("gender", "");
            if (memberlist.get(position).get("uid").equals(preferences.getString("owner_id", ""))) {

                if (image.equalsIgnoreCase("")) {

                    if (memberlist.get(position).containsKey("gender")) {

                        if (memberlist.get(position).get("gender").equals("Male")) {

                            holder.ivImage.setImageResource(R.drawable.avatar);
                            holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                            holder.ivImage.setBorderWidth(2);
                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                        Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                        intent1.putExtra("uid", preferences.getString("uid", ""));
                                        intent1.putExtra("myProfile", "");
                                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        startActivity(intent1);

                                    } else {
                                        Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                        intent.putExtra("UID", memberlist.get(position).get("uid"));
                                        intent.putExtra("group_id", group_id);
                                        intent.putExtra("group_name", groupname);
                                        intent.putExtra("group_detail", grpdetail);
                                        intent.putExtra("group_image", grpimg);
                                        intent.putExtra("group_type", grptype);
                                        intent.putExtra("group_tags", grptag);
                                        intent.putExtra("group_create_date", groupcreatdate);
                                        intent.putExtra("group_status", grpstatus);
                                        intent.putExtra("owner_id", owner_id);
                                        intent.putExtra("owner_type", owner_type);
                                        intent.putExtra("you_group", you_grp);
                                        startActivity(intent);
                                    }
                                }
                            });

                        } else if (memberlist.get(position).get("gender").equals("Female")) {

                            holder.ivImage.setImageResource(R.drawable.avatar);
                            holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                            holder.ivImage.setBorderWidth(2);
                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                        Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                        intent1.putExtra("uid", preferences.getString("uid", ""));
                                        intent1.putExtra("myProfile", "");
                                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        startActivity(intent1);

                                    } else {
                                        Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                        intent.putExtra("UID", memberlist.get(position).get("uid"));
                                        intent.putExtra("group_id", group_id);
                                        intent.putExtra("group_name", groupname);
                                        intent.putExtra("group_detail", grpdetail);
                                        intent.putExtra("group_image", grpimg);
                                        intent.putExtra("group_type", grptype);
                                        intent.putExtra("group_tags", grptag);
                                        intent.putExtra("group_create_date", groupcreatdate);
                                        intent.putExtra("group_status", grpstatus);
                                        intent.putExtra("owner_id", owner_id);
                                        intent.putExtra("owner_type", owner_type);
                                        intent.putExtra("you_group", you_grp);
                                        startActivity(intent);
                                    }
                                }
                            });
                        } else if (memberlist.get(position).get("gender").equals("")) {

                            holder.ivImage.setImageResource(R.drawable.avatar);
                            holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                            holder.ivImage.setBorderWidth(2);
                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                        Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                        intent1.putExtra("uid", preferences.getString("uid", ""));
                                        intent1.putExtra("myProfile", "");
                                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        startActivity(intent1);

                                    } else {
                                        Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                        intent.putExtra("UID", memberlist.get(position).get("uid"));
                                        intent.putExtra("group_id", group_id);
                                        intent.putExtra("group_name", groupname);
                                        intent.putExtra("group_detail", grpdetail);
                                        intent.putExtra("group_image", grpimg);
                                        intent.putExtra("group_type", grptype);
                                        intent.putExtra("group_tags", grptag);
                                        intent.putExtra("group_create_date", groupcreatdate);
                                        intent.putExtra("group_status", grpstatus);
                                        intent.putExtra("owner_id", owner_id);
                                        intent.putExtra("owner_type", owner_type);
                                        intent.putExtra("you_group", you_grp);
                                        startActivity(intent);
                                    }
                                }
                            });
                        }
                    }
                } else {
                    Picasso.with(getApplicationContext()).load(image).into(holder.ivImage);
                    holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                    holder.ivImage.setBorderWidth(2);
                    holder.ivImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                intent1.putExtra("uid", preferences.getString("uid", ""));
                                intent1.putExtra("myProfile", "");
                                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(intent1);

                            } else {
                                Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                intent.putExtra("UID", memberlist.get(position).get("uid"));
                                intent.putExtra("group_id", group_id);
                                intent.putExtra("group_name", groupname);
                                intent.putExtra("group_detail", grpdetail);
                                intent.putExtra("group_image", grpimg);
                                intent.putExtra("group_type", grptype);
                                intent.putExtra("group_tags", grptag);
                                intent.putExtra("group_create_date", groupcreatdate);
                                intent.putExtra("group_status", grpstatus);
                                intent.putExtra("owner_id", owner_id);
                                intent.putExtra("owner_type", owner_type);
                                intent.putExtra("you_group", you_grp);
                                startActivity(intent);
                            }
                        }
                    });

                }
            } else {
                if (image.equalsIgnoreCase("")) {

                    if (memberlist.get(position).containsKey("gender")) {

                        if (memberlist.get(position).get("gender").equals("Male")) {

                            holder.ivImage.setImageResource(R.drawable.avatar);
                            holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                            holder.ivImage.setBorderWidth(2);
                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                        Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                        intent1.putExtra("uid", preferences.getString("uid", ""));
                                        intent1.putExtra("myProfile", "");
                                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        startActivity(intent1);

                                    } else {
                                        Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                        intent.putExtra("UID", memberlist.get(position).get("uid"));
                                        intent.putExtra("group_id", group_id);
                                        intent.putExtra("group_name", groupname);
                                        intent.putExtra("group_detail", grpdetail);
                                        intent.putExtra("group_image", grpimg);
                                        intent.putExtra("group_type", grptype);
                                        intent.putExtra("group_tags", grptag);
                                        intent.putExtra("group_create_date", groupcreatdate);
                                        intent.putExtra("group_status", grpstatus);
                                        intent.putExtra("owner_id", owner_id);
                                        intent.putExtra("owner_type", owner_type);
                                        intent.putExtra("you_group", you_grp);
                                        startActivity(intent);
                                    }
                                }
                            });

                        } else if (memberlist.get(position).get("gender").equals("Female")) {

                            holder.ivImage.setImageResource(R.drawable.avatar);
                            holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                            holder.ivImage.setBorderWidth(2);
                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                        Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                        intent1.putExtra("uid", preferences.getString("uid", ""));
                                        intent1.putExtra("myProfile", "");
                                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        startActivity(intent1);

                                    } else {

                                        Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                        intent.putExtra("UID", memberlist.get(position).get("uid"));
                                        intent.putExtra("group_id", group_id);
                                        intent.putExtra("group_name", groupname);
                                        intent.putExtra("group_detail", grpdetail);
                                        intent.putExtra("group_image", grpimg);
                                        intent.putExtra("group_type", grptype);
                                        intent.putExtra("group_tags", grptag);
                                        intent.putExtra("group_create_date", groupcreatdate);
                                        intent.putExtra("group_status", grpstatus);
                                        intent.putExtra("owner_id", owner_id);
                                        intent.putExtra("owner_type", owner_type);
                                        intent.putExtra("you_group", you_grp);
                                        startActivity(intent);
                                    }
                                }
                            });
                        } else if (memberlist.get(position).get("gender").equals("")) {
                            holder.ivImage.setImageResource(R.drawable.avatar);
                            holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                            holder.ivImage.setBorderWidth(2);
                            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                        Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                        intent1.putExtra("uid", preferences.getString("uid", ""));
                                        intent1.putExtra("myProfile", "");
                                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                        startActivity(intent1);

                                    } else {

                                        Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                        intent.putExtra("UID", memberlist.get(position).get("uid"));
                                        intent.putExtra("group_id", group_id);
                                        intent.putExtra("group_name", groupname);
                                        intent.putExtra("group_detail", grpdetail);
                                        intent.putExtra("group_image", grpimg);
                                        intent.putExtra("group_type", grptype);
                                        intent.putExtra("group_tags", grptag);
                                        intent.putExtra("group_create_date", groupcreatdate);
                                        intent.putExtra("group_status", grpstatus);
                                        intent.putExtra("owner_id", owner_id);
                                        intent.putExtra("owner_type", owner_type);
                                        intent.putExtra("you_group", you_grp);
                                        startActivity(intent);
                                    }
                                }
                            });
                        }
                    }
                } else {
                    Picasso.with(getApplicationContext()).load(image).into(holder.ivImage);
                    holder.ivImage.setBorderColor(Color.parseColor("#F15A51"));
                    holder.ivImage.setBorderWidth(2);
                    holder.ivImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (memberlist.get(position).get("uid").equals(preferences.getString("uid", ""))) {
                                Intent intent1 = new Intent(CrewActivity.this, MyProfileActivity.class);
                                intent1.putExtra("uid", preferences.getString("uid", ""));
                                intent1.putExtra("myProfile", "");
                                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(intent1);

                            } else {
                                Intent intent = new Intent(CrewActivity.this, MyProfile4.class);
                                intent.putExtra("UID", memberlist.get(position).get("uid"));
                                intent.putExtra("group_id", group_id);
                                intent.putExtra("group_name", groupname);
                                intent.putExtra("group_detail", grpdetail);
                                intent.putExtra("group_image", grpimg);
                                intent.putExtra("group_type", grptype);
                                intent.putExtra("group_tags", grptag);
                                intent.putExtra("group_create_date", groupcreatdate);
                                intent.putExtra("group_status", grpstatus);
                                intent.putExtra("owner_id", owner_id);
                                intent.putExtra("owner_type", owner_type);
                                intent.putExtra("you_group", you_grp);
                                startActivity(intent);
                            }

                        }
                    });

                }
            }
        }

        @Override
        public int getItemCount() {
            return memberlist.size();
        }
    }

    public class LikeServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "", moodString = "";

        int position = 0;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "comment_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "comment");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    scrolledPosition = scrollView.getScrollY();

                    //  callMainApis();

                    new CommentData().execute();

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class RatingData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_comment_rating");

                jo.put("login_token", preferences.getString("login_token", ""));

                // jo.put("ref_id",preferences.getString("message_id","") );
                jo.put("ref_id", strings[0]);

                jo.put("group_id", group_id);

                jo.put("type", "group");

                jo.put("like_star", rating_selected);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    // tvTotalGlobal.setText(rating_selected);
                    callMainApis();
                }

                //setAdapter();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class FlagData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(CrewActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "comment_flag");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[0]);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CrewActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Flag added successfully.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    setAdapter();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CrewActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(CrewActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(CrewActivity.this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra("joined")) {
                    rl_send.setVisibility(View.VISIBLE);
                    rvImages.setVisibility(View.VISIBLE);
                    group_list.setVisibility(View.VISIBLE);
                } else {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }
        } else {
            if (data != null) {
                if (resultCode != Activity.RESULT_OK) {
                    return;
                }

                Bitmap bitmap;

                switch (requestCode) {
                    case Constants.REQUEST_CODE_GALLERY:
                        try {
                            checkExternalStorageAndCreateTempFile();

                            InputStream inputStream = getContentResolver().openInputStream(data.getData());

                            FileOutputStream fileOutputStream = new FileOutputStream(tempImageFile);

                            copyStream(inputStream, fileOutputStream);

                            fileOutputStream.close();

                            inputStream.close();

                            startCropImage();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;
                    case Constants.REQUEST_CODE_TAKE_PICTURE:
                        startCropImage();
                        break;
                    case Constants.REQUEST_CODE_CROP_IMAGE:

                        String path = data.getStringExtra(CropImage.IMAGE_PATH);
                        if (path == null) {

                            return;
                        }

                        bitmap = BitmapFactory.decodeFile(tempImageFile.getPath());

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                        ImageView attached = (ImageView) findViewById(R.id.iv_attached_image);
                        attached.setImageBitmap(bitmap);

                        findViewById(R.id.rellay_attachment).setVisibility(View.VISIBLE);

                        if (tempImageFile.exists()) {
                            tempImageFile.delete();
                        }

                        base64frombitmap(bitmap);

                    /*iv_signup_img.setImageBitmap(bitmap);
                    iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
                    iv_signup_img.setBorderWidth(2);*/

                }
            } else {
                if (requestCode == Constants.REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                    startCropImage();
                }
            }
        }
    }

    private void checkExternalStorageAndCreateTempFile() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            tempImageFile = new File(Environment.getExternalStorageDirectory(),
                    Constants.TEMP_PHOTO_FILE_NAME);
        } else {
            tempImageFile = new File(getFilesDir(), Constants.TEMP_PHOTO_FILE_NAME);
        }
    }

    public class CommentData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_comment_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", group_id);

                jo.put("source", "group");

                jo.put("post_value", count);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    tv_no_comment.setVisibility(View.GONE);

                    if (!getIntent().hasExtra("options_next"))
                        group_list.setVisibility(View.VISIBLE);

                    has_more = resultObj.getBoolean("has_more");

                    if (has_more)
                        tv_load_more.setVisibility(View.VISIBLE);

                    else
                        tv_load_more.setVisibility(View.GONE);

                    JSONArray jArray = resultObj.getJSONArray("comments");

                    if (count == 0) {
                        groupList.clear();

                        groupList = new ArrayList<>();
                    }
                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, Object> hmap = new HashMap<>();
                        HashMap<String, String> hmap1;
                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String message_id = user_dataObj.getString("message_id");
                        String ref_id = user_dataObj.getString("ref_id");
                        String content = user_dataObj.getString("content");
                        String source = user_dataObj.getString("source");
                        String uid = user_dataObj.getString("uid");
                        String message_status = user_dataObj.getString("message_status");
                        String user_type = user_dataObj.getString("user_type");
                        String message_create_date = user_dataObj.getString("message_create_date");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String gender = user_dataObj.getString("gender");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String you_flag = user_dataObj.getString("you_flag");
                        String you_like = user_dataObj.getString("you_like");
                        String comment_like = user_dataObj.getString("comment_like");
                        try {

                            String moods = "";

                            String count = "";

                            JSONArray jArrayy = user_dataObj.getJSONArray("moods");
                            moodList1 = new ArrayList<>();
                            for (int j = 0; j < jArrayy.length(); j++) {
                                hmap1 = new HashMap<>();

                                JSONObject user_dataObj1 = jArrayy.getJSONObject(j);
                                moods = user_dataObj1.getString("moods");

                                count = user_dataObj1.getString("con");

                                hmap1.put("con", count);
                                hmap1.put("moods", moods);
                                moodList1.add(hmap1);
                            }

                            JSONObject total_moods = user_dataObj.getJSONObject("total_moods");
                            String moo1 = total_moods.getString("moo");
                            ///JSONObject you_moods = user_dataObj.getJSONObject("you_moods");

                            ///String moo = you_moods.getString("moo");
                            ///String mood = you_moods.getString("moods");
                            hmap.put("message_id", message_id);
                            hmap.put("ref_id", ref_id);
                            hmap.put("content", content);
                            hmap.put("source", source);
                            hmap.put("uid", uid);
                            hmap.put("message_status", message_status);
                            hmap.put("user_type", user_type);
                            hmap.put("message_create_date", message_create_date);
                            hmap.put("first_name", first_name);
                            hmap.put("last_name", last_name);
                            hmap.put("user_image", user_image);
                            hmap.put("gender", gender);
                            hmap.put("user_thumbnail", user_thumbnail);
                            hmap.put("you_flag", you_flag);
                            hmap.put("you_like", you_like);
                            hmap.put("moo", moo1);
                            ///hmap.put("you_moods", you_moods);
                            hmap.put("comment_like", comment_like);
                            hmap.put("total_moods", moo1);
                            preferences.edit().putString("message_id", message_id).commit();
                            hmap.put("moods", moodList1);
                            ///hmap.put("mood_like", mood);
                            ///hmap.put("moo", moo);
                            groupList.add(hmap);

                            String time_stamp = resultObj.getString("time_stamp");
                            String has_more = resultObj.getString("has_more");
                            preferences.edit().putString("time_stamp", time_stamp).commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    Collections.reverse(groupList);

                    setAdapter();
                } else if (errCode.equals("300")) {

                    tv_no_comment.setVisibility(View.VISIBLE);
                    group_list.setVisibility(View.GONE);

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (scrolledPosition > 0) {
                scrollView.scrollTo(0, scrolledPosition);
            }

            //LoaderClass.HideProgressWheel(CrewActivity.this);
        }
    }

    public class PingServiceData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "ping_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", group_id);

                jo.put("source", "group");

                jo.put("time_stamp", preferences.getString("time_stamp", ""));

                Log.e("JSON DATA FOR PING = ", jo.toString());

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is PING  : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                String time_stamp = resultObj.getString("time_stamp");

                preferences.edit().putString("time_stamp", time_stamp).commit();

                if (errCode.equals("0")) {

                    boolScroolToBottom = true;
                    JSONArray jArray = resultObj.getJSONArray("message");

                    pingList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, Object> hmap = new HashMap<>();
                        HashMap<String, String> hmap1;
                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String message_id = user_dataObj.getString("message_id");
                        String ref_id = user_dataObj.getString("ref_id");
                        String content = user_dataObj.getString("content");
                        String source = user_dataObj.getString("source");
                        String uid = user_dataObj.getString("uid");
                        String message_status = user_dataObj.getString("message_status");
                        String user_type = user_dataObj.getString("user_type");
                        String message_create_date = user_dataObj.getString("message_create_date");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String gender = user_dataObj.getString("gender");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String you_flag = user_dataObj.getString("you_flag");
                        String you_like = user_dataObj.getString("you_like");
                        String comment_like = user_dataObj.getString("comment_like");

                        try {

                            String moods = "";

                            String count = "";

                            JSONArray jArrayy = user_dataObj.getJSONArray("moods");
                            moodList1 = new ArrayList<>();
                            for (int j = 0; j < jArrayy.length(); j++) {
                                hmap1 = new HashMap<>();

                                JSONObject user_dataObj1 = jArrayy.getJSONObject(j);
                                moods = user_dataObj1.getString("moods");

                                count = user_dataObj1.getString("con");

                                hmap1.put("con", count);
                                hmap1.put("moods", moods);
                                moodList1.add(hmap1);
                            }

                            JSONObject total_moods = user_dataObj.getJSONObject("total_moods");
                            String moo1 = total_moods.getString("moo");
                            JSONObject you_moods = user_dataObj.getJSONObject("you_moods");

                            String moo = you_moods.getString("moo");
                            String mood = you_moods.getString("moods");
                            hmap.put("message_id", message_id);
                            hmap.put("ref_id", ref_id);
                            hmap.put("content", content);
                            hmap.put("source", source);
                            hmap.put("uid", uid);
                            hmap.put("message_status", message_status);
                            hmap.put("user_type", user_type);
                            hmap.put("message_create_date", message_create_date);
                            hmap.put("first_name", first_name);
                            hmap.put("last_name", last_name);
                            hmap.put("user_image", user_image);
                            hmap.put("gender", gender);
                            hmap.put("user_thumbnail", user_thumbnail);
                            hmap.put("you_flag", you_flag);
                            hmap.put("you_like", you_like);
                            hmap.put("moo", moo1);
                            hmap.put("you_moods", you_moods);
                            hmap.put("comment_like", comment_like);
                            hmap.put("total_moods", moo1);
                            preferences.edit().putString("message_id", message_id).commit();
                            hmap.put("moods", moodList1);
                            hmap.put("mood_like", mood);
                            hmap.put("moo", moo);
                            groupList.add(hmap);

                            // String has_more = resultObj.getString("has_more");
                            preferences.edit().putString("time_stamp", time_stamp).commit();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                        /*{
                        HashMap<String, Object> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String message_id = user_dataObj.getString("message_id");
                        String ref_id = user_dataObj.getString("ref_id");
                        String content = user_dataObj.getString("content");
                        String source = user_dataObj.getString("source");
                        String uid = user_dataObj.getString("uid");
                        String message_status = user_dataObj.getString("message_status");
                        String message_create_date = user_dataObj.getString("message_create_date");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String user_type = user_dataObj.getString("user_type");
                        String comment_like = user_dataObj.getString("comment_like");
                        String you_like = user_dataObj.getString("you_like");

                        hmap.put("message_id", message_id);
                        hmap.put("ref_id", ref_id);
                        hmap.put("content", content);
                        hmap.put("source", source);
                        hmap.put("uid", uid);
                        hmap.put("message_status", message_status);
                        hmap.put("message_create_date", message_create_date);
                        hmap.put("first_name", first_name);
                        hmap.put("user_type", user_type);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("comment_like", comment_like);
                        hmap.put("you_like", you_like);

                        pingList.add(hmap);

                        groupList.add(groupList.size(),hmap);
                    }*/

                    setAdapter();

                } else if (errCode.equals("300")) {
                    boolScroolToBottom = false;

                    AlertDialog.Builder alert = new AlertDialog.Builder(CrewActivity.this);

                    alert.setCancelable(false);

                    alert.setTitle("");

                    alert.setMessage("Try Again");

                    alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();

                            startActivity(new Intent(CrewActivity.this, ArticleScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                            finish();
                        }
                    });

                    //alert.show();
                } else if (errCode.equals("700")) {
                    boolScroolToBottom = false;

                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class PostCommentData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(CrewActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_comments");

                jo.put("content", CommonUtilities.encodeUTF8(commentStr));

                if (encodedImage != null) ///
                    jo.put("image", encodedImage);

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", group_id);

                jo.put("source", "group");

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }

        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                String time_stamp = resultObj.getString("time_stamp");


                if (errCode.equals("0")) {
                    boolScroolToBottom = true;

                    preferences.edit().putString("time_stamp", time_stamp).commit();

                    JSONObject user_dataObj = resultObj.getJSONObject("comment");

                    String message_id = user_dataObj.getString("message_id");
                    String ref_id = user_dataObj.getString("ref_id");
                    String content = user_dataObj.getString("content");
                    String source = user_dataObj.getString("source");
                    String uid = user_dataObj.getString("uid");
                    String message_status = user_dataObj.getString("message_status");
                    String message_create_date = user_dataObj.getString("message_create_date");
                    String first_name = user_dataObj.getString("first_name");
                    String last_name = user_dataObj.getString("last_name");
                    String user_image = user_dataObj.getString("user_image");
                    String user_thumbnail = user_dataObj.getString("user_thumbnail");
                    String user_type = user_dataObj.getString("user_type");

                    HashMap<String, Object> hm2 = new HashMap<>();
                    hm2.put("content", content);
                    hm2.put("first_name", first_name);
                    hm2.put("last_name", last_name);
                    hm2.put("user_image", user_image);
                    hm2.put("message_id", message_id);
                    hm2.put("user_thumbnail", user_thumbnail);
                    hm2.put("source", source);
                    hm2.put("uid", uid);
                    hm2.put("ref_id", ref_id);
                    hm2.put("message_status", message_status);
                    hm2.put("user_type", user_type);
                    hm2.put("message_create_date", message_create_date);

                    groupList.add(groupList.size(), hm2);

                    setAdapter();

                    int total_commmments = Integer.parseInt(tv_msg_count.getText().toString().trim()) + 1;

                    tv_msg_count.setText("" + total_commmments);

                    if (!connDec.isConnectingToInternet()) {
                        //    alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new PushNotificationData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new PushNotificationData().execute();
                        }
                  /*      if (!connDec.isConnectingToInternet()) {
                            //    alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new CommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new CommentData().execute();
                            }
                        }*/
                    }
                    scrollView.fullScroll(View.FOCUS_DOWN);

                } else if (errCode.equals("700")) {
                    boolScroolToBottom = false;
                } else {
                    boolScroolToBottom = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            removeAttachedPicture(null);

            LoaderClass.HideProgressWheel(CrewActivity.this);

        }
    }

    public class PushNotificationData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "push_notification");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", group_id);

                jo.put("source", "group");


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            // pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class ViewGroupData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //   LoaderClass.ShowProgressWheel(CrewActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_detail");

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            if (!connDec.isConnectingToInternet()) {
                alert.showNetAlert();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

                    new CommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                } else {

                    new CommentData().execute();
                }
            }

            try {
                JSONObject resultObj = new JSONObject(result);
                Log.v("Viewgroupdata", resultObj.toString());

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    JSONObject user_dataObj = resultObj.getJSONObject("detail");
                    String uid1 = "";
                    String group_id = user_dataObj.getString("group_id");
                    groupname = CommonUtilities.decodeUTF8(user_dataObj.getString("group_name"));
                    grpdetail = CommonUtilities.decodeUTF8(user_dataObj.getString("group_detail"));
                    grpimg = user_dataObj.getString("group_image");
                    String group_type = user_dataObj.getString("group_type");
                    String group_tags = user_dataObj.getString("group_tags");
                    String you_like = user_dataObj.getString("you_like");
                    your_like = user_dataObj.getString("your_like");
                    owner_id = user_dataObj.getString("owner_id");
                    String owner_type = user_dataObj.getString("owner_type");
                    String you_group = user_dataObj.getString("you_group");

                    grptype = group_type;

                    updateHeartVisibility(you_like);

                    if (you_group.equals("")) {
                        // tv_heart.setClickable(false);
                        //iv_menu_icon.setClickable(false);
                        // iv_menu_icon1.setClickable(false);

                    }

                    if (!you_group.equals("")) {
                        JSONObject user_dataObj1 = new JSONObject(you_group);
                        String member_id = user_dataObj1.getString("member_id");
                        String group_id1 = user_dataObj1.getString("group_id");
                        uid = user_dataObj1.getString("uid");
                        member_status = user_dataObj1.getString("member_status");
                        mute_group = user_dataObj1.getString("mute_group");
                        is_fav = user_dataObj1.getString("is_fav");
                        String member_create_date = user_dataObj1.getString("member_create_date");
                        preferences.edit().putString("member_id", member_id).commit();
                    }

                    if (!member_status.equalsIgnoreCase("Active"))
                        tv_heart.setEnabled(false);
                    try {
                        jArrayy = new JSONArray();

                        jArrayy = user_dataObj.getJSONArray("moods");

                        updateSmiley(your_like, false);
                        //setAdapter();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    tv_mygroup.setText(CommonUtilities.decodeUTF8(groupname));
                    tv_subheading.setText(CommonUtilities.decodeUTF8(grpdetail));
                    Picasso.with(getApplicationContext()).load(grpimg).into(iv_groupImage);
                    iv_groupImage.setBorderColor(Color.parseColor("#F15A51"));
                    iv_groupImage.setBorderWidth(2);
                    preferences.edit().putString("owner_id", owner_id).commit();
                    preferences.edit().putString("YOU", you_like).commit();


                    if (isUnreadNotification.equals("yes")) {
                        isUnreadNotification = "";
                        Intent intent1 = new Intent(CrewActivity.this, RequestScreenActivity.class);
                        intent1.putExtra("group_id", group_id);
                        intent1.putExtra("member_id", preferences.getString("member_id", ""));
                        startActivity(intent1);

                    }


                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //  LoaderClass.HideProgressWheel(CrewActivity.this);
        }
    }

    private void updateHeartVisibility(final String you_like) {
        if (you_like != null) {
            if (you_like.equals("1")) {
                iv_heart_fill.setVisibility(View.GONE);
                tv_heart.setVisibility(View.GONE);
                rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSecondDialogSmiley(your_like, "2");
                    }
                });

            } else if (you_like.equals("0")) {
                iv_heart_fill.setVisibility(View.GONE);
                tv_heart.setVisibility(View.VISIBLE);
                tv_heart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSecondDialogSmiley(you_like, "1");
                    }
                });
            }

        }
    }

    private void updateSmiley(String your_like, boolean update) {

        if (jArrayy != null) {
            updateSmileyArray(your_like);

            updateOtherSmileys();

            inflateSmileys();
        }

        if (update == true && jArrayy.length() > 0) {
            int count = Integer.valueOf(tv_count.getText().toString().trim());
            count++;
            tv_count.setText(count + "");
        }
    }

    @SuppressLint("NewApi")
    private void updateSmileyArray(String your_like1) {
        boolean likecontains = false;

        for (int k = 0; k < jArrayy.length(); k++) {
            try {
                JSONObject curntObj = jArrayy.getJSONObject(k);

                if (curntObj.get("moods").equals(this.your_like)) {
                    curntObj.remove("isYour");

                    if (curntObj.has("isNew")) {
                        try {
                            jArrayy.remove(k);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        curntObj = null;
                    }
                }

                if (curntObj != null) {
                    if (curntObj.get("moods").equals(your_like1)) {
                        curntObj.put("isYour", "true");

                        likecontains = true;
                    }

                    jArrayy.put(k, curntObj);
                }

                int countArray = jArrayy.length();

                if (countArray > 0) {
                    countArray = countArray - 1;
                }
                if (k == countArray) {
                    if (!likecontains) {
                        JSONObject newObj1 = new JSONObject();

                        newObj1.put("isYour", "true");

                        newObj1.put("isNew", "true");

                        newObj1.put("moods", your_like1);

                        newObj1.put("modcount", 1);

                        jArrayy.put(newObj1);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        this.your_like = your_like1;
    }

    private void updateOtherSmileys() {
        mainMapList.clear();

        final int numberOfItemsInResp = jArrayy.length();

        for (int j = 0; j < numberOfItemsInResp; j++) {
            try {
                mainMap = new HashMap<>();

                JSONObject user_dataObj1 = jArrayy.getJSONObject(j);

                String moods = user_dataObj1.getString("moods");

                String count = user_dataObj1.getString("modcount");

                HashMap<String, Object> hmap = new HashMap<>();

                hmap.put(moods, count);

                mainMap.put("moods", moods);

                mainMap.put("count", count);

                if (your_like.equalsIgnoreCase(moods)) {
                    mainMap.put("isYour", "true");
                    user_dataObj1.put("isYour", "true");

                    if (count.equals("1")) {
                        mainMap.put("isNew", "true");

                        user_dataObj1.put("isNew", "true");
                    }

                    jArrayy.put(j, user_dataObj1);
                }


                mainMapList.add(mainMap);

                groupList.add(hmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void hideAllSmileys() {
        iv_smiley1.setVisibility(View.GONE);

        iv_smiley2.setVisibility(View.GONE);

        iv_smiley3.setVisibility(View.GONE);

        iv_smiley4.setVisibility(View.GONE);

        iv_smiley5.setVisibility(View.GONE);
    }

    private void inflateSmileys() {
        hideAllSmileys();

        for (int x = 0; x < mainMapList.size(); x++) {

            final HashMap<String, Object> mapHash = mainMapList.get(x);

            String moods = mapHash.get("moods").toString();

            String count = mapHash.get("count").toString();

            if (moods.equalsIgnoreCase("happy")) {
                iv_smiley1.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley1.setImageResource(R.drawable.happy);
                } else {
                    iv_smiley1.setImageResource(R.drawable.happy);
                }
            } else if (moods.equalsIgnoreCase("indifferen")) {
                iv_smiley2.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley2.setImageResource(R.drawable.indifferent);
                } else {
                    iv_smiley2.setImageResource(R.drawable.indifferent);
                }
            } else if (moods.equalsIgnoreCase("amazing")) {
                iv_smiley3.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley3.setImageResource(R.drawable.amazing);
                } else {
                    iv_smiley3.setImageResource(R.drawable.amazing);
                }
            } else if (moods.equalsIgnoreCase("love")) {
                iv_smiley4.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley4.setImageResource(R.drawable.love);
                } else {
                    iv_smiley4.setImageResource(R.drawable.love);
                }
            } else if (moods.equalsIgnoreCase("sad")) {
                iv_smiley5.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley5.setImageResource(R.drawable.sad);
                } else {
                    iv_smiley5.setImageResource(R.drawable.sad);
                }
            }
        }
    }

    public void showSecondDialogSmiley(String your_like, final String val) {

        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0f);
        dialog.setContentView(R.layout.moods_activity_one);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        for (int x = 0; x < mainMapList.size(); x++) {

            final HashMap<String, Object> mapHash = mainMapList.get(x);

            String moods = mapHash.get("moods").toString();

            String count = mapHash.get("count").toString();

            tempPos = x;

            /*your_like = "";

            your_like = "" + mapHash.get("your_like");*/

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");

                    break;

                case "indifferen":

                    count_indiff.setText("(" + count + ")");

                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");

                    break;
                case "love":
                    count_love.setText("(" + count + ")");

                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");

                    break;

            }
        }
        if (!your_like.equals("")) {

            if (your_like.equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }
        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);

                if (moodStr == "happy") {
                    iv_smiley1.setVisibility(View.VISIBLE);
                }

                moodStr = "happy";

                callAPI(val);

                dialog.dismiss();


            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "indifferen") {
                    iv_smiley2.setVisibility(View.VISIBLE);
                }

                moodStr = "indifferen";

                callAPI(val);

                dialog.dismiss();


            }

        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "amazing") {
                    iv_smiley3.setVisibility(View.VISIBLE);
                }
                moodStr = "amazing";

                callAPI(val);

                dialog.dismiss();


            }

        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "love") {
                    iv_smiley4.setVisibility(View.VISIBLE);
                }
                moodStr = "love";

                callAPI(val);

                dialog.dismiss();


            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "sad") {
                    iv_smiley5.setVisibility(View.VISIBLE);
                }
                moodStr = "sad";

                callAPI(val);

                dialog.dismiss();


            }
        });

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    private void callAPI(String val) {
        if (val.equals("1")) {
            if (!connDec.isConnectingToInternet()) {
                //    alert.showNetAlert();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, group_id, "1", "" + tempPos);
                } else {
                    new LikeServiceData1().execute(moodStr, group_id, "1", "" + tempPos);
                }
            }
        } else if (val.equals("2")) {
            if (!connDec.isConnectingToInternet()) {
                //    alert.showNetAlert();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, group_id, "1", "" + tempPos);
                } else {
                    new ChangePostData().execute(moodStr, group_id, "1", "" + tempPos);
                }
            }
        }
    }

    public class ChangePostData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            //   position = Integer.parseInt(strings[1]);

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "change_post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "group");

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {


                    //  updateSmiley(moodString, true);

                    callMainApis();


                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class GroupmemberData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_member");

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //  pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                Log.v("GroupmemberData", resultObj.toString());

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");
                String you_like = resultObj.getString("you_like");
                String total_like = resultObj.getString("total_like");
                String total_comment = resultObj.getString("total_comment");


                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");
                    memberlist.clear();
                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();
                        //  JSONObject user_dataObj = jArray.getJSONObject(i);

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String uid1 = "";
                        String member_id = user_dataObj.getString("member_id");
                        String group_id = user_dataObj.getString("group_id");
                        uid1 = user_dataObj.getString("uid");
                        String member_status = user_dataObj.getString("member_status");
                        String mute_group = user_dataObj.getString("mute_group");
                        String member_create_date = user_dataObj.getString("member_create_date");
                        String profile_id = user_dataObj.getString("profile_id");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");

                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String hobbies = user_dataObj.getString("hobbies");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String batch = user_dataObj.getString("batch");
                        String course = user_dataObj.getString("course");
                        String total_pelcians = user_dataObj.getString("total_pelcians");
                        hmap.put("profile_id", profile_id);
                        hmap.put("uid", uid1);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("course", course);
                        hmap.put("total_pelcians", total_pelcians);

                        hmap.put("member_create_date", member_create_date);
                        hmap.put("mute_group", mute_group);

                        hmap.put("member_status", member_status);
                        hmap.put("group_id", group_id);

                        hmap.put("member_id", member_id);

                        hmap.put("batch", batch);
                        hmap.put("year_of_passing", year_of_passing);

                        // preferences.edit().putString("uid", uid1).commit();
                        preferences.edit().putString("gender", gender).commit();

                        memberlist.add(hmap);

                    }
                    horizontalAdapter = new HorizontalAdapter(CrewActivity.this, memberlist);
                    rvImages.setAdapter(horizontalAdapter);
                    tv_count.setText(total_like);
                    tv_msg_count.setText(total_comment);

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {

        callMainApis();

        super.onStart();

        getAnalyticTracker().setScreenName("Group Detail Screen");

        FlurryAgent.onStartSession(CrewActivity.this);
    }

    private void showCustomMessagephoto() {
        final Dialog dialogphoto = new Dialog(CrewActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        if (apiLevel >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(CrewActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(CrewActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequestForGallery();
                            } else {
                                openGallery();
                            }
                        } else {
                            openGallery();
                        }
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        if (apiLevel >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(CrewActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(CrewActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequestForCamera();
                            } else {
                                takePicture();
                            }
                        } else {
                            takePicture();
                        }

                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();

    }

    private void makeRequestForCamera() {
        ActivityCompat
                .requestPermissions
                        (CrewActivity.this,
                                new String[]{android.Manifest.permission.CAMERA, Constants.ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE},
                                Constants.REQUEST_CODE_TAKE_PICTURE);
    }

    private void makeRequestForGallery() {
        ActivityCompat
                .requestPermissions
                        (CrewActivity.this,
                                new String[]{Constants.ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE, Constants.ANDROID_PERMISSION_READ_EXTERNAL_STORAGE},
                                Constants.REQUEST_CODE_GALLERY);
    }

    @SuppressLint("Override")
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.REQUEST_CODE_TAKE_PICTURE:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(CrewActivity.this, Constants.PERMISSION_HAS_BEEN_DENIED_BY_USER, Toast.LENGTH_SHORT).show();

                } else {
                    takePicture();
                }
                break;

            case Constants.REQUEST_CODE_GALLERY:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(CrewActivity.this, Constants.PERMISSION_HAS_BEEN_DENIED_BY_USER, Toast.LENGTH_SHORT).show();

                } else {
                    openGallery();
                }
                break;
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);

        photoPickerIntent.setType("userImage/*");

        startActivityForResult(photoPickerIntent, Constants.REQUEST_CODE_GALLERY);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            checkExternalStorageAndCreateTempFile();

            Uri mImageCaptureUri;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(tempImageFile);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, Constants.REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeAttachedPicture(View v) { ///
        findViewById(R.id.rellay_attachment).setVisibility(View.GONE);

        ImageView thumbmailImageView = (ImageView) findViewById(R.id.iv_attached_image);

        thumbmailImageView.setImageDrawable(null);

        encodedImage = null;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (tempImageFile != null)
            outState.putSerializable("tempImageFile", tempImageFile);
    }

    @Override
    protected void onStop() {
        super.onStop();

        removeAttachedPicture(null); ///

        FlurryAgent.onEndSession(CrewActivity.this);
        timer.cancel();
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();


    }
}


