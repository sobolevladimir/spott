package com.spott.app;

/**
 * Created by Adimn on 9/30/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.spott.app.chip.TokenCompleteTextView;



public class ContactsCompletionView extends TokenCompleteTextView<Person> {

    Context context;

    public ContactsCompletionView(Context context) {
        super(context);

        this.context = context;
    }

    public ContactsCompletionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public ContactsCompletionView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;


    }

    @Override
    protected View getViewForObject(final Person person) {
        LayoutInflater l = (LayoutInflater)getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        TokenTextView token = (TokenTextView) l.inflate(R.layout.contact_token, (ViewGroup) getParent(), false);
        token.setEllipsize(null);
        token.setText(person.getEmail());


      /*  TokenTextView name = (TokenTextView)findViewById(R.id.name);
        name.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Remove" , Toast.LENGTH_SHORT).show();
                removeObject(person);
            }
        });*/
        return token;
    }

    @Override
    protected Person defaultObject(String completionText) {
        //Stupid simple example of guessing if we have an email or not
        int index = completionText.indexOf('@');
        if (index == -1) {
            return new Person(completionText, completionText.replace(" ", ""));
        } else {
            return new Person(completionText.substring(0, index), completionText);
        }
    }
}

