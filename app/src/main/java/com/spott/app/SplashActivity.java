package com.spott.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.testfairy.TestFairy;
import com.spott.app.common.CommonUtilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

/**
 * Created by Adimn on 10/5/2016.
 */
public class SplashActivity extends Activity {

    private SharedPreferences preferences;
    private static final String TAG = "MyFirebaseIIDService";
    boolean opened = false;
    private String version;
    com.spott.app.ConnectionDetector connDec;
    com.spott.app.Alert_Dialog_Manager alert;
    private boolean versionOk = false;
    private AdView mAdView;
    Context context;
    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.

    //Database database;


    class fetchingToken extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {

            try {
                String authorizedEntity = "223670287681"; // Project id from Google Developer Console
                String scope = "FCM"; // e.g. communicating using GCM, but you can use any
                InstanceID instanceID = InstanceID.getInstance(SplashActivity.this);
                String token = instanceID.getToken(authorizedEntity,
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                System.out.println("TOKENNN  " + token);

                preferences.edit().putString("TOKEN", token).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        chandeStatusBarColor(Color.parseColor("#ffffff"));
        context=SplashActivity.this;
        connDec = new com.spott.app.ConnectionDetector(SplashActivity.this);
        alert = new com.spott.app.Alert_Dialog_Manager(context);
        TextView textView = (TextView) findViewById(R.id.versioTV);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            String version = "Version:" + pInfo.versionName;

           textView.setText(version);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        TestFairy.begin(this, "b64dc110c4943170bc84b278a3bdebd8fe0d1ec0");

        new fetchingToken().execute();

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (!preferences.getString("login_token", "").equals("")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new FetchBannedWords().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new FetchBannedWords().execute();
                }
            }
        }
        System.out.println("HERE !");

        Thread mSplashThread = new Thread() {
            @Override
            public void run() {
                synchronized (this) {
                    try {

                        wait(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    openNewClass();

                    if (checkLocationEnabled(SplashActivity.this)) {
                        if (CommonUtilities.checkPermission_location(SplashActivity.this) == true) {

                            // checkVersionOnServer();


                        }
                    }
                }
            }
        };

        mSplashThread.start();

    }

    private void checkVersionOnServer() {
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            version = String.valueOf(pInfo.versionCode);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                new CheckVersion().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

             else
                new CheckVersion().execute();

        }
        catch (Exception e1)
        {
            e1.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (opened == true) {
            if (checkLocationEnabled(SplashActivity.this)) {
                if (CommonUtilities.checkPermission_location(SplashActivity.this) == true) {
                    openNewClass();
                }
            }
        }
    }

    private void openNewClass()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                /*if (versionOk)
                {*/
                    if (getSharedPreferences("UTC", MODE_PRIVATE).getString("login_token", "").equals(""))
                    {
                        if (!getSharedPreferences("UTC", MODE_PRIVATE).getString("email", "").equals(""))
                        {
                            startActivity(new Intent(SplashActivity.this, RegisterConfirmationScreen.class));
                            finish();
                        }
                        else
                        {
                            startActivity(new Intent(SplashActivity.this, MainActivity.class));
                            finish();
                        }

                    }
                    else
                    {
                        if (getSharedPreferences("UTC", MODE_PRIVATE).getString("password_change", "").equals("yes")) {
                            startActivity(new Intent(SplashActivity.this, com.spott.app.ResetPssword.class).putExtra("values", ""));
                        }
                            else {
                            startActivity(new Intent(SplashActivity.this, com.spott.app.MyFeedActivity.class));
                        }

                        finish();
                    }
               /* }
                else
                {
                    checkVersionOnServer();
                }*/
            }
        });


    }

    void checkLocationPermission() {
        int permission = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION}, 3);
        } else {
            Intent intent = new Intent(SplashActivity.this, com.spott.app.LoginScreenActivity.class);
            startActivity(intent);
            // InitCurrentLocation();
        }

    }


    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CommonUtilities.MY_PERMISSIONS_REQUEST_FINE_LOCATION:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //if(new CommonUtilities().checkLocationEnabled(Splash.this))
                    {

                        openNewClass();
                        opened = true;
                    }

                } else {

                }
                break;
        }
    }

    public boolean checkLocationEnabled(final Activity context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(context);
            dialog.setMessage("Your GPS is not enabled.");
            dialog.setCancelable(false);
            dialog.setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);

                    opened = true;
                    //get gps
                }
            });
            dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    openNewClass();

                }
            });

            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                 //  dialog.show();
                    openNewClass();
                }
            });


            return false;
        } else {
            return true;
        }
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        MultiDex.install(this);

    }

    public class FetchBannedWords extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;

        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "banned_words");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray banned_list = resultObj.getJSONArray("list");

                    for (int i = 0; i < banned_list.length(); i++) {

                        HashMap<String, Object> hash = new HashMap<>();

                        JSONObject object = banned_list.getJSONObject(i);

                        hash.put("banned_id", object.getString("banned_id"));

                        hash.put("banned_word", object.getString("banned_word"));

                        hash.put("banned_create_date", object.getString("banned_create_date"));

                        ((com.spott.app.UTCAPP) getApplication()).bannedWordsList.add(object.getString("banned_word"));

                        ((com.spott.app.UTCAPP) getApplication()).bannedWordsDetailList.add(hash);

                    }

                } else if (errCode.equals("700")) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class CheckVersion extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;

        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "version_update");

                jo.put("device_type", "android");

                jo.put("version", version);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            try {
                final JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    final AlertDialog.Builder alert = new AlertDialog.Builder(SplashActivity.this);

                    alert.setCancelable(false);

                    alert.setTitle("UTC U-NITE APP");

                    alert.setMessage("A newer version of app is available on the server. Please update.");

                    alert.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();

                            try {
                                String linkUpdate = resultObj.getString("update");

                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(linkUpdate));

                                startActivity(browserIntent);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            alert.show();
                        }
                    });


                } else if (errCode.equals("300")) {
                    versionOk = true;

                    openNewClass();
                } else if (errCode.equals("700")) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Splash Screen");

        FlurryAgent.onStartSession(SplashActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(SplashActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        com.spott.app.UTCAPP myApplication = (com.spott.app.UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    private void chandeStatusBarColor(int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }
}



