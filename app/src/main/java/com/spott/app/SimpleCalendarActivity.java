package com.spott.app;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.stacktips.view.CalendarListener;
import com.stacktips.view.CustomCalendarView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by artman2111 on 23.09.17.
 */

public class SimpleCalendarActivity extends Activity implements View.OnClickListener/*, AbsListView.OnScrollListener*/ {




    TextView iv_red_icon, tv_num;
    Activity activity;
    DiamondImageView img_profile;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout rlMainFront, ll_groups;


    LayoutInflater layoutInflater = null;

    Context context;

    View view = null;
    int height, width;
    boolean drawer_open = false;

    SharedPreferences preferences;

    int count = 0;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    ImageView iv_menu_icon;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory;
    long last_text_edit = 0;
    long idle_min = 1500; // 4 seconds after user stops typing

    Handler h = new Handler();
    boolean showDropDown = true;
    boolean already_queried = false;



    String firstname = "", lastname = "", star = "", group = "", post = "", method = "";

    String has_more = "", feed_count = "";


    String encoded;
    String et_mailtr = "", gender = "", gender1 = "";


    private List<ImageView> dots, dotsNavigate;

    private boolean isLoading = false;


    CustomCalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //Initialize CustomCalendarView from layout
        calendarView = (CustomCalendarView) findViewById(R.id.calendar_view);

        //Initialize calendar with date
        Calendar currentCalendar = Calendar.getInstance(Locale.getDefault());

        //Show monday as first date of week
        calendarView.setFirstDayOfWeek(Calendar.MONDAY);

        //Show/hide overflow days of a month
        calendarView.setShowOverflowDate(false);

        //call refreshCalendar to update calendar the view
        calendarView.refreshCalendar(currentCalendar);

        //Handling custom calendar events
        calendarView.setCalendarListener(new CalendarListener() {
            @Override
            public void onDateSelected(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                Toast.makeText(SimpleCalendarActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMonthChanged(Date date) {
                SimpleDateFormat df = new SimpleDateFormat("MM-yyyy");
                Toast.makeText(SimpleCalendarActivity.this, df.format(date), Toast.LENGTH_SHORT).show();
            }
        });





        context = getApplicationContext();




//        addDotsNavigation();

        layoutInflater = LayoutInflater.from(this);

        initialise();

        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");
        gender = preferences.getString("gender", "");

        clickables();
//        if (!connDec.isConnectingToInternet()) {
//            alert.showNetAlert();
////        } else {
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
////                new MyDirectoryActivity.DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////            } else {
////                new MyDirectoryActivity.DirectoryListData().execute();
////            }
//        }
//        if (!connDec.isConnectingToInternet()) {
//            alert.showNetAlert();
////        } else {
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
////                new MyDirectoryActivity.FeaturedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
////            } else {
////                new MyDirectoryActivity.FeaturedData().execute();
////            }
//        }

        setting();

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        //     view.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }





    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_directory.setBackgroundColor(Color.parseColor("#FF7F60"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#ffffff"));
    }


    private void initialise() {
        alert = new Alert_Dialog_Manager(context);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);

        //     view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");


    }

//    @Override
//    public void onResume() {

//        super.onResume();
//        if (!connDec.isConnectingToInternet()) {
    //    alert.showNetAlert();
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                new MyDirectoryActivity.GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            } else {
//                new MyDirectoryActivity.GroupFavData().execute();
//            }
//        }
//    }

    /// final int pos = 1;
//        pager.postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
//            }
//        }, 100);
//
//        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//
//                ((UTCAPP) getApplication()).pagerPos = position;
//
//                selectDotNavigation(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//    }

//    @Override
//    public void onBackPressed(){
//        super.onBackPressed();
//    }

// For Navigation Animation

//    private class NavigationFragmentAdapter extends PagerAdapter {
//
//        TextView tv_grp1, tv_grp2, tv_grp3, tv_grp4, tv_grp5, tv_grp6, tv_browsing;
//        ImageView iv_group_add_icon;
//        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront;
//        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_profile_name, tv_profile_username, tv_notification_count, tv_update, tv_feed, tv_profile, tv_groups, tv_staff_dir, tv_log_out;
//
//        @Override
//        public int getCount() {
//            return 2;
//        }
//
//        @Override
//        public boolean isViewFromObject(View view, Object object) {
//            return view == ((View) object);
//        }
//
//        @Override
//        public void notifyDataSetChanged() {
//            if (feed_count.equals("0"))
//                tv_notification_count.setVisibility(View.GONE);
//
//            else {
//                tv_notification_count.setVisibility(View.VISIBLE);
//
//                tv_notification_count.setText(feed_count);
//            }
//
//        }
//
//        @Override
//        public Object instantiateItem(final ViewGroup container, final int position) {
//            View view = null;
//
//            if (position == 0) {
//                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
//                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
//                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);
//
//                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(MyDirectoryActivity.this, AddGroupActivity.class);
//                        startActivity(intent);
//                    }
//                });
//                if (!connDec.isConnectingToInternet()) {
//                    //    alert.showNetAlert();
//                } else {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        new MyDirectoryActivity.GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    } else {
//                        new MyDirectoryActivity.GroupFavData().execute();
//                    }
//                }
//                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {
//
//                    @Override
//                    public void onLastItemVisible() {
//
//                    }
//                });
//                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
//                    @Override
//                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {
//
//                        count = 0;
//                        if (!connDec.isConnectingToInternet()) {
//                            //    alert.showNetAlert();
//                        } else {
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                                new MyDirectoryActivity.GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                            } else {
//                                new MyDirectoryActivity.GroupFavData().execute();
//                            }
//                        }
//
//                        //    mygrouplist.clear();
//
//                    }
//                });
//
//                tv_browsing.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(MyDirectoryActivity.this, JoinGroupActivity.class);
//                        startActivity(intent);
//                    }
//                });
//                container.addView(view, 0);
//
//            } else if (position == 1) {
//                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
//                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
//                white_box = (TextView) view.findViewById(R.id.white_box);
//                orange_box = (TextView) view.findViewById(R.id.orange_box);
//                white_box1 = (TextView) view.findViewById(R.id.white_box1);
//                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
//                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/utc/app/fonts/OpenSans_Bold.ttf"));
//                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
//                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/utc/app/fonts/OpenSans_Light.ttf"));
//                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
//                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
//                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
//                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
//                tv_update = (TextView) view.findViewById(R.id.tv_update);
//                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
//                tv_log_out = (TextView) view.findViewById(R.id.tv_logout);
//                tv_post = (TextView) view.findViewById(R.id.tv_post);
//                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
//                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
//                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
//                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
//                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
//                tv_profile_name.setText(CommonUtilities.decodeUTF8(preferences.getString("first_name", "")) + " " + CommonUtilities.decodeUTF8(preferences.getString("last_name", "")));
//                white_box1.setText(post);
//                white_box.setText(group);
//                orange_box.setText(preferences.getString("pelican", "0"));
//                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
//                if (feed_count.equals("0"))
//                    tv_notification_count.setVisibility(View.GONE);
//
//                else {
//                    tv_notification_count.setVisibility(View.VISIBLE);
//
//                    tv_notification_count.setText(feed_count);
//                }
//                if (encoded.equalsIgnoreCase("")) {
//
//                    img_profile.setImageResource(R.drawable.app);
//                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
//                    img_profile.setBorderWidth(2);
//                } else {
//                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
//                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
//                    img_profile.setBorderWidth(2);
//                }
//                tv_profile.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent1 = new Intent(MyDirectoryActivity.this, MyProfileActivity.class);
//                        intent1.putExtra("uid", preferences.getString("uid", ""));
//                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent1);
//
//                    }
//                });
//
//                tv_notification_count.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent = new Intent(MyDirectoryActivity.this, UpdatesActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }
//                });
//                white_box.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent2 = new Intent(MyDirectoryActivity.this, MyCrewsActivity.class);
//                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent2);
//
//                    }
//                });
//                tv_groupss.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent2 = new Intent(MyDirectoryActivity.this, MyCrewsActivity.class);
//                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent2);
//                    }
//                });
//                orange_box.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        CommonUtilities.showStarsPopup(activity);
//                    }
//                });
//
//                img_profile.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent1 = new Intent(MyDirectoryActivity.this, MyProfileActivity.class);
//                        intent1.putExtra("uid", preferences.getString("uid", ""));
//                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent1);
//                    }
//                });
//                tv_groups.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent = new Intent(MyDirectoryActivity.this, MyCrewsActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }
//                });
//                tv_update.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent = new Intent(MyDirectoryActivity.this, UpdatesActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }
//                });
//                tv_feed.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//                        Intent intent = new Intent(MyDirectoryActivity.this, MyFeedActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                        startActivity(intent);
//                    }
//                });
//                tv_log_out.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        StopDrawerAnim();
//
//                        customShareLogOutPopup();
//                    }
//                });
//
//                container.addView(view, 0);
//            }
//            //   ((ViewPager) container).addView(view, 0);
//
//            return view;
//        }
//
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            ((ViewPager) container).removeView((View) object);
//        }
//
//
//    }






//    private String base64frombitmap(Bitmap bitmap) {
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
//        byte[] byteArray = byteArrayOutputStream.toByteArray();
//
//        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
//
//        return encoded;
//    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }


    private void clickables() {

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();

                    drawer_open = true;
//                    pager.setAdapter(nadapter);
//                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
//
//                    mAdapter.notifyDataSetChanged();
                    if (!(preferences.getString("user_image", "").equals(""))) {
                        Picasso.with(getApplicationContext()).load(preferences.getString("user_image", "")).into(img_profile);
                        img_profile.setBorderColor(Color.parseColor("#F15A51"));
                        img_profile.setBorderWidth(2);
                    }

                } else {
                    StopDrawerAnim();
                }

            }
        });




        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SimpleCalendarActivity.this, UpdatesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });



    }

    private void callAPI() {
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
//        } else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                new MyDirectoryActivity.search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            } else {
//                new MyDirectoryActivity.search_help().execute();
//            }
        }

    }




    @Override
    public void onClick(View view) {

        Handler handler = new Handler();

        switch (view.getId()) {


            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:
                Intent intent = new Intent(SimpleCalendarActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(SimpleCalendarActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(SimpleCalendarActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:

//                Intent intent3 = new Intent(MyDirectoryActivity.this, SimpleCalendarActivity.class);
//               // intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                startActivity(intent3);

                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));

                break;
        }
    }







    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }











    private void showDrop() {
        try {
            if (showDropDown == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!SimpleCalendarActivity.this.isFinishing()) {
                            // iv_serach_view.showDropDown();
                        }
                    }
                }, 500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class LogOutData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);
                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    {
                        preferences.edit().clear().commit();
                        ((UTCAPP) getApplication()).joingroupList.clear();

                        Intent i = new Intent(activity, SplashActivity.class);
                        startActivity(i);
                        finishAffinity();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SimpleCalendarActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(SimpleCalendarActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();

                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
//                } else {
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        new SimpleCalendarActivity().LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    } else {
//                        new SimpleCalendarActivity().LogOutData().execute();
//                    }
                }
            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("My Directory Screen");

        FlurryAgent.onStartSession(SimpleCalendarActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(SimpleCalendarActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }



}