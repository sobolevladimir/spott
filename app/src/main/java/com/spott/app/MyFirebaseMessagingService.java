package com.spott.app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Adimn on 11/3/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "UTC APP FireBase";
    String type = "", value = "", msg = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {//Displaying data in log
        //It is optional
        Log.d(TAG, "From: " + remoteMessage.getData());

        type = remoteMessage.getData().get("action");
        value = remoteMessage.getData().get("value");
        msg = remoteMessage.getData().get("message");

        if (type == null)
            type = "";

        if (value == null)
            value = "";

        if (msg == null)
            msg = "";

        //Calling method to generate notification
        sendNotification(msg);
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String messageBody) {


        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.app);

        Intent intent = null;
        if (type.equalsIgnoreCase("post")) {
            intent = new Intent(this, ArticleScreenActivity.class);
            intent.putExtra("scrollTobottom", "TRUE");
            intent.putExtra("post_id", value);

        } else if (type.equalsIgnoreCase("group") || type.equals("group_request")) {
            intent = new Intent(this, CrewActivity.class);
            intent.putExtra("group_id", value);
            intent.putExtra("scrollTobottom", "TRUE");
            intent.putExtra("from_update", "");
            if (type.equals("group_request")) {
                intent.putExtra("isUnreadNotification", "yes");
            }

        } else if (type.equalsIgnoreCase("invite_group")) {
            intent = new Intent(this, InviteActivity.class);

        } else if (type.equalsIgnoreCase("survey")) {
            intent = new Intent(this, SurveyScreenActivity.class);
            intent.putExtra("value", value);
        } else if (type.equalsIgnoreCase("common_push")) {
            intent = new Intent(this, UpdatesActivity.class);
            intent.putExtra("value", value);
        }
        if (intent != null) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.utc_organ_chord);
            // Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setLargeIcon(Bitmap.createBitmap(bm))
                    .setContentTitle("U-NITE")
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(sound)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(Integer.valueOf((int)System.currentTimeMillis()), notificationBuilder.build());
        }
    }

}