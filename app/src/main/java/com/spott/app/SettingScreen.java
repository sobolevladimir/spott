package com.spott.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Acer on 9/21/2016.
 */
public class SettingScreen extends Activity implements View.OnClickListener{

    ImageView iv_back_icon,iv_red_icon;
    Activity activity;
    ImageView iv_my_feed,iv_profile,iv_groups,iv_directory;
    TextView tv_fed,tv_profile_txt,tv_my_group,tv_staf_directory,tv_number,tv_num;
    RelativeLayout ll_groups;
    LinearLayout ll_feed, ll_profile, ll_directory;
    SharedPreferences preferences;
    BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        this.activity = (Activity) SettingScreen.this;
        initialise();
        clickables();
        ll_profile.setBackgroundColor(Color.parseColor("#B4BC35"));
        iv_profile.setBackgroundResource(R.drawable.sel_btm_myprofile);
        tv_profile_txt.setTextColor(Color.parseColor("#ffffff"));
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);

    }
    private void initialise() {

        iv_back_icon=(ImageView)findViewById(R.id.iv_back_icon);
        iv_red_icon=(ImageView)findViewById(R.id.iv_red_icon);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        iv_my_feed=(ImageView)findViewById(R.id.iv_my_feed);
        iv_profile=(ImageView)findViewById(R.id.iv_profile);
        iv_groups=(ImageView)findViewById(R.id.iv_groups);
        iv_directory=(ImageView)findViewById(R.id.iv_directory);
        tv_number = (TextView) findViewById(R.id.tv_num);
        tv_my_group=(TextView)findViewById(R.id.tv_my_group);
        tv_fed=(TextView)findViewById(R.id.tv_fed);
        tv_profile_txt=(TextView)findViewById(R.id.tv_profile_txt);
        tv_staf_directory=(TextView)findViewById(R.id.tv_staf_directory);
        tv_num=(TextView)findViewById(R.id.tv_num);
        preferences=getSharedPreferences("UTC",MODE_PRIVATE);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    /*int i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }*/
                    tv_num.setText(new_message);
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }

    private void clickables() {

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SettingScreen.this,MyProfileActivity.class);
                intent.putExtra("uid",preferences.getString("uid",""));
                startActivity(intent);
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(SettingScreen.this,MyProfileActivity.class);
                intent.putExtra("uid",preferences.getString("uid",""));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:

                ll_feed.setBackgroundColor(Color.parseColor("#FFAD3F"));
                iv_my_feed.setBackgroundResource(R.drawable.sel_btm_myfeed);
                tv_fed.setTextColor(Color.parseColor("#ffffff"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent = new Intent(SettingScreen.this, MyFeedActivity.class);
                startActivity(intent);

                break;

            case R.id.ll_profile:

                ll_profile.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent1 = new Intent(SettingScreen.this, MyFeedActivity.class);
                startActivity(intent1);

                break;

            case R.id.ll_groups:

                ll_groups.setBackgroundColor(Color.parseColor("#fff3e5"));
                iv_groups.setBackgroundResource(R.drawable.sel_btm_mygroups);
                tv_my_group.setTextColor(Color.parseColor("#ffffff"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent2 = new Intent(SettingScreen.this, JoinGroupActivity.class);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:

                ll_directory.setBackgroundColor(Color.parseColor("#FF7F60"));
                iv_directory.setBackgroundResource(R.drawable.sel_btm_staff);
                tv_staf_directory.setTextColor(Color.parseColor("#ffffff"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent3 = new Intent(SettingScreen.this, MyDirectoryActivity.class);
                startActivity(intent3);

                break;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Setting Screen");

        FlurryAgent.onStartSession(SettingScreen.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(SettingScreen.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}
