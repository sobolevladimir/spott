package com.spott.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.spott.app.services.api.FacebookLoginResponse;
import com.spott.app.services.api.FacebookTokenLogin;
import com.spott.app.services.api.RetrofitBuilder;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity {

    TestFragmentAdapter mAdapter;
    ViewPager mPager;
    String string = "#ed3224";
    String string1 = "#ffffff";
    String token;
    LayoutInflater layoutInflater = null;
    SharedPreferences preferences;
    ConnectionDetector connDec;


    TextView register_22, login_22;

    LoginButton loginFacebook;
    private ArrayList<ImageView> dots;
    CallbackManager _callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new TestFragmentAdapter();
        mPager = (ViewPager) findViewById(R.id.pager);
        login_22 = (TextView) findViewById(R.id.login_22);
        register_22 = (TextView) findViewById(R.id.register_22);
        loginFacebook = (LoginButton) findViewById(R.id.login_fb);
        mPager.setAdapter(mAdapter);

        layoutInflater = LayoutInflater.from(this);
        initialise();
        clickables();

        addDots();
        selectDot(0);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                selectDot(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void initialise() {
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        token = preferences.getString("TOKEN", "");
        _callbackManager = CallbackManager.Factory.create();
    }


    private void clickables() {
        login_22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginScreenActivity.class);
                startActivity(intent);
            }
        });

        register_22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterScreenActivity.class);
                startActivity(intent);
            }
        });

        LoginManager.getInstance().logOut();
        loginFacebook.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        loginFacebook.registerCallback(_callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String accessToken = loginResult.getAccessToken().getToken();
                final FacebookTokenLogin request = new FacebookTokenLogin(MainActivity.this, accessToken, token);
                RetrofitBuilder.getSpottApi().loginWithFacebook(request).enqueue(new Callback<FacebookLoginResponse>() {
                    @Override
                    public void onResponse(Call<FacebookLoginResponse> call, Response<FacebookLoginResponse> response) {
                        processLogin(response.body());
                    }

                    @Override
                    public void onFailure(Call<FacebookLoginResponse> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this, "Login via Facebook canceled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this, "Failed to login via Facebook", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        _callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private class TestFragmentAdapter extends PagerAdapter {
        TextView tv_text;

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.loginmain1, container, false);
                tv_text = (TextView) view.findViewById(R.id.tv_text);
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.loginmain2, container, false);
                tv_text = (TextView) view.findViewById(R.id.tv_text);
                container.addView(view, 0);


            } else if (position == 2) {
                view = layoutInflater.inflate(R.layout.loginmain3, container, false);
                tv_text = (TextView) view.findViewById(R.id.tv_text);
                container.addView(view, 0);

            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAnalyticTracker().setScreenName("Main Screen (Pagination)");
        FlurryAgent.onStartSession(MainActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(MainActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();
        return myApplication.getDefaultTracker();
    }

    // add dots dynamically
    public void addDots() {
        dots = new ArrayList<>();
        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.indicator);
        for (int i = 0; i < 3; i++) {
            ImageView dot = new ImageView(this);
            dot.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.dot_unselected));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.leftMargin = 5;
            params.rightMargin = 5;
            dotsLayout.addView(dot, params);
            dots.add(dot);
        }
    }

    public void selectDot(int idx) {
        System.out.println("LALALA : " + dots);
        for (int i = 0; i < 3; i++) {
            int drawableId = (i == idx) ? (R.drawable.dot_selected) : (R.drawable.dot_unselected);
            Drawable drawable = ContextCompat.getDrawable(this, drawableId);
            dots.get(i).setImageDrawable(drawable);
        }
    }

    private void processLogin(FacebookLoginResponse response) {
        if (response.isSuccessfulStatusCode()) {
            preferences.edit()
                    .putString("device_token", response.user.deviceToken)
                    .putString("uid", response.user.uid)
                    .putString("latitude", response.user.latitude)
                    .putString("longitude", response.user.longitude)
                    .putString("login_token", response.user.loginToken)
                    .putString("password", response.user.password)
                    .apply();

            if (response.isChangePasswordNeeded()) {
                Intent intent1 = new Intent(MainActivity.this, ResetPssword.class);
                intent1.putExtra("values", "");
                preferences.edit().putString("password_change", "yes").apply();
                startActivity(intent1);
            } else {
                Intent intent = new Intent(MainActivity.this, MyFeedActivity.class);
                intent.putExtra("first_name", response.user.profile.firstName);
                intent.putExtra("last_name", response.user.profile.lastName);
                intent.putExtra("user_image", "");
                intent.putExtra("username", response.user.username);
                intent.putExtra("gender", response.user.profile.gender);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
            finish();
        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
            alertDialogBuilder.setTitle(R.string.app_name);
            alertDialogBuilder.setMessage(response.message).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            final AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            alertDialog.setCancelable(true);
        }
    }
}