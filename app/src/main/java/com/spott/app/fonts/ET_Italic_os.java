package com.spott.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Acer on 9/10/2016.
 */
public class ET_Italic_os extends EditText {
    public ET_Italic_os(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Italic.ttf");
        this.setTypeface(face);
    }

    public ET_Italic_os(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Italic.ttf");
        this.setTypeface(face);
    }

    public ET_Italic_os(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Italic.ttf");
        this.setTypeface(face);

    }
}
