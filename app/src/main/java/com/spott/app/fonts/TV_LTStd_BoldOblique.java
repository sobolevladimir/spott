package com.spott.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Acer on 9/10/2016.
 */
public class TV_LTStd_BoldOblique extends TextView {
    public TV_LTStd_BoldOblique(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/GlyphaLTStd_BoldOblique.otf");
        this.setTypeface(face);
    }

    public TV_LTStd_BoldOblique(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/GlyphaLTStd_BoldOblique.otf");
        this.setTypeface(face);
    }

    public TV_LTStd_BoldOblique(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/GlyphaLTStd_BoldOblique.otf");
        this.setTypeface(face);
    }
}
