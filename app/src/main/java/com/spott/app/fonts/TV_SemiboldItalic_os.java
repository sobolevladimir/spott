package com.spott.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Acer on 9/10/2016.
 */
public class TV_SemiboldItalic_os extends TextView {
    public TV_SemiboldItalic_os(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_SemiboldItalic.ttf");
        this.setTypeface(face);
    }

    public TV_SemiboldItalic_os(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_SemiboldItalic.ttf");
        this.setTypeface(face);
    }

    public TV_SemiboldItalic_os(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_SemiboldItalic.ttf");
        this.setTypeface(face);
    }
}
