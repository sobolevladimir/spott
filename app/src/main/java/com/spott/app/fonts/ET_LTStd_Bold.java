package com.spott.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by Acer on 9/10/2016.
 */
public class ET_LTStd_Bold extends EditText {
    public ET_LTStd_Bold(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/GlyphaLTStd_Bold.otf");
        this.setTypeface(face);
    }

    public ET_LTStd_Bold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/GlyphaLTStd_Bold.otf");
        this.setTypeface(face);
    }

    public ET_LTStd_Bold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/GlyphaLTStd_Bold.otf");
        this.setTypeface(face);
    }
}
