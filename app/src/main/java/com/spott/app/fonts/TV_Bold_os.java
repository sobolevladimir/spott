package com.spott.app.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Acer on 9/10/2016.
 */

    public class TV_Bold_os extends TextView
    {
        public TV_Bold_os(Context context, AttributeSet attributeSet)
        {
            super(context,attributeSet);
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));

        }
    }

