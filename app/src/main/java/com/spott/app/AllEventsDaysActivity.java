package com.spott.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.spott.app.common.DrawableHelper;
import com.spott.app.data_models.CalendarEventsDay;
import com.spott.app.data_models.CalendarEventsDayEvent;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

public class AllEventsDaysActivity extends Activity implements View.OnClickListener {

    RelativeLayout rlMainFront, ll_groups;
    LinearLayout ll_feed, ll_profile, ll_calendar;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory;
    View view;
    boolean drawer_open = false;
    SharedPreferences preferences;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_all_events_days);

        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_calendar = (LinearLayout) findViewById(R.id.ll_calendar);

        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        view = (View) findViewById(R.id.vw);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_calendar.setOnClickListener(this);
        RecyclerView allEventsDays = (RecyclerView) findViewById(R.id.allEventsDaysRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        ArrayList<CalendarEventsDay> eventsDays = this.getIntent().getParcelableArrayListExtra("extraextra");

        EventsAdapter eventsAdapter = new EventsAdapter(eventsDays);
        allEventsDays.setLayoutManager(layoutManager);
        allEventsDays.setAdapter(eventsAdapter);
        setting();

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_calendar.setBackgroundColor(Color.parseColor("#FF7F60"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#ffffff"));

        DrawableHelper.setBottomNavigationBackground(ll_calendar);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {


            case R.id.ll_feed:
                Intent intent = new Intent(AllEventsDaysActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(AllEventsDaysActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(AllEventsDaysActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:



                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));

                break;
        }

    }

    public void backArrowOnclick(View view) {

        finish();
    }

    public class EventsAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

        private ArrayList<CalendarEventsDay> mEventsDays = new ArrayList<>();
        private LayoutInflater meventsInflater;

        private EventsAdapter(ArrayList<CalendarEventsDay> eventsDays){
            EventsAdapter.this.mEventsDays.addAll(eventsDays);
            meventsInflater = (LayoutInflater) getApplicationContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_day, parent, false);

            return new ItemView(itemView);
        }
        private class ItemView extends RecyclerView.ViewHolder{
            View rootView;
            TextView dateText;
            LinearLayout containerForEvents;

            private ItemView(View itemView) {
                super(itemView);
                rootView = itemView;
                dateText = rootView.findViewById(R.id.dateTextView);
                containerForEvents = rootView.findViewById(R.id.layoutCantainer);
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

            CalendarEventsDay calendarEventsDay = mEventsDays.get(position);

            ArrayList<CalendarEventsDayEvent> currentEventsDay = calendarEventsDay.getEvents();
            ((ItemView) holder).dateText.setText(calendarEventsDay.getDate());
            for (int i = 0 ; i<currentEventsDay.size();i++){
                final View v = meventsInflater.inflate(R.layout.adapter_events, null);
                TextView startTiming,endTiming,titel;
                startTiming = (TextView) v.findViewById(R.id.start_timing);
                endTiming = (TextView) v.findViewById(R.id.end_timing);
                titel = (TextView) v.findViewById(R.id.title);
                startTiming.setText(currentEventsDay.get(i).getStartTime());
                endTiming.setText(currentEventsDay.get(i).getEndTime());
                titel.setText(currentEventsDay.get(i).getName());
                ((ItemView) holder).containerForEvents.addView(v);
            }




        }

        @Override
        public int getItemCount() {
            return mEventsDays.size();
        }
    }
}
