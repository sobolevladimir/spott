package com.spott.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Adimn on 11/5/2016.
 */
public class SurveyScreenActivity extends Activity implements View.OnClickListener {


    TextView tv_count_survey, tv_detail_survey, survey_btn, iv_red_icon, tv_num;
    Context context;
    DiamondImageView img_profile;
    ArrayList<HashMap<String, Object>> surveylist;
    String value = "";
    SharedPreferences preferences;
    SurveyAdapter spinnerArrayAdapter;
    PullToRefreshListView list1;
    ArrayList<String> list;
    JSONArray jsonarray;
    ImageView iv_menu_icon;
    private String survey_pelicans;
    NavigationFragmentAdapter mAdapter;
    private PullToRefreshListView pulllist;
    ViewPager pager;
    LayoutInflater layoutInflater = null;
    private BroadcastReceiver broadcastReceiver;
    GroupAdapter1 gAdapter;
    String encoded;
    String firstname = "", lastname = "",gender = "", phone = "", quote = "";
    String you_like = "";
    String star = "", group = "", post = "";
    ArrayList<HashMap<String, String>> mygrouplist;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout rlMainFront, ll_groups;
    int count = 0;
    int countt = 1;
    View shadowView = null, view = null;
    int height, width;
    boolean drawer_open = false;
    PageIndicator mIndicator;
    String string = "#ed3224";
    String string1 = "#ffffff";
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.survey_screen);

        getIds();

        if (getIntent().hasExtra("fromupdate")) {
            callOncreateData();
        } else {
            showDialog(SurveyScreenActivity.this);
        }

    }

    private void getIds()
    {
        context = SurveyScreenActivity.this;
        layoutInflater = LayoutInflater.from(this);
        mygrouplist = new ArrayList<HashMap<String, String>>();
        surveylist = new ArrayList<HashMap<String, Object>>();
        mAdapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(mAdapter);
        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.circle_indicator);
        mIndicator = indicator;
        indicator.setViewPager(pager);
        indicator.setPageColor(Color.parseColor(string1));
        indicator.setFillColor(Color.parseColor(string));
        indicator.setStrokeColor(Color.parseColor(string));
        layoutInflater = LayoutInflater.from(this);
        initialise();
        clickables();
        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        quote = preferences.getString("quote", "");
        gender = preferences.getString("gender", "");
        phone = preferences.getString("phone", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");
        jsonarray = new JSONArray();
        Intent intent = getIntent();
        value = intent.getStringExtra("value");
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        view.setOnClickListener(this);
    }

    private void callOncreateData() {
        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new SurveyScreenData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new SurveyScreenData().execute();
            }
        }
        spinnerArrayAdapter = new SurveyAdapter(SurveyScreenActivity.this, surveylist);
        list1.setAdapter(spinnerArrayAdapter);
    }

    private void initialise() {
        connDec = new ConnectionDetector(SurveyScreenActivity.this);
        alert = new Alert_Dialog_Manager(context);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        tv_count_survey = (TextView) findViewById(R.id.tv_count_survey);
        tv_detail_survey = (TextView) findViewById(R.id.tv_detail_survey);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        tv_num = (TextView) findViewById(R.id.tv_num);
        survey_btn = (TextView) findViewById(R.id.survey_btn);
        list1 = (PullToRefreshListView) findViewById(R.id.list);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);
                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();
        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }

    private void clickables() {

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();
                    drawer_open = true;
                    pager.setAdapter(mAdapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);

                } else if (drawer_open == true) {

                    StopDrawerAnim();

                    drawer_open = false;
                }

            }
        });

        survey_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (allQuesAnswered()==true)
                {
                    if (connDec.isConnectingToInternet())
                    {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                            new SubmitData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        else
                            new SubmitData().execute();

                    }
                }
                else
                {
                    Toast.makeText(context,"Please fill all the answers.",Toast.LENGTH_SHORT).show();
                }
            }
// else {
//
//                    Toast.makeText(getApplicationContext(), R.string.question, Toast.LENGTH_SHORT).show();
//                }
            // }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SurveyScreenActivity.this, UpdatesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

    }

    private boolean allQuesAnswered() {

        for(int i = 0;i<surveylist.size();i++)
        {
            HashMap<String,Object> hMap = new HashMap<>();
            hMap = surveylist.get(i);

            if(hMap.get("answer").toString().equals("0"))
            {
                return  false;
            }
        }

        return true;
    }


    @Override
    public void onResume() {

        super.onResume();

        final int pos = 1;

        if(pager!=null) {
            pager.postDelayed(new Runnable() {

                @Override
                public void run() {
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
                }
            }, 100);

            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {

                    ((UTCAPP) getApplication()).pagerPos = position;
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    private class NavigationFragmentAdapter extends PagerAdapter {


        TextView tv_browsing;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront, rll;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_profile_name, tv_profile_username, tv_feed, tv_profile, tv_update, tv_groups, tv_staff_dir, tv_logout;

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);
                rll = (RelativeLayout) view.findViewById(R.id.rll);
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SurveyScreenActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                    }
                });
                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupData().execute();
                    }
                }
                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {

                    }

                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;

                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new GroupData().execute();
                            }

                            mygrouplist.clear();
                        }

                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(SurveyScreenActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_logout = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(preferences.getString("first_name", "") + " " + preferences.getString("last_name", ""));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));

                if (encoded.equals("")) {

                    img_profile.setImageResource(R.drawable.app);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }


                tv_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        Intent intent = new Intent(SurveyScreenActivity.this, MyProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);

                    }
                });

                img_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(SurveyScreenActivity.this, MyProfileActivity.class);
                        intent1.putExtra("myProfile", "");
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);
                    }
                });
                tv_groups.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(SurveyScreenActivity.this, MyCrewsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(SurveyScreenActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_staff_dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(SurveyScreenActivity.this, MyDirectoryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });

                tv_logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new LogOutData().execute();
                            }
                        }
                    }
                });
                container.addView(view, 0);
            }
            //   ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    @Override
    public void onClick(View view) {

        Handler handler = new Handler();

        switch (view.getId()) {

            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:

                Intent intent4 = new Intent(SurveyScreenActivity.this, MyFeedActivity.class);
                intent4.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent4);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(SurveyScreenActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(SurveyScreenActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(SurveyScreenActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    public class GroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {
                        mygrouplist.clear();
                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");

                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);

                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygrouplist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(SurveyScreenActivity.this, mygrouplist);

                        pulllist.setAdapter(gAdapter);

                        gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(SurveyScreenActivity.this);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygrouplist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygrouplist) {
            this.context = context;
            this.mygrouplist = mygrouplist;
        }

        @Override
        public int getCount() {
            return mygrouplist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            ViewHolder holder = new ViewHolder();
            String value = mygrouplist.get(i).get("unread");
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygrouplist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygrouplist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygrouplist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygrouplist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygrouplist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygrouplist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygrouplist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygrouplist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygrouplist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygrouplist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygrouplist.get(i).get("you_group"));

                    startActivity(intent);
                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygrouplist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygrouplist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygrouplist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygrouplist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygrouplist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygrouplist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygrouplist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygrouplist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygrouplist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygrouplist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygrouplist.get(i).get("you_group"));

                    startActivity(intent);
                }
            });


            if (value == "0") {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygrouplist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygrouplist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setText(value);
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    private class SurveyAdapter extends BaseAdapter {

        ArrayList<HashMap<String, Object>> surveylist;

        public SurveyAdapter(SurveyScreenActivity surveyScreenActivity, ArrayList<HashMap<String, Object>> surveylist) {
            this.surveylist = surveylist;
        }

        @Override
        public int getCount() {
            return surveylist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.surveyadapter, null);
            final ViewHolder holder = new ViewHolder();
            holder.spinner = (Spinner) convertView.findViewById(R.id.spinner);
            holder.tv_question_second = (TextView) convertView.findViewById(R.id.tv_question_second);
            holder.tv_question = (TextView) convertView.findViewById(R.id.tv_question);
            holder.spinner.setBackgroundResource(R.drawable.drop_arrow);
            holder.tv_question_second.setText(surveylist.get(i).get("survey_question").toString());
            holder.tv_question.setText("Question " + " " + (countt++));

            list = new ArrayList<>();

            JSONArray array = null;
            try {
                array = new JSONArray(surveylist.get(i).get("question_option").toString());
                list.add("Select a answer");
                for (int k = 0; k < array.length(); k++) {
                    HashMap<String, String> hMap = new HashMap<>();
                    JSONObject jo = array.getJSONObject(k);

                    list.add(jo.getString("option_text"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.spinner.setAdapter(new ArrayAdapter<String>(context, R.layout.spinner_dropdown, list));
            holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    if (position > 0)
                        surveylist.get(i).put("answer", "1");
                     else
                        surveylist.get(i).put("answer", "0");

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            return convertView;
        }

        class ViewHolder {
            Spinner spinner;
            TextView tv_question_second, tv_question;
        }
    }

    public class SurveyScreenData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            //LoaderClass.ShowProgressWheel(SurveyScreenActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "survey_detail");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("survey_id", value);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("survey_data");

                    // updateList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, Object> hmap = new HashMap<>();
                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String survery_question = user_dataObj.getString("survery_question");

                        if (!survery_question.equals("")) {
                            JSONObject user_dataObj1 = user_dataObj.getJSONObject("survery_question");
                            String question_id = user_dataObj1.getString("question_id");
                            String survey_id = user_dataObj1.getString("survey_id");
                            String survey_question = user_dataObj1.getString("survey_question");
                            String create_date = user_dataObj1.getString("create_date");
                            hmap.put("question_id", question_id);
                            hmap.put("survey_id", survey_id);
                            hmap.put("survey_question", survey_question);
                            hmap.put("create_date", create_date);
                            hmap.put("answer", "0");
                        }

                        JSONArray array = user_dataObj.getJSONArray("question_option");

                        hmap.put("question_option", array);

                        surveylist.add(hmap);
                        JSONObject jo = new JSONObject();
                        jo.put("question_id", surveylist.get(i).get("question_id"));
                        jo.put("option_id", array.getJSONObject(0).getString("option_id"));
                        jsonarray.put(jo);
                        System.out.println("JSONLIST" + jsonarray);
                    }
                    spinnerArrayAdapter.notifyDataSetChanged();
                    JSONObject user_dataObject = resultObj.getJSONObject("survey_detail");
                    if (user_dataObject != null) {
                        String survey_id = user_dataObject.getString("survey_id");
                        String survey_question = user_dataObject.getString("survey_question");
                        survey_pelicans = user_dataObject.getString("survey_pelicans");
                        String survey_status = user_dataObject.getString("survey_status");
                        String survey_create_date = user_dataObject.getString("survey_create_date");

                        tv_detail_survey.setText(survey_question);

                        tv_count_survey.setText("This Survey is worth " + " " + survey_pelicans + " stars");
                    } else {

                    }

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(SurveyScreenActivity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // LoaderClass.HideProgressWheel(SurveyScreenActivity.this);
        }
    }

    public class SubmitData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            LoaderClass.ShowProgressWheel(SurveyScreenActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "submit_survey");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("survey_id", value);

                jo.put("question", jsonarray);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    int total = Integer.valueOf(preferences.getString("pelican", "0"));

                    int newpel = Integer.valueOf(survey_pelicans);

                    int finalVal = total + newpel;

                    String survey_pelicans1 = "" + finalVal;

                    preferences.edit().putString("pelican", survey_pelicans1).commit();

                    customShareDetailsPopup();

                    if (getIntent().hasExtra("pos"))
                    {
                        int pos = Integer.parseInt(getIntent().getStringExtra("pos"));

                        UpdatesActivity.updateList.get(pos).put("survey_added","1");

                    }


                    //finish();

                } else if (errCode.equals("700")) {


                    // alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(SurveyScreenActivity.this);
        }
    }

    private void customShareDetailsPopup() {
        final Dialog dialog = new Dialog(SurveyScreenActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.update_popup);
        final TextView tv_title_survey, ok_btn, tv_title_participate,tv_title_banned;
        ok_btn = (TextView) dialog.findViewById(R.id.ok_btn);
        tv_title_survey = (TextView) dialog.findViewById(R.id.tv_title_survey);
        tv_title_participate = (TextView) dialog.findViewById(R.id.tv_title_participate);
        tv_title_banned = (TextView) dialog.findViewById(R.id.tv_title_banned);
        tv_title_participate.setVisibility(View.VISIBLE);
        tv_title_survey.setVisibility(View.GONE);
        tv_title_banned.setVisibility(View.GONE);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Intent intent = new Intent(SurveyScreenActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    public void showDialog(Activity activity) {
        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCanceledOnTouchOutside(false);

        dialog.setCancelable(false);

        dialog.setContentView(R.layout.survey_dialog);

        TextView submitBT = (TextView) dialog.findViewById(R.id.startTV);

        TextView cancelBT = (TextView) dialog.findViewById(R.id.cancelTV);

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                callOncreateData();

            }
        });

        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                Intent intent = new Intent(SurveyScreenActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });

        dialog.show();

    }

    public class LogOutData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    {

                        preferences.edit().clear().commit();
                        ((UTCAPP) getApplication()).joingroupList.clear();
                        Intent i = new Intent(SurveyScreenActivity.this, SplashActivity.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SurveyScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(SurveyScreenActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Survey Screen");

        FlurryAgent.onStartSession(SurveyScreenActivity.this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(SurveyScreenActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

}