package com.spott.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;
import com.spott.app.cropiimage.CropImage;
import com.spott.app.fonts.TV_Bold_os;
import com.spott.app.fonts.TV_Regular_os;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import io.github.rockerhieu.emojicon.EmojiconEditText;
import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

/**
 * Created by oleg on 16.09.2017.
 */

public class EventActivity extends FragmentActivity implements View.OnClickListener,
        EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener, AdapterView.OnItemSelectedListener {
    private static final String SPACE = " ";

    RecyclerView commentsRecycler;

    private SharedPreferences preferences;

    private String post_id;

    String rating_selected = "";

    private EventsCommentsAdapter mEventCommentsAdapter;

    private String your_like = "";


    String messageId = "";

    private JSONArray moodsJSONArray;

    private HashMap<String, Object> mainMap;

    ScrollView scrollView;

    private ArrayList<HashMap<String, Object>> mainMapList = new ArrayList<>();

    private ImageView iv_heart;

    private TextView tv_noti; /// кількість смайликів

    private RelativeLayout rl_sel_smileys; /// контейнер зі смайликами до цього івента

    private ImageView iv_smiley1, iv_smiley2, iv_smiley3, iv_smiley4, iv_smiley5;

    private RelativeLayout rl_background;

    private int tempPos = 0;

    private String moodStr = "";

    private EmojiconEditText et_leave_comment;

    private int apiLevel;

    private File tempImageFile;

    private String encodedImage;

    private ImageView iv_emojibtn;

    private LinearLayout emoLay;

    private ImageView iv_keyboard;

    private ConnectionDetector connDec;

    private Alert_Dialog_Manager alert;

    private LinearLayout ll_feed, ll_profile, ll_calendar;

    private RelativeLayout ll_groups;

    private TV_Regular_os tv_send;

    private ArrayList<EventCommentData> mEventCommentDatas;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        connDec = new ConnectionDetector(EventActivity.this);

        Bundle intentExtras = getIntent().getExtras();

        post_id = intentExtras.getString("post_id");

        if (!new ConnectionDetector(EventActivity.this).isConnectingToInternet()) {
            Toast.makeText(EventActivity.this, "No internet!", Toast.LENGTH_SHORT).show();
        } else {
            new EventScreenData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new CommentData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        setContentView(R.layout.activity_event);
        scrollView = (ScrollView) findViewById(R.id.activity_event_event_details_layout);
        commentsRecycler = (RecyclerView) findViewById(R.id.recycl_event_comments);
        layoutManager = new LinearLayoutManager(EventActivity.this);
        commentsRecycler.setLayoutManager(layoutManager);


        initComponents(getIntent().getExtras());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        new SetEventStatusData().execute(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private class EventScreenData extends AsyncTask<String, String, String> {
        private String getData = "";

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "feed_detail");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("post_id", post_id);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {
                    JSONObject user_dataObj = resultObj.getJSONObject("post_detail");

                    your_like = user_dataObj.getString("your_like");

                    String you_like = user_dataObj.getString("you_like");

                    updateHeartVisibility(you_like);

                    try {
                        moodsJSONArray = new JSONArray();

                        moodsJSONArray = user_dataObj.getJSONArray("moods");

                        updateSmiley(your_like, false);
                        //setAdapter();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(EventActivity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateHeartVisibility(final String you_like) {
        ImageView iv_smile_default = (ImageView) findViewById(R.id.iv_smile_default);
        if (you_like != null) {
            if (you_like.equals("1")) {
                iv_heart.setVisibility(View.GONE);
                iv_smile_default.setVisibility(View.VISIBLE);
                rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSecondDialogSmiley(your_like, "2");
                    }
                });

            } else if (you_like.equals("0")) {
                ///iv_heart_fill.setVisibility(View.GONE);
                iv_heart.setVisibility(View.VISIBLE);
                iv_smile_default.setVisibility(View.GONE);
                iv_heart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSecondDialogSmiley(you_like, "1");
                    }
                });
            }

        }
    }

    private void updateSmiley(String your_like, boolean update) {
        if (moodsJSONArray != null) {
            updateSmileyArray(your_like);

            updateOtherSmileys();

            inflateSmileys();
        }

        if (update == true && moodsJSONArray.length() > 0) {
            int count = Integer.valueOf(tv_noti.getText().toString().trim());
            count++;
            tv_noti.setText(count + "");
        }
    }

    private void updateSmileyArray(String your_like1) {
        boolean likecontains = false;

        try {
            for (int k = 0; k < moodsJSONArray.length(); k++) {
                try {
                    JSONObject curntObj = moodsJSONArray.getJSONObject(k);

                    if (curntObj.get("moods").equals(this.your_like)) {
                        curntObj.remove("isYour");

                        if (curntObj.has("isNew")) {
                            ArrayList<String> list = new ArrayList<String>();
                            int len = moodsJSONArray.length();
                            if (moodsJSONArray != null) {
                                for (int i = 0; i < len; i++) {
                                    list.add(moodsJSONArray.get(i).toString());
                                }
                            }
                            //Remove the element from arraylist
                            list.remove(k);

                            moodsJSONArray = new JSONArray(list);

                            //jArrayy.remove(k);

                            curntObj = null;
                        }
                    }

                    if (curntObj != null) {
                        if (curntObj.get("moods").equals(your_like1)) {
                            curntObj.put("isYour", "true");

                            likecontains = true;
                        }

                        moodsJSONArray.put(k, curntObj);
                    }

                    int countArray = moodsJSONArray.length();

                    if (countArray > 0) {
                        countArray = countArray - 1;
                    }
                    if (k == countArray) {
                        if (!likecontains) {
                            JSONObject newObj1 = new JSONObject();

                            newObj1.put("isYour", "true");

                            newObj1.put("isNew", "true");

                            newObj1.put("moods", your_like1);

                            newObj1.put("modcount", 1);

                            moodsJSONArray.put(newObj1);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            this.your_like = your_like1;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateOtherSmileys() {
        mainMapList.clear();

        final int numberOfItemsInResp = moodsJSONArray.length();

        for (int j = 0; j < numberOfItemsInResp; j++) {
            try {
                mainMap = new HashMap<>();

                JSONObject user_dataObj1 = null;

                if (moodsJSONArray.get(j) instanceof String) {
                    user_dataObj1 = new JSONObject(moodsJSONArray.getString(j));
                } else
                    user_dataObj1 = moodsJSONArray.getJSONObject(j);

                String moods = user_dataObj1.getString("moods");

                String count = user_dataObj1.getString("modcount");

                HashMap<String, Object> hmap = new HashMap<>();

                hmap.put(moods, count);

                mainMap.put("moods", moods);

                mainMap.put("count", count);

                if (your_like.equalsIgnoreCase(moods)) {
                    mainMap.put("isYour", "true");
                    user_dataObj1.put("isYour", "true");

                    if (count.equals("1")) {
                        mainMap.put("isNew", "true");

                        user_dataObj1.put("isNew", "true");
                    }

                    moodsJSONArray.put(j, user_dataObj1);
                }


                mainMapList.add(mainMap);

                ///commentLists.add(hmap);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void inflateSmileys() {
        hideAllSmileys();

        for (int x = 0; x < mainMapList.size(); x++) {

            final HashMap<String, Object> mapHash = mainMapList.get(x);

            String moods = mapHash.get("moods").toString();

            if (moods.equalsIgnoreCase("happy")) {
                iv_smiley1.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley1.setImageResource(R.drawable.happy);
                } else {
                    iv_smiley1.setImageResource(R.drawable.happy);
                }
            } else if (moods.equalsIgnoreCase("indifferen")) {
                iv_smiley2.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley2.setImageResource(R.drawable.indifferent);
                } else {
                    iv_smiley2.setImageResource(R.drawable.indifferent);
                }
            } else if (moods.equalsIgnoreCase("amazing")) {
                iv_smiley3.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley3.setImageResource(R.drawable.amazing);
                } else {
                    iv_smiley3.setImageResource(R.drawable.amazing);
                }
            } else if (moods.equalsIgnoreCase("love")) {
                iv_smiley4.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley4.setImageResource(R.drawable.love);
                } else {
                    iv_smiley4.setImageResource(R.drawable.love);
                }
            } else if (moods.equalsIgnoreCase("sad")) {
                iv_smiley5.setVisibility(View.VISIBLE);

                if (mapHash.containsKey("isYour")) {
                    iv_smiley5.setImageResource(R.drawable.sad);
                } else {
                    iv_smiley5.setImageResource(R.drawable.sad);
                }
            }
        }
    }

    private void hideAllSmileys() {
        iv_smiley1.setVisibility(View.GONE);

        iv_smiley2.setVisibility(View.GONE);

        iv_smiley3.setVisibility(View.GONE);

        iv_smiley4.setVisibility(View.GONE);

        iv_smiley5.setVisibility(View.GONE);
    }

    private class CommentData extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            String gotData = null;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_comment_list");

                jo.put("login_token", getSharedPreferences("UTC", MODE_PRIVATE).getString("login_token", ""));

                jo.put("ref_id", post_id);

                jo.put("source", "post");

                ///jo.put("post_value", count);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                gotData = content.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return gotData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ///System.out.println("Event comments result : " + result);

            try {
                JSONArray comments = new JSONObject(result).optJSONArray("comments");

                mEventCommentDatas = new ArrayList<>();

                for (int i = comments.length() - 1; i >= 0; i--) {
                    JSONObject iCommentJSONObject = comments.optJSONObject(i);

                    EventCommentData iElement = new EventCommentData(
                            iCommentJSONObject.optString("message_id"),
                            iCommentJSONObject.optString("user_image"),
                            iCommentJSONObject.optString("content"),
                            iCommentJSONObject.optString("first_name"),
                            iCommentJSONObject.optString("last_name"),
                            iCommentJSONObject.optString("message_create_date"),
                            iCommentJSONObject.optString("you_like"),
                            iCommentJSONObject.optString("comment_like"));

                    String commentImage = iCommentJSONObject.getString("post_image");

                    if (commentImage != null)
                        iElement.setImage(commentImage);

                    JSONObject you_moods = iCommentJSONObject.optJSONObject("you_moods"); /// кореневий JSON Object з моїми смайликами до цього коментаря

                    String mood = null;

                    String moo = null;

                    if (you_moods != null) { /// якщо JSON Object моїх смайликів не null
                        mood = you_moods.getString("moods"); /// вид (-и) моїх смайликів до цбого коментаря

                        moo = you_moods.getString("moo"); /// кількість млїх смайликів до цього коментаря
                    }

                    try {
                        String moods = "";

                        String count = "";

                        JSONArray jArrayy = iCommentJSONObject.getJSONArray("moods"); /// JSON Array зі смайликами від інших користувачів, які були поставлені до цього коментаря
                        // final int numberOfItemsInResp = jArrayy.length();

                        ArrayList<HashMap<String, String>> moodList = new ArrayList<>();

                        for (int j = 0; j < jArrayy.length(); j++) {
                            HashMap<String, String> hmap1 = new HashMap<>();

                            JSONObject user_dataObj1 = jArrayy.getJSONObject(j);
                            moods = user_dataObj1.getString("moods"); /// конкретний вид смайлика від іншого користувача, який був поставлений до цього коментаря

                            count = user_dataObj1.getString("con"); /// кількість цих смайликів

                            hmap1.put(moods, count);
                            moodList.add(hmap1);
                        }

                        JSONObject total_moods = iCommentJSONObject.getJSONObject("total_moods");
                        String moo1 = total_moods.getString("moo");

                        if (mood != null)
                            iElement.setMyMoods(mood);

                        if (mood != null)
                            iElement.setMyMoodsCount(moo);

                        if (moodList.size() != 0)
                            iElement.setUsersMoods(moodList);

                        iElement.setTotalMoodsCount(moo1);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mEventCommentDatas.add(iElement);
                }

                mEventCommentsAdapter = new EventsCommentsAdapter(mEventCommentDatas, EventActivity.this);
                commentsRecycler.setAdapter(mEventCommentsAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class EventCommentData {
        private String mMessageId;

        private String mAvatar;

        private String mText;

        private String mImage;

        private String mFirstName;

        private String mLastName;

        private String mMyMoods;

        private String mMyMoodsCount;

        private String mCommentLike;

        private String mYouLike;

        private String mCreationDate;

        private ArrayList<HashMap<String, String>> mUsersMoodsList;

        private String mTotalMoodsCount;

        private EventCommentData(String id, String avatar, String mText, String mFirstName,
                                 String mLastName, String mCreationDate,String mYouLike,String mCommentLike) {
            EventCommentData.this.mMessageId = id;
            EventCommentData.this.mAvatar = avatar;
            EventCommentData.this.mText = mText;
            EventCommentData.this.mFirstName = mFirstName;
            EventCommentData.this.mLastName = mLastName;
            EventCommentData.this.mCreationDate = mCreationDate;
            EventCommentData.this.mYouLike = mYouLike;
            EventCommentData.this.mCommentLike = mCommentLike;
        }

        public String getMessageId() {
            return EventCommentData.this.mMessageId;
        }

        private String getAvatar() {
            return EventCommentData.this.mAvatar;
        }

        private String getmCommentLike(){
            return EventCommentData.this.mCommentLike;
        }

        private void setmYouLike(String youLike){
            EventCommentData.this.mYouLike = youLike;
        }

        private void setmCommentLike(String commentLike){
            EventCommentData.this.mCommentLike = commentLike;
        }

        private String getmYouLike(){
            return EventCommentData.this.mYouLike;
        }

        private String getText() {
            return EventCommentData.this.mText;
        }

        private String getFirstName() {
            return EventCommentData.this.mFirstName;
        }

        private String getLastName() {
            return EventCommentData.this.mLastName;
        }

        public String getCreationDate() {
            return EventCommentData.this.mCreationDate;
        }

        private void setImage(String image) {
            EventCommentData.this.mImage = image;
        }

        private String getImage() {
            return EventCommentData.this.mImage;
        }

        private void setMyMoods(String moods) {
            EventCommentData.this.mMyMoods = moods;
        }

        public String getMyMoods() {
            return EventCommentData.this.mMyMoods;
        }

        private void setMyMoodsCount(String moodsCount) {
            EventCommentData.this.mMyMoodsCount = moodsCount;
        }

        public String getMyMoodsCount() {
            return EventCommentData.this.mMyMoodsCount;
        }

        private void setUsersMoods (ArrayList<HashMap<String, String>> moodsList) {
            EventCommentData.this.mUsersMoodsList = moodsList;
        }

        private ArrayList<HashMap<String, String>> getUsersMoods () {
            return EventCommentData.this.mUsersMoodsList;
        }

        public void setTotalMoodsCount(String moodsCount) {
            EventCommentData.this.mTotalMoodsCount = moodsCount;
        }

        public String getTotalMoodsCount() {
            return EventCommentData.this.mTotalMoodsCount;
        }
    }



    private class EventsCommentsAdapter extends RecyclerView.Adapter<EventComentViewHolder> {
        private ArrayList<EventCommentData> mData;

        private EventActivity mParentActivity;

        private EventsCommentsAdapter(ArrayList<EventCommentData> mData, EventActivity parentActivity) {
            EventsCommentsAdapter.this.mData = new ArrayList<>();
            EventsCommentsAdapter.this.mData.addAll(mData);
            EventsCommentsAdapter.this.mParentActivity = parentActivity;
        }

        private int getListSize(){
            return mData.size();
        }

        @Override
        public EventComentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new EventComentViewHolder(getLayoutInflater().inflate(R.layout.item_event_comment, null, false));
        }

        @Override
        public void onBindViewHolder(final EventComentViewHolder holder, final int position) {

            final int i = position;
            EventCommentData currentElement = mData.get(position);

            String commentAvatar = currentElement.getAvatar();

            if (commentAvatar != null&&!commentAvatar.equals("")) { /// TODO тут перевірити, яке дефолтне значення
                Picasso.with(mParentActivity).load(commentAvatar).into(holder.mImgvEventCommentAvatar);
            }

            String commentImage = currentElement.getImage();

            if (commentImage != null) { /// TODO тут перевірити, яке дефолтне значення
                String commentImageFullURL = CommonUtilities.LOGIN_URL1 + commentImage;

                Picasso.with(mParentActivity).load(commentImageFullURL).into(holder.mImgvEventCommentImage);
            }

            TV_Bold_os txtCommentatorName = holder.mTxtEventCommentName;

            txtCommentatorName.setText(CommonUtilities.decodeUTF8(currentElement.getFirstName()));
            txtCommentatorName.append(SPACE + CommonUtilities.decodeUTF8(currentElement.getLastName()));

            holder.mTxtEventCommentText.setText(CommonUtilities.decodeUTF8(currentElement.getText()));

            String commentDate = currentElement.getCreationDate();

            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                Date newDate = format.parse(commentDate);

                format = new SimpleDateFormat("hh:mm, MMM'.' dd, ''yy", Locale.US);

                String commentDateFormatted = format.format(newDate);

                holder.mTxtEventCommentDate.setText("");

                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                Date date1 = dates.parse(date);

                Date date2 = dates.parse(commentDate);

                long diff = date1.getTime() - date2.getTime();

                holder.mTxtEventCommentTimeAgo.setText(CommonUtilities.getDaysMonths(diff));

            } catch (ParseException e) {
                e.printStackTrace();
            }

            EventCommentData currentComment = mData.get(position);


                String youLikes = currentComment.getmYouLike();

                if (youLikes.equals("0")) {

                    holder.ratingImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            customShareDetailsPopup(mEventCommentDatas.get(position).getMessageId(),position);
                        }
                    });

                } else {


                        holder.tv_total_rating.setText((String) mData.get(position).getmCommentLike());

                        holder.ratingImageView.setImageResource(R.drawable.sel_star);

                    }







            /// TODO доробити тут логіку сетіння смайликів

            ArrayList hujnya = currentComment.getUsersMoods();

            String countSmile = currentComment.getTotalMoodsCount();
            if (countSmile!=null){
                holder.txt_totalsmilecount.setText(countSmile);
                if (countSmile.equals("0")){
                    holder.ll_heart_smiley.setVisibility(View.VISIBLE);
                    holder.ll_heart_smiley.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showDialogSmiley(i);

                        }
                    });
                    holder.iv_smile_default.setVisibility(View.GONE);

                }else {
                    holder.ll_heart_smiley.setVisibility(View.GONE);
                    holder.iv_smile_default.setVisibility(View.VISIBLE);
                }
            }

            if (hujnya!=null){
            for (int j = 0; hujnya.size()>j;j++){

                HashMap hujnya2 = (HashMap) hujnya.get(j);

                if (hujnya2.containsKey("happy")){
                    holder.mImgvEventCommentSmileyHappy.setVisibility(View.VISIBLE);

                }
//                else {
//                    holder.mImgvEventCommentSmileyHappy.setVisibility(View.GONE);
//                }

                if (hujnya2.containsKey("indifferen")){
                    holder.mImgvEventCommentSmileyIndifferent.setVisibility(View.VISIBLE);
                }
//                else {
//                    holder.mImgvEventCommentSmileyIndifferent.setVisibility(View.GONE);
//                }
                 if (hujnya2.containsKey("amazing")){
                    holder.mImgvEventCommentSmileyAmazing.setVisibility(View.VISIBLE);
                }
//                else {
//                     holder.mImgvEventCommentSmileyAmazing.setVisibility(View.GONE);
//                 }
                if (hujnya2.containsKey("love")){
                    holder.mImgvEventCommentSmileyLove.setVisibility(View.VISIBLE);
                }
//                else {
//                    holder.mImgvEventCommentSmileyLove.setVisibility(View.GONE);
//                }
                if (hujnya2.containsKey("sad")){
                    holder.mImgvEventCommentSmileySad.setVisibility(View.VISIBLE);
                }
//                else {
//                    holder.mImgvEventCommentSmileySad.setVisibility(View.GONE);
//                }

            }
            }


            String mySmiley = currentComment.getMyMoods();

                if (mySmiley != null) {
                    if (mySmiley.equals("happy")) {
                        holder.mImgvEventCommentSmileyHappy.setImageResource(R.drawable.happy);
                        holder.mImgvEventCommentSmileyHappy.setVisibility(View.VISIBLE);
                    } else if (mySmiley.equals("indifferen")) {
                        holder.mImgvEventCommentSmileyIndifferent.setImageResource(R.drawable.indifferent);
                        holder.mImgvEventCommentSmileyIndifferent.setVisibility(View.VISIBLE);
                    } else if (mySmiley.equals("amazing")) {
                        holder.mImgvEventCommentSmileyAmazing.setImageResource(R.drawable.amazing);
                        holder.mImgvEventCommentSmileyAmazing.setVisibility(View.VISIBLE);
                    } else if (mySmiley.equals("love")) {
                        holder.mImgvEventCommentSmileyLove.setImageResource(R.drawable.love);
                        holder.mImgvEventCommentSmileyLove.setVisibility(View.VISIBLE);
                    } else if (mySmiley.equals("sad")) {
                        holder.mImgvEventCommentSmileySad.setImageResource(R.drawable.sad);
                        holder.mImgvEventCommentSmileySad.setVisibility(View.VISIBLE);
                    }
                }

            String myMoodsCount = currentComment.getMyMoodsCount();
            }
        @Override
        public int getItemCount() {
            return mData.size();
        }

        private void addComment(EventCommentData comment) {
            ///Log.d("EventActivity.EventsCommentsAdapter...addComment", comment.getmText());

            mData.add(comment);
            notifyDataSetChanged();
        }
    }


    class EventComentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TV_Bold_os mTxtEventCommentName;

        private TV_Bold_os mTxtEventCommentDate;

        private TV_Regular_os mTxtEventCommentTimeAgo;

        private TV_Regular_os mTxtEventCommentText;

        private ImageView mImgvEventCommentImage;

        private DiamondImageView mImgvEventCommentAvatar;

        private ImageView mImgvEventCommentSmileyHappy;

        private ImageView mImgvEventCommentSmileyIndifferent;

        private ImageView mImgvEventCommentSmileyAmazing;

        private ImageView mImgvEventCommentSmileyLove;

        private ImageView mImgvEventCommentSmileySad;

        private ImageView ratingImageView;

        private ImageView mImgvEventCommentSetSmiley;

        private ImageView ll_heart_smiley;

        private ImageView iv_smile_default;

        private LinearLayout layoutForHeart;

        private TextView tv_total_rating;

        private TV_Regular_os txt_totalsmilecount;





        EventComentViewHolder(View itemView) {
            super(itemView);

            mTxtEventCommentName = (TV_Bold_os) itemView.findViewById(R.id.txt_event_comment_name);

            mImgvEventCommentAvatar = (DiamondImageView) itemView.findViewById(R.id.imgv_event_comment_avatar);

            mTxtEventCommentDate = (TV_Bold_os) itemView.findViewById(R.id.txt_event_comment_date);

            mTxtEventCommentTimeAgo = (TV_Regular_os) itemView.findViewById(R.id.txt_event_comment_time_ago);

            mTxtEventCommentText = (TV_Regular_os) itemView.findViewById(R.id.txt_event_comment_text);

            mImgvEventCommentImage = (ImageView) itemView.findViewById(R.id.imgv_event_comment_image);

            iv_smile_default = (ImageView) itemView.findViewById(R.id.iv_smile_default);


            mImgvEventCommentSmileyHappy = (ImageView) itemView.findViewById(R.id.iv_smiley1);

            mImgvEventCommentSmileyIndifferent = (ImageView) itemView.findViewById(R.id.iv_smiley2);

            mImgvEventCommentSmileyAmazing = (ImageView) itemView.findViewById(R.id.iv_smiley3);

            ll_heart_smiley = (ImageView) itemView.findViewById(R.id.iv_heart_smile);

            mImgvEventCommentSmileyLove = (ImageView) itemView.findViewById(R.id.iv_smiley4);

            mImgvEventCommentSmileySad = (ImageView) itemView.findViewById(R.id.iv_smiley5);

            tv_total_rating = (TextView) itemView.findViewById(R.id.tv_total_rating);

            iv_smile_default = (ImageView) itemView.findViewById(R.id.iv_smile_default);

            ratingImageView = (ImageView) itemView.findViewById(R.id.ratingImageView);

            txt_totalsmilecount = (TV_Regular_os) itemView.findViewById(R.id.txt_totalsmilecount);

            mImgvEventCommentSetSmiley = (ImageView) itemView.findViewById(R.id.imgv_event_comment_set_smiley);
            mImgvEventCommentSetSmiley.setOnClickListener(EventComentViewHolder.this);
        }

        @Override
        public void onClick(View v) {
            showDialogSmiley(getAdapterPosition());
        }
    }

    private void customShareDetailsPopup(final String msg_id, final int k) {
        if (rl_background == null)
            rl_background = (RelativeLayout) findViewById(R.id.rl_background);

        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.rating_star);
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setDimAmount(0f);

        final RatingBar ratingBar = (RatingBar) dialog.findViewById(R.id.ratingBar);
        TextView cancel = (TextView) dialog.findViewById(R.id.cancel);


        messageId = msg_id;

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {


            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                rating_selected = rating + "";

                int starts = (int) rating;
//                tvTotalGlobal = tvTotalGlobal1;
                mEventCommentDatas.get(k).setmCommentLike(String.valueOf(starts));
                mEventCommentDatas.get(k).setmYouLike(String.valueOf(starts));
                mEventCommentsAdapter.notifyDataSetChanged();
                new RatingData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msg_id);
                dialog.dismiss();
                rl_background.setVisibility(View.GONE);

//                mEventCommentDatas.get(k).set("comment_like", "1");


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
    }

    public class RatingData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_comment_rating");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", messageId);

                jo.put("group_id", post_id);

                jo.put("type", "post");

                jo.put("like_star", rating_selected);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    //tvTotalGlobal.setText(rating_selected);
                    new EventScreenData().execute();
                }

                //  setAdapter();

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }



    @SuppressLint("SetTextI18n")
    public void showDialogSmiley(final int position) {
        if (rl_background == null)
            rl_background = (RelativeLayout) findViewById(R.id.rl_background);

        rl_background.setVisibility(View.VISIBLE);
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.moods_activity_one);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setDimAmount(0f);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        final EventCommentData currentComment = mEventCommentDatas.get(position);

        ArrayList<HashMap<String, String>> lis = currentComment.getUsersMoods();

        if (lis != null) {
            for (int x = 0; x < lis.size(); x++) {

                HashMap moods = lis.get(x);


                if (moods.containsKey("happy")){

                    count_happy.setText("(" + moods.get("happy") + ")");

                }
                if (moods.containsKey("indifferen")){

                    count_indiff.setText("(" + moods.get("indifferen") + ")");

                }
                if (moods.containsKey("amazing")){

                    count_amaz.setText("(" + moods.get("amazing") + ")");

                }
                if (moods.containsKey("love")){

                    count_love.setText("(" + moods.get("love") + ")");

                }
                if (moods.containsKey("sad")){
                    count_sad.setText("(" + moods.get("sad") + ")");
                }

//                switch (moods) {
//                    case "happy":
//                        count_happy.setText("(" + count + ")");
//
//                        break;
//
//                    case "indifferen":
//                        count_indiff.setText("(" + count + ")");
//
//                        break;
//
//                    case "amazing":
//                        count_amaz.setText("(" + count + ")");
//
//                        break;
//                    case "love":
//                        count_love.setText("(" + count + ")");
//
//                        break;
//
//                    case "sad":
//                        count_sad.setText("(" + count + ")");
//
//                        break;
//                }
            }
        }

       Object mySmiley = currentComment.getMyMoods();

        if (mySmiley != null) {
            if (mySmiley.equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);

            } else if (mySmiley.equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }

        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///commentList.get(position).put("you_like", "1");

                String happyCountString = "0";

                ArrayList<HashMap<String, String>> usersMoods = currentComment.getUsersMoods();

                boolean isCollectionUpdated = false;

                if (usersMoods != null) {

                    for (int i = 0; i < usersMoods.size(); i++) {
                        HashMap <String, String> currentUsersMood = usersMoods.get(i);

                        if (currentUsersMood.containsKey("happy")) {
                            happyCountString = "" + currentUsersMood.get("happy");

                            int happyCountInt = Integer.parseInt(happyCountString);

                            happyCountInt++;

                            currentUsersMood.put("happy", String.valueOf(happyCountInt));

                            int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                            totalMoodsCountInt++;

                            currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));

                            isCollectionUpdated = true;
                            break;
                        }
                    }
                }

                if (!isCollectionUpdated) {
                    int happyCountInt = Integer.parseInt(happyCountString);

                    happyCountInt++;

                    HashMap<String, String> newMoodHmap = new HashMap<>();
                    newMoodHmap.put("happy", String.valueOf(happyCountInt));

                    if (usersMoods==null){
                        usersMoods = new ArrayList<>();
                    }

                    usersMoods.add(newMoodHmap);

                    int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                    totalMoodsCountInt++;

                    currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));
                }

                moodStr = "happy";

                mEventCommentDatas.get(position).setUsersMoods(usersMoods);

                mEventCommentsAdapter.notifyDataSetChanged();

                ///tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_happy.setImageResource(R.drawable.happy);


                new LikeServiceData()
                        .executeOnExecutor
                                (AsyncTask.THREAD_POOL_EXECUTOR, moodStr, currentComment.getMessageId(), "1", "" + position);

                dialog.dismiss();

            }
        });



        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///commentList.get(position).put("you_like", "1");

                String indifferentCountString = "0";

                ArrayList<HashMap<String, String>> usersMoods = currentComment.getUsersMoods();

                boolean isCollectionUpdated = false;

                if (usersMoods != null) {
                    for (int i = 0; i < usersMoods.size(); i++) {
                        HashMap <String, String> currentUsersMood = usersMoods.get(i);

                        if (currentUsersMood.containsKey("indifferen")) {
                            indifferentCountString = "" + currentUsersMood.get("indifferen");

                            int indifferentCountInt = Integer.parseInt(indifferentCountString);

                            indifferentCountInt++;

                            currentUsersMood.put("indifferen", String.valueOf(indifferentCountInt));

                            int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                            totalMoodsCountInt++;

                            currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));

                            isCollectionUpdated = true;

                            break;
                        }
                    }
                }

                if (!isCollectionUpdated) {
                    int indifferentCountInt = Integer.parseInt(indifferentCountString);

                    indifferentCountInt++;

                    HashMap<String, String> newMoodHmap = new HashMap<>();
                    newMoodHmap.put("indifferen", String.valueOf(indifferentCountInt));

                    if (usersMoods==null){
                        usersMoods = new ArrayList<>();
                    }

                    usersMoods.add(newMoodHmap);

                    int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                    totalMoodsCountInt++;

                    currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));
                }

                moodStr = "indifferen";
                mEventCommentDatas.get(position).setUsersMoods(usersMoods);

                mEventCommentsAdapter.notifyDataSetChanged();

                ///tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.indifferent);

                new LikeServiceData()
                        .executeOnExecutor
                                (AsyncTask.THREAD_POOL_EXECUTOR, moodStr, currentComment.getMessageId(), "1", "" + position);

                dialog.dismiss();

            }
        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///commentList.get(position).put("you_like", "1");

                String amazingCountString = "0";

                ArrayList<HashMap<String, String>> usersMoods = currentComment.getUsersMoods();

                boolean isCollectionUpdated = false;

                if (usersMoods != null) {
                    for (int i = 0; i < usersMoods.size(); i++) {
                        HashMap <String, String> currentUsersMood = usersMoods.get(i);

                        if (currentUsersMood.containsKey("amazing")) {
                            amazingCountString = "" + currentUsersMood.get("amazing");

                            int indifferentCountInt = Integer.parseInt(amazingCountString);

                            indifferentCountInt++;

                            currentUsersMood.put("amazing", String.valueOf(indifferentCountInt));

                            int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                            totalMoodsCountInt++;

                            currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));

                            isCollectionUpdated = true;

                            break;
                        }
                    }
                }

                if (!isCollectionUpdated) {
                    int indifferentCountInt = Integer.parseInt(amazingCountString);

                    indifferentCountInt++;

                    HashMap<String, String> newMoodHmap = new HashMap<>();
                    newMoodHmap.put("amazing", String.valueOf(indifferentCountInt));

                    if (usersMoods==null){
                        usersMoods = new ArrayList<>();
                    }

                    usersMoods.add(newMoodHmap);

                    int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                    totalMoodsCountInt++;

                    currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));
                }

                moodStr = "amazing";

                mEventCommentDatas.get(position).setUsersMoods(usersMoods);

                mEventCommentsAdapter.notifyDataSetChanged();

                ///tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.amazing);

                new LikeServiceData()
                        .executeOnExecutor
                                (AsyncTask.THREAD_POOL_EXECUTOR, moodStr, currentComment.getMessageId(), "1", "" + position);

                dialog.dismiss();
            }


        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///commentList.get(position).put("you_like", "1");

                String loveCountString = "0";

                ArrayList<HashMap<String, String>> usersMoods = currentComment.getUsersMoods();

                boolean isCollectionUpdated = false;

                if (usersMoods != null) {
                    for (int i = 0; i < usersMoods.size(); i++) {
                        HashMap <String, String> currentUsersMood = usersMoods.get(i);

                        if (currentUsersMood.containsKey("love")) {
                            loveCountString = "" + currentUsersMood.get("love");

                            int indifferentCountInt = Integer.parseInt(loveCountString);

                            indifferentCountInt++;

                            currentUsersMood.put("love", String.valueOf(indifferentCountInt));

                            int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                            totalMoodsCountInt++;

                            currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));

                            isCollectionUpdated = true;

                            break;
                        }
                    }
                }

                if (!isCollectionUpdated) {
                    int indifferentCountInt = Integer.parseInt(loveCountString);

                    indifferentCountInt++;

                    HashMap<String, String> newMoodHmap = new HashMap<>();
                    newMoodHmap.put("love", String.valueOf(indifferentCountInt));

                    if (usersMoods==null){
                        usersMoods = new ArrayList<>();
                    }

                    usersMoods.add(newMoodHmap);

                    int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                    totalMoodsCountInt++;

                    currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));
                }

                moodStr = "love";

                mEventCommentDatas.get(position).setUsersMoods(usersMoods);

                mEventCommentsAdapter.notifyDataSetChanged();

                ///tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.love);

                new LikeServiceData()
                        .executeOnExecutor
                                (AsyncTask.THREAD_POOL_EXECUTOR, moodStr, currentComment.getMessageId(), "1", "" + position);

                dialog.dismiss();

            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ///commentList.get(position).put("you_like", "1");

                String sadCountString = "0";

                ArrayList<HashMap<String, String>> usersMoods = currentComment.getUsersMoods();

                boolean isCollectionUpdated = false;

                if (usersMoods != null) {
                    for (int i = 0; i < usersMoods.size(); i++) {
                        HashMap <String, String> currentUsersMood = usersMoods.get(i);

                        if (currentUsersMood.containsKey("sad")) {
                            sadCountString = "" + currentUsersMood.get("sad");

                            int indifferentCountInt = Integer.parseInt(sadCountString);

                            indifferentCountInt++;

                            currentUsersMood.put("sad", String.valueOf(indifferentCountInt));

                            int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                            totalMoodsCountInt++;

                            currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));

                            isCollectionUpdated = true;

                            break;
                        }
                    }
                }

                if (!isCollectionUpdated) {
                    int indifferentCountInt = Integer.parseInt(sadCountString);

                    indifferentCountInt++;

                    HashMap<String, String> newMoodHmap = new HashMap<>();
                    newMoodHmap.put("sad", String.valueOf(indifferentCountInt));

                    if (usersMoods==null){
                        usersMoods = new ArrayList<>();
                    }

                    usersMoods.add(newMoodHmap);

                    int totalMoodsCountInt = Integer.parseInt(currentComment.getTotalMoodsCount());

                    totalMoodsCountInt++;

                    currentComment.setTotalMoodsCount(String.valueOf(totalMoodsCountInt));
                }

                moodStr = "sad";

                mEventCommentDatas.get(position).setUsersMoods(usersMoods);

                mEventCommentsAdapter.notifyDataSetChanged();

                ///tv_noti.setText(commentList.get(position).get("total_moods").toString());

                iv_indiff.setImageResource(R.drawable.sad);

                new LikeServiceData()
                        .executeOnExecutor
                                (AsyncTask.THREAD_POOL_EXECUTOR, moodStr, currentComment.getMessageId(), "1", "" + position);

                dialog.dismiss();

            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    private void initComponents(Bundle intentExtras) {
        String emptyString = "";

        String comaString = ",";

        String value;

        value = intentExtras.getString("title");
        if (value != null && !value.equals(emptyString)) {
            com.spott.app.fonts.TV_Bold_os txtEventName = (TV_Bold_os) findViewById(R.id.txt_event_name);
            txtEventName.setText(value);
        }
        LinearLayout spinerLayout =(LinearLayout) findViewById(R.id.spinnerLayout);

        final Spinner spnrEventStatus = (Spinner) findViewById(R.id.spnr_event_status);
        value = intentExtras.getString("status");
        if (value != null && !value.equals(emptyString)) {

            switch (value) {
                case "Not Attending":
                    spnrEventStatus.setSelection(0, false);
                    break;
                case "Interested":
                    spnrEventStatus.setSelection(1, false);
                    break;
                case "Attending":
                    spnrEventStatus.setSelection(2, false);
                    break;
                case "Has Ticket":
                    spnrEventStatus.setSelection(3, false);
            }
        }
        spnrEventStatus.setOnItemSelectedListener(EventActivity.this);

        spinerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               spnrEventStatus.performClick();
            }
        });


        value = intentExtras.getString("post_create_date");
        if (value != null && !value.equals(emptyString)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date newDate = null;
            try {
                newDate = format.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("MMM'.' dd, ''yy", Locale.US);
            String date = format.format(newDate);

            com.spott.app.fonts.TV_Regular_os txtPostDate = (TV_Regular_os) findViewById(R.id.txt_post_date);
            txtPostDate.setText(date);
        }

        value = intentExtras.getString("popup_image");
        if (value != null && !value.equals(emptyString)) {
            ImageView eventImage = (ImageView) findViewById(R.id.activity_event_img);
            Picasso.with(EventActivity.this)
                    .load(value)
                    .into(eventImage);
        }

        value = intentExtras.getString("date_of_start");
        if (value != null && !value.equals(emptyString)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = null;
            try {
                newDate = format.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("MMMM dd yyyy", Locale.US);
            String date = format.format(newDate);

            TV_Regular_os txtEventDateFrom = (TV_Regular_os) findViewById(R.id.txt_event_date_from);
            txtEventDateFrom.setText("From " + date);

            value = intentExtras.getString("start_timing");

            if (value != null && !value.equals(emptyString)) {
                txtEventDateFrom.append(comaString + SPACE + value);
            }
        }

        value = intentExtras.getString("date_of_end");
        if (value != null && !value.equals(emptyString)) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = null;
            try {
                newDate = format.parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            format = new SimpleDateFormat("MMMM dd yyyy", Locale.US);
            String date = format.format(newDate);

            TV_Regular_os txtEventDateTo = (TV_Regular_os) findViewById(R.id.txt_event_date_to);
            txtEventDateTo.setText(date);

            value = intentExtras.getString("end_timing");

            if (value != null && !value.equals(emptyString)) {
                txtEventDateTo.append(comaString + SPACE + value);
            }
        }

        value = intentExtras.getString("latitude");
        if (value != null && !value.equals(emptyString)) {
            TV_Regular_os txtEventCoordinates = (TV_Regular_os) findViewById(R.id.txt_event_coordinates);
            txtEventCoordinates.setText(value);

            value = intentExtras.getString("longitude");
            if (value != null && !value.equals(emptyString)) {
                txtEventCoordinates.append("," + SPACE + value);
            }
        }

        value = intentExtras.getString("detail");
        if (value != null && !value.equals(emptyString)) {
            TV_Regular_os txtEventDetails = (TV_Regular_os) findViewById(R.id.txt_event_details);
            txtEventDetails.setText(value);
        }

        tv_noti = (TV_Regular_os) findViewById(R.id.tv_noti);
        value = intentExtras.getString("total_likes");
        if (value != null && !value.equals(emptyString)) {
            tv_noti.setText(value);
        }

        value = intentExtras.getString("total_comments");
        if (value != null && !value.equals(emptyString)) {
            TV_Regular_os txtTotalComments = (TV_Regular_os) findViewById(R.id.txt_event_total_comments);
            txtTotalComments.setText(value);
        }

        iv_heart = (ImageView) findViewById(R.id.iv_heart);

        tv_noti = (TextView) findViewById(R.id.tv_noti);

        rl_sel_smileys = (RelativeLayout) findViewById(R.id.rl_sel_smileys);

        iv_smiley1 = (ImageView) findViewById(R.id.iv_smiley1);
        iv_smiley2 = (ImageView) findViewById(R.id.iv_smiley2);
        iv_smiley3 = (ImageView) findViewById(R.id.iv_smiley3);
        iv_smiley4 = (ImageView) findViewById(R.id.iv_smiley4);
        iv_smiley5 = (ImageView) findViewById(R.id.iv_smiley5);

        et_leave_comment = (EmojiconEditText) findViewById(R.id.et_leave_comment);
        et_leave_comment.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                ///Log.d("EventActivity...onFocusChange", String.valueOf(hasFocus));

                if (hasFocus) {
                    if (iv_emojibtn == null) {
                        iv_emojibtn = (ImageView) findViewById(R.id.iv_emojibtn);
                        iv_emojibtn.setOnClickListener(EventActivity.this);

                        iv_keyboard = (ImageView) findViewById(R.id.iv_keyboard);
                        iv_keyboard.setOnClickListener(EventActivity.this);
                    }

                    iv_emojibtn.setVisibility(View.VISIBLE);
                } else {
                    iv_emojibtn.setVisibility(View.GONE);

                    iv_keyboard.setVisibility(View.GONE);

                    emoLay.setVisibility(View.GONE);
                }
            }
        });

        et_leave_comment.setOnClickListener(EventActivity.this);

        ll_calendar = (LinearLayout) findViewById(R.id.ll_calendar);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);


        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_calendar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        ///Log.d("EventActivity...onClick", String.valueOf(v));

        if (view == iv_emojibtn) {
            iv_emojibtn.setVisibility(View.GONE);

            try {
                CommonUtilities.hideSoftKeyboard(EventActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (emoLay == null)
                emoLay = (LinearLayout) findViewById(R.id.emoLay);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    emoLay.setVisibility(View.VISIBLE);
                }
            }, 200);

            iv_keyboard.setVisibility(View.VISIBLE);
        } else if (view == iv_keyboard) {
            iv_keyboard.setVisibility(View.GONE);

            iv_emojibtn.setVisibility(View.VISIBLE);
        } else if (view == et_leave_comment) {
            iv_emojibtn.setVisibility(View.VISIBLE);
            iv_keyboard.setVisibility(View.GONE); /// TODO nullpointer?
        } else {
            switch (view.getId()) {

                case R.id.ll_feed:
                    finish();
                    setResult(RESULT_OK);
                    Intent intent = new Intent(EventActivity.this, MyFeedActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent);

                    break;

                case R.id.ll_profile:
                    finish();
                    setResult(RESULT_OK);
                    Intent intent1 = new Intent(EventActivity.this, MyProfileActivity.class);
                    intent1.putExtra("uid", preferences.getString("uid", ""));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent1);

                    break;

                case R.id.ll_groups:

                    ///
                    /*Intent intent2 = new Intent(EventActivity.this, MyCrewsActivity.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent2);*/
                    finish();
                    setResult(RESULT_OK);
                    Intent intent2 = new Intent(EventActivity.this, JoinGroupActivity.class);
                    startActivity(intent2);

                    break;

                case R.id.ll_calendar:
                    finish();
                    setResult(RESULT_OK);
                    Intent intent3 = new Intent(EventActivity.this, CalendarActivity.class);
                    intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(intent3);
            }
        }
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(et_leave_comment, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(et_leave_comment);
    }

    public void showSecondDialogSmiley(String your_like, final String val) {
        if (rl_background == null)
            rl_background = (RelativeLayout) findViewById(R.id.rl_background);

        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(EventActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.moods_activity_one);
        dialog.getWindow().setDimAmount(0f);

        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);

        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);

        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);

        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);

        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);

        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);

        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);

        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);

        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);

        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);

        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);

        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);

        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);

        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);

        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);

        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);

        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);

        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);

        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);

        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);

        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        for (int x = 0; x < mainMapList.size(); x++) {

            final HashMap<String, Object> mapHash = mainMapList.get(x);

            String moods = mapHash.get("moods").toString();

            String count = mapHash.get("count").toString();

            tempPos = x;

            /*your_like = "";

            your_like = "" + mapHash.get("your_like");*/

            switch (moods) {
                case "happy":

                    count_happy.setText("(" + count + ")");

                    break;

                case "indifferen":

                    count_indiff.setText("(" + count + ")");

                    break;

                case "amazing":
                    count_amaz.setText("(" + count + ")");

                    break;
                case "love":
                    count_love.setText("(" + count + ")");

                    break;

                case "sad":
                    count_sad.setText("(" + count + ")");

                    break;

            }
        }
        if (!your_like.equals("")) {

            if (your_like.equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);
            }
            if (your_like.equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }
        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);

                if (moodStr == "happy") {
                    iv_smiley1.setVisibility(View.VISIBLE);
                }
                moodStr = "happy";

                callAPI(val);

                dialog.dismiss();


            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "indifferen") {
                    iv_smiley2.setVisibility(View.VISIBLE);
                }

                moodStr = "indifferen";

                callAPI(val);

                dialog.dismiss();


            }

        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "amazing") {
                    iv_smiley3.setVisibility(View.VISIBLE);
                }
                moodStr = "amazing";

                callAPI(val);

                dialog.dismiss();


            }

        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "love") {
                    iv_smiley4.setVisibility(View.VISIBLE);
                }
                moodStr = "love";

                callAPI(val);

                dialog.dismiss();


            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_sel_smileys.setVisibility(View.VISIBLE);
                if (moodStr == "sad") {
                    iv_smiley5.setVisibility(View.VISIBLE);
                }
                moodStr = "sad";

                callAPI(val);

                dialog.dismiss();


            }
        });

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });


    }

    private void callAPI(String val) {
        if (val.equals("1")) {
            new LikeServiceData1().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, post_id, "1", "" + tempPos);
        } else if (val.equals("2")) {
            new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, post_id, "1", "" + tempPos);
        }
    }

    private class SetEventStatusData extends AsyncTask<Integer, String, String> {
        @Override
        protected String doInBackground(Integer... integers) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "set_event_status");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("event_id", post_id);

                int eventStatusSpinnerPosition = integers[0];

                switch (eventStatusSpinnerPosition) {
                    case 1:
                        jo.put("status", "Interested");
                        break;

                    case 2:
                        jo.put("status", "Attending");
                        break;

                    case 3:
                        jo.put("status", "Has Ticket");
                        break;

                    case 0:
                        jo.put("status", "Not Attending");
                }

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                return content.toString();

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            ///Log.d("EventActivity...SetEventStatusData...onPostExecute", result);
        }
    }

    private class LikeServiceData1 extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "post");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {
                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        new EventScreenData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class LikeServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "", moodString = "";
        int position = 0;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "comment_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "comment");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]); /// TODO не використовується

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    new EventScreenData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class ChangePostData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "change_post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "post");

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {
                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        new EventScreenData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void postCommentOnclick(View v) {
        if (tv_send == null) {
            tv_send = (TV_Regular_os) findViewById(R.id.tv_send);
        }

        tv_send.setEnabled(false);

        String commentString = String.valueOf(et_leave_comment.getText());

        if (!commentString.equals("") || encodedImage != null) {
            if (!connDec.isConnectingToInternet()) {
                if (alert == null)
                    alert = new Alert_Dialog_Manager(EventActivity.this);

                alert.showNetAlert();
            } else {
                new PostCommentData(commentString).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        } else
            Toast.makeText(EventActivity.this, "Please, provide your comment!", Toast.LENGTH_SHORT).show();

    }

    public void showCustomMessagephoto(View v) {
        final Dialog dialogphoto = new Dialog(EventActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();

                        if (apiLevel == 0)
                            apiLevel = Build.VERSION.SDK_INT;

                        if (apiLevel >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(EventActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(EventActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequestForGallery();
                            } else {
                                OpenGallery();
                            }
                        } else {
                            OpenGallery();
                        }
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();

                        if (apiLevel == 0)
                            apiLevel = Build.VERSION.SDK_INT;

                        if (apiLevel >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(EventActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(EventActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequestForCamera();
                            } else {
                                takePicture();
                            }
                        } else {
                            takePicture();
                        }

                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();
    }

    private void makeRequestForGallery() {
        ActivityCompat
                .requestPermissions
                        (EventActivity.this,
                                new String[]{Constants.ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE, Constants.ANDROID_PERMISSION_READ_EXTERNAL_STORAGE},
                                Constants.REQUEST_CODE_GALLERY);
    }

    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), Constants.REQUEST_CODE_GALLERY);
    }

    private void makeRequestForCamera() {
        ActivityCompat
                .requestPermissions
                        (EventActivity.this,
                                new String[]{android.Manifest.permission.CAMERA, Constants.ANDROID_PERMISSION_WRITE_EXTERNAL_STORAGE},
                                Constants.REQUEST_CODE_TAKE_PICTURE);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            checkExternalStorageAndCreateTempFile();

            Uri mImageCaptureUri;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(tempImageFile);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, Constants.REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkExternalStorageAndCreateTempFile() {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            tempImageFile = new File(Environment.getExternalStorageDirectory(),
                    Constants.TEMP_PHOTO_FILE_NAME);
        } else {
            tempImageFile = new File(getFilesDir(), Constants.TEMP_PHOTO_FILE_NAME);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Bitmap bitmap;

            switch (requestCode) {
                case Constants.REQUEST_CODE_GALLERY:
                    try {
                        checkExternalStorageAndCreateTempFile();

                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        FileOutputStream fileOutputStream = new FileOutputStream(tempImageFile);

                        copyStream(inputStream, fileOutputStream);

                        fileOutputStream.close();

                        inputStream.close();

                        startCropImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case Constants.REQUEST_CODE_TAKE_PICTURE:
                    startCropImage();
                    break;
                case Constants.REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(tempImageFile.getPath());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                    ImageView attached = (ImageView) findViewById(R.id.iv_attached_image);
                    attached.setImageBitmap(bitmap);

                    findViewById(R.id.rellay_attachment).setVisibility(View.VISIBLE);

                    if (tempImageFile.exists()) {
                        tempImageFile.delete();
                    }

                    base64frombitmap(bitmap);

                    /*iv_signup_img.setImageBitmap(bitmap);
                    iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
                    iv_signup_img.setBorderWidth(2);*/

            }
        } else {
            if (requestCode == Constants.REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                startCropImage();
            }
        }
    }

    private void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encodedImage;
    }

    private void startCropImage() {

        //System.out.println("cropimage");

        Intent intent = new Intent(EventActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, tempImageFile.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, Constants.REQUEST_CODE_CROP_IMAGE);
    }

    private class PostCommentData extends AsyncTask<String, String, String> {
        private String postingText;

        private String getData;

        private PostCommentData(String postingText) {
            PostCommentData.this.postingText = postingText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            LoaderClass.ShowProgressWheel(EventActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_comments");

                jo.put("content", CommonUtilities.encodeUTF8(postingText));

                if (encodedImage != null) ///
                    jo.put("image", encodedImage);

                jo.put("login_token", preferences.getString("login_token", ""));

                post_id = getIntent().getStringExtra("post_id");

                jo.put("ref_id", post_id);

                jo.put("source", "post");

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();
                getData = content.toString();


            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                LoaderClass.HideProgressWheel(EventActivity.this);

                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                if (errCode.equals("0")) {
                    ///String time_stamp = resultObj.getString("time_stamp");

                    ///preferences.edit().putString("time_stamp", time_stamp).commit();

                    JSONObject user_dataObj = resultObj.getJSONObject("comment");

                    String messageId = user_dataObj.getString("message_id");
                    String avatar = user_dataObj.getString("user_image"); /// TODO тут перевірити, яке дефолтне значення
                    String content = user_dataObj.getString("content");
                    String image = user_dataObj.getString("post_image");
                    String first_name = user_dataObj.getString("first_name");
                    String last_name = user_dataObj.getString("last_name");
                    String message_create_date = user_dataObj.getString("message_create_date");
                    String youLikes = user_dataObj.optString("you_like");
                    String comment_like  = user_dataObj.optString("comment_like");

                    EventCommentData sentCommentData = new EventCommentData(messageId, avatar, content, first_name, last_name, message_create_date,youLikes,comment_like);

                    if (image != null)
                        sentCommentData.setImage(image);

                    mEventCommentsAdapter.addComment(sentCommentData);

                    String sentCommentText = String.valueOf(et_leave_comment.getText());

                    if (!sentCommentText.equals("")) {
                        et_leave_comment.setText("");

                        if (encodedImage != null)
                            removeAttachedPicture(null);
                    } else
                        removeAttachedPicture(null);

                    tv_send.setEnabled(true);
                    mEventCommentsAdapter.notifyDataSetChanged();

                    View lastChild = scrollView.getChildAt(scrollView.getChildCount() - 1);
                    int bottom = lastChild.getBottom();
                    int sy = scrollView.getScrollY();
                    int sh = scrollView.getHeight();
                    int delta = bottom;

                    scrollView.smoothScrollBy(0, delta);


//                    mEventCommentsAdapter.notifyDataSetChanged();
//                    commentsRecycler.scrollToPosition(mEventCommentsAdapter.getListSize()-1);
//                    scrollView.fullScroll(View.FOCUS_DOWN);

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

    }

    public void removeAttachedPicture(View v) { ///
        findViewById(R.id.rellay_attachment).setVisibility(View.GONE);

        ImageView thumbmailImageView = (ImageView) findViewById(R.id.iv_attached_image);

        thumbmailImageView.setImageDrawable(null);

        encodedImage = null;
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (encodedImage != null)
            removeAttachedPicture(null);
    }

    @Override
    public void onBackPressed() {
        if (emoLay != null) {
            if (emoLay.getVisibility() == View.VISIBLE) {
                emoLay.setVisibility(View.GONE);

                iv_keyboard.setVisibility(View.GONE);

                iv_emojibtn.setVisibility(View.VISIBLE);
            }

            else {
                setResult(RESULT_OK);

                finish();
            }
        } else {
            setResult(RESULT_OK);

            finish();
        }
    }

    public void backArrowOnclick(View v) {
        setResult(RESULT_OK);

        finish();
    }
}
