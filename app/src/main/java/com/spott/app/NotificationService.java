package com.spott.app;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.text.Html;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Random;



/**
 * Created by Admin on 10/12/2016.
 */

public class NotificationService extends FirebaseMessagingService
{

    String  action , value = "";

    public static final String NOTIFICATION_DATA = "NOTIFICATION_DATA";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage)
    {
        super.onMessageReceived(remoteMessage);

        System.out.println("From "+remoteMessage.getFrom()+" bundle "+remoteMessage.getData());

        String message = remoteMessage.getData().get("message");

        ShowMessage(message,remoteMessage);
    }

    private void ShowMessage(String msg,RemoteMessage remoteMessage)
    {
        JSONObject jsonObject = null;

        String getAction = "" , getJobId = "",getJobAdminId = "";
        try {
            jsonObject = new JSONObject(remoteMessage.getData().get("action"));

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        Intent intent = new Intent(NotificationService.this, MyFeedActivity.class).
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
       TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack
        stackBuilder.addParentStack(MyFeedActivity.class);
// Adds the Intent to the top of the stack
       stackBuilder.addNextIntent(intent);
//
//


       PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.addLine(Html.fromHtml(msg));

        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(getString(R.string.app_name));
        bigTextStyle.bigText(Html.fromHtml(msg));

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this).
                setSmallIcon(R.drawable.flag_icon)
              ///  .setStyle(inboxStyle)
                .setStyle(bigTextStyle)
                .setColor(ContextCompat.getColor(NotificationService.this,R.color.red))
                .setContentIntent(resultPendingIntent)
                 .setContentTitle(this.getString(R.string.app_name)).setContentText(Html.fromHtml(msg))
                .setAutoCancel(true).setSound(defaultSoundUri);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        Random random = new Random();

       int code =  random.nextInt(8888989-59648)+2965;

        notificationManager.notify(code, notificationBuilder.build());

    }

}
