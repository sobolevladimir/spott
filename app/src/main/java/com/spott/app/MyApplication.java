package com.spott.app;

import android.app.Application;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Satvir on 11/21/2016.
 */

public class MyApplication extends Application {

    Tracker tracker = null;

    @Override
    public void onCreate()
    {
        super.onCreate();

        new FlurryAgent.Builder().withLogEnabled(true).build(this, "");
    }

    synchronized public Tracker getDefaultTracker()
    {
        if (tracker == null)
        {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);

            tracker = analytics.newTracker(R.xml.global_tracker);
        }

        return tracker;
    }
}