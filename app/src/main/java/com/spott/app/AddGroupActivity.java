package com.spott.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;
import com.spott.app.cropiimage.CropImage;

/**
 * Created by Acer on 9/21/2016.
 */
public class AddGroupActivity extends FragmentActivity implements View.OnClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    ImageView iv_back_icon, iv_red_icon, iv_red_cube,iv_emojibtn, iv_emojibtn_grp_detail, iv_keyboard, iv_keyboard_grp_detail;
    DiamondImageView iv_group_photo;
    EditText et_grp_name, et_grp_detail;
    ContactsCompletionView et_tags, et_people_add;
    Button btn_add;
    RadioButton first, second;
    TextView tv_click, tv_num, tv_grp_photo_txt;
    LinearLayout ll_feed, ll_profile, ll_directory, emoLay;
    RelativeLayout rl_tags,ll_groups,rl_emo_buttons_grp_name, rl_emo_buttons_grp_detail;
    Activity activity;
    SpannableString ss5;
    SharedPreferences preferences;
    String namestr = "", grpdetailStr = "", radioButtonText;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory;
    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    private String img_state;
    private File mFileTemp;
    private BroadcastReceiver broadcastReceiver;
    com.spott.app.ConnectionDetector connDec;
    com.spott.app.Alert_Dialog_Manager alert;
    public static final int REQUEST_CODE_GALLERY = 0x1,
            REQUEST_CODE_TAKE_PICTURE = 0x2, REQUEST_CODE_CROP_IMAGE = 0x3;
    String encoded = "";
    String Radiobtn = "public";
    String photo = "";
    StringBuilder stringBuilder = new StringBuilder(9999999);
    ArrayList<Object> tokens = new ArrayList<>();
    JSONArray jArray;
    String grpname = "", grpdetal = "", profile_id = "", grpType = "", grpTag = "";
    String chipArr = "", id = "";
    ArrayList<HashMap<String, String>> alreadySelectedList = null, listtToSend = null;
    public static int api_level = 0;
    Context context;

    @Override
    public void onBackPressed() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(et_tags.getWindowToken(), 0);

        if (emoLay.getVisibility() == View.VISIBLE)
            emoLay.setVisibility(View.GONE);
        else
            finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_group_activity);
        this.activity = (Activity) AddGroupActivity.this;
        context = AddGroupActivity.this;
        initialise();
        clickables();
        alreadySelectedList = new ArrayList<>();
        listtToSend = new ArrayList<>();
        setting();
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        Intent intent = getIntent();

        ss5 = new SpannableString("☺");
        ss5.setSpan(new StyleSpan(Typeface.BOLD), 0, ss5.length(), 0);


        if (intent.hasExtra("group_type")) {
            grpname = intent.getStringExtra("group_name");
            grpdetal = intent.getStringExtra("group_detail");
            photo = intent.getStringExtra("group_image");
            Radiobtn = intent.getStringExtra("group_type");
            grpTag = intent.getStringExtra("group_tags");
            if (Radiobtn.equals("Public")) {
                first.setChecked(true);
                second.setChecked(false);
            } else {
                second.setChecked(true);
                first.setChecked(false);
            }
            if (!encoded.equals("")) {
                Picasso.with(getApplicationContext()).load(encoded).into(iv_group_photo);
            }
            et_grp_name.setText(grpname);
            et_grp_detail.setText(grpdetal);
            et_tags.setText(grpTag);
        }
    }


    private void initialise() {
        rl_tags=(RelativeLayout)findViewById(R.id.rl_tags);
        tv_grp_photo_txt = (TextView) findViewById(R.id.tv_grp_photo_txt);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        connDec = new ConnectionDetector(AddGroupActivity.this);
        alert = new Alert_Dialog_Manager(context);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_group_photo = (DiamondImageView) findViewById(R.id.iv_group_photo);
       /* iv_group_photo.setBorderColor(Color.parseColor("#F15A51"));
        iv_group_photo.setBorderWidth(2);*/
        iv_red_cube = (ImageView) findViewById(R.id.iv_red_cube);
        iv_emojibtn=(ImageView)findViewById(R.id.iv_emojibtn);
        iv_keyboard = (ImageView) findViewById(R.id.iv_keyboard);
        rl_emo_buttons_grp_name = (RelativeLayout) findViewById(R.id.rl_emo_buttons_grp_name);
        iv_keyboard_grp_detail = (ImageView) findViewById(R.id.iv_keyboard_grp_detail);
        rl_emo_buttons_grp_detail = (RelativeLayout) findViewById(R.id.rl_emo_buttons_grp_detail);
        iv_emojibtn_grp_detail=(ImageView)findViewById(R.id.iv_emojibtn_grp_detail);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        emoLay = (LinearLayout) findViewById(R.id.emoLay);
        tv_click = (TextView) findViewById(R.id.tv_click);
        tv_num = (TextView) findViewById(R.id.tv_num);
        et_grp_name = (EditText) findViewById(R.id.et_grp_name);
        et_grp_detail = (EditText) findViewById(R.id.et_grp_detail);
        et_grp_detail = (EditText) findViewById(R.id.et_grp_detail);
        et_people_add = (ContactsCompletionView) findViewById(R.id.et_people_add);
        et_people_add.allowCollapse(false);
        et_tags = (ContactsCompletionView) findViewById(R.id.et_tags);
        btn_add = (Button) findViewById(R.id.btn_add);
        first = (RadioButton) findViewById(R.id.first);
        second = (RadioButton) findViewById(R.id.second);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        et_tags.allowCollapse(false);
        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);
    }

    private void clickables() {
        rl_tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_tags.isEnabled()) {
                    et_tags.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(et_tags, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddGroupActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });

        iv_emojibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard.setVisibility(View.VISIBLE);
                iv_emojibtn.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);


            }
        });

        iv_keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard.setVisibility(View.GONE);
                iv_emojibtn.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_grp_name);
            }
        });

        et_grp_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons_grp_name.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons_grp_name.setVisibility(View.GONE);
                    iv_emojibtn.setVisibility(View.VISIBLE);
                    iv_keyboard.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_grp_name.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_grp_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn.setVisibility(View.VISIBLE);
                iv_keyboard.setVisibility(View.GONE);
            }
        });

        iv_emojibtn_grp_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_grp_detail.setVisibility(View.VISIBLE);
                iv_emojibtn_grp_detail.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);
            }
        });

        iv_keyboard_grp_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_grp_detail.setVisibility(View.GONE);
                iv_emojibtn_grp_detail.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_grp_detail);
            }
        });

        et_grp_detail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    rl_emo_buttons_grp_detail.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons_grp_detail.setVisibility(View.GONE);
                    iv_emojibtn_grp_detail.setVisibility(View.VISIBLE);
                    iv_keyboard_grp_detail.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });



        et_grp_detail.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);
                return false;
            }
        });

        et_grp_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_grp_detail.setVisibility(View.VISIBLE);
                iv_keyboard_grp_detail.setVisibility(View.GONE);
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(AddGroupActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                second.setChecked(false);
                Radiobtn = "public";

                System.out.println("RADIO=" + Radiobtn);
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                first.setChecked(false);
                Radiobtn = "private";
                System.out.println("RADIO=" + Radiobtn);
            }
        });

        iv_group_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMessagephoto("UTC", "Profile Pic");
            }
        });

        tv_grp_photo_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMessagephoto("UTC", "Profile Pic");
            }
        });
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }

        tv_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listtToSend.clear();

                List<Person> list = et_people_add.getObjects();
                for (int i = 0; i < list.size(); i++) {
                    String id = list.get(i).getName();

                    for (int J = 0; J < alreadySelectedList.size(); J++) {
                        String matchId = alreadySelectedList.get(J).get("id");

                        if (id.equals(matchId)) {
                            listtToSend.add(alreadySelectedList.get(J));
                        }
                    }

                }
                Intent intent = new Intent(AddGroupActivity.this, SelectMemberActivity.class);
                intent.putExtra("group_name", et_grp_name.getText().toString());
                intent.putExtra("group_type", Radiobtn);
                System.out.println("RADIO1=" + Radiobtn);
                intent.putExtra("group_detail", et_grp_detail.getText().toString());
                intent.putExtra("group_image", encoded);
                intent.putExtra("group_tags", stringBuilder.toString());

                if (listtToSend.size() > 0) {
                    intent.putExtra("list", listtToSend);
                }
                startActivity(intent);
            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                namestr = et_grp_name.getText().toString().trim();
                namestr = CommonUtilities.encodeUTF8(namestr);

                grpdetailStr = et_grp_detail.getText().toString().trim();
                grpdetailStr = CommonUtilities.encodeUTF8(grpdetailStr);

                radioButtonText = first.getText().toString();
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else if (encoded.equals("") || (encoded == null)) {

                    Toast.makeText(getApplicationContext(), "Please select userImage", Toast.LENGTH_SHORT).show();

                } else if (!(namestr.length() > 0)) {

                    Toast.makeText(getApplicationContext(), "Please enter Group Name", Toast.LENGTH_SHORT).show();
                } else if (radioButtonText.matches("")) {
                    Toast.makeText(getApplicationContext(), "Please select Group Type", Toast.LENGTH_SHORT).show();

                } else if (!(grpdetailStr.length() > 0)) {

                    Toast.makeText(getApplicationContext(), "Please enter Group Detail", Toast.LENGTH_SHORT).show();
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new AddGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new AddGroupData().execute();
                    }

                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentfilterList = new IntentFilter("add_cross");

        registerReceiver(addCross, new IntentFilter(intentfilterList));
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Add Group Screen");

        FlurryAgent.onStartSession(AddGroupActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(AddGroupActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    private ArrayList<String> uidArray = new ArrayList<>();

    private BroadcastReceiver addCross = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent in) {

            System.out.println("BroadCast RECEIVED");
            try {
                //  if (getIntent().hasExtra("selctedArraylist")) {
                alreadySelectedList = (ArrayList<HashMap<String, String>>) in.getSerializableExtra("selctedArraylist");

                uidArray = (ArrayList<String>) in.getSerializableExtra("uidArray");

                System.out.println("BroadCast RECEIVED1" + alreadySelectedList);

                et_people_add.clear();

                for (int i = 0; i < alreadySelectedList.size(); i++) {

                    if (chipArr.equals("")) {
                        chipArr = alreadySelectedList.get(i).get("first_name") + " " + alreadySelectedList.get(i).get("last_name");
                        id = alreadySelectedList.get(i).get("id");
                        chipArr = chipArr.replace(", ", "");
                        et_people_add.addObject(new Person(id, CommonUtilities.decodeUTF8(chipArr)));
                    } else {
                        chipArr = "";
                        chipArr = chipArr + " , " + alreadySelectedList.get(i).get("first_name") + " " + alreadySelectedList.get(i).get("last_name");
                        id = alreadySelectedList.get(i).get("id");
                        chipArr = chipArr.replace(", ", "");
                        et_people_add.addObject(new Person(id, CommonUtilities.decodeUTF8(chipArr)));
                    }
                }


                //et_people_add.setText(chipArr);

                // }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();

        System.out.println(" DESTROYED ");

        try {
            unregisterReceiver(addCross);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCustomMessagephoto(String string, String string2) {
        final Dialog dialogphoto = new Dialog(AddGroupActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        api_level = Build.VERSION.SDK_INT;
                        if (api_level >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(AddGroupActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(AddGroupActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequest2();
                            } else {
                                OpenGallery();
                            }
                        } else {
                            OpenGallery();
                        }
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        api_level = Build.VERSION.SDK_INT;
                        if (api_level >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(AddGroupActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(AddGroupActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequest1();
                            } else {
                                takePicture();
                            }
                        } else {
                            takePicture();
                        }

                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();

    }

    public void makeRequest1() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 1);
    }

    @SuppressLint("Override")
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(AddGroupActivity.this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();

                } else {
                    takePicture();
                }
                break;

            case 2:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(AddGroupActivity.this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();

                } else {
                    OpenGallery();
                }
                break;
        }
    }

    public void makeRequest2() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 2);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:

                Intent intent = new Intent(AddGroupActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:

                Intent intent1 = new Intent(AddGroupActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:

                Intent intent2 = new Intent(AddGroupActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:

                Intent intent3 = new Intent(AddGroupActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }


    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_GALLERY);
    }

    static ArrayList<Bitmap> bitmaps_arrayList = new ArrayList<Bitmap>();

    File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Bitmap bitmap;

            byte[] b;
            switch (requestCode) {
                case REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                        copyStream(inputStream, fileOutputStream);

                        fileOutputStream.close();

                        inputStream.close();

                        startCropImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case REQUEST_CODE_TAKE_PICTURE:
                    startCropImage();
                    break;
                case REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                    b = baos.toByteArray();

                    //String baseimg = Base64.encodeToString(b, Base64.DEFAULT);

                    System.out.println("bitmap in return : " + bitmap);

                    bitmaps_arrayList.add(bitmap);

                    if (mFileTemp.exists()) {
                        mFileTemp.delete();
                    }

                    encoded = base64frombitmap(bitmap);

                    int bwidth = bitmap.getWidth();

                    int bheight = bitmap.getHeight();

                    int swidth = iv_group_photo.getWidth();

                    int sheight = iv_group_photo.getHeight();

                    int new_width = swidth;

                    iv_group_photo.setImageBitmap(bitmap);

                    tv_grp_photo_txt.setVisibility(View.GONE);
            }
        } else {
            if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                startCropImage();
            }
        }
    }

    private void startCropImage() {

        Intent intent = new Intent(AddGroupActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        if (et_grp_name.hasFocus()) {
            EmojiconsFragment.input(et_grp_name, emojicon);
        } else if (et_grp_detail.hasFocus()) {
            EmojiconsFragment.input(et_grp_detail, emojicon);
        }
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        if (et_grp_name.hasFocus()) {
            EmojiconsFragment.backspace(et_grp_name);
        } else if (et_grp_detail.hasFocus()) {
            EmojiconsFragment.backspace(et_grp_detail);
        }

    }

    public class AddGroupData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        String[] tagarr;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(AddGroupActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "create_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_name", namestr);

                jo.put("group_type", Radiobtn);
                jo.put("group_detail", grpdetailStr);

                jo.put("group_image", encoded);


                List<Person> array = et_tags.getObjects();

                StringBuilder stringBuilder = new StringBuilder(9999999);

                for (int i = 0; i < array.size(); i++) {
                    stringBuilder.append(array.get(i).getName() + ",");
                }

                jo.put("group_tags", stringBuilder.toString());

                jo.put("member", new JSONArray(uidArray));


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    /*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AddGroupActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Group Created Successfully.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    activity.runOnUiThread(new Runnable() {
                        public void run() {

                            alertDialog.show();

                        }
                    });*/

                    Toast.makeText(AddGroupActivity.this, "Group created successfully.", Toast.LENGTH_SHORT).show();

                    Intent intent1 = new Intent(AddGroupActivity.this, MyCrewsActivity.class);
                    startActivity(intent1);

                    /*Intent intent = getIntent();
                    grpname = intent.getStringExtra("group_name");
                    grpdetal = intent.getStringExtra("group_detail");
                    encoded = intent.getStringExtra("group_image");
                    grpType = intent.getStringExtra("group_type");
                    grpTag = intent.getStringExtra("group_tags");*/

                    Picasso.with(getApplicationContext()).load(encoded).into(iv_group_photo);
                    et_grp_name.setText(grpname);
                    first.setText(grpType);
                    second.setText(grpType);
                    et_grp_detail.setText(grpdetal);
                    et_tags.setText(grpTag);


                } else if (errCode.equals("700")) {

                    //showDialogSession();


                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    startActivity(new Intent(AddGroupActivity.this, LoginScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                                    finish();
                                    // activity.finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    activity.runOnUiThread(new Runnable() {
                        public void run() {

                            alertDialog.show();

                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(AddGroupActivity.this);

        }

        private void showDialogSession() {

            final Dialog dialog = new Dialog(activity);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            dialog.setContentView(R.layout.session_popup);

            TextView ok_button = (TextView) dialog.findViewById(R.id.ok_button);

            ok_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AddGroupActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });

            dialog.show();
        }

    }
}
