package com.spott.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.DrawableHelper;
import com.spott.app.common.ForegroundCheckTask;
import com.spott.app.common.LoaderClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Acer on 9/13/2016.
 */
public class MyFeedActivity extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {


    FeedAdapter adapter;
    static ArrayList<HashMap<String, Object>> wishList = new ArrayList<>();
    Context context;
    PullToRefreshListView list;
    DiamondImageView img_profile;
    AlertDialog alertDialog;
    ImageView iv_menu_icon;
    Bitmap theBitmap = null;
    ImageView iv_smile;
    TextView tv_noti, iv_red_icon, tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_num;
    Activity activity;
    ImageView iv_smiley1, iv_smiley2, iv_smiley3, iv_smiley4, iv_smiley5;
    LinearLayout ll_feed, ll_profile, ll_calendar;
    RelativeLayout rlMainFront, ll_groups, rl_background;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "", feed_count = "";
    ArrayList<HashMap<String, String>> mygrouplist;
    ArrayList<HashMap<String, String>> list1;
    GroupAdapter1 gAdapter;
    View shadowView = null, view = null, sliderView;
    int height, width;
    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
    boolean drawer_open = false;
    ImageView iv_heart;
    private Timer timer;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    public int lastCount;
    int count = 0;
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    String imageStr;
    NavigationFragmentAdapter mAdapter;
    String string = "#ed3224";
    String string1 = "#ffffff";
    LayoutInflater layoutInflater = null;
    ViewPager pager;
    private PullToRefreshListView pulllist;
    String firstname = "", lastname = "", gender = "", phone = "", quote = "";
    String encoded;
    String post_id = "", you_like = "";
    String moodStr = "", star = "", group = "", post = "";
    String popup = "", postId = "";
    private boolean isLoading = false, isfirsTime = true;
    ArrayList<HashMap<String, String>> mygroupfavlist;
    private ArrayList<ImageView> dotsNavigate;
    private AlertDialog.Builder alert_session_dialog;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_feed);
        this.activity = (Activity) MyFeedActivity.this;
        mAdapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(mAdapter);
        MobileAds.initialize(this, String.valueOf(R.string.MobileAds_initialize));
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        addDotsNavigation();


        layoutInflater = LayoutInflater.from(this);
        list = (PullToRefreshListView) findViewById(R.id.list);
        context = MyFeedActivity.this;
        wishList = new ArrayList<HashMap<String, Object>>();
        adapter = new FeedAdapter();
        list.setAdapter(adapter);
        mygrouplist = new ArrayList<HashMap<String, String>>();
        list1 = new ArrayList<HashMap<String, String>>();
        mygroupfavlist = new ArrayList<HashMap<String, String>>();
        Intent intent = getIntent();
        post_id = intent.getStringExtra("post_id");
        you_like = intent.getStringExtra("you_like");
        initialise();
        clickables();
        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        quote = preferences.getString("quote", "");
        gender = preferences.getString("gender", "");
        phone = preferences.getString("phone", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new FeedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new FeedData().execute();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new FeedCountData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new FeedCountData().execute();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new MessageData().execute();
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new FetchBannedWords().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new FetchBannedWords().execute();
            }
        }

        setting();
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_calendar.setOnClickListener(this);
        view.setOnClickListener(this);

        timer.scheduleAtFixedRate(new RemindTask(), 0, 7000);

        list.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (!isLoading) {
                        if (has_more.equalsIgnoreCase("true")) {
                            isLoading = true;
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new FeedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                            } else {
                                new FeedData().execute("loadmore");
                            }
                        }
                    } else {
                        Toast.makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });
        list.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {
                isfirsTime = true;
                count = 0;
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    new FeedData().execute();

                    wishList.clear();
                }
            }
        });


        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                for (int listItemIndex = 0; listItemIndex <= list.getRefreshableView().getLastVisiblePosition() - list.getRefreshableView().getFirstVisiblePosition(); listItemIndex++) {

                    View listItem = list.getRefreshableView().getChildAt(listItemIndex);
                    final ImageView iv_play_gif = (ImageView) listItem.findViewById(R.id.iv_play_gif);


                    if (iv_play_gif != null && iv_play_gif.getVisibility() == View.VISIBLE) {
                    }

                    ImageView iv_gif1 = (ImageView) listItem.findViewById(R.id.iv_gif1);

                    if (iv_gif1 != null && iv_gif1.getVisibility() == View.VISIBLE) {

                        try {


                            int a[] = new int[2];
                            iv_gif1.getLocationOnScreen(a);
                            int y = a[0];
                            int x = a[1];
                            if (x <= height / 2) {

                                if (x <= 10) {
                                    final GlideDrawable gifDrawable = (GlideDrawable) iv_gif1.getDrawable();
                                    gifDrawable.stop();
                                    iv_play_gif.setVisibility(View.VISIBLE);
                                } else {
                                    final GlideDrawable gifDrawable = (GlideDrawable) iv_gif1.getDrawable();
                                    gifDrawable.start();
                                    iv_play_gif.setVisibility(View.GONE);
                                }


                            } else {
                                final GlideDrawable gifDrawable = (GlideDrawable) iv_gif1.getDrawable();
                                gifDrawable.stop();
                                iv_play_gif.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            Log.w("CRASH", e.toString());
                        }
                    }
                }
            }

        });
    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#FFAD3F"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#ffffff"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#F15A51"));

        ll_calendar.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground( ll_feed);
    }

    class RemindTask extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pingServices();
                }
            });
        }
    }

    private void pingServices() {
        if (!connDec.isConnectingToInternet()) {

        } else {
            boolean foregraund = false;

            try {
                foregraund = new ForegroundCheckTask().execute(getApplication()).get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (foregraund) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new FeedCountData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new FeedCountData().execute();
                    new MessageData().execute();
                }
            }
        }
    }

    private void initialise() {
        timer = new Timer();
        rl_background = (RelativeLayout) findViewById(R.id.rl_background);
        connDec = new ConnectionDetector(MyFeedActivity.this);
        alert = new Alert_Dialog_Manager(context);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_calendar = (LinearLayout) findViewById(R.id.ll_calendar);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();
        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }


    private void clickables() {

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();

                    drawer_open = true;
                    pager.setAdapter(mAdapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
                    if (!(preferences.getString("user_image", "").equals(""))) {
                        // Picasso.with(getApplicationContext()).load(preferences.getString("user_image", "")).into(img_profile);

                        Bitmap bitmap = CommonUtilities.getDecoadedBitmap(preferences.getString("user_image", ""), img_profile, activity);
                        //img_profile.setImageBitmap(bitmap);

                        img_profile.setBorderColor(Color.parseColor("#F15A51"));
                        img_profile.setBorderWidth(2);
                    }


                } else if (drawer_open == true) {

                    StopDrawerAnim();

                    drawer_open = false;
                }

            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyFeedActivity.this, UpdatesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {

        Handler handler = new Handler();

        switch (view.getId()) {

            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(MyFeedActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(MyFeedActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(MyFeedActivity.this, CalendarActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    @Override
    public void onResume() {

        super.onResume();
//        if (!connDec.isConnectingToInternet()) {
//           // alert.showNetAlert();
//        }
//        else {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//            } else {
//                new GroupFavData().execute();
//            }
//        }
        adapter.notifyDataSetChanged();


        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new MessageData().execute();
            }
        }
        if (!connDec.isConnectingToInternet()) {
            // alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new FeedCountData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new FeedCountData().execute();
            }
        }
        final int pos = 1;
        pager.postDelayed(new Runnable() {

            @Override
            public void run() {
                pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
            }
        }, 100);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ((UTCAPP) getApplication()).pagerPos = position;

                selectDotNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

// For Navigation Animation

    private class NavigationFragmentAdapter extends PagerAdapter {


        TextView tv_browsing;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront, rll;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_profile_name,
                tv_profile_username, tv_feed, tv_profile, tv_notification_count, tv_update, tv_groups, tv_staff_dir, tv_logout;

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            if (feed_count.equals("0"))
                tv_notification_count.setVisibility(View.GONE);

            else {
                tv_notification_count.setVisibility(View.VISIBLE);

                tv_notification_count.setText(feed_count);
            }
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);
                rll = (RelativeLayout) view.findViewById(R.id.rll);
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyFeedActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                    }
                });
                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupFavData().execute();
                    }
                }
                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {
                        if (!isLoading) {
                            if (has_more.equalsIgnoreCase("true")) {
                                isLoading = true;

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                                } else {
                                    new GroupFavData().execute("loadmore");
                                }
                            } else {
                                Toast.makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;
                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new GroupFavData().execute();
                            }

                            mygroupfavlist.clear();
                        }
                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyFeedActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
                tv_logout = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(CommonUtilities.decodeUTF8(preferences.getString("first_name", "")) + " " + CommonUtilities.decodeUTF8(preferences.getString("last_name", "")));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                if (feed_count.equals("0"))
                    tv_notification_count.setVisibility(View.GONE);

                else {
                    tv_notification_count.setVisibility(View.VISIBLE);

                    tv_notification_count.setText(feed_count);
                }
                if (encoded.equalsIgnoreCase("")) {

                    img_profile.setImageResource(R.drawable.avatar);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    // Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    Bitmap bitmap = CommonUtilities.getDecoadedBitmap(encoded, img_profile, activity);
                    //img_profile.setImageBitmap(bitmap);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }


                tv_notification_count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyFeedActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });

                tv_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(MyFeedActivity.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);

                    }
                });
                white_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(MyFeedActivity.this, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);

                    }
                });
                tv_groupss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(MyFeedActivity.this, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                    }
                });
                orange_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonUtilities.showStarsPopup(activity);
                    }
                });
                img_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(MyFeedActivity.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);
                    }
                });

                tv_groups.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyFeedActivity.this, JoinGroupActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyFeedActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_staff_dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyFeedActivity.this, MyDirectoryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });

                tv_logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                       /* if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        }
                        else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new LogOutData().execute();
                            }
                        }*/

                        customShareLogOutPopup();
                    }
                });
                container.addView(view, 0);
            }
            //   ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = mygroupfavlist.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    new FeedData().execute();
                }
            } else {
            }
        }
    }

    public class GroupFavData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();
                if (strings.length > 0) {
                    if (strings[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }
                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            pulllist.onRefreshComplete();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                // has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        if (!isLoadMore) {
                            mygroupfavlist.clear();
                        }

                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");
                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");
                                String is_fav = user_dataObj1.getString("is_fav");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);
                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygroupfavlist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(MyFeedActivity.this, mygroupfavlist);

                        pulllist.setAdapter(gAdapter);

                        //  gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                    showSessionExpireDialog();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            isLoading = false;

        }
    }


    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygroupfavlist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygroupfavlist) {
            this.context = context;
            this.mygroupfavlist = mygroupfavlist;
        }

        @Override
        public int getCount() {
            return mygroupfavlist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);

            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);
                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);
                }
            });


            if (mygroupfavlist.get(i).get("unread").equals("0")) {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setText(mygroupfavlist.get(i).get("unread"));
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    public class MessageData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "total_new_message");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    String new_message = resultObj.getString("new_message");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.putExtra("message_count", new_message);
                    sendBroadcast(intent);
                } else if (errCode.equals("700")) {
                    showSessionExpireDialog();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //Listing of Full Feed Data

    public class FeedData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(MyFeedActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                if (strings.length > 0) {
                    if (strings[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "feed_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                if (!isLoadMore) {

                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", wishList.size() + "");
                }

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            list.onRefreshComplete();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                has_more = resultObj.getString("has_more");

                if (resultObj.has("time")) {
                    String time = resultObj.getString("time");
                    preferences.edit().putString("time", time).commit();
                } else {
                    preferences.edit().putString("time", "").commit();
                }

                if (errCode.equals("0")) {

                    JSONObject jtotal_stars = resultObj.getJSONObject("total_stars");

                    String pelican = jtotal_stars.getString("pelican");

                    preferences.edit().putString("pelican", pelican).commit();

                    JSONObject jtotalgroups = resultObj.getJSONObject("total_group");

                    String total_grp = jtotalgroups.getString("total_group");

                    preferences.edit().putString("total_group", total_grp).commit();

                    group = total_grp;

                    JSONObject jtotalpost = resultObj.getJSONObject("total_post");

                    String total_posst = jtotalpost.getString("total_post");

                    preferences.edit().putString("total_post", total_posst).commit();

                    post = total_posst;

                    mAdapter.notifyDataSetChanged();

                    JSONArray jArray = resultObj.getJSONArray("list");

                    if (!isLoadMore) {
                        wishList.clear();
                    }

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, Object> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String post_id = user_dataObj.getString("post_id");
                        String type = user_dataObj.getString("type");
                        String title = user_dataObj.getString("title");
                        String status = user_dataObj.getString("status");
                        String detail = user_dataObj.getString("detail");
                        String video_link = user_dataObj.getString("video_link");
                        String post_create_date = user_dataObj.getString("post_create_date");
                        String date_of_start = user_dataObj.getString("date_of_start");
                        String date_of_end = user_dataObj.getString("date_of_end");
                        String start_timing = user_dataObj.getString("start_timing");
                        String end_timing = user_dataObj.getString("end_timing");
                        String venue = user_dataObj.getString("venue");
                        String latitude = user_dataObj.getString("latitude");
                        String longitude = user_dataObj.getString("longitude");
                        String location_address = user_dataObj.getString("location_address");
                        String post_status = user_dataObj.getString("post_status");
                        String admin_id = user_dataObj.getString("admin_id");
                        String visibility = user_dataObj.getString("visibility");
                        String publish_date = user_dataObj.getString("publish_date");
                        String archive = user_dataObj.getString("archive");
                        String image_big = user_dataObj.getString("image_big");
                        String facebook = user_dataObj.getString("facebook");
                        String facebook_id = user_dataObj.getString("facebook_id");
                        String facebook_url = user_dataObj.getString("facebook_url");
                        String post_hide = user_dataObj.getString("post_hide");
                        String total_likes = user_dataObj.getString("total_likes");
                        String total_comments = user_dataObj.getString("total_comments");
                        String you_like = user_dataObj.getString("you_like");
                        String your_like = user_dataObj.getString("your_like");
                        String primary_image = user_dataObj.getString("primary_image");
                        String is_gif = user_dataObj.getString("is_gif");
                        String popup_image = "";

                        if (user_dataObj.has("popup_image"))
                            popup_image = user_dataObj.getString("popup_image");

                        preferences.edit().putString("you_like", you_like).commit();

                        try {

                            JSONArray jArrayy = user_dataObj.getJSONArray("moods");
                            final int numberOfItemsInResp = jArrayy.length();
                            for (int j = 0; j < numberOfItemsInResp; j++) {
                                JSONObject user_dataObj1 = jArrayy.getJSONObject(j);
                                String moods = user_dataObj1.getString("moods");

                                String count = user_dataObj1.getString("modcount");

                                hmap.put(moods, count);
                                hmap.put("moods", moods);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        hmap.put("post_id", post_id);
                        hmap.put("type", type);
                        hmap.put("title", title);
                        hmap.put("status", status);
                        hmap.put("detail", detail);
                        hmap.put("video_link", video_link);
                        hmap.put("post_create_date", post_create_date);
                        hmap.put("date_of_start", date_of_start);
                        hmap.put("date_of_end", date_of_end);
                        hmap.put("start_timing", start_timing);
                        hmap.put("end_timing", end_timing);
                        hmap.put("venue", venue);
                        hmap.put("latitude", latitude);
                        hmap.put("longitude", longitude);
                        hmap.put("location_address", location_address);
                        hmap.put("post_status", post_status);
                        hmap.put("admin_id", admin_id);
                        hmap.put("visibility", visibility);
                        hmap.put("publish_date", publish_date);
                        hmap.put("archive", archive);
                        hmap.put("image_big", image_big);
                        hmap.put("facebook", facebook);
                        hmap.put("facebook_id", facebook_id);
                        hmap.put("facebook_url", facebook_url);
                        hmap.put("post_hide", post_hide);
                        hmap.put("total_likes", total_likes);
                        hmap.put("total_comments", total_comments);
                        hmap.put("you_like", you_like);
                        hmap.put("your_like", your_like);
                        hmap.put("primary_image", primary_image);
                        hmap.put("popup_image", popup_image);
                        hmap.put("is_gif", is_gif);

                        wishList.add(hmap);

                    }

                    adapter.notifyDataSetChanged();

                } else if (errCode.equals("700")) {
                    showSessionExpireDialog();


                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            LoaderClass.HideProgressWheel(MyFeedActivity.this);
            isLoading = false;
        }
    }

    public class ChangePostData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            //   position = Integer.parseInt(strings[1]);

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "change_post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "post");

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    wishList.get(position).put("your_like", moodString);

                    if (moodString == "happy") {

                    } else if (moodString == "indifferen") {

                    } else if (moodString == "amazing") {

                    } else if (moodString == "love") {

                    } else if (moodString == "sad") {

                    }

                    adapter.notifyDataSetChanged();
                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new FeedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new FeedData().execute();
                        }
                    }


                } else if (errCode.equals("700")) {

                    showSessionExpireDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class LikeServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        String moodString = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            //   position = Integer.parseInt(strings[1]);

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);


                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "post_like");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("ref_id", strings[1]);

                jo.put("source", "post");

                jo.put("like_status", strings[2]);

                position = Integer.valueOf(strings[3]);

                moodString = strings[0];

                jo.put("moods", moodString);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    String total_likes = resultObj.getString("total_likes");

                   /* if (moodStr.contains("happy")) {

                        int x = Integer.parseInt(wishList.get(position).get("happy").toString());

                        wishList.get(position).put("happy", x++);

                    } else if (moodStr.contains("indifferen")) {

                        int x = Integer.parseInt(wishList.get(position).get("indifferen").toString());

                        wishList.get(position).put("indifferen", x++);

                    } else if (moodStr.contains("amazing")) {

                        int x = Integer.parseInt(wishList.get(position).get("amazing").toString());

                        wishList.get(position).put("amazing", x++);

                    } else if (moodStr.contains("love")) {

                        int x = Integer.parseInt(wishList.get(position).get("love").toString());

                        wishList.get(position).put("love", x++);

                    } else if (moodStr.contains("sad")) {

                        int x = Integer.parseInt(wishList.get(position).get("sad").toString());

                        wishList.get(position).put("sad", x++);

                    }*/

                    wishList.get(position).put("your_like", moodString);

                    adapter.notifyDataSetChanged();

                    //new FeedData().execute();


                } else if (errCode.equals("700")) {

                    showSessionExpireDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class FeedCountData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        int position = 0;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "total_new_feed");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("post_time", preferences.getString("time", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //  LoaderClass.HideProgressWheel(MyFeedActivity.this);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    String new_feed = resultObj.getString("new_feed");
                    int i = Integer.parseInt(new_feed);
                    if (i == 0) {

                        iv_red_icon.setVisibility(View.GONE);
                        feed_count = i + "";
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        feed_count = i + "";
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        feed_count = "99+";
                        iv_red_icon.setText("99+");
                    }
                    mAdapter.notifyDataSetChanged();

                    Intent intent = new Intent("android.intent.action.MAIN");
                    intent.putExtra("feed_count", new_feed);
                    sendBroadcast(intent);


                } else if (errCode.equals("700")) {
                    showSessionExpireDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    //Log Out the App

    public class LogOutData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    preferences.edit().clear().commit();
                    ((UTCAPP) getApplication()).joingroupList.clear();

                    Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                    startActivity(i);
                    finishAffinity();
                    // customShareLogOutPopup();


                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyFeedActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(MyFeedActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(MyFeedActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LogOutData().execute();
                    }
                }
            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }


    /*private Bitmap getDecoadedBitmap(final String url,final ImageView imageView) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                   *//* theBitmap = Glide.
                            with(activity).
                            load(url).
                            asBitmap().
                            into(-1, -1).
                            get();*//*
                    Glide.with(activity)
                            .load(url)
                            .asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    int height=resource.getHeight()/2;
                                    int width=resource.getWidth()/2;
                                    //Bitmap b = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
                                    imageView.setImageBitmap(Bitmap.createScaledBitmap(resource, width, height, false));
                                    //imageView.setImageBitmap(resource);
                                    //imageView.setImageBitmap(resource);
                                }
                            });
                *//*} catch (final ExecutionException e) {
                    Log.e("getDecoadedBitmap", e.getMessage());
                } catch (final InterruptedException e) {
                    Log.e("getDecoadedBitmap", e.getMessage());*//*
                } catch (Exception e) {

                }
            }
        });

        return theBitmap;
    }*/
    /*private boolean isOffScreenHorizontal(ViewGroup parent, View child)
    {
        Rect mTempRect = new Rect();
        child.getDrawingRect(mTempRect);
        parent.offsetDescendantRectToMyCoords(child, mTempRect);

        return !((mTempRect.right) >= parent.getScrollX()
                && (mTempRect.left) <= (parent.getScrollX() + parent.getWidth()));
    }*/

    private class FeedAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return wishList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            if (wishList.get(i).get("primary_image").equals("")) {

                convertView = inflater.inflate(R.layout.feed_adapter_layout1, null);
                ViewHolder holder = new ViewHolder();
                holder.iv_news = (ImageView) convertView.findViewById(R.id.iv_news);
                iv_heart = (ImageView) convertView.findViewById(R.id.iv_heart);
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_clock_time = (TextView) convertView.findViewById(R.id.tv_clock_time);
                holder.tv_paragp = (TextView) convertView.findViewById(R.id.tv_paragp);
                tv_noti = (TextView) convertView.findViewById(R.id.tv_noti);
                holder.tv_msg_noti = (TextView) convertView.findViewById(R.id.tv_msg_noti);
                holder.rl_feed_info = (RelativeLayout) convertView.findViewById(R.id.rl_feed_info);
                iv_smiley1 = (ImageView) convertView.findViewById(R.id.iv_smiley1);
                iv_smiley2 = (ImageView) convertView.findViewById(R.id.iv_smiley2);
                iv_smiley3 = (ImageView) convertView.findViewById(R.id.iv_smiley3);
                iv_smiley4 = (ImageView) convertView.findViewById(R.id.iv_smiley4);
                iv_smiley5 = (ImageView) convertView.findViewById(R.id.iv_smiley5);
                holder.rl_sel_smileys = (RelativeLayout) convertView.findViewById(R.id.rl_sel_smileys);

                holder.iv_msg = (ImageView) convertView.findViewById(R.id.iv_msg);
                holder.iv_msg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                        intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                        intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                        intent.putExtra("type", wishList.get(i).get("type").toString());
                        intent.putExtra("is_comment", "yes");
                        intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                        startActivity(intent);
                    }
                });

                if (wishList.get(i).get("type").equals("artical")) {

                    holder.iv_news.setImageResource(R.drawable.news);

                    holder.tv_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                            startActivity(intent);
                        }
                    });

                    holder.tv_paragp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                            startActivity(intent);
                        }
                    });
                } else if (wishList.get(i).get("type").equals("event")) {

                    holder.iv_news.setImageResource(R.drawable.event_2);

                    holder.tv_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });

                    holder.tv_paragp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });
                }

                if (wishList.size() != 0) {

                    if (wishList.get(i).containsKey("you_like")) {

                        if (wishList.get(i).get("you_like").equals("0")) {

                            iv_heart.setVisibility(View.VISIBLE);
                            iv_heart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showDialogSmiley(i);

                                }
                            });
                        } else {

                            iv_heart.setVisibility(View.GONE);

                            holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDialogSmiley1(i);
                                }
                            });
                        }
                    }
                    /*holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogSmiley1(i);
                        }
                    });*/
                }
                if (wishList.get(i).containsKey("happy")) {

                    iv_smiley1.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley1.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("indifferen")) {

                    iv_smiley2.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley2.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("amazing")) {

                    iv_smiley3.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley3.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("love")) {

                    iv_smiley4.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley4.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("sad")) {

                    iv_smiley5.setVisibility(View.VISIBLE);
                } else {
                    iv_smiley5.setVisibility(View.GONE);
                }

                holder.tv_title.setText(wishList.get(i).get("title").toString());
                holder.tv_paragp.setText(Html.fromHtml(wishList.get(i).get("detail").toString()));

                tv_noti.setText(wishList.get(i).get("total_likes").toString());
                holder.tv_msg_noti.setText(wishList.get(i).get("total_comments").toString());

                if (!wishList.get(i).get("your_like").equals("")) {

                    if (wishList.get(i).get("your_like").equals("happy")) {

                        iv_smiley1.setImageResource(R.drawable.happy);
                    }
                    if (wishList.get(i).get("your_like").equals("indifferen")) {

                        iv_smiley2.setImageResource(R.drawable.indifferent);
                    }
                    if (wishList.get(i).get("your_like").equals("amazing")) {

                        iv_smiley3.setImageResource(R.drawable.amazing);
                    }
                    if (wishList.get(i).get("your_like").equals("love")) {

                        iv_smiley4.setImageResource(R.drawable.love);
                    }
                    if (wishList.get(i).get("your_like").equals("sad")) {

                        iv_smiley5.setImageResource(R.drawable.sad);
                    }
                }
                try {
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

                    String FinalDate = (String) wishList.get(i).get("publish_date");

                    Date date1;
                    Date date2;

                    SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

                    //Setting dates
                    date1 = dates.parse(date);
                    date2 = dates.parse(FinalDate);

                    //Comparing dates
                    long difference = Math.abs(date1.getTime() - date2.getTime());
                    //long differenceDates = difference / (24 * 60 * 60 * 1000);
                    holder.tv_clock_time.setText(" " + CommonUtilities.getDaysMonths(difference));
                    //Convert long to String
                    /*String dayDifference = Long.toString(differenceDates);

                    System.out.println("HERE " + dayDifference);
                    holder.tv_clock_time.setText(dayDifference + "d");*/

                } catch (Exception exception) {

                    System.out.println("Work " + exception);
                }

            } else if (wishList.get(i).get("image_big").equals("1")) {

                convertView = inflater.inflate(R.layout.feed_adapter_layout4, null);
                final ViewHolder holder = new ViewHolder();
                holder.iv_full = (ImageView) convertView.findViewById(R.id.iv_full);
                holder.iv_news = (ImageView) convertView.findViewById(R.id.iv_news);
                //    holder.iv_smiley = (ImageView) convertView.findViewById(R.id.iv_smiley);
                iv_heart = (ImageView) convertView.findViewById(R.id.iv_heart);
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_clock_time = (TextView) convertView.findViewById(R.id.tv_clock_time);
                holder.tv_paragp = (TextView) convertView.findViewById(R.id.tv_paragp);
                holder.iv_play_gif = (ImageView) convertView.findViewById(R.id.iv_play_gif);
                tv_noti = (TextView) convertView.findViewById(R.id.tv_noti);
                holder.tv_msg_noti = (TextView) convertView.findViewById(R.id.tv_msg_noti);
                holder.rl_feed_info = (RelativeLayout) convertView.findViewById(R.id.rl_feed_info);
                holder.rl_sel_smileys = (RelativeLayout) convertView.findViewById(R.id.rl_sel_smileys);
                holder.iv_msg = (ImageView) convertView.findViewById(R.id.iv_msg);
                iv_smiley1 = (ImageView) convertView.findViewById(R.id.iv_smiley1);
                iv_smiley2 = (ImageView) convertView.findViewById(R.id.iv_smiley2);
                iv_smiley3 = (ImageView) convertView.findViewById(R.id.iv_smiley3);
                iv_smiley4 = (ImageView) convertView.findViewById(R.id.iv_smiley4);
                iv_smiley5 = (ImageView) convertView.findViewById(R.id.iv_smiley5);
                iv_smile = (ImageView) convertView.findViewById(R.id.iv_smile);
                popup = (String) wishList.get(i).get("popup_image");
                postId = (String) wishList.get(i).get("post_id");

                holder.iv_gif = (ImageView) convertView.findViewById(R.id.iv_gif1);


                holder.iv_msg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                        intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                        intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                        intent.putExtra("type", wishList.get(i).get("type").toString());
                        intent.putExtra("is_comment", "yes");
                        intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                        startActivity(intent);
                    }
                });

                if (wishList.get(i).get("type").equals("artical")) {

                    holder.iv_news.setImageResource(R.drawable.news);

                    holder.tv_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                            startActivity(intent);
                        }
                    });
                    holder.tv_paragp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                            startActivity(intent);
                        }
                    });
                    holder.iv_full.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                            startActivity(intent);


                        }
                    });
                } else if (wishList.get(i).get("type").equals("event")) {

                    holder.iv_news.setImageResource(R.drawable.event_2);

                    holder.tv_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });
                    holder.tv_paragp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });
                    holder.iv_full.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });
                }
                if (wishList.size() != 0) {

                    if (wishList.get(i).containsKey("you_like")) {

                        if (wishList.get(i).get("you_like").equals("0")) {
                            iv_smile.setVisibility(View.GONE);
                            iv_heart.setVisibility(View.VISIBLE);
                            iv_heart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showDialogSmiley(i);

                                }
                            });
                        } else {
                            iv_smile.setVisibility(View.VISIBLE);

                            iv_heart.setVisibility(View.GONE);

                            holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDialogSmiley1(i);
                                }
                            });
                        }
                    }
                    /*holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogSmiley1(i);
                        }
                    });*/
                }
                if (wishList.get(i).containsKey("happy")) {

                    iv_smiley1.setVisibility(View.VISIBLE);
                } else {
                    iv_smiley1.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("indifferen")) {

                    iv_smiley2.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley2.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("amazing")) {

                    iv_smiley3.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley3.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("love")) {

                    iv_smiley4.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley4.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("sad")) {

                    iv_smiley5.setVisibility(View.VISIBLE);
                } else {
                    iv_smiley5.setVisibility(View.GONE);
                }
                holder.tv_title.setText(wishList.get(i).get("title").toString());
                holder.tv_paragp.setText(Html.fromHtml(wishList.get(i).get("detail").toString()));
                tv_noti.setText(wishList.get(i).get("total_likes").toString());
                holder.tv_msg_noti.setText(wishList.get(i).get("total_comments").toString());

                if (wishList.get(i).get("is_gif").equals("1")) {


                    holder.iv_full.setVisibility(View.GONE);
                    holder.iv_gif.setVisibility(View.VISIBLE);
                    holder.iv_play_gif.setVisibility(View.VISIBLE);

                    try {
                        final GlideDrawable gifDrawable = (GlideDrawable) holder.iv_gif.getDrawable();
                        gifDrawable.stop();
                    } catch (Exception e) {

                    }


                    Glide.with(context)
                            .load(wishList.get(i).get("primary_image").toString())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .dontAnimate()
                            .into(new GlideDrawableImageViewTarget(holder.iv_gif) {
                                @Override
                                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                                    super.onResourceReady(resource, animation);
                                    resource.stop();
                                }

                                @Override
                                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                    super.onLoadFailed(e, errorDrawable);
                                }
                            });

                    holder.iv_play_gif.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                final GlideDrawable gifDrawable = (GlideDrawable) holder.iv_gif.getDrawable();
                                gifDrawable.start();
                                holder.iv_play_gif.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });


                    holder.iv_gif.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            intent.putExtra("video_link", wishList.get(i).get("video_link").toString());
                            startActivity(intent);
                        }
                    });

                } else {
                    holder.iv_full.setVisibility(View.VISIBLE);
                    holder.iv_gif.setVisibility(View.GONE);
                    holder.iv_play_gif.setVisibility(View.GONE);
                   /*// Picasso.with(getApplicationContext()).load(wishList.get(i).get("primary_image").toString()).into(holder.iv_full);

                    Glide.with(context)
                            .load(wishList.get(i).get("primary_image").toString())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(holder.iv_full);*/

                    Bitmap bitmap = CommonUtilities.getDecoadedBitmap(wishList.get(i).get("primary_image").toString(), holder.iv_full, activity);

                    //holder.iv_full.setImageBitmap(bitmap);
                }
                if (!wishList.get(i).get("your_like").equals("")) {

                    if (wishList.get(i).get("your_like").equals("happy")) {

                        iv_smiley1.setImageResource(R.drawable.happy);
                    }
                    if (wishList.get(i).get("your_like").equals("indifferen")) {

                        iv_smiley2.setImageResource(R.drawable.indifferent);
                    }
                    if (wishList.get(i).get("your_like").equals("amazing")) {

                        iv_smiley3.setImageResource(R.drawable.amazing);
                    }
                    if (wishList.get(i).get("your_like").equals("love")) {

                        iv_smiley4.setImageResource(R.drawable.love);
                    }
                    if (wishList.get(i).get("your_like").equals("sad")) {

                        iv_smiley5.setImageResource(R.drawable.sad);
                    }
                }
                try {
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

                    String FinalDate = (String) wishList.get(i).get("publish_date");

                    Date date1;
                    Date date2;

                    SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

                    //Setting dates
                    date1 = dates.parse(date);
                    date2 = dates.parse(FinalDate);

                    //Comparing dates
                    long difference = Math.abs(date1.getTime() - date2.getTime());
                    //long differenceDates = difference / (24 * 60 * 60 * 1000);
                    holder.tv_clock_time.setText(" " + CommonUtilities.getDaysMonths(difference));
                    //Convert long to String
                    /*String dayDifference = Long.toString(differenceDates);

                    System.out.println("HERE " + dayDifference);
                    holder.tv_clock_time.setText(dayDifference + "d");*/

                } catch (Exception exception) {

                    System.out.println("Work " + exception);
                }

            } else if (wishList.get(i).get("image_big").equals("0")) {

                convertView = inflater.inflate(R.layout.feed_adapter_layout3, null);
                ViewHolder holder = new ViewHolder();
                iv_heart = (ImageView) convertView.findViewById(R.id.iv_heart);
                holder.iv_office = (ImageView) convertView.findViewById(R.id.iv_office);
                holder.iv_news = (ImageView) convertView.findViewById(R.id.iv_news1);
                holder.tv_clock_time = (TextView) convertView.findViewById(R.id.tv_clock_time);
                holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
                holder.tv_paragp = (TextView) convertView.findViewById(R.id.tv_paragp);
                tv_noti = (TextView) convertView.findViewById(R.id.tv_noti);
                holder.tv_msg_noti = (TextView) convertView.findViewById(R.id.tv_msg_noti);
                holder.iv_fb = (ImageView) convertView.findViewById(R.id.iv_fb);
                iv_smiley1 = (ImageView) convertView.findViewById(R.id.iv_smiley1);
                iv_smiley2 = (ImageView) convertView.findViewById(R.id.iv_smiley2);
                iv_smiley3 = (ImageView) convertView.findViewById(R.id.iv_smiley3);
                iv_smiley4 = (ImageView) convertView.findViewById(R.id.iv_smiley4);
                iv_smiley5 = (ImageView) convertView.findViewById(R.id.iv_smiley5);
                iv_smile = (ImageView) convertView.findViewById(R.id.iv_smile);
                holder.rl_feed_info = (RelativeLayout) convertView.findViewById(R.id.rl_feed_info);
                holder.rl_sel_smileys = (RelativeLayout) convertView.findViewById(R.id.rl_sel_smileys);

                holder.iv_msg = (ImageView) convertView.findViewById(R.id.iv_msg);

                if (wishList.get(i).get("type").equals("event")) {

                    holder.iv_news.setImageResource(R.drawable.calendar);


                    holder.tv_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });
                    holder.tv_paragp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });
                    holder.iv_office.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, EventActivity.class);

                            HashMap<String, Object> stringObjectHashMap = wishList.get(i);

                            intent.putExtra("post_id", stringObjectHashMap.get("post_id").toString());
                            intent.putExtra("you_like", stringObjectHashMap.get("you_like").toString());
                            intent.putExtra("type", stringObjectHashMap.get("type").toString());
                            intent.putExtra("video_link", stringObjectHashMap.get("video_link").toString());

                            intent.putExtra("title", stringObjectHashMap.get("title").toString());
                            intent.putExtra("status", stringObjectHashMap.get("status").toString());
                            intent.putExtra("post_create_date", stringObjectHashMap.get("post_create_date").toString());
                            intent.putExtra("popup_image", stringObjectHashMap.get("popup_image").toString());
                            intent.putExtra("date_of_start", stringObjectHashMap.get("date_of_start").toString());
                            intent.putExtra("start_timing", stringObjectHashMap.get("start_timing").toString());
                            intent.putExtra("date_of_end", stringObjectHashMap.get("date_of_end").toString());
                            intent.putExtra("end_timing", stringObjectHashMap.get("end_timing").toString());
                            intent.putExtra("latitude", stringObjectHashMap.get("latitude").toString());
                            intent.putExtra("longitude", stringObjectHashMap.get("longitude").toString());
                            intent.putExtra("detail", stringObjectHashMap.get("detail").toString());
                            intent.putExtra("total_likes", stringObjectHashMap.get("total_likes").toString());
                            intent.putExtra("total_comments", stringObjectHashMap.get("total_comments").toString());

                            startActivityForResult(intent, 1);
                        }
                    });

                } else if (wishList.get(i).get("type").equals("artical")) {

                    holder.iv_news.setImageResource(R.drawable.news);

                    holder.tv_title.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            startActivity(intent);
                        }
                    });
                    holder.tv_paragp.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            startActivity(intent);
                        }
                    });
                    holder.iv_office.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(MyFeedActivity.this, ArticleScreenActivity.class);
                            intent.putExtra("post_id", wishList.get(i).get("post_id").toString());
                            intent.putExtra("you_like", wishList.get(i).get("you_like").toString());
                            intent.putExtra("type", wishList.get(i).get("type").toString());
                            startActivity(intent);
                        }
                    });
                }
                holder.tv_title.setText(wishList.get(i).get("title").toString());
                holder.tv_paragp.setText(Html.fromHtml(wishList.get(i).get("detail").toString()));
                tv_noti.setText(wishList.get(i).get("total_likes").toString());
                holder.tv_msg_noti.setText(wishList.get(i).get("total_comments").toString());

                if (wishList.size() != 0) {

                    if (wishList.get(i).containsKey("you_like")) {
                        if (wishList.get(i).get("you_like").equals("0")) {
                            iv_heart.setVisibility(View.VISIBLE);
                            iv_heart.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    showDialogSmiley(i);

                                }
                            });
                        } else {
                            iv_smile.setVisibility(View.VISIBLE);
                            iv_heart.setVisibility(View.GONE);

                            holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    showDialogSmiley1(i);
                                }
                            });
                        }
                    }
                   /* holder.rl_sel_smileys.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showDialogSmiley1(i);
                        }
                    });*/
                }

                if (wishList.get(i).containsKey("happy")) {

                    iv_smiley1.setVisibility(View.VISIBLE);
                } else {
                    iv_smiley1.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("indifferen")) {

                    iv_smiley2.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley2.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("amazing")) {

                    iv_smiley3.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley3.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("love")) {

                    iv_smiley4.setVisibility(View.VISIBLE);

                } else {
                    iv_smiley4.setVisibility(View.GONE);
                }
                if (wishList.get(i).containsKey("sad")) {

                    iv_smiley5.setVisibility(View.VISIBLE);
                } else {
                    iv_smiley5.setVisibility(View.GONE);
                }

                //Picasso.with(getApplicationContext()).load(wishList.get(i).get("primary_image").toString()).into(holder.iv_office);

                Bitmap bitmap = CommonUtilities.getDecoadedBitmap(wishList.get(i).get("primary_image").toString(), holder.iv_office, activity);

                //holder.iv_office.setImageBitmap(bitmap);

                if (!wishList.get(i).get("your_like").equals("")) {

                    if (wishList.get(i).get("your_like").equals("happy")) {

                        iv_smiley1.setImageResource(R.drawable.happy);
                    }
                    if (wishList.get(i).get("your_like").equals("indifferen")) {

                        iv_smiley2.setImageResource(R.drawable.indifferent);
                    }
                    if (wishList.get(i).get("your_like").equals("amazing")) {

                        iv_smiley3.setImageResource(R.drawable.amazing);
                    }
                    if (wishList.get(i).get("your_like").equals("love")) {

                        iv_smiley4.setImageResource(R.drawable.love);
                    }
                    if (wishList.get(i).get("your_like").equals("sad")) {

                        iv_smiley5.setImageResource(R.drawable.sad);
                    }
                }
                try {
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

                    String FinalDate = (String) wishList.get(i).get("publish_date");

                    Date date1;
                    Date date2;

                    SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd");

                    //Setting dates
                    date1 = dates.parse(date);
                    date2 = dates.parse(FinalDate);

                    //Comparing dates
                    long difference = Math.abs(date1.getTime() - date2.getTime());
                    //long differenceDates = difference / (24 * 60 * 60 * 1000);
                    holder.tv_clock_time.setText(" " + CommonUtilities.getDaysMonths(difference));

                    //Convert long to String
                   /* String dayDifference = Long.toString(differenceDates);

                    System.out.println("HERE " + dayDifference);
                    holder.tv_clock_time.setText(dayDifference + "d");*/

                } catch (Exception exception) {

                    System.out.println("Work " + exception);
                }
            }
            return convertView;
        }

        class ViewHolder {
            ImageView iv_news, iv_fb, iv_office, iv_msg, iv_full_img, img_facebook, iv_full, iv_smiley, iv_play_gif;
            TextView tv_clock_time, tv_title, tv_paragp, tv_msg_noti, tv_view_fb, tv_cal_date, tv_location;
            RelativeLayout rl_feed_info, rl_sel_smileys;
            ImageView iv_gif;

        }
    }

    private boolean isViewVisible(View view) {
        Rect scrollBounds = new Rect();
        view.getDrawingRect(scrollBounds);

        float top = view.getY();
        float bottom = top + view.getHeight();

        if (scrollBounds.top < top && scrollBounds.bottom > bottom) {
            return true;
        } else {
            return false;
        }
    }

    public void showDialogSmiley(final int position) {
        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.moods_activity_one);
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setDimAmount(0f);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        if (wishList.get(position).containsKey("happy")) {
            count_happy.setText("(" + wishList.get(position).get("happy") + "" + ")");
        }

        if (wishList.get(position).containsKey("indifferen")) {
            count_indiff.setText("(" + wishList.get(position).get("indifferen") + "" + ")");
        }

        if (wishList.get(position).containsKey("amazing")) {
            count_amaz.setText("(" + wishList.get(position).get("amazing") + "" + ")");

        }

        if (wishList.get(position).containsKey("love")) {
            count_love.setText("(" + wishList.get(position).get("love") + "" + ")");
        }

        if (wishList.get(position).containsKey("sad")) {
            count_sad.setText("(" + wishList.get(position).get("sad") + "" + ")");

        }

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (!wishList.get(position).get("your_like").equals("")) {

            if (wishList.get(position).get("your_like").equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }

        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(),"Happy",Toast.LENGTH_SHORT).show();

                iv_heart.setVisibility(View.GONE);

                wishList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (wishList.get(position).containsKey("happy")) {
                    happyCount = "" + wishList.get(position).get("happy");
                }
                moodStr = "happy";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                counting++;

                wishList.get(position).put("happy", "" + count);

                wishList.get(position).put("total_likes", "" + counting);

                adapter.notifyDataSetChanged();

                tv_noti.setText(wishList.get(position).get("total_likes").toString());

                iv_happy.setImageResource(R.drawable.happy);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                } else {
                    new LikeServiceData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                }

                dialog.dismiss();


            }
        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Toast.makeText(getApplicationContext(),"InDifferen",Toast.LENGTH_SHORT).show();
                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String indifferenCount = "0";

                if (wishList.get(position).containsKey("indifferen")) {
                    indifferenCount = "" + wishList.get(position).get("indifferen");
                }

                // if (wishList.get(position).containsKey("indifferen"))
                {

                    moodStr = "indifferen";

                    int count = Integer.parseInt(indifferenCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    counting++;

                    wishList.get(position).put("indifferen", "" + count);

                    wishList.get(position).put("total_likes", "" + counting);

                    adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_indiff.setImageResource(R.drawable.indifferent);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }
                    dialog.dismiss();

                }

            }

        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String amazingCount = "0";

                if (wishList.get(position).containsKey("amazing")) {
                    amazingCount = "" + wishList.get(position).get("amazing");
                }
                // if (wishList.get(position).containsKey("amazing"))
                {

                    moodStr = "amazing";

                    int count = Integer.parseInt(amazingCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    counting++;

                    wishList.get(position).put("amazing", "" + count);

                    wishList.get(position).put("total_likes", "" + counting);

                    adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_amazing.setImageResource(R.drawable.amazing);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }
                    dialog.dismiss();

                }

            }

        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String loveCount = "0";

                if (wishList.get(position).containsKey("love")) {
                    loveCount = "" + wishList.get(position).get("love");
                }
                //  if (wishList.get(position).containsKey("love"))
                {

                    moodStr = "love";

                    int count = Integer.parseInt(loveCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    counting++;

                    wishList.get(position).put("love", "" + count);

                    wishList.get(position).put("total_likes", "" + counting);

                    adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_love.setImageResource(R.drawable.love);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }

                    dialog.dismiss();

                }
            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String sadCount = "0";

                if (wishList.get(position).containsKey("sad")) {
                    sadCount = "" + wishList.get(position).get("sad");
                }
                // if (wishList.get(position).containsKey("sad"))
                {

                    moodStr = "sad";

                    int count = Integer.parseInt(sadCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    counting++;

                    wishList.get(position).put("sad", "" + count);

                    wishList.get(position).put("total_likes", "" + counting);

                    adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_sad.setImageResource(R.drawable.sad);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LikeServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new LikeServiceData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }

                    dialog.dismiss();

                }


            }
        });

        dialog.show();

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    @TargetApi(11)
    public void showDialogSmiley1(final int position) {
        rl_background.setVisibility(View.VISIBLE);

        final Dialog dialog = new Dialog(activity);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setDimAmount(0f);
        dialog.setContentView(R.layout.moods_activity_one);
        final LinearLayout ll_happy = (LinearLayout) dialog.findViewById(R.id.ll_happy);
        final LinearLayout ll_sad = (LinearLayout) dialog.findViewById(R.id.ll_sad);
        final LinearLayout ll_love = (LinearLayout) dialog.findViewById(R.id.ll_love);
        final LinearLayout ll_amazing = (LinearLayout) dialog.findViewById(R.id.ll_amazing);
        final LinearLayout ll_indiff = (LinearLayout) dialog.findViewById(R.id.ll_indiff);
        final ImageView iv_happy = (ImageView) dialog.findViewById(R.id.iv_happy);
        final ImageView iv_happy_sel = (ImageView) dialog.findViewById(R.id.iv_happy_sel);
        final ImageView iv_indiff = (ImageView) dialog.findViewById(R.id.iv_indiff);
        final ImageView iv_indiff_sel = (ImageView) dialog.findViewById(R.id.iv_indiff_sel);
        final ImageView iv_amazing = (ImageView) dialog.findViewById(R.id.iv_amazing);
        final ImageView iv_amazing_sel = (ImageView) dialog.findViewById(R.id.iv_amazing_sel);
        final ImageView iv_love = (ImageView) dialog.findViewById(R.id.iv_love);
        final ImageView iv_love_sel = (ImageView) dialog.findViewById(R.id.iv_love_sel);
        final ImageView iv_sad = (ImageView) dialog.findViewById(R.id.iv_sad);
        final ImageView iv_sad_sel = (ImageView) dialog.findViewById(R.id.iv_sad_sel);
        final TextView count_happy = (TextView) dialog.findViewById(R.id.count_happy);
        final TextView count_indiff = (TextView) dialog.findViewById(R.id.count_indiff);
        final TextView count_amaz = (TextView) dialog.findViewById(R.id.count_amaz);
        final TextView count_love = (TextView) dialog.findViewById(R.id.count_love);
        final TextView count_sad = (TextView) dialog.findViewById(R.id.count_sad);
        final RelativeLayout rl = (RelativeLayout) dialog.findViewById(R.id.rl);
        if (wishList.get(position).containsKey("happy")) {
            count_happy.setText("(" + wishList.get(position).get("happy") + "" + ")");
        }

        if (wishList.get(position).containsKey("indifferen")) {
            count_indiff.setText("(" + wishList.get(position).get("indifferen") + "" + ")");
        }

        if (wishList.get(position).containsKey("amazing")) {
            count_amaz.setText("(" + wishList.get(position).get("amazing") + "" + ")");

        }

        if (wishList.get(position).containsKey("love")) {
            count_love.setText("(" + wishList.get(position).get("love") + "" + ")");
        }

        if (wishList.get(position).containsKey("sad")) {
            count_sad.setText("(" + wishList.get(position).get("sad") + "" + ")");

        }

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if (!wishList.get(position).get("your_like").equals("")) {

            if (wishList.get(position).get("your_like").equals("happy")) {

                iv_happy.setVisibility(View.GONE);
                iv_happy_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("indifferen")) {

                iv_indiff.setVisibility(View.GONE);
                iv_indiff_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("amazing")) {

                iv_amazing.setVisibility(View.GONE);
                iv_amazing_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("love")) {

                iv_love.setVisibility(View.GONE);
                iv_love_sel.setVisibility(View.VISIBLE);
            }
            if (wishList.get(position).get("your_like").equals("sad")) {

                iv_sad.setVisibility(View.GONE);
                iv_sad_sel.setVisibility(View.VISIBLE);
            }
        }
        ll_happy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_heart.setVisibility(View.GONE);

                wishList.get(position).put("you_like", "1");

                String happyCount = "0";

                if (wishList.get(position).containsKey("happy")) {
                    happyCount = "" + wishList.get(position).get("happy");
                }
                moodStr = "happy";

                int count = Integer.parseInt(happyCount + "");

                count++;

                System.out.println("COUNT" + count);

                //  int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                // counting++;

                wishList.get(position).put("happy", "" + count);

                //  wishList.get(position).put("total_likes", "" + counting);

                // adapter.notifyDataSetChanged();

                tv_noti.setText(wishList.get(position).get("total_likes").toString());

                iv_happy.setImageResource(R.drawable.happy);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                } else {
                    new ChangePostData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                }

                dialog.dismiss();

            }


        });

        ll_indiff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String indifferenCount = "0";

                if (wishList.get(position).containsKey("indifferen")) {
                    indifferenCount = "" + wishList.get(position).get("indifferen");
                }

                // if (wishList.get(position).containsKey("indifferen"))
                {

                    moodStr = "indifferen";

                    int count = Integer.parseInt(indifferenCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    // int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    // counting++;

                    wishList.get(position).put("indifferen", "" + count);

                    // wishList.get(position).put("total_likes", "" + counting);

                    //  adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_indiff.setImageResource(R.drawable.indifferent);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }
                    dialog.dismiss();

                }

            }

        });

        ll_amazing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String amazingCount = "0";

                if (wishList.get(position).containsKey("amazing")) {
                    amazingCount = "" + wishList.get(position).get("amazing");
                }
                // if (wishList.get(position).containsKey("amazing"))
                {

                    moodStr = "amazing";

                    int count = Integer.parseInt(amazingCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    // int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    //  counting++;

                    wishList.get(position).put("amazing", "" + count);

                    //  wishList.get(position).put("total_likes", "" + counting);

                    // adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_amazing.setImageResource(R.drawable.amazing);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }
                    dialog.dismiss();

                }

            }

        });

        ll_love.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String loveCount = "0";

                if (wishList.get(position).containsKey("love")) {
                    loveCount = "" + wishList.get(position).get("love");
                }
                //  if (wishList.get(position).containsKey("love"))
                {

                    moodStr = "love";

                    int count = Integer.parseInt(loveCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    // int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    //  counting++;

                    wishList.get(position).put("love", "" + count);

                    //  wishList.get(position).put("total_likes", "" + counting);

                    // adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_love.setImageResource(R.drawable.love);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }

                    dialog.dismiss();

                }

            }
        });

        ll_sad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_heart.setVisibility(View.GONE);
                wishList.get(position).put("you_like", "1");

                String sadCount = "0";

                if (wishList.get(position).containsKey("sad")) {
                    sadCount = "" + wishList.get(position).get("sad");
                }
                // if (wishList.get(position).containsKey("sad"))
                {

                    moodStr = "sad";

                    int count = Integer.parseInt(sadCount + "");

                    count++;

                    System.out.println("COUNT" + count);

                    //  int counting = Integer.parseInt(wishList.get(position).get("total_likes") + "");

                    //  counting++;

                    wishList.get(position).put("sad", "" + count);

                    //   wishList.get(position).put("total_likes", "" + counting);

                    //  adapter.notifyDataSetChanged();

                    tv_noti.setText(wishList.get(position).get("total_likes").toString());

                    iv_sad.setImageResource(R.drawable.sad);


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new ChangePostData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    } else {
                        new ChangePostData().execute(moodStr, wishList.get(position).get("post_id").toString(), "1", "" + position);
                    }

                    dialog.dismiss();

                }


            }
        });
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                rl_background.setVisibility(View.GONE);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        // timer.cancel();

        getAnalyticTracker().setScreenName("My Feed Screen");

        FlurryAgent.onStartSession(MyFeedActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        isfirsTime = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(MyFeedActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    // drawer navigate functionality
    public void addDotsNavigation() {
        dotsNavigate = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.circle_indicator);

        for (int i = 0; i < 2; i++) {
            ImageView dot = new ImageView(this);

            dot.setImageDrawable(getResources().getDrawable(R.drawable.under_slide_black_out));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            final int pos = i;

            dot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pager.setCurrentItem(pos);
                }
            });

            dotsLayout.addView(dot, params);

            dotsNavigate.add(dot);
        }
    }

    public void selectDotNavigation(int idx) {

        Resources res = getResources();
        for (int i = 0; i < 2; i++) {
            int drawableId = (i == idx) ? (R.drawable.under_slide_black) : (R.drawable.under_slide_black_out);
            Drawable drawable = res.getDrawable(drawableId);
            dotsNavigate.get(i).setImageDrawable(drawable);
        }
    }

    private void showSessionExpireDialog() {
        if (isfirsTime) {
            CommonUtilities.showSessionExpirePopup(activity);
            isfirsTime = false;
        }
    }

    public class FetchBannedWords extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;

        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "banned_words");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray banned_list = resultObj.getJSONArray("list");

                    for (int i = 0; i < banned_list.length(); i++) {

                        HashMap<String, Object> hash = new HashMap<>();

                        JSONObject object = banned_list.getJSONObject(i);

                        hash.put("banned_id", object.getString("banned_id"));

                        hash.put("banned_word", object.getString("banned_word"));

                        hash.put("banned_create_date", object.getString("banned_create_date"));

                        ((com.spott.app.UTCAPP) getApplication()).bannedWordsList.add(object.getString("banned_word"));

                        ((com.spott.app.UTCAPP) getApplication()).bannedWordsDetailList.add(hash);

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("MyFeedActivity...onActivityResult", "request code: " + requestCode + ", result code: " + resultCode);

        switch (requestCode) {
            case 1:
                Log.d("MyFeedActivity...onActivityResult", "need to refresh feed!");

                if (connDec.isConnectingToInternet()) {
                    new FeedData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    alert.showNetAlert();

                    break;
                }
        }
    }
}
