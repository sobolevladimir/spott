package com.spott.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.LoaderClass;

/**
 * Created by Acer on 9/12/2016.
 */
public class LoginScreenActivity extends Activity {

    LayoutInflater layoutInflater = null;
    EditText et_email, et_password;
    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    private String mailstr = "", passwordstr = "";
    SharedPreferences preferences, prefLogin;
    TextView tv_remember, tv_forgot, btn_login,btn_register;
    ArrayList<HashMap<String, String>> loginList;
    ConnectionDetector connDec;
    Activity activity;
    Alert_Dialog_Manager alert;
    String et_mailtr = "", token = "";
    String encoded, email = "", pass = "";
    private CheckBox saveLoginCheckBox;
    private Boolean saveLogin;
    RelativeLayout rl_bck;
    private SharedPreferences.Editor loginPrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        this.activity = (Activity) LoginScreenActivity.this;
        layoutInflater = LayoutInflater.from(this);
        loginList = new ArrayList<>();
        initialise();
        clickables();

        email = prefLogin.getString("email", "");
        pass = prefLogin.getString("password", "");

        et_email.setText(email);
        et_password.setText(pass);
        token = preferences.getString("TOKEN", token);
        System.out.println("LOGIN TOKEN" + token);
    }


    private void initialise() {
        alert = new Alert_Dialog_Manager(LoginScreenActivity.this);
        connDec = new ConnectionDetector(LoginScreenActivity.this);
        et_email = (EditText) findViewById(R.id.et_email);
        et_password = (EditText) findViewById(R.id.et_password);
        tv_remember = (TextView) findViewById(R.id.tv_remember);
        tv_forgot = (TextView) findViewById(R.id.tv_forgot_password);
        btn_login = (TextView) findViewById(R.id.btn_login);
        btn_register=(TextView)findViewById(R.id.btn_register);
        rl_bck = (RelativeLayout) findViewById(R.id.rl_bck);
        saveLoginCheckBox = (CheckBox) findViewById(R.id.saveLoginCheckBox);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        prefLogin = getSharedPreferences("LOGIN", MODE_PRIVATE);
//        loginPrefsEditor = preferences.edit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void clickables() {

        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customShareDetailsPopup();
            }
        });

        rl_bck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mailstr = et_email.getText().toString().trim();

                passwordstr = et_password.getText().toString().trim();

                if (connDec == null) {
                    connDec = new ConnectionDetector(LoginScreenActivity.this);
                }

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else if (!(mailstr.length() > 0)) {
                    Toast.makeText(getApplicationContext(), R.string.email, Toast.LENGTH_SHORT).show();
                } else if (!(passwordstr.length() > 0)) {
                    Toast.makeText(getApplicationContext(), R.string.password, Toast.LENGTH_SHORT).show();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LoginData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mailstr, passwordstr);
                    } else {
                        new LoginData().execute(mailstr, passwordstr);
                    }
                }
            }
        });

        et_email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    et_email.setHint("");
                else
                    et_email.setHint("EMAIL");
            }
        });
        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    et_password.setHint("");
                else
                    et_password.setHint("PASSWORD");
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginScreenActivity.this,RegisterScreenActivity.class));
            }
        });
    }

    private void customShareDetailsPopup() {
        final Dialog dialog = new Dialog(LoginScreenActivity.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.forget_popup);
        final EditText et_email_txt;
        Button submit_btn, cancel_button;
        et_email_txt = (EditText) dialog.findViewById(R.id.et_email_txt);
        submit_btn = (Button) dialog.findViewById(R.id.submit_button);
        cancel_button = (Button) dialog.findViewById(R.id.cancel_button);
        submit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_mailtr = et_email_txt.getText().toString();
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else if (!(et_mailtr.length() > 0)) {

                    Toast.makeText(getApplicationContext(), R.string.email, Toast.LENGTH_SHORT).show();
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new Forget_password().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new Forget_password().execute();
                    }
                }
            }
        });
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    public class Forget_password extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "", errorCode = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... params) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "forgot_password");

                jo.put("email", et_mailtr);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Please check your email id for new password.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    alertDialog.setCancelable(true);

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(LoginScreenActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class LoginData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(LoginScreenActivity.this);
        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "user_login");

                jo.put("email", mailstr);

                jo.put("password", passwordstr);

                jo.put("device_type", "android");

                jo.put("latitude", "");

                jo.put("longitude", "");

                jo.put("device_token", token);           // for push notification

                System.out.println("JSON DATA = " + jo);

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONObject user_dataObj = resultObj.getJSONObject("user");

                    String uid = user_dataObj.getString("uid");

                    String latitude = user_dataObj.getString("latitude");

                    String longitude = user_dataObj.getString("longitude");

                    token = user_dataObj.getString("device_token");

                    String login_token = user_dataObj.getString("login_token");

                    String username = user_dataObj.getString("username");
                    mailstr = user_dataObj.getString("email");
                    //passwordstr = user_dataObj.getString("password");

                    String user_create_date = user_dataObj.getString("user_create_date");
                    String badge = user_dataObj.getString("badge");
                    String facebook_id = user_dataObj.getString("facebook_id");
                    String survey_alert = user_dataObj.getString("survey_alert");
                    String password_token = user_dataObj.getString("password_token");
                    String register_check = user_dataObj.getString("register_check");
                    String otp = user_dataObj.getString("otp");
                    String verified = user_dataObj.getString("verified");
                    String password_change = user_dataObj.getString("password_change");
                    String phone_no = user_dataObj.getString("phone_no");
                    String achievements=user_dataObj.getString("achievements");

                    preferences.edit().putString("interests", achievements).commit();

                    preferences.edit().putString("device_token", token).commit();

                    preferences.edit().putString("uid", uid).commit();

                    if (saveLoginCheckBox.isChecked()) {

                        preferences.edit().putString("email", mailstr).commit();

                        preferences.edit().putString("password", passwordstr).commit();

                        prefLogin.edit().putString("email", mailstr).commit();

                        prefLogin.edit().putString("password", passwordstr).commit();

                    } else {
                        preferences.edit().putString("email", "").commit();

                        preferences.edit().putString("password", "").commit();

                        prefLogin.edit().putString("email", "").commit();

                        prefLogin.edit().putString("password", "").commit();
                    }

                    preferences.edit().putString("latitude", latitude).commit();

                    preferences.edit().putString("longitude", longitude).commit();

                    preferences.edit().putString("login_token", login_token).commit();

                    JSONObject user_dataObject = user_dataObj.getJSONObject("profile");

                    String profile_id = user_dataObject.getString("profile_id");
                    String first_name = user_dataObject.getString("first_name");
                    String last_name = user_dataObject.getString("last_name");
                    String userImage = user_dataObject.getString("user_image");
                    String user_thumbnail = user_dataObject.getString("user_thumbnail");
                    String hobbies = user_dataObject.getString("hobbies");
                    String phone = user_dataObject.getString("phone");
                    String dob = user_dataObject.getString("dob");
                    String quotes = user_dataObject.getString("quotes");
                    String profile_create_date = user_dataObject.getString("profile_create_date");
                    String profile_modify_date = user_dataObject.getString("profile_modify_date");
                    String gender = user_dataObject.getString("gender");

                    preferences.edit().putString("first_name", CommonUtilities.decodeUTF8(first_name)).commit();

                    preferences.edit().putString("last_name", CommonUtilities.decodeUTF8(last_name)).commit();

                    preferences.edit().putString("user_image", userImage).commit();

                    preferences.edit().putString("username", username).commit();

                    preferences.edit().putString("quotes", quotes).commit();

                    preferences.edit().putString("gender", gender).commit();

                    System.out.println("QUOTES=" + preferences.getString("quotes", ""));

                    if (password_change.equals("1")) {

                        Intent intent1 = new Intent(LoginScreenActivity.this, ResetPssword.class);

                        intent1.putExtra("values", "");

                        preferences.edit().putString("password_change", "yes").commit();

                        startActivity(intent1);
                    } else {

                        Intent intent = new Intent(LoginScreenActivity.this, MyFeedActivity.class);

                        intent.putExtra("first_name", first_name);

                        intent.putExtra("last_name", last_name);

                        intent.putExtra("user_image", userImage);

                        intent.putExtra("username", username);

                        intent.putExtra("gender", gender);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                    }

                    finish();

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LoginScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    alertDialog.setCancelable(true);

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(LoginScreenActivity.this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Login Screen");

        FlurryAgent.onStartSession(LoginScreenActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(LoginScreenActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}

