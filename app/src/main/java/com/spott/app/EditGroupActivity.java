package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;

import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;
import com.spott.app.cropiimage.CropImage;

/**
 * Created by Adimn on 10/3/2016.
 */
public class EditGroupActivity extends FragmentActivity implements View.OnClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    ImageView iv_back_icon, iv_red_icon, iv_emojibtn, iv_emojibtn_grp_detail, iv_keyboard, iv_keyboard_grp_detail;
    DiamondImageView iv_group_photo;
    EditText et_grp_name, et_grp_detail, et_people_add;
    ContactsCompletionView et_tags;
    Button btn_save;
    RadioButton first, second;
    TextView tv_click, tv_num;
    LinearLayout ll_feed, ll_profile, ll_directory, emoLay;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_number, tv_grp_photo_txt;
    RelativeLayout rl_tags,ll_groups, rl_emo_buttons_grp_name, rl_emo_buttons_grp_detail;
    Activity activity;
    SharedPreferences preferences;
    String namestr = "", grpdetailStr = "", radioButtonText;
    int setPic = 0, img_clicked;
    byte[] pro_pic_img_byte;
    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    private String img_state;
    private File mFileTemp;
    public static final int REQUEST_CODE_GALLERY = 0x1,
            REQUEST_CODE_TAKE_PICTURE = 0x2, REQUEST_CODE_CROP_IMAGE = 0x3;
    String encoded;
    ArrayList<Object> tokens = new ArrayList<>();
    private BroadcastReceiver broadcastReceiver;
    JSONArray jArray;
    String chipArr = "";
    String group_id = "", groupname = "", grpdetail = "", grpimg = "", grptype = "", grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    String GROUPNameStr = "", GroupDetailStr = "", Grouptag = "";
    String Radiobtn = "";

    @Override
    public void onBackPressed() {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.hideSoftInputFromWindow(et_tags.getWindowToken(), 0);

        if (emoLay.getVisibility() == View.VISIBLE)
            emoLay.setVisibility(View.GONE);
        else
            finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_group);
        this.activity = (Activity) EditGroupActivity.this;
        initialise();
        clickables();
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new ViewGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new ViewGroupData().execute();
        }
        setting();
//        Intent intent = getIntent();
//        group_id = intent.getStringExtra("group_id");
//        groupname = intent.getStringExtra("group_name");
//        grpdetail = intent.getStringExtra("group_detail");
//        grpimg = intent.getStringExtra("group_image");
//        grptype = intent.getStringExtra("group_type");
//        grptag = intent.getStringExtra("group_tags");
//        groupcreatdate = intent.getStringExtra("group_create_date");
//        grpstatus = intent.getStringExtra("group_status");
//        owner_id = intent.getStringExtra("owner_id");
//        owner_type = intent.getStringExtra("owner_type");
//        you_grp = intent.getStringExtra("you_group");

//        String[] tagsArray = grptag.split(",");
//
//        for (int i = 0; i < tagsArray.length; i++) {
//            et_tags.addObject(new Person(tagsArray[i], tagsArray[i]));
//        }


    }


    private void initialise() {
        rl_tags=(RelativeLayout)findViewById(R.id.rl_tags);
        tv_grp_photo_txt = (TextView) findViewById(R.id.tv_grp_photo_txt);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_emojibtn = (ImageView) findViewById(R.id.iv_emojibtn);
        iv_keyboard = (ImageView) findViewById(R.id.iv_keyboard);
        iv_keyboard_grp_detail = (ImageView) findViewById(R.id.iv_keyboard_grp_detail);
        iv_emojibtn_grp_detail = (ImageView) findViewById(R.id.iv_emojibtn_grp_detail);
        tv_number = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        iv_group_photo = (DiamondImageView) findViewById(R.id.iv_group_photo);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        rl_emo_buttons_grp_name = (RelativeLayout) findViewById(R.id.rl_emo_buttons_grp_name);
        rl_emo_buttons_grp_detail = (RelativeLayout) findViewById(R.id.rl_emo_buttons_grp_detail);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        emoLay = (LinearLayout) findViewById(R.id.emoLay);
        et_grp_name = (EditText) findViewById(R.id.et_grp_name);
        et_grp_name.setTypeface(Typeface.createFromAsset(getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
        et_grp_detail = (EditText) findViewById(R.id.et_grp_detail);
        et_grp_detail.setTypeface(Typeface.createFromAsset(getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_people_add = (EditText) findViewById(R.id.et_people_add);
        tv_num = (TextView) findViewById(R.id.tv_num);
        et_tags = (ContactsCompletionView) findViewById(R.id.et_tags);

        et_tags.allowCollapse(false);

        btn_save = (Button) findViewById(R.id.btn_save);
        first = (RadioButton) findViewById(R.id.first);
        second = (RadioButton) findViewById(R.id.second);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();

        if (getIntent().hasExtra("List")) {
            arrayList = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("List");

            jArray = new JSONArray();

            for (int i = 0; i < arrayList.size(); i++) {
                jArray.put(arrayList.get(i).get("uid"));
                if (chipArr.equals("")) {
                    chipArr = arrayList.get(i).get("first_name") + " " + arrayList.get(i).get("last_name");
                } else {
                    chipArr = chipArr + " , " + arrayList.get(i).get("first_name") + " " + arrayList.get(i).get("last_name");
                }
            }
            System.out.println("SELECTED ARRAY " + jArray);

            et_people_add.setText(chipArr);
        }


        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void clickables() {
        rl_tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_tags.isEnabled()) {
                    et_tags.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(et_tags, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                imm.hideSoftInputFromWindow(et_tags.getWindowToken(), 0);

                if (emoLay.getVisibility() == View.VISIBLE)
                    emoLay.setVisibility(View.GONE);
                else
                    finish();
            }
        });

        iv_emojibtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard.setVisibility(View.VISIBLE);
                iv_emojibtn.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);


            }
        });

        iv_keyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard.setVisibility(View.GONE);
                iv_emojibtn.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_grp_name);
            }
        });

        et_grp_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons_grp_name.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons_grp_name.setVisibility(View.GONE);
                    iv_emojibtn.setVisibility(View.VISIBLE);
                    iv_keyboard.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_grp_name.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_grp_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn.setVisibility(View.VISIBLE);
                iv_keyboard.setVisibility(View.GONE);
            }
        });

        iv_emojibtn_grp_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_grp_detail.setVisibility(View.VISIBLE);
                iv_emojibtn_grp_detail.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);
            }
        });

        iv_keyboard_grp_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_grp_detail.setVisibility(View.GONE);
                iv_emojibtn_grp_detail.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_grp_detail);
            }
        });

        et_grp_detail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {
                    rl_emo_buttons_grp_detail.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons_grp_detail.setVisibility(View.GONE);
                    iv_emojibtn_grp_detail.setVisibility(View.VISIBLE);
                    iv_keyboard_grp_detail.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });



        et_grp_detail.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);
                return false;
            }
        });

        et_grp_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_grp_detail.setVisibility(View.VISIBLE);
                iv_keyboard_grp_detail.setVisibility(View.GONE);
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditGroupActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                second.setChecked(false);
                Radiobtn = "public";
            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                first.setChecked(false);
                Radiobtn = "private";
            }
        });

        iv_group_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMessagephoto("UTC", "Profile Pic");
            }
        });

        tv_grp_photo_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomMessagephoto("UTC", "Profile Pic");
            }
        });
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GROUPNameStr = CommonUtilities.encodeUTF8(et_grp_name.getText().toString().trim());

                GroupDetailStr = CommonUtilities.encodeUTF8(et_grp_detail.getText().toString().trim());
                Grouptag = et_tags.getText().toString().trim();
                if (first.isChecked()) {
                    Radiobtn = "public";
                } else {
                    Radiobtn = "private";
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new EditServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new EditServiceData().execute();
                }
            }
        });
    }

    private void showCustomMessagephoto(String string, String string2) {
        final Dialog dialogphoto = new Dialog(EditGroupActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        OpenGallery();
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        takePicture();
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();
    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:
                Intent intent = new Intent(EditGroupActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(EditGroupActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(EditGroupActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(EditGroupActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }


    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_GALLERY);
    }

    static ArrayList<Bitmap> bitmaps_arrayList = new ArrayList<Bitmap>();

    File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Bitmap bitmap;

            byte[] b;
            switch (requestCode) {
                case REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                        copyStream(inputStream, fileOutputStream);

                        fileOutputStream.close();

                        inputStream.close();

                        startCropImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case REQUEST_CODE_TAKE_PICTURE:
                    //System.out.println("onact camera");
                    startCropImage();
                    break;
                case REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                    b = baos.toByteArray();

                    //String baseimg = Base64.encodeToString(b, Base64.DEFAULT);

                    System.out.println("bitmap in return : " + bitmap);

                    bitmaps_arrayList.add(bitmap);

                    if (mFileTemp.exists()) {
                        mFileTemp.delete();
                    }

                    base64frombitmap(bitmap);

                    int bwidth = bitmap.getWidth();

                    int bheight = bitmap.getHeight();

                    int swidth = iv_group_photo.getWidth();

                    int sheight = iv_group_photo.getHeight();

                    int new_width = swidth;

//                    int new_height = (int) Math.floor((double) bheight * ((double) new_width / (double) bwidth));
//
//                    Bitmap newbitMap = Bitmap.createScaledBitmap(bitmap, new_width, new_height, true);
//
//                    iv_group_photo.setImageBitmap(newbitMap);

                    iv_group_photo.setImageBitmap(bitmap);

                    tv_grp_photo_txt.setVisibility(View.GONE);

            }
        } else {
            if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                startCropImage();
            }
        }
    }

    private void startCropImage() {

        //System.out.println("cropimage");

        Intent intent = new Intent(EditGroupActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //System.out.println("takepict inner");
        try {
            Uri mImageCaptureUri = null;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        if (et_grp_name.hasFocus()) {
            EmojiconsFragment.input(et_grp_name, emojicon);
        } else if (et_grp_detail.hasFocus()) {
            EmojiconsFragment.input(et_grp_detail, emojicon);
        }
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        if (et_grp_name.hasFocus()) {
            EmojiconsFragment.backspace(et_grp_name);
        } else if (et_grp_detail.hasFocus()) {
            EmojiconsFragment.backspace(et_grp_detail);
        }
    }

    public class ViewGroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(EditGroupActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_detail");

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {


                    JSONObject user_dataObj = resultObj.getJSONObject("detail");
                    String uid1 = "";
                    String group_id = user_dataObj.getString("group_id");
                    groupname = user_dataObj.getString("group_name");
                    grpdetail = user_dataObj.getString("group_detail");
                    String group_image = user_dataObj.getString("group_image");
                    String group_type = user_dataObj.getString("group_type");
                    String group_tags = user_dataObj.getString("group_tags");
                    String group_create_date = user_dataObj.getString("group_create_date");
                    String group_status = user_dataObj.getString("group_status");
                    String you_like = user_dataObj.getString("you_like");
                    String your_like = user_dataObj.getString("your_like");
                    String owner_id = user_dataObj.getString("owner_id");
                    String owner_type = user_dataObj.getString("owner_type");
                    String you_group = user_dataObj.getString("you_group");


                    if (!you_group.equals("")) {
                        JSONObject user_dataObj1 = new JSONObject(you_group);
                        String member_id = user_dataObj1.getString("member_id");
                        String group_id1 = user_dataObj1.getString("group_id");
                        uid1 = user_dataObj1.getString("uid");
                        String member_status = user_dataObj1.getString("member_status");
                        String mute_group = user_dataObj1.getString("mute_group");
                        String is_fav = user_dataObj1.getString("is_fav");
                        String member_create_date = user_dataObj1.getString("member_create_date");

                        preferences.edit().putString("member_id", member_id).commit();
                        preferences.edit().putString("is_fav", is_fav).commit();
                    }


                    /////////////////////set data/////////////////////////////////////////////////////////////////

                    if (group_type.equals("Public")) {
                        first.setChecked(true);
                        second.setChecked(false);
                    } else {
                        second.setChecked(true);
                        first.setChecked(false);
                    }

                    et_grp_name.setText(CommonUtilities.decodeUTF8(groupname));
                    et_grp_detail.setText(CommonUtilities.decodeUTF8(grpdetail));
                    String[] tagsArray = group_tags.split(",");

                    for (int i = 0; i < tagsArray.length; i++) {
                        et_tags.addObject(new Person(tagsArray[i], tagsArray[i]));
                    }
                    Picasso.with(getApplicationContext()).load(group_image).into(iv_group_photo, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            tv_grp_photo_txt.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
                    iv_group_photo.setBorderColor(Color.parseColor("#F15A51"));
                    iv_group_photo.setBorderWidth(2);
                    preferences.edit().putString("owner_id", owner_id).commit();
                    preferences.edit().putString("YOU", you_like).commit();

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(EditGroupActivity.this);
        }
    }

    public class EditServiceData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(EditGroupActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "edit_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("group_name", GROUPNameStr);

                jo.put("group_type", Radiobtn);

                jo.put("group_detail", GroupDetailStr);

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("group_image", encoded);

                List<Person> array = et_tags.getObjects();

                StringBuilder stringBuilder = new StringBuilder(9999999);

                for (int i = 0; i < array.size(); i++) {
                    stringBuilder.append(array.get(i).getName() + ",");
                }

                jo.put("group_tags", stringBuilder.toString());


                //  jo.put("group_tags",Grouptag);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent();

                    intent.putExtra("group_type", Radiobtn);
                    intent.putExtra("group_name", GROUPNameStr);

                    setResult(RESULT_OK, intent);

                    finish();

                } else if (errCode.equals("300")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(EditGroupActivity.this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Edit Group Screen");

        FlurryAgent.onStartSession(EditGroupActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(EditGroupActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}
