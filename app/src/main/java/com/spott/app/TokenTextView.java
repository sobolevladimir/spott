package com.spott.app;

/**
 * Created by Adimn on 9/30/2016.
 */

import android.content.Context;
import android.util.AttributeSet;

import io.github.rockerhieu.emojicon.EmojiconTextView;

public class TokenTextView extends EmojiconTextView {

    public TokenTextView(Context context) {
        super(context);
    }

    public TokenTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        setCompoundDrawablesWithIntrinsicBounds(0, 0, selected ? R.drawable.close_x : 0, 0);
    }
}
