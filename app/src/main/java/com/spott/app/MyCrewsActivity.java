package com.spott.app;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.spott.app.activities.crewLending.CrewLandingActivity;
import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Acer on 9/16/2016.
 */
public class MyCrewsActivity extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {

    ImageView iv_search, add_sign;
    ArrayList<HashMap<String, String>> searchlist;
    GroupPagingAdapter groupAdapter;
    ArrayList<HashMap<String, String>> mygrouplist;
    ArrayList<HashMap<String, String>> mygroupfavlist;
    ArrayList<HashMap<String, String>> dataList;
    ArrayList<HashMap<String, String>> viewlist;
    Context context;
    DiamondImageView img_profile;
    PullToRefreshListView listview;
    TextView tv_number, tv_num;
    Activity activity;
    FrameLayout fl_view_pager;
    ViewPager viewpager;
    TextView browse_btn, iv_red_icon;
    GroupAdapter1 gAdapter;
    private BroadcastReceiver broadcastReceiver;
    LinearLayout ll_feed, ll_profile, ll_calendar;
    RelativeLayout rlMainFront, ll_groups;
    View shadowView = null, view = null;
    int height, width;
    ImageView iv_menu_icon;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory;
    boolean drawer_open = false;
    SharedPreferences preferences;
    String course = "", skill = "", interst = "";
    private File mFileTemp;
    private int currentPage = 0, maxPages = 0;
    String strBase64;
    String[] courses, skills, inerests;
    String finalInterests = "";
    String group_id = "";
    int count = 0;
    private AdView mAdView;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "", feed_count = "";
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    EditText iv_serach_view;
    String encoded;
    GroupAdapter adapter;
    NavigationFragmentAdapter nadapter;
    String string = "#ed3224";
    String string1 = "#ffffff";
    LayoutInflater layoutInflater = null;
    ViewPager pager;
    TextView tv_msg_grp, tv_msg_recomgrp;
    private PullToRefreshListView pulllist;
    String firstname = "", lastname = "", login = "", star = "", group = "", post = "", gender = "";
    ArrayList<HashMap<String, String>> joingroupList1;
    ArrayList<HashMap<String, String>> pingList;
    private boolean isLoading = false;
    private ArrayList<ImageView> dotsNavigate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_groups);
        this.activity = (Activity) MyCrewsActivity.this;
        listview = (PullToRefreshListView) findViewById(R.id.list);
        context = MyCrewsActivity.this;
        MobileAds.initialize(this, String.valueOf(R.string.MobileAds_initialize));
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        dataList = new ArrayList<HashMap<String, String>>();
        viewlist = new ArrayList<HashMap<String, String>>();
        mygrouplist = new ArrayList<HashMap<String, String>>();
        mygroupfavlist = new ArrayList<HashMap<String, String>>();
        searchlist = new ArrayList<HashMap<String, String>>();

        joingroupList1 = new ArrayList<>();
        pingList = new ArrayList<>();

        joingroupList1.addAll(((UTCAPP) getApplication()).joingroupList);
        groupAdapter = new GroupPagingAdapter(MyCrewsActivity.this, dataList);
        viewpager = (ViewPager) activity.findViewById(R.id.viewpager);
        //viewpager.setAdapter(groupAdapter);
        nadapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(nadapter);
        layoutInflater = LayoutInflater.from(this);
        initialise();

        addDotsNavigation();

        firstname = preferences.getString("first_name", "");
        lastname = preferences.getString("last_name", "");
        encoded = preferences.getString("user_image", "");
        login = preferences.getString("login_token", "");
        star = preferences.getString("pelican", "");
        group = preferences.getString("total_group", "");
        post = preferences.getString("total_post", "");
        gender = preferences.getString("gender", "");

        clickables();

        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new RecommendGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new RecommendGroupData().execute();
            }
        }

        view.setOnClickListener(this);

        initBottomBar();

        skill = preferences.getString("skills", "");
        interst = preferences.getString("interests", "");
        course = preferences.getString("course", "");
        System.out.println("skills=" + skill);
        System.out.println("interst=" + interst);
        System.out.println("course=" + course);
        courses = course.split(",");
        inerests = interst.split(",");
        skills = skill.split(",");

        arrayCheck();

        listview.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {


            }
        });
        listview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                if (!connDec.isConnectingToInternet()) {
                    // alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupData().execute();
                    }
                }


                count = 0;

            }
        });
    }

    private void initBottomBar() {
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_calendar.setOnClickListener(this);
        setting();
    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#FFFFFF"));

        ll_calendar.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);

    }

    private void arrayCheck() {

        for (int i = 0; i < courses.length; i++) {
            courses[i] = courses[i].trim();

            if (finalInterests.equals("")) {
                finalInterests = finalInterests + courses[i];
            } else {
                finalInterests = finalInterests + "," + courses[i];
            }
        }

        for (int i = 0; i < inerests.length; i++) {
            inerests[i] = inerests[i].trim();

            finalInterests = finalInterests + "," + inerests[i];
        }

        for (int i = 0; i < skills.length; i++) {
            skills[i] = skills[i].trim();

            finalInterests = finalInterests + "," + skills[i];
        }

        System.out.println("FINAL INTERESTS=" + finalInterests);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void initialise() {
        connDec = new ConnectionDetector(MyCrewsActivity.this);
        alert = new Alert_Dialog_Manager(context);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        add_sign = (ImageView) findViewById(R.id.add_sign);
        browse_btn = (TextView) findViewById(R.id.browse_btn);
        tv_msg_grp = (TextView) findViewById(R.id.tv_msg_grp);
        tv_msg_recomgrp = (TextView) findViewById(R.id.tv_msg_recomgrp);
        fl_view_pager = (FrameLayout) activity.findViewById(R.id.fl_view_pager);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_red_icon = (TextView) findViewById(R.id.iv_red_icon);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_calendar = (LinearLayout) findViewById(R.id.ll_calendar);
        shadowView = (View) findViewById(R.id.shadowView);
        view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        iv_serach_view = (EditText) findViewById(R.id.iv_serach_view);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.GONE);
                        feed_count = i + "";
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        feed_count = i + "";
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        feed_count = "99+";
                        iv_red_icon.setText("99+");
                    }
                    nadapter.notifyDataSetChanged();
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    if (!tv_num.getText().toString().equals(new_message)) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new GroupData().execute();
                        }
                    }
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                        tv_num.setText("" + i);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }

                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onResume() {

        super.onResume();

        getAllData();
    }

    private void getAllData() {
        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GroupData().execute();
            }
        }
        if (!connDec.isConnectingToInternet()) {
            //alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GroupFavData().execute();
            }
        }

        final int pos = 1;
        pager.postDelayed(new Runnable() {

            @Override
            public void run() {
                pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
            }
        }, 100);

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ((UTCAPP) getApplication()).pagerPos = position;

                selectDotNavigation(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

// For Navigation Animation

    private class NavigationFragmentAdapter extends PagerAdapter {

        TextView tv_browsing;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_profile_name, tv_profile_username, tv_notification_count, tv_update, tv_feed, tv_profile, tv_groups, tv_staff_dir, tv_log_out;


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            if (feed_count.equals("0"))
                tv_notification_count.setVisibility(View.GONE);

            else {
                tv_notification_count.setVisibility(View.VISIBLE);

                tv_notification_count.setText(feed_count);
            }

        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);
                if (!connDec.isConnectingToInternet()) {
                    // alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupFavData().execute();
                    }
                }
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyCrewsActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                    }
                });
                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {

                        Toast.makeText(MyCrewsActivity.this, "End of List!", Toast.LENGTH_SHORT).show();
                    }
//                String count = preferences.getString("page_count", "0");
//
//                if (!count.equals("0")) {
//
//                    list.setRefreshing(true);
//
//                    lastCount = adapter.getCount();
//
//                    new FeedData().execute();
//                }
//            }
                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;
                        if (!connDec.isConnectingToInternet()) {
                            //  alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            } else {
                                new GroupFavData().execute();
                            }

                            mygroupfavlist.clear();
                        }
                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MyCrewsActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
                tv_log_out = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(CommonUtilities.decodeUTF8(preferences.getString("first_name", "")) + " " + CommonUtilities.decodeUTF8(preferences.getString("last_name", "")));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                if (feed_count.equals("0"))
                    tv_notification_count.setVisibility(View.GONE);

                else {
                    tv_notification_count.setVisibility(View.VISIBLE);

                    tv_notification_count.setText(feed_count);
                }
                if (encoded.equalsIgnoreCase("")) {

                    img_profile.setImageResource(R.drawable.avatar);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }
                tv_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(activity, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);

                    }
                });

                orange_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonUtilities.showStarsPopup(activity);
                    }
                });

                tv_notification_count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyCrewsActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });

                img_profile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent1 = new Intent(activity, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);
                    }
                });
                tv_feed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyCrewsActivity.this, MyFeedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });

                tv_staff_dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyCrewsActivity.this, MyDirectoryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(MyCrewsActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_log_out.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        customShareLogOutPopup();
                    }
                });
                container.addView(view, 0);
            }
            //   ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

    }

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    private void clickables() {

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();

                    drawer_open = true;
                    pager.setAdapter(nadapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
                    if (!(preferences.getString("user_image", "").equals(""))) {
                        Picasso.with(getApplicationContext()).load(preferences.getString("user_image", "")).into(img_profile);
                        img_profile.setBorderColor(Color.parseColor("#F15A51"));
                        img_profile.setBorderWidth(2);
                    }

                } else {
                    StopDrawerAnim();
                }

            }
        });

        browse_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyCrewsActivity.this, JoinGroupActivity.class);
                startActivity(intent);
            }
        });

        add_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyCrewsActivity.this, AddGroupActivity.class);
                startActivity(intent);
            }
        });

        iv_serach_view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String searchString = iv_serach_view.getText().toString();
                int txtLength = searchString.length();
                searchlist.clear();
                for (int i = 0; i < mygrouplist.size(); i++) {
                    String jobNumber = mygrouplist.get(i).get("group_name");

                    if (txtLength <= jobNumber.length()) {

                        if (jobNumber.toLowerCase().contains(searchString.toLowerCase())) {
                            searchlist.add(mygrouplist.get(i));
                        }
                    }
                }

                adapter = new GroupAdapter(MyCrewsActivity.this, searchlist);

                listview.setAdapter(adapter);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyCrewsActivity.this, UpdatesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = joingroupList1.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupData().execute();
                    }
                }
            } else {
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    public static void setListViewHeightBasedOnItems(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public void startDrawerAnim() {

        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }

    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:
                Intent intent = new Intent(MyCrewsActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(MyCrewsActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(MyCrewsActivity.this, CalendarActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }


    private class GroupAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygrouplist;

        public GroupAdapter(Context context, ArrayList<HashMap<String, String>> mygrouplist) {
            this.context = context;
            this.mygrouplist = mygrouplist;
        }

        @Override
        public int getCount() {
            return mygrouplist.size();

        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group, null);
            System.out.println("SIZE" + mygrouplist.size());
            final ViewHolder holder = new ViewHolder();
            holder.iv_list_img = (DiamondImageView) convertView.findViewById(R.id.iv_list_img);
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);

            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.badge_icon = (TextView) convertView.findViewById(R.id.badge_icon);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);

            final String unreadCount = mygrouplist.get(i).get("unread");
            group_id = mygrouplist.get(i).get("group_id");


            if (unreadCount.equals("0") || unreadCount.equals("")) {
                holder.badge_icon.setVisibility(View.GONE);
            } else {
                holder.badge_icon.setText(unreadCount);

                holder.badge_icon.setVisibility(View.VISIBLE);
            }
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*Intent intent = new Intent(context, CrewActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                    //TODO test navigation
                    Intent intent = new Intent(context, CrewLandingActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.putExtra("group_id", mygrouplist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygrouplist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygrouplist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygrouplist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygrouplist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygrouplist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygrouplist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygrouplist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygrouplist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygrouplist.get(i).get("owner_type"));

                    if (!unreadCount.equals("0")) {
                        intent.putExtra("scrollTobottom", "TRUE");
                    } else {
                        intent.putExtra("scrollTobottom", "FALSE");
                    }

                    if (!mygrouplist.get(i).get("you_group").equals("") &&
                            mygrouplist.get(i).get("member_status").equalsIgnoreCase("Active")) {
                        startActivityForResult(intent, 1);
                    } else {
                        intent.putExtra("options_next", "");

                        startActivityForResult(intent, 1);

                    }
                }
            });

            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                     /*Intent intent = new Intent(context, CrewActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
                    //TODO test navigation
                    Intent intent = new Intent(context, CrewLandingActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.putExtra("group_id", mygrouplist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygrouplist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygrouplist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygrouplist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygrouplist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygrouplist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygrouplist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygrouplist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygrouplist.get(i).get("owner_id"));

                    intent.putExtra("imp_id", mygrouplist.get(i).get("uid"));

                    intent.putExtra("owner_type", mygrouplist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygrouplist.get(i).get("you_group"));

                    startActivityForResult(intent, 1);
                }
            });

            if (mygrouplist.equals("")) {
                //listview.setVisibility(View.GONE);
                tv_msg_grp.setVisibility(View.VISIBLE);
            } else {
                listview.setVisibility(View.VISIBLE);
                tv_msg_grp.setVisibility(View.GONE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygrouplist.get(i).get("group_name")));
                if (!mygrouplist.get(i).get("group_image").toString().equals("")) {
                    Picasso.with(getApplicationContext()).load(mygrouplist.get(i).get("group_image")).into(holder.iv_list_img);
                    holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_list_img.setBorderWidth(2);
                }
            }
            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, badge_icon;
            RelativeLayout rll;
        }
    }


    private class GroupPagingAdapter extends PagerAdapter {


        DiamondImageView iv_group_img;
        TextView tv_grp_name, tv_descrp, tv_join, tv_recom, tv_pending, tv_view;
        ArrayList<HashMap<String, String>> dataList;
        Context context;
        LayoutInflater inflater;
        RelativeLayout rl_recommend;

        public GroupPagingAdapter(Context context, ArrayList<HashMap<String, String>> dataList) {

            this.dataList = dataList;
            this.context = context;
            System.out.println("dataList=" + this.dataList);
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewGroup) container).removeView((View) object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            try {
                view = layoutInflater.inflate(R.layout.group_adapter, container, false);
                iv_group_img = (DiamondImageView) view.findViewById(R.id.iv_group_img);
                tv_grp_name = (TextView) view.findViewById(R.id.tv_grp_name);
                tv_grp_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_recom = (TextView) view.findViewById(R.id.tv_recom);
                rl_recommend = (RelativeLayout) view.findViewById(R.id.rl_recommend);
//                tv_descrp = (TextView) view.findViewById(R.id.tv_descrp);
//                tv_join = (TextView) view.findViewById(R.id.tv_jon);
//                tv_pending = (TextView) view.findViewById(R.id.tv_pending);
//                tv_view = (TextView) view.findViewById(R.id.tv_view);

                if ((dataList != null)) {
                    System.out.println("dataList:" + dataList);

                    viewpager.setVisibility(View.VISIBLE);
                    tv_msg_recomgrp.setVisibility(View.GONE);

                    if (!dataList.get(position).get("group_image").toString().equals("")) {
                        Picasso.with(context).load(dataList.get(position).get("group_image")).into(iv_group_img);
                        iv_group_img.setBorderColor(Color.parseColor("#F15A51"));
                        iv_group_img.setBorderWidth(1);
                    }
                    tv_grp_name.setText(CommonUtilities.decodeUTF8(dataList.get(position).get("group_name").toString()));

                    rl_recommend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            if (!(dataList.get(position).get("you_group").equals(""))) {
//                                if (dataList.get(position).get("uid").equals(preferences.getString("uid", ""))) {
//                                    Intent intent1 = new Intent(MyCrewsActivity.this, MyProfileActivity.class);
//                                    intent1.putExtra("uid", preferences.getString("uid", ""));
//                                    intent1.putExtra("myProfile", "");
//                                    intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                                    startActivity(intent1);
//                                }
//                            }else {
//                                Intent intent = new Intent(MyCrewsActivity.this, MyProfile.class);
//                                intent.putExtra("Group_select", dataList.get(position).get("group_id"));
//                                intent.putExtra("Group_select_owner", dataList.get(position).get("owner_id"));
//                                intent.putExtra("group_id", group_id);
//                                intent.putExtra("group_name", dataList.get(position).get("group_name"));
//                                intent.putExtra("group_detail",  dataList.get(position).get("group_detail"));
//                                intent.putExtra("group_image",  dataList.get(position).get("group_image"));
//                                intent.putExtra("group_type",  dataList.get(position).get("group_type"));
//                                intent.putExtra("group_tags",  dataList.get(position).get("group_tags"));
//                                intent.putExtra("group_create_date",  dataList.get(position).get("group_create_date"));
//                                intent.putExtra("group_status",  dataList.get(position).get("group_status"));
//                                intent.putExtra("owner_type",  dataList.get(position).get("owner_type"));
//                                intent.putExtra("you_group",  dataList.get(position).get("you_group"));
//                                startActivity(intent);
//                            }
                            if ((dataList.get(position).get("you_group").equals(""))) {
                                Intent intent = new Intent(MyCrewsActivity.this, CrewActivity.class);
                                intent.putExtra("group_id", dataList.get(position).get("group_id"));

                                intent.putExtra("group_name", dataList.get(position).get("group_name"));

                                intent.putExtra("group_detail", dataList.get(position).get("group_detail"));

                                intent.putExtra("group_image", dataList.get(position).get("group_image"));

                                intent.putExtra("group_type", dataList.get(position).get("group_type"));

                                intent.putExtra("group_tags", dataList.get(position).get("group_tags"));

                                intent.putExtra("group_create_date", dataList.get(position).get("group_create_date"));

                                intent.putExtra("group_status", dataList.get(position).get("group_status"));

                                intent.putExtra("owner_id", dataList.get(position).get("owner_id"));

                                intent.putExtra("owner_type", dataList.get(position).get("owner_type"));

                                intent.putExtra("options_next", "");

                                startActivity(intent);
                            } else if ((!dataList.get(position).get("you_group").equals(""))) {
                                Intent intent = new Intent(MyCrewsActivity.this, CrewActivity.class);
                                intent.putExtra("group_id", dataList.get(position).get("group_id"));

                                intent.putExtra("group_name", dataList.get(position).get("group_name"));

                                intent.putExtra("group_detail", dataList.get(position).get("group_detail"));

                                intent.putExtra("group_image", dataList.get(position).get("group_image"));

                                intent.putExtra("group_type", dataList.get(position).get("group_type"));

                                intent.putExtra("group_tags", dataList.get(position).get("group_tags"));

                                intent.putExtra("group_create_date", dataList.get(position).get("group_create_date"));

                                intent.putExtra("group_status", dataList.get(position).get("group_status"));

                                intent.putExtra("owner_id", dataList.get(position).get("owner_id"));

                                intent.putExtra("owner_type", dataList.get(position).get("owner_type"));

                                startActivity(intent);
                            }
                        }

                    });
                    //  tv_descrp.setText(dataList.get(position).get("group_create_date").toString());

//
//                    tv_join.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            System.out.println("dataList:" + dataList);
//
//                            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
//                            {
//                                new GroupRequestData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                            }
//                            else
//                            {
//                                new GroupRequestData().execute();
//                            }
//                        }
//                        // }
//                    });


//                    tv_pending.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            Intent intent = new Intent(context, PendingActivity.class);
//
//                            intent.putExtra("group_id", dataList.get(position).get("group_id"));
//
//                            intent.putExtra("group_name", dataList.get(position).get("group_name"));
//
//                            intent.putExtra("group_detail", dataList.get(position).get("group_detail"));
//
//                            startActivity(intent);
//
//                        }
//                    });
//
//                    tv_view.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//
//                            Intent intent = new Intent(context, CrewActivity.class);
//
//                            intent.putExtra("group_id", dataList.get(position).get("group_id"));
//
//                            intent.putExtra("group_name", dataList.get(position).get("group_name"));
//
//                            intent.putExtra("group_detail", dataList.get(position).get("group_detail"));
//
//                            intent.putExtra("group_image", dataList.get(position).get("group_image"));
//
//                            intent.putExtra("group_type", dataList.get(position).get("group_type"));
//
//                            intent.putExtra("group_tags", dataList.get(position).get("group_tags"));
//
//                            intent.putExtra("group_create_date", dataList.get(position).get("group_create_date"));
//
//                            intent.putExtra("group_status", dataList.get(position).get("group_status"));
//
//                            intent.putExtra("owner_id", dataList.get(position).get("owner_id"));
//
//                            intent.putExtra("owner_type", dataList.get(position).get("owner_type"));
//
//                            intent.putExtra("you_group", dataList.get(position).get("you_group"));
//
//                            startActivity(intent);
//
//                        }
//                    });
                } else {
                    tv_msg_recomgrp.setVisibility(View.VISIBLE);
                }
                container.addView(view, 0);

            } catch (InflateException e) {

            }

            return view;
        }


    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if (!connDec.isConnectingToInternet()) {
                    // alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupData().execute();
                    }
                }
            }
        }
    }

    public class GroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "my_groups");

                jo.put("login_token", login);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            listview.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    mygrouplist.clear();

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        for (int i = 0; i < help_array.length(); i++) {

                            //  mygrouplist.clear();

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = CommonUtilities.decodeUTF8(helpObject.getString("group_name"));
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String owner_id = helpObject.getString("owner_id");
                            String unread = helpObject.getString("unread");
                            String owner_type = helpObject.getString("owner_type");
                            JSONObject you_group = helpObject.getJSONObject("you_group");

                            hmap.put("member_id", you_group.getString("member_id"));
                            hmap.put("group_id", you_group.getString("group_id"));
                            hmap.put("uid", you_group.getString("uid"));
                            hmap.put("member_status", you_group.getString("member_status"));
                            hmap.put("mute_group", you_group.getString("mute_group"));
                            hmap.put("is_fav", you_group.getString("is_fav"));
                            hmap.put("member_create_date", you_group.getString("member_create_date"));

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group.toString());
                            hmap.put("unread", unread);

                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygrouplist.add(hmap);
                        }
                        adapter = new GroupAdapter(MyCrewsActivity.this, mygrouplist);

                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("300")) {

                    // listview.setVisibility(View.GONE);
                    tv_msg_grp.setVisibility(View.VISIBLE);

                } else if (errCode.equals("700")) {

                    CommonUtilities.showSessionExpirePopup(activity);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class RecommendGroupData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "recommend_groups");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("user_interest", interst);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Recomm data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    dataList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String group_id = user_dataObj.getString("group_id");
                        String group_name = user_dataObj.getString("group_name");
                        String group_detail = user_dataObj.getString("group_detail");
                        String group_image = user_dataObj.getString("group_image");
                        String group_type = user_dataObj.getString("group_type");
                        String group_tags = user_dataObj.getString("group_tags");
                        String group_create_date = user_dataObj.getString("group_create_date");
                        String group_status = user_dataObj.getString("group_status");
                        String owner_id = user_dataObj.getString("owner_id");
                        String owner_type = user_dataObj.getString("owner_type");
                        String you_group1 = user_dataObj.getString("you_group");
                        String memberId = "";
                        String groupId = "";
                        String uid = "";
                        String memberStatus = "";
                        String mute_group = "";
                        String is_fav = "";
                        String member_create_date = "";
                        if (user_dataObj.get("you_group") instanceof JSONObject) {
                            JSONObject you_group = user_dataObj.getJSONObject("you_group");

                            memberId = you_group.getString("member_id");
                            groupId = you_group.getString("group_id");
                            uid = you_group.getString("uid");
                            memberStatus = you_group.getString("member_status");
                            mute_group = you_group.getString("mute_group");
                            is_fav = you_group.getString("is_fav");
                            member_create_date = you_group.getString("member_create_date");

                            hmap.put("uid", uid);

                            updateHash(hmap, memberId, groupId, uid, memberStatus, mute_group, is_fav, member_create_date, you_group1);
                        }
                        hmap.put("group_id", group_id);
                        hmap.put("group_name", group_name);
                        hmap.put("group_detail", group_detail);
                        hmap.put("group_image", group_image);
                        hmap.put("group_type", group_type);
                        hmap.put("group_tags", group_tags);
                        hmap.put("group_create_date", group_create_date);
                        hmap.put("group_status", group_status);
                        hmap.put("owner_id", owner_id);
                        hmap.put("owner_type", owner_type);
                        hmap.put("you_group", you_group1);
                        dataList.add(hmap);

                    }
                    groupAdapter = new GroupPagingAdapter(MyCrewsActivity.this, dataList);

                    viewpager.setAdapter(groupAdapter);

                    groupAdapter.notifyDataSetChanged();

                    viewpager.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            playPager(dataList.size());

                        }
                    }, 500);

                } else if (errCode.equals("300")) {

                    tv_msg_recomgrp.setVisibility(View.GONE);
                    fl_view_pager.setVisibility(View.GONE);
                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        private void playPager(final int NUM_PAGES) {
            final Handler handler = new Handler();

            final Runnable update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    viewpager.setCurrentItem(currentPage++, true);
                }
            };


            new Timer().schedule(new TimerTask() {

                @Override
                public void run() {
                    handler.post(update);
                }
            }, 100, 3000);
        }


        private void updateHash(HashMap<String, String> hmap, String memberId, String groupId, String uid, String memberStatus, String mute_group, String is_fav, String member_create_date, String you_group1) {
            hmap.put("member_id", memberId);
            hmap.put("group_id", groupId);
            hmap.put("uid", uid);
            hmap.put("member_status", memberStatus);
            hmap.put("mute_group", mute_group);
            hmap.put("is_fav", is_fav);
            hmap.put("member_create_date", member_create_date);
            hmap.put("you_group", you_group1);
            preferences.edit().putString("you_group", you_group1).commit();
        }
    }


    public class LogOutData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(MyCrewsActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "logout");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            LoaderClass.HideProgressWheel(MyCrewsActivity.this);

            try {
                JSONObject resultObj = new JSONObject(result);
                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    {
                        preferences.edit().clear().commit();
                        ((UTCAPP) getApplication()).joingroupList.clear();

                        Intent i = new Intent(activity, SplashActivity.class);
                        startActivity(i);
                        finishAffinity();
                    }
                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MyCrewsActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    preferences.edit().clear().commit();
                                    ((UTCAPP) getApplication()).joingroupList.clear();

                                    Intent i = new Intent(MyCrewsActivity.this, LoginScreenActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LogOutData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LogOutData().execute();
                    }
                }
            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    public class GroupFavData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            // LoaderClass.HideProgressWheel(MyCrewsActivity.this);
            pulllist.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        mygroupfavlist.clear();

                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");
                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);
                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygroupfavlist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(MyCrewsActivity.this, mygroupfavlist);

                        pulllist.setAdapter(gAdapter);

                        gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygroupfavlist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygroupfavlist) {
            this.context = context;
            this.mygroupfavlist = mygroupfavlist;
        }

        @Override
        public int getCount() {
            return mygroupfavlist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    intent.putExtra("member_status", mygroupfavlist.get(i).get("member_status"));

                    startActivityForResult(intent, 1);

                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("uid", mygroupfavlist.get(i).get("uid"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    intent.putExtra("member_status", mygroupfavlist.get(i).get("member_status"));

                    startActivityForResult(intent, 1);
                }
            });
            if (mygroupfavlist.get(i).get("unread").equals("0")) {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setText(mygroupfavlist.get(i).get("unread"));
            }
            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("My Group Screen");

        FlurryAgent.onStartSession(MyCrewsActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(MyCrewsActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    // drawer navigate functionality
    public void addDotsNavigation() {
        dotsNavigate = new ArrayList<>();

        LinearLayout dotsLayout = (LinearLayout) findViewById(R.id.circle_indicator);

        for (int i = 0; i < 2; i++) {
            ImageView dot = new ImageView(this);

            dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            final int pos = i;

            dot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    pager.setCurrentItem(pos);
                }
            });

            dotsLayout.addView(dot, params);

            dotsNavigate.add(dot);
        }
    }

    public void selectDotNavigation(int idx) {

        Resources res = getResources();
        for (int i = 0; i < 2; i++) {
            int drawableId = (i == idx) ? (R.drawable.dot_selected) : (R.drawable.dot_unselected);
            Drawable drawable = res.getDrawable(drawableId);
            dotsNavigate.get(i).setImageDrawable(drawable);
        }
    }
}
