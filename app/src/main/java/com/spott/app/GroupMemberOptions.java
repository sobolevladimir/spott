package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Acer on 9/21/2016.
 */
public class GroupMemberOptions extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {

    ImageView iv_back_icon, iv_unmute, iv_fav, iv_unfav;
    TextView tv_num, tv_no_member, tv_title_group, tv_mute_group, tv_edit_group, tv_delete, tv_invite, tv_request;
    ListView group_list_members;
    LayoutInflater layoutInflater = null;
    ArrayList<HashMap<String, String>> groupList;
    Context context;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout ll_groups, rl_leave, rl_groups_infm;
    LinearLayout rl_mute, rl_unmute, rl_req, rl_fav, rl_unfav, rl_favr, rlll;
    Activity activity;
    String post_id = "", you_like = "", mute_group = "", groupJoined = "";
    SharedPreferences preferences;
    int count = 0;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "";
    ConnectionDetector connDec;
    private BroadcastReceiver broadcastReceiver;
    Alert_Dialog_Manager alert;
    GroupMemberAdapter adapter;
    Button ok_btn, cancel_btn;
    PopupWindow popup_window;
    ImageView iv_menu_icon, iv_red_icon;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_number;
    String group_id = "", uid1 = "", groupname = "", grpdetail = "", grpimg = "", grptype = "",
            grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    ArrayList<HashMap<String, String>> requestlist;
    JSONArray jArray;
    String uid = "", gender = "", is_fav = "", value = "", mute = "", optionNext = "";
    Button btn_join_grp;
    LinearLayout tv_pending;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_member_options);
        this.activity = (Activity) GroupMemberOptions.this;
        group_list_members = (ListView) findViewById(R.id.group_list_members);
        context = GroupMemberOptions.this;
        groupList = new ArrayList<HashMap<String, String>>();
        adapter = new GroupMemberAdapter(context, groupList);
        group_list_members.setAdapter(adapter);
        requestlist = new ArrayList<HashMap<String, String>>();
        initialise();
        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        owner_id = intent.getStringExtra("owner_id");
        owner_type = intent.getStringExtra("owner_type");
        you_grp = intent.getStringExtra("you_group");
        is_fav = intent.getStringExtra("is_fav");
        mute = intent.getStringExtra("mute_group");
        tv_title_group.setText(groupname);
        uid = intent.getStringExtra("imp_id");
        value = intent.getStringExtra("key");
        optionNext = intent.getStringExtra("options_next");
        gender = preferences.getString("gender", "");

        System.out.println("ZUID" + uid);

        if (intent.hasExtra("options_next")) {
            rl_groups_infm.setVisibility(View.GONE);
            btn_join_grp.setVisibility(View.VISIBLE);
            group_list_members.setVisibility(View.GONE);

            if (getIntent().hasExtra("member_status")) {
                String member_status = getIntent().getStringExtra("member_status");

                if (member_status != null) {
                    if (member_status.equalsIgnoreCase("")) {
                        btn_join_grp.setEnabled(true);
                    } else if (!member_status.equalsIgnoreCase("Active")) {
                        btn_join_grp.setText("Pending");

                        btn_join_grp.setEnabled(false);
                    }
                }
            }
        } else {
            /*rl_groups_infm.setVisibility(View.VISIBLE);
            btn_join_grp.setVisibility(View.GONE);

            if (owner_id != null) {
                if (owner_id.equals(uid) || owner_id.equals(value)) {

                    if(grptype!=null) {

                        if (grptype.equalsIgnoreCase("private")) {
                            tv_request.setVisibility(View.VISIBLE);
                            tv_invite.setVisibility(View.VISIBLE);
                            tv_edit_group.setVisibility(View.VISIBLE);
                            tv_delete.setVisibility(View.VISIBLE);
                            rl_fav.setVisibility(View.VISIBLE);
                        } else {
                            tv_request.setVisibility(View.GONE);
                            tv_invite.setVisibility(View.VISIBLE);
                            tv_edit_group.setVisibility(View.VISIBLE);
                            tv_delete.setVisibility(View.VISIBLE);
                            rl_fav.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    tv_request.setVisibility(View.GONE);
                    tv_invite.setVisibility(View.GONE);
                    tv_edit_group.setVisibility(View.GONE);
                    tv_delete.setVisibility(View.GONE);
                    rl_fav.setVisibility(View.VISIBLE);
                    rl_mute.setVisibility(View.VISIBLE);
                    rl_leave.setVisibility(View.VISIBLE);

                }
            }*/

            if (!connDec.isConnectingToInternet()) {
                alert.showNetAlert();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ViewGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new ViewGroupData().execute();
                }
            }


        }

        /*if (intent.hasExtra("mute_group")) {

            if (mute.equals("1")) {

                rl_mute.setVisibility(View.GONE);
                rl_unmute.setVisibility(View.VISIBLE);
            } else {
                rl_unmute.setVisibility(View.GONE);
                rl_mute.setVisibility(View.VISIBLE);
            }
        }*/
        setting();
        clickables();
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new GroupListMember().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new GroupListMember().execute();
            }
        }

        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);


        /*if (is_fav.equals("0")) {

            rl_fav.setVisibility(View.VISIBLE);
            rl_unfav.setVisibility(View.GONE);

        } else {
            rl_unfav.setVisibility(View.VISIBLE);
            rl_fav.setVisibility(View.GONE);
        }*/
        if (!connDec.isConnectingToInternet()) {
            // alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new RequestMemberData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new RequestMemberData().execute();
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            if (resultCode == RESULT_OK) {
                /*grptype = data.getStringExtra("group_type");

                if (grptype.equalsIgnoreCase("private")) {
                    tv_request.setVisibility(View.VISIBLE);
                    tv_invite.setVisibility(View.VISIBLE);
                    tv_edit_group.setVisibility(View.VISIBLE);
                    tv_delete.setVisibility(View.VISIBLE);
                    rl_fav.setVisibility(View.VISIBLE);
                } else {
                    tv_request.setVisibility(View.GONE);
                    tv_invite.setVisibility(View.VISIBLE);
                    tv_edit_group.setVisibility(View.VISIBLE);
                    tv_delete.setVisibility(View.VISIBLE);
                    rl_fav.setVisibility(View.VISIBLE);
                }

                groupname = data.getStringExtra("group_name");

                tv_title_group.setText(groupname);*/

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    new ViewGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                } else {
                    new ViewGroupData().execute();
                }


            }
        }
    }


    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);
    }

    private void initialise() {
        connDec = new ConnectionDetector(GroupMemberOptions.this);
        alert = new Alert_Dialog_Manager(context);
        rl_fav = (LinearLayout) findViewById(R.id.rl_fav);
        rl_unfav = (LinearLayout) findViewById(R.id.rl_unfav);
        rl_favr = (LinearLayout) findViewById(R.id.rl_favr);
        rlll = (LinearLayout) findViewById(R.id.rlll);
        rl_groups_infm = (RelativeLayout) findViewById(R.id.rl_groups_infm);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_unfav = (ImageView) findViewById(R.id.iv_unfav);
        iv_fav = (ImageView) findViewById(R.id.iv_fav);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_unmute = (ImageView) findViewById(R.id.iv_unmute);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_no_member = (TextView) findViewById(R.id.tv_no_member);
        tv_title_group = (TextView) findViewById(R.id.tv_title_group);
        tv_title_group.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        tv_mute_group = (TextView) findViewById(R.id.tv_mute_group);
        tv_pending = (LinearLayout) findViewById(R.id.tv_pending);
        tv_edit_group = (TextView) findViewById(R.id.tv_edit_group);
        tv_delete = (TextView) findViewById(R.id.tv_delete);
        tv_invite = (TextView) findViewById(R.id.tv_invite);
        tv_request = (TextView) findViewById(R.id.tv_request);
        ok_btn = (Button) findViewById(R.id.ok_btn);
        cancel_btn = (Button) findViewById(R.id.cancel_btn);
        btn_join_grp = (Button) findViewById(R.id.btn_join_grp);
        tv_number = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        rl_mute = (LinearLayout) findViewById(R.id.rl_mute);
        rl_unmute = (LinearLayout) findViewById(R.id.rl_unmute);
        rl_leave = (RelativeLayout) findViewById(R.id.rl_leave);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);
    }


    private void clickables() {
        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
          /*      Intent intent = new Intent(GroupMemberOptions.this, CrewActivity.class);
                intent.putExtra("imp_id", uid);
                intent.putExtra("is_fav", is_fav);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("group_image", grpimg);
                intent.putExtra("group_type", grptype);
                intent.putExtra("group_tags", grptag);
                intent.putExtra("group_create_date", groupcreatdate);
                intent.putExtra("group_status", grpstatus);
                intent.putExtra("owner_id", owner_id);
                intent.putExtra("owner_type", owner_type);
                intent.putExtra("you_group", you_grp);
                intent.putExtra("options_next", "");
                startActivity(intent);*/

                if (groupJoined.equals("Public")) {
                    Intent intent = new Intent();
                    intent.putExtra("joined", groupJoined);
                    setResult(Activity.RESULT_OK, intent);
                }

                finish();
            }
        });

        btn_join_grp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rl_favr.setVisibility(View.VISIBLE);
                rlll.setVisibility(View.VISIBLE);
                if (!connDec.isConnectingToInternet()) {
                    //   alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupRequestData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupRequestData().execute();
                    }
                }
            }
        });
        rl_mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rl_mute.getVisibility() == View.VISIBLE) {
                    mute_group = "1";
                    rl_mute.setVisibility(View.GONE);
                    rl_unmute.setVisibility(View.VISIBLE);

                }

// else if (rl_unmute.getVisibility() == View.VISIBLE) {
//                    mute_group = "0";
//
//                    rl_unmute.setVisibility(View.GONE);
//                    rl_mute.setVisibility(View.VISIBLE);
//                }

                if (!connDec.isConnectingToInternet()) {
                    //    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new MuteServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new MuteServiceData().execute();
                    }
                }

            }
        });

        rl_unmute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_unmute.getVisibility() == View.VISIBLE) {
                    mute_group = "0";

                    rl_unmute.setVisibility(View.GONE);
                    rl_mute.setVisibility(View.VISIBLE);

                }
//                else if (rl_mute.getVisibility() == View.VISIBLE) {
//                    mute_group = "1";
//                    rl_mute.setVisibility(View.GONE);
//                    rl_unmute.setVisibility(View.VISIBLE);
//                }
                if (!connDec.isConnectingToInternet()) {
                    //   alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new MuteServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new MuteServiceData().execute();
                    }
                }
            }
        });

        rl_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_fav.getVisibility() == View.VISIBLE) {
                    is_fav = "1";

                    rl_fav.setVisibility(View.GONE);
                    rl_unfav.setVisibility(View.VISIBLE);

                } else if (rl_unfav.getVisibility() == View.VISIBLE) {
                    is_fav = "0";

                    rl_unfav.setVisibility(View.GONE);
                    rl_fav.setVisibility(View.VISIBLE);
                }

                if (!connDec.isConnectingToInternet()) {
                    // alert.showNetAlert();
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new FavouriteServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new FavouriteServiceData().execute();
                    }
                }
            }
        });

        rl_unfav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rl_unfav.getVisibility() == View.VISIBLE) {
                    is_fav = "0";
                    rl_unfav.setVisibility(View.GONE);
                    rl_fav.setVisibility(View.VISIBLE);

                } else if (rl_fav.getVisibility() == View.VISIBLE) {
                    is_fav = "1";

                    rl_fav.setVisibility(View.GONE);
                    rl_unfav.setVisibility(View.VISIBLE);
                }
                if (!connDec.isConnectingToInternet()) {
                    //   alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new FavouriteServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new FavouriteServiceData().execute();
                    }
                }
            }
        });
        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        rl_leave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customLeavePopup();

            }
        });

        tv_edit_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupMemberOptions.this, EditGroupActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("group_image", grpimg);
                intent.putExtra("group_type", grptype);
                intent.putExtra("group_tags", grptag);
                intent.putExtra("group_create_date", groupcreatdate);
                intent.putExtra("group_status", grpstatus);
                intent.putExtra("owner_id", owner_id);
                intent.putExtra("owner_type", owner_type);
                intent.putExtra("you_group", you_grp);
                startActivityForResult(intent, 111);
                //startActivity(intent);
            }
        });

        tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customShareDetailsPopup(activity);
            }
        });

        tv_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupMemberOptions.this, InviteMemberActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("group_image", grpimg);
                intent.putExtra("group_type", grptype);
                intent.putExtra("group_tags", grptag);
                intent.putExtra("group_create_date", groupcreatdate);
                intent.putExtra("group_status", grpstatus);
                intent.putExtra("owner_id", owner_id);
                intent.putExtra("owner_type", owner_type);
                intent.putExtra("you_group", you_grp);
                startActivity(intent);
            }
        });


        tv_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(GroupMemberOptions.this, RequestScreenActivity.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("member_id", preferences.getString("member_id", ""));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (groupJoined.equals("Public")) {
            Intent intent = new Intent();
            intent.putExtra("joined", groupJoined);
            setResult(Activity.RESULT_OK, intent);
        }

        finish();
    }

    private void customLeavePopup() {
        final Dialog dialog = new Dialog(GroupMemberOptions.this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        TextView ok_button, cancel_button, tv_title_leave, tv_title_logout;
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_leave.setVisibility(View.VISIBLE);
        tv_title_logout.setVisibility(View.GONE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LeaveServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LeaveServiceData().execute();
                    }
                }
                dialog.cancel();
            }
        });
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    public static void setListViewHeightBasedOnItems(ListView group_list_members) {
        ListAdapter listAdapter = group_list_members.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, group_list_members);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = group_list_members.getLayoutParams();
        params.height = totalHeight + (group_list_members.getDividerHeight() * (listAdapter.getCount() - 1));
        group_list_members.setLayoutParams(params);
        group_list_members.requestLayout();
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = groupList.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new GroupListMember().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new GroupListMember().execute();
                    }
                }
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:
                Intent intent = new Intent(GroupMemberOptions.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(GroupMemberOptions.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(GroupMemberOptions.this, CalendarActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    private void customShareDetailsPopup(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.popup_design);
        Button ok_btn, cancel_btn;
        ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new DeleteServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new DeleteServiceData().execute();
                    }
                }
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private class GroupMemberAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> groupList;

        public GroupMemberAdapter(Context context, ArrayList<HashMap<String, String>> groupList) {
            this.context = context;
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return groupList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group_member_options_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.iv_group_member_profile = (DiamondImageView) convertView.findViewById(R.id.iv_group_member_profile);
            holder.rl_members = (ViewGroup) convertView.findViewById(R.id.rl_members);
            holder.tv_txt_memberName = (TextView) convertView.findViewById(R.id.tv_txt_memberName);
            holder.tv_txt_memberName.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_likes = (TextView) convertView.findViewById(R.id.tv_likes);
            holder.tv_txt_msg = (TextView) convertView.findViewById(R.id.tv_txt_msg);
            holder.tv_txt_msg.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_txt_year = (TextView) convertView.findViewById(R.id.tv_txt_year);
            String image = groupList.get(i).get("user_image");
            String batch = groupList.get(i).get("batch");
            if (!(groupList.get(i).get("first_name").equals("null") && groupList.get(i).get("last_name").equals("null"))) {

                holder.tv_txt_memberName.setText(CommonUtilities.decodeUTF8(groupList.get(i).get("first_name")) + " " + CommonUtilities.decodeUTF8(groupList.get(i).get("last_name")));
            } else {

                holder.tv_txt_memberName.setText("");
            }
            if (!(groupList.get(i).get("job_company").equals("null"))) {

                // holder.tv_txt_msg.setText(CommonUtilities.decodeUTF8(groupList.get(i).get("job_company")));
            } else {
                //  holder.tv_txt_msg.setText("");
            }
            if (image.isEmpty() || image == null) {

                if (gender.equals("Male") || gender.equals("")) {
                    holder.iv_group_member_profile.setImageResource(R.drawable.avatar);
                    holder.iv_group_member_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_member_profile.setBorderWidth(2);

                } else if (gender.equals("Female")) {
                    holder.iv_group_member_profile.setImageResource(R.drawable.avatar);
                    holder.iv_group_member_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_member_profile.setBorderWidth(2);
                }
            } else {
                Picasso.with(getApplicationContext()).load(image).into(holder.iv_group_member_profile);
                holder.iv_group_member_profile.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_group_member_profile.setBorderWidth(2);
            }

            holder.tv_likes.setText(groupList.get(i).get("total_pelcians"));

            holder.rl_members.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (groupList.get(i).get("uid").equals(preferences.getString("uid", ""))) {
                        Intent intent1 = new Intent(GroupMemberOptions.this, MyProfileActivity.class);
                        intent1.putExtra("uid", preferences.getString("uid", ""));
                        intent1.putExtra("myProfile", "");
                        intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent1);

                    } else {
                        Intent intent = new Intent(GroupMemberOptions.this, MyProfile1.class);
                        intent.putExtra("uid", groupList.get(i).get("uid"));
                        intent.putExtra("owner_id", groupList.get(i).get("owner_id"));
                        intent.putExtra("uer_image", groupList.get(i).get("user_image"));
                        intent.putExtra("is_fav", is_fav);
                        intent.putExtra("group_id", group_id);
                        intent.putExtra("group_name", groupname);
                        intent.putExtra("group_detail", grpdetail);
                        intent.putExtra("group_image", grpimg);
                        intent.putExtra("group_type", grptype);
                        intent.putExtra("group_tags", grptag);
                        intent.putExtra("group_create_date", groupcreatdate);
                        intent.putExtra("group_status", grpstatus);
                        intent.putExtra("owner_id", owner_id);
                        intent.putExtra("owner_type", owner_type);
                        intent.putExtra("you_group", you_grp);
                        startActivity(intent);
                    }
                }
            });

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_group_member_profile;
            TextView tv_txt_memberName, tv_likes, tv_txt_msg, tv_txt_year;
            ViewGroup rl_members;
        }
    }

    public class MuteServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "mute_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                jo.put("mute_group", mute_group);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {
//                    if (mute_group.equals("0")) {
//                        mute_group = "1";
//                        rl_mute.setVisibility(View.VISIBLE);
//                        rl_unmute.setVisibility(View.GONE);
//                    } else if (mute_group.equals("1")) {
//                        mute_group = "0";
//                        rl_mute.setVisibility(View.GONE);
//                        rl_unmute.setVisibility(View.VISIBLE);
//                    }

                } else if (errCode.equals("700")) {

                    //alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class FavouriteServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //LoaderClass.ShowProgressWheel(GroupMemberOptions.this);
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                jo.put("is_fav", is_fav);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {
                    /*if (mute_group.equals("0"))
                    {
                        mute_group = "1";
                        rl_mute.setVisibility(View.VISIBLE);
                        rl_unmute.setVisibility(View.GONE);
                    }
                    else if (mute_group.equals("1"))
                    {
                        mute_group = "0";
                        rl_mute.setVisibility(View.GONE);
                        rl_unmute.setVisibility(View.VISIBLE);
                    }*/

                } else if (errCode.equals("700")) {

                    // alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // LoaderClass.HideProgressWheel(GroupMemberOptions.this);
        }
    }

    public class GroupListMember extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(GroupMemberOptions.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_member");

                jo.put("group_id", group_id);

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");
                String you_like = resultObj.getString("you_like");
                String total_like = resultObj.getString("total_like");
                String total_comment = resultObj.getString("total_comment");


                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    ((TextView) (findViewById(R.id.tv_group_members))).setText(String.format("Members (%d)", jArray.length()));

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();
                        //  JSONObject user_dataObj = jArray.getJSONObject(i);

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String uid1 = "";
                        String member_id = user_dataObj.getString("member_id");
                        String group_id = user_dataObj.getString("group_id");
                        uid1 = user_dataObj.getString("uid");
                        String member_status = user_dataObj.getString("member_status");
                        String mute_group = user_dataObj.getString("mute_group");
                        String is_fav = user_dataObj.getString("is_fav");
                        String member_create_date = user_dataObj.getString("member_create_date");
                        String profile_id = user_dataObj.getString("profile_id");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");

                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String hobbies = user_dataObj.getString("hobbies");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String batch = user_dataObj.getString("batch");
                        String course = user_dataObj.getString("course");
                        String job_company = user_dataObj.getString("job_company");
                        String designation = user_dataObj.getString("designation");
                        String total_pelcians = user_dataObj.getString("total_pelcians");
                        hmap.put("group_id", group_id);
                        hmap.put("member_id", member_id);
                        hmap.put("profile_id", profile_id);
                        hmap.put("member_status", member_status);
                        hmap.put("uid", uid1);
                        hmap.put("mute_group", mute_group);
                        hmap.put("is_fav", is_fav);
                        hmap.put("member_create_date", member_create_date);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("course", course);
                        hmap.put("job_company", job_company);
                        hmap.put("designation", designation);
                        hmap.put("total_pelcians", total_pelcians);
                        hmap.put("batch", batch);
                        hmap.put("year_of_passing", year_of_passing);

                        preferences.edit().putString("gender", gender).commit();

                        preferences.edit().putString("member_id", member_id).commit();

                        groupList.add(hmap);

                    }
                    System.out.println("GroupList" + groupList);

                    adapter = new GroupMemberAdapter(GroupMemberOptions.this, groupList);

                    group_list_members.setAdapter(adapter);


                } else if (errCode.equals("300")) {

                    tv_no_member.setVisibility(View.VISIBLE);
                    group_list_members.setVisibility(View.GONE);

                } else if (errCode.equals("700")) {

                    // alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(GroupMemberOptions.this);
        }
    }

    public class LeaveServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pdDialog = new Dialog(GroupMemberOptions.this);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            pdDialog.show();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "leave_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("result", result);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();

                } else if (errCode.equals("700")) {

                    //alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class DeleteServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "delete_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            // pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(GroupMemberOptions.this, MyCrewsActivity.class);
                    startActivity(intent);

                } else if (errCode.equals("700")) {
                    // alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class RequestMemberData extends AsyncTask<String, String, String> {


        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "request_member");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String uid1 = "";
                        String member_id = user_dataObj.getString("member_id");
                        String group_id = user_dataObj.getString("group_id");
                        uid1 = user_dataObj.getString("uid");
                        String member_status = user_dataObj.getString("member_status");
                        String mute_group = user_dataObj.getString("mute_group");
                        String member_create_date = user_dataObj.getString("member_create_date");
                        String profile_id = user_dataObj.getString("profile_id");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");

                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String hobbies = user_dataObj.getString("hobbies");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        hmap.put("member_id", member_id);
                        hmap.put("uid", uid1);
                        hmap.put("profile_id", profile_id);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("member_create_date", member_create_date);
                        hmap.put("mute_group", mute_group);

                        hmap.put("member_status", member_status);
                        hmap.put("group_id", group_id);

                        hmap.put("member_id", member_id);

                        //  preferences.edit().putString("member_id",member_id);

                        requestlist.add(hmap);

                        System.out.println("Request List: " + requestlist.size());

                        tv_request.setText("REQUESTS (" + requestlist.size() + ")");
                    }


                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public class ViewGroupData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //   LoaderClass.ShowProgressWheel(CrewActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_detail");

                jo.put("group_id", group_id);

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {
                    JSONObject user_dataObj = resultObj.getJSONObject("detail");
                    String uid1 = "";
                    String group_id = user_dataObj.getString("group_id");
                    groupname = user_dataObj.getString("group_name");
                    grpdetail = user_dataObj.getString("group_detail");
                    grpimg = user_dataObj.getString("group_image");
                    String group_type = user_dataObj.getString("group_type");
                    String group_tags = user_dataObj.getString("group_tags");
                    String group_create_date = user_dataObj.getString("group_create_date");
                    String group_status = user_dataObj.getString("group_status");
                    String you_like = user_dataObj.getString("you_like");
                    String your_like = user_dataObj.getString("your_like");
                    String owner_id = user_dataObj.getString("owner_id");
                    String owner_type = user_dataObj.getString("owner_type");
                    String you_group = user_dataObj.getString("you_group");

                    grptype = group_type;

                    if (you_group.equals("")) {
                        // tv_heart.setClickable(false);
                        //iv_menu_icon.setClickable(false);
                        // iv_menu_icon1.setClickable(false);

                    }

                    if (!you_group.equals("")) {
                        JSONObject user_dataObj1 = new JSONObject(you_group);
                        String member_id = user_dataObj1.getString("member_id");
                        String group_id1 = user_dataObj1.getString("group_id");
                        uid = user_dataObj1.getString("uid");
                        String member_status = user_dataObj1.getString("member_status");
                        mute_group = user_dataObj1.getString("mute_group");
                        is_fav = user_dataObj1.getString("is_fav");
                        String member_create_date = user_dataObj1.getString("member_create_date");
                        preferences.edit().putString("member_id", member_id).commit();
                    }

                    rl_groups_infm.setVisibility(View.VISIBLE);
                    btn_join_grp.setVisibility(View.GONE);

                    if (owner_id != null) {
                        if (owner_id.equals(uid) || owner_id.equals(value)) {

                            if (group_type != null) {

                                if (group_type.equalsIgnoreCase("private")) {
                                    tv_request.setVisibility(View.VISIBLE);
                                    tv_invite.setVisibility(View.VISIBLE);
                                    tv_edit_group.setVisibility(View.VISIBLE);
                                    tv_delete.setVisibility(View.VISIBLE);
                                    rl_fav.setVisibility(View.VISIBLE);
                                } else {
                                    tv_request.setVisibility(View.GONE);
                                    tv_invite.setVisibility(View.VISIBLE);
                                    tv_edit_group.setVisibility(View.VISIBLE);
                                    tv_delete.setVisibility(View.VISIBLE);
                                    rl_fav.setVisibility(View.VISIBLE);
                                }
                            }
                        } else {
                            tv_request.setVisibility(View.GONE);
                            tv_invite.setVisibility(View.GONE);
                            tv_edit_group.setVisibility(View.GONE);
                            tv_delete.setVisibility(View.GONE);
                            rl_fav.setVisibility(View.VISIBLE);
                            rl_mute.setVisibility(View.VISIBLE);
                            rl_leave.setVisibility(View.VISIBLE);

                        }
                    }

                    tv_title_group.setText(CommonUtilities.decodeUTF8(groupname));

                    if (mute_group.equals("1")) {

                        rl_mute.setVisibility(View.GONE);
                        rl_unmute.setVisibility(View.VISIBLE);
                    } else {
                        rl_unmute.setVisibility(View.GONE);
                        rl_mute.setVisibility(View.VISIBLE);
                    }

                    if (is_fav.equals("0")) {

                        rl_fav.setVisibility(View.VISIBLE);
                        rl_unfav.setVisibility(View.GONE);

                    } else {
                        rl_unfav.setVisibility(View.VISIBLE);
                        rl_fav.setVisibility(View.GONE);
                    }

                    /*if (!member_status.equalsIgnoreCase("Active"))
                        tv_heart.setEnabled(false);
                    try {
                        jArrayy = new JSONArray();

                        jArrayy = user_dataObj.getJSONArray("moods");

                        updateSmiley(your_like, false);
                        //setAdapter();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/


                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            //  LoaderClass.HideProgressWheel(CrewActivity.this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Group Member Screen");

        FlurryAgent.onStartSession(GroupMemberOptions.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(GroupMemberOptions.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }


    public class GroupRequestData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdDialog = new Dialog(activity);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            pdDialog.show();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_request");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                jo.put("group_type", grptype);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Group request data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    if (grptype.equals("Private")) {

                        btn_join_grp.setText("Pending");

                        btn_join_grp.setEnabled(false);
                    } else {

                        groupJoined = "Public";

                        rl_fav.setVisibility(View.VISIBLE);

                        rl_mute.setVisibility(View.VISIBLE);

                        rl_leave.setVisibility(View.VISIBLE);

                        rl_groups_infm.setVisibility(View.VISIBLE);

                        btn_join_grp.setVisibility(View.GONE);
                    }

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
