package com.spott.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.LoaderClass;

/**
 * Created by Adimn on 11/5/2016.
 */
public class RegisterConfirmationScreen extends Activity {

    TextView btn_login;
    EditText et_password;
    String passwordstr = "", emailstr = "", encoded = "", device_token = "";
    ConnectionDetector connDec;
    Activity activity;
    Alert_Dialog_Manager alert;
    RelativeLayout rl_back;
    SharedPreferences preferences;
    ImageView iv_red_icon;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_confirm);
        context = RegisterConfirmationScreen.this;
        initialise();
        clickables();

        Intent intent = getIntent();
        emailstr = intent.getStringExtra("email");
        encoded = intent.getStringExtra("user_image");
        device_token = intent.getStringExtra("device_token");

        System.out.println("COMING INTENT " + emailstr);
    }

    private void initialise() {

        et_password = (EditText) findViewById(R.id.et_password);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        et_password.setTextColor(Color.parseColor("#ffffff"));
        btn_login = (TextView) findViewById(R.id.btn_login);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        connDec = new ConnectionDetector(RegisterConfirmationScreen.this);
        alert = new Alert_Dialog_Manager(context);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
    }

    private void clickables() {

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passwordstr = et_password.getText().toString().trim();
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else if (!(passwordstr.length() > 0)) {

                    Toast.makeText(getApplicationContext(), R.string.password, Toast.LENGTH_SHORT).show();
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new LoginData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new LoginData().execute();
                    }
                }
            }

        });

        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
            }
        });

        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    et_password.setHint("");
                else
                    et_password.setHint("PASSWORD");
            }
        });
    }

    public class LoginData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(RegisterConfirmationScreen.this);
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "user_login");

                System.out.println("EMAIL GOING " + getSharedPreferences("UTC", MODE_PRIVATE).getString("email", ""));

                jo.put("email", getSharedPreferences("UTC", MODE_PRIVATE).getString("email", ""));

                jo.put("password", passwordstr);

                jo.put("device_type", "android");

                jo.put("latitude", "");

                jo.put("longitude", "");

                jo.put("device_token", device_token);           // for push notification

                //  jo.put("is_app_login", "no");       // for social media

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONObject user_dataObj = resultObj.getJSONObject("user");

                    String uid = user_dataObj.getString("uid");

                    String latitude = user_dataObj.getString("latitude");

                    String longitude = user_dataObj.getString("longitude");

                    device_token = user_dataObj.getString("device_token");

                    String login_token = user_dataObj.getString("login_token");

                    String username = user_dataObj.getString("username");

                    String user_create_date = user_dataObj.getString("user_create_date");
                    String badge = user_dataObj.getString("badge");
                    String facebook_id = user_dataObj.getString("facebook_id");
                    String survey_alert = user_dataObj.getString("survey_alert");
                    String password_token = user_dataObj.getString("password_token");
                    String register_check = user_dataObj.getString("register_check");
                    String otp = user_dataObj.getString("otp");
                    String verified = user_dataObj.getString("verified");
                    String password_change = user_dataObj.getString("password_change");
                    String phone_no = user_dataObj.getString("phone_no");

                    preferences.edit().putString("device_token", device_token).commit();

                    preferences.edit().putString("uid", uid).commit();

                    preferences.edit().putString("login_token", login_token).commit();

                    JSONObject user_dataObject = user_dataObj.getJSONObject("profile");

                    String profile_id = user_dataObject.getString("profile_id");
                    String first_name = user_dataObject.getString("first_name");
                    String last_name = user_dataObject.getString("last_name");
                    String userImage = user_dataObject.getString("user_image");
                    String user_thumbnail = user_dataObject.getString("user_thumbnail");
                    String hobbies = user_dataObject.getString("hobbies");
                    String phone = user_dataObject.getString("phone");
                    String dob = user_dataObject.getString("dob");
                    String quotes = user_dataObject.getString("quotes");
                    String profile_create_date = user_dataObject.getString("profile_create_date");
                    String profile_modify_date = user_dataObject.getString("profile_modify_date");
                    String gender = user_dataObject.getString("gender");

                    preferences.edit().putString("first_name", first_name).commit();

                    preferences.edit().putString("last_name",  last_name).commit();

                    preferences.edit().putString("user_image", userImage).commit();
                    preferences.edit().putString("username", username).commit();
                    preferences.edit().putString("quotes", quotes).commit();

                    preferences.edit().putString("gender", gender).commit();

                    System.out.println("QUOTES=" + preferences.getString("quotes", ""));

                    if (password_change.equals("1")) {

                        Intent intent1 = new Intent(RegisterConfirmationScreen.this, ResetPssword.class);

                        intent1.putExtra("first_name", first_name);

                        intent1.putExtra("last_name", last_name);

                        intent1.putExtra("user_image", userImage);

                        intent1.putExtra("username", username);

                        intent1.putExtra("gender", gender);

                        intent1.putExtra("values", "");
                        preferences.edit().putString("password", passwordstr).commit();
                        preferences.edit().putString("password_change", "yes").commit();
                        startActivity(intent1);
                    } else {

                        Intent intent = new Intent(RegisterConfirmationScreen.this, MyFeedActivity.class);

                        intent.putExtra("first_name", first_name);

                        intent.putExtra("last_name", last_name);

                        intent.putExtra("user_image", userImage);

                        intent.putExtra("username", username);

                        intent.putExtra("gender", gender);

                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);

                    }

                    finish();

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterConfirmationScreen.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    //  finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(RegisterConfirmationScreen.this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Register Confirmation Screen");

        FlurryAgent.onStartSession(RegisterConfirmationScreen.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(RegisterConfirmationScreen.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}
