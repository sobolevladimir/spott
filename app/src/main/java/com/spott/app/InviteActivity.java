package com.spott.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;

/**
 * Created by Admin on 12/2/2016.
 */

public class InviteActivity extends Activity implements View.OnClickListener {

    ImageView iv_red_icon, iv_back_icon, iv_red_cube;
    Activity activity;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout  rlMainFront, ll_groups;
    ListView listview;
    Context context;
    RelativeLayout rl_request;
    ArrayList<HashMap<String, String>> inviteGroupList;
    SharedPreferences preferences;
    private BroadcastReceiver broadcastReceiver;
    RequestAdapter adapter;
    TextView tv_num,tv_no_request;
    ProgressWheel progressWheel = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.request_activity);
        this.activity = (Activity) InviteActivity.this;
        listview = (ListView) findViewById(R.id.listview);
        context = getApplicationContext();
        inviteGroupList = new ArrayList<HashMap<String, String>>();
        adapter = new RequestAdapter(context, inviteGroupList);
        listview.setAdapter(adapter);
        initialise();
        clickables();
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            new RequestMemberData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        else
            new RequestMemberData().execute();

        /*if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            new MessageData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        else
            new MessageData().execute();*/
    }

    private void initialise()
    {
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_red_cube = (ImageView) findViewById(R.id.iv_red_cube);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_no_request = (TextView) findViewById(R.id.tv_no_request);
        progressWheel = (ProgressWheel)findViewById(R.id.progressBar);

        progressWheel.setBarColor(context.getResources().getColor(R.color.red));
        progressWheel.setRimWidth(progressWheel.getBarWidth()*3);
        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    }
                    else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }

    private void clickables() {
        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(InviteActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ll_feed:

                ll_feed.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent = new Intent(InviteActivity.this, MyFeedActivity.class);
                startActivity(intent);

                break;

            case R.id.ll_profile:

                ll_profile.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent1 = new Intent(InviteActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                startActivity(intent1);

                break;

            case R.id.ll_groups:

                ll_groups.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent2 = new Intent(InviteActivity.this, JoinGroupActivity.class);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:

                ll_directory.setBackgroundColor(Color.parseColor("#fff3e5"));
                ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_groups.setBackgroundColor(Color.parseColor("#ffffff"));
                ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
                Intent intent3 = new Intent(InviteActivity.this, MyDirectoryActivity.class);
                startActivity(intent3);

                break;
        }
    }

    private class RequestAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> groupList;

        public RequestAdapter(Context context, ArrayList<HashMap<String, String>> groupList) {
            this.context = context;
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return inviteGroupList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.request_adapter, null);
            ViewHolder holder = new  ViewHolder();
            holder.iv_group_profile = (DiamondImageView) convertView.findViewById(R.id.iv_group_profile);
            holder.tv_name_request = (TextView) convertView.findViewById(R.id.tv_name_request);
            holder.tv_name_request.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_accept = (TextView) convertView.findViewById(R.id.tv_accept);
            holder.tv_decline = (TextView) convertView.findViewById(R.id.tv_decline);
            holder.tv_pending_txt = (TextView) convertView.findViewById(R.id.tv_pending_txt);
            rl_request=(RelativeLayout)convertView.findViewById(R.id.rl_request);

            if( inviteGroupList == null|| inviteGroupList.size()==0){
                rl_request.setVisibility(View.GONE);
                holder.tv_pending_txt.setVisibility(View.VISIBLE);
            }
            else
            {
                rl_request.setVisibility(View.VISIBLE);
                holder.tv_pending_txt.setVisibility(View.GONE);

                if (inviteGroupList.get(i).get("group_image").toString().equals(""))
                {
                    holder.iv_group_profile.setImageResource(R.drawable.avatar);
                    holder.iv_group_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_profile.setBorderWidth(2);
                }
                else
                {
                    Picasso.with(getApplicationContext()).load(inviteGroupList.get(i).get("group_image")).into(holder.iv_group_profile);
                    holder.iv_group_profile.setBorderColor(Color.parseColor("#F15A51"));
                    holder.iv_group_profile.setBorderWidth(2);
                }

            }
            holder.tv_name_request.setText(CommonUtilities.decodeUTF8(inviteGroupList.get(i).get("group_name")));

            holder.tv_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String gpId = inviteGroupList.get(i).get("group_id").toString();

                    String memberId = inviteGroupList.get(i).get("member_id").toString();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        new AcceptInvitation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,memberId,gpId,i+"");

                    else
                        new AcceptInvitation().execute(memberId,gpId,i+"");

                }
            });
            holder.tv_decline.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String gpId = inviteGroupList.get(i).get("group_id").toString();

                    String memberId = inviteGroupList.get(i).get("member_id").toString();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                        new DeclineInvitation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,memberId,gpId,i+"");
                    else
                        new DeclineInvitation().execute(memberId,gpId,i+"");

                }
            });

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_group_profile;
            TextView tv_accept, tv_decline, tv_name_request,tv_pending_txt;

        }
    }

    public class RequestMemberData extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            progressWheel.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "invite_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("Invite Data  = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Invite Response : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            progressWheel.setVisibility(View.GONE);
            try
            {

                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                inviteGroupList = new ArrayList<>();

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {
                        for (int i = 0; i < help_array.length(); i++) {

                            //  my grouplist.clear();

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String unread = helpObject.getString("unread");
                            String owner_type = helpObject.getString("owner_type");
                            JSONObject you_group = helpObject.getJSONObject("you_group");

                            hmap.put("member_id", you_group.getString("member_id"));
                            hmap.put("group_id", you_group.getString("group_id"));
                            hmap.put("uid", you_group.getString("uid"));
                            hmap.put("member_status", you_group.getString("member_status"));
                            hmap.put("mute_group", you_group.getString("mute_group"));
                            hmap.put("is_fav", you_group.getString("is_fav"));
                            hmap.put("member_create_date", you_group.getString("member_create_date"));

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group.toString());
                            hmap.put("unread", unread);

                            inviteGroupList.add(hmap);
                        }
                        //  adapter = new MyCrewsActivity.GroupAdapter(MyCrewsActivity.this, mygrouplist);

                        listview.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("300")) {

                    listview.setVisibility(View.GONE);
                    tv_no_request.setVisibility(View.VISIBLE);

                } else if (errCode.equals("700")) {
                }

            }
            catch (Exception e)
            {
                progressWheel.setVisibility(View.GONE);
                e.printStackTrace();
            }

            progressWheel.setVisibility(View.GONE);        }
    }

    public class AcceptInvitation extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        int position = 0;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progressWheel.setVisibility(View.VISIBLE);
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                position = Integer.parseInt(strings[2]);

                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "request_status");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("member_id",strings[0]);

                jo.put("member_status", "Accept");

                jo.put("group_id",strings[1]);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            progressWheel.setVisibility(View.GONE);
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    inviteGroupList.remove(position);

                    adapter.notifyDataSetChanged();

                    if(inviteGroupList.size()==0)
                        tv_no_request.setVisibility(View.VISIBLE);
                    else
                        tv_no_request.setVisibility(View.GONE);
                }
                else if (errCode.equals("700"))
                {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            }
            catch (Exception e)
            {
                progressWheel.setVisibility(View.GONE);
                e.printStackTrace();
            }

        }
    }

    public class DeclineInvitation extends AsyncTask<String, String, String> {

        String getData = "";

        String message = "";

        int position = 0;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            progressWheel.setVisibility(View.GONE);
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                position = Integer.parseInt(strings[2]);

                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "request_status");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("member_id", strings[0]);

                jo.put("member_status", "Decline");

                jo.put("group_id", strings[1]);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);

            progressWheel.setVisibility(View.GONE);

            try
            {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0"))
                {
                    inviteGroupList.remove(position);

                    adapter.notifyDataSetChanged();

                    if(inviteGroupList.size()==0)
                        tv_no_request.setVisibility(View.VISIBLE);
                    else
                        tv_no_request.setVisibility(View.GONE);
                }
                else if (errCode.equals("700"))
                {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                progressWheel.setVisibility(View.GONE);
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Invite Group List Screen");

        FlurryAgent.onStartSession(InviteActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(InviteActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}

