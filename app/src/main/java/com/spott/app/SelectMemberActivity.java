package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Adimn on 9/30/2016.
 */
public class SelectMemberActivity extends Activity implements AbsListView.OnScrollListener {

    PullToRefreshListView list;
    ListInflater listInflater;
    TextView done;
    Activity activity;
    ArrayList<HashMap<String, String>> directoryList;
    ArrayList<HashMap<String, String>> selctedArraylist = new ArrayList<>();
    int count = 0;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "", encoded = "";
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    Context context;
    SharedPreferences preferences;
    String grpname = "", grpdetal = "", grpimage = "", profile_id = "", grpTag = "";
    String Radiobtn = "";
    ImageView iv_red_icon;
    ArrayList<String> uidArray = new ArrayList<>();
    private boolean isLoading =false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_member);
        this.activity = (Activity) SelectMemberActivity.this;
        context=SelectMemberActivity.this;
        connDec = new ConnectionDetector(SelectMemberActivity.this);
        alert = new Alert_Dialog_Manager(context);
        list = (PullToRefreshListView) findViewById(R.id.list);
        done = (TextView) findViewById(R.id.done_btn);
        directoryList = new ArrayList<HashMap<String, String>>();
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);

        setAdapter();
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new DirectoryListData().execute();
            }
        }
        Intent intent = getIntent();
        grpname = intent.getStringExtra("group_name");
        grpdetal = intent.getStringExtra("group_detail");
        encoded = intent.getStringExtra("group_image");
        Radiobtn = intent.getStringExtra("group_type");
        grpTag = intent.getStringExtra("group_tags");

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //selctedArraylist.clear();

                for (int i = 0; i < directoryList.size(); i++) {

                    if (directoryList.get(i).get("selected").equals("1")) {
                        selctedArraylist.add(directoryList.get(i));

                        uidArray.add(directoryList.get(i).get("uid"));

                    }
                }

                System.out.println("SELECTED LIST " + selctedArraylist);
                Intent intent = new Intent();

                intent.setAction("add_cross");

                intent.putExtra("selctedArraylist", selctedArraylist);

                intent.putExtra("uidArray", uidArray);

                sendBroadcast(intent);

                finish();

            }
        });

        list.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {

                if(!isLoading) {
                    if (has_more.equalsIgnoreCase("true"))
                    {
                        isLoading=true;
                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                            } else {
                                new DirectoryListData().execute("loadmore");
                            }
                        }
                    } else {
                        Toast.makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                    }
                }

            }


        });
        list.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                count = 0;
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new DirectoryListData().execute();
                    }

                    directoryList.clear();
                }
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = directoryList.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new DirectoryListData().execute();
                    }
                }
            } else {
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }


    private class ListInflater extends BaseAdapter {

        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, String>> directoryList;

        public ListInflater(Context context, ArrayList<HashMap<String, String>> directoryList) {
            this.context = context;
            this.directoryList = directoryList;
        }

        @Override
        public int getCount() {
            return directoryList.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_selected_member, null);
            ViewHolder holder = new ViewHolder();
            holder.iv_profile_alumni = (DiamondImageView) convertView.findViewById(R.id.iv_profile_alumni);

            holder.tv_name_alumni = (TextView) convertView.findViewById(R.id.tv_name_alumni);
            holder.tv_name_alumni.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_date_alumni = (TextView) convertView.findViewById(R.id.tv_date_alumni);
            holder.tv_date_alumni.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tick_icon = (ImageView) convertView.findViewById(R.id.tick_icon);
            String image = (String) directoryList.get(i).get("user_image");
            String genderr = (String) directoryList.get(i).get("gender");
            holder.tick_icon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (directoryList.get(i).get("selected").equals("0")) {
                        directoryList.get(i).put("selected", "1");
                    } else {
                        directoryList.get(i).put("selected", "0");
                    }
                    notifyDataSetChanged();
                }
            });

            holder.tv_name_alumni.setText(CommonUtilities.decodeUTF8(directoryList.get(i).get("first_name")) + " "
                    + CommonUtilities.decodeUTF8(directoryList.get(i).get("last_name")));
            holder.tv_date_alumni.setText(CommonUtilities.decodeUTF8(directoryList.get(i).get("job_company")));

            if (directoryList.get(i).get("user_image").toString().equals("") || directoryList.get(i).get("user_image").toString() == null) {
                holder.iv_profile_alumni.setImageResource(R.drawable.app);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(2);
            }
            if (directoryList.get(i).get("gender").toString().equals("") || directoryList.get(i).get("gender").toString() == null) {
                holder.iv_profile_alumni.setImageResource(R.drawable.app);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(2);
            }
            if (!directoryList.get(i).get("user_image").toString().equals("")) {

                Picasso.with(context).load(directoryList.get(i).get("user_image").toString()).into(holder.iv_profile_alumni);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(2);
            }

            if (directoryList.get(i).get("selected").equals("0")) {
                holder.tick_icon.setImageResource(R.drawable.check_box_unsel);
            } else {
                holder.tick_icon.setImageResource(R.drawable.check_box_sel);
            }

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_profile_alumni;
            ImageView tick_icon;
            TextView tv_name_alumni, tv_date_alumni;
        }

    }

    public class DirectoryListData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        boolean isLoadMore = false;
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(SelectMemberActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                if(strings.length>0)
                {
                    if(strings[0].equals("loadmore"))
                    {
                        isLoadMore = true;
                    }
                }
                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "alumni_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                if(!isLoadMore) {
                    jo.put("post_value", "0");
                }
                else
                {
                    jo.put("post_value", directoryList.size()+"");
                }

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            list.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(getData);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    if(!isLoadMore) {
                        directoryList.clear();
                    }

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);
                        String uid = user_dataObj.getString("uid");
                        String profile_id = user_dataObj.getString("profile_id");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String hobbies = user_dataObj.getString("hobbies");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String username = user_dataObj.getString("username");
                        String email = user_dataObj.getString("email");
                        String password = user_dataObj.getString("password");
                        String user_status = user_dataObj.getString("user_status");
                        String login_token = preferences.getString("login_token", "");
                        String device_type = preferences.getString("device_type", "");
                        String device_token = preferences.getString("device_token", "");
                        String latitude = preferences.getString("latitude", "");
                        String longitude = preferences.getString("longitude", "");
                        String user_create_date = user_dataObj.getString("user_create_date");
                        String badge = user_dataObj.getString("badge");
                        String facebook_id = user_dataObj.getString("facebook_id");
                        String survey_alert = user_dataObj.getString("survey_alert");
                        String password_token = user_dataObj.getString("password_token");
                        String register_check = user_dataObj.getString("register_check");
                        String batch = user_dataObj.getString("batch");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String job_company = user_dataObj.getString("job_company");
                        String designation = user_dataObj.getString("designation");
                        String id=""+(directoryList.size()+1);

                        hmap.put("profile_id", profile_id);
                        hmap.put("uid", uid);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("username", username);
                        hmap.put("email", email);
                        hmap.put("password", password);
                        hmap.put("user_status", user_status);
                        hmap.put("login_token", login_token);
                        hmap.put("device_type", device_type);
                        hmap.put("device_token", device_token);
                        hmap.put("latitude", latitude);
                        hmap.put("longitude", longitude);
                        hmap.put("user_create_date", user_create_date);
                        hmap.put("badge", badge);
                        hmap.put("facebook_id", facebook_id);
                        hmap.put("survey_alert", survey_alert);
                        hmap.put("password_token", password_token);
                        hmap.put("register_check", register_check);
                        hmap.put("batch", batch);
                        hmap.put("year_of_passing", year_of_passing);
                        hmap.put("job_company", job_company);
                        hmap.put("designation", designation);
                        hmap.put("selected", "0");
                        hmap.put("id",id);


                        preferences.edit().putString("profile_id", profile_id).commit();


                        directoryList.add(hmap);

                    }


                    if (getIntent().hasExtra("list")) {
                        ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("list");

                        for (int z = 0; z < list.size(); z++) {
                            String getPid = list.get(z).get("profile_id");

                            for (int a = 0; a < directoryList.size(); a++) {
                                String getDirectoryPid = directoryList.get(a).get("profile_id");

                                if (getDirectoryPid.equalsIgnoreCase(getPid)) {
                                    directoryList.get(a).put("selected", "1");//, list.get(z));
                                }
                            }
                        }
                    }


                    setAdapter();

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(SelectMemberActivity.this);
            isLoading = false;
        }
    }

    private void setAdapter()
    {
        if(listInflater==null)
        {
            listInflater = new ListInflater(SelectMemberActivity.this, directoryList);

            list.setAdapter(listInflater);
        }
        else
        {
            listInflater.notifyDataSetChanged();
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Select Member Screen");

        FlurryAgent.onStartSession(SelectMemberActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(SelectMemberActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}
