package com.spott.app.services.api;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPasswordRequest extends BaseSpottRequest {

    private final static String METHOD = "change_password";

    @SerializedName("password")
    @Expose
    String password;

    @SerializedName("old_password")
    @Expose
    String oldPassword;

    public ResetPasswordRequest(Context context, String password, String oldPassword) {
        super(context, METHOD);

        this.password = password;
        this.oldPassword = oldPassword;
    }
}
