package com.spott.app.services.api;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class FacebookLoginRequest extends BaseSpottRequest {

    private final static String METHOD_NAME = "facebook_login";
    private final static String TAG = "FACEBOOK";
    private final static String FACEBOOK_ID = "facebookId";
    private final static String FIRST_NAME = "first_name";
    private final static String LAST_NAME = "last_name";
    private final static String EMAIL = "email";
    private final static String GENDER = "gender";
    private final static String BIRTHDAY = "birthday";
    private final static String LOCATION = "location";
    private final static String LONGITUDE = "longitude";
    private final static String LATITUDE = "latitude";

    @SerializedName("first_name")
    @Expose
    public String firstName;

    @SerializedName("last_name")
    @Expose
    public String lastName;

    @SerializedName("username")
    @Expose
    public String username = "";

    @SerializedName("facebook_id")
    @Expose
    public String facebookId;

    @SerializedName("device_type")
    @Expose
    public String deviceType = "android";

    @SerializedName("device_token")
    @Expose
    public String deviceToken;

    @SerializedName("profile_url")
    @Expose
    public String profileUrl = "";

    @SerializedName("latitude")
    @Expose
    public String latitude = "";

    @SerializedName("longitude")
    @Expose
    public String longitude = "";

    @SerializedName("dob")
    @Expose
    public  String birthday;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("register_check")
    @Expose
    public String register_check = "0";

    @SerializedName("bio")
    @Expose
    public String bio = "";

    public FacebookLoginRequest(Context context, JSONObject facebookResponse, String token) {
        super(context, METHOD_NAME);
        try {
            if (facebookResponse.has(FACEBOOK_ID)) {
                this.facebookId = facebookResponse.getString(FACEBOOK_ID);
            }
            if (facebookResponse.has(FIRST_NAME)) {
                this.firstName = facebookResponse.getString(FIRST_NAME);
            }
            if (facebookResponse.has(LAST_NAME)) {
                this.lastName = facebookResponse.getString(LAST_NAME);
            }
            if (facebookResponse.has(EMAIL)) {
                this.email = facebookResponse.getString(EMAIL);
            }
            if (facebookResponse.has(GENDER)) {
                this.gender = facebookResponse.getString(GENDER);
            }
            if (facebookResponse.has(BIRTHDAY)) {
                this.birthday = facebookResponse.getString(BIRTHDAY);
            }
            if (facebookResponse.has(LOCATION)) {
                this.longitude = facebookResponse.getJSONObject(LOCATION).getString(LONGITUDE);
                this.latitude = facebookResponse.getJSONObject(LOCATION).getString(LATITUDE);
            }

            this.deviceToken = token;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
