package com.spott.app.services.api;

import com.spott.app.common.CommonUtilities;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by oleg on 08.10.2017.
 */

public class RetrofitBuilder {

    public static SpottApi getSpottApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CommonUtilities.LOGIN_URL1)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(SpottApi.class);
    }
}
