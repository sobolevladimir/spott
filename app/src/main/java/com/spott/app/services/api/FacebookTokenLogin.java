package com.spott.app.services.api;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacebookTokenLogin extends BaseSpottRequest {
    private static final String METHOD_NAME = "facebook_login_by_fbtoken";

    @SerializedName("fbtoken")
    @Expose
    String _token;

    @SerializedName("device_type")
    @Expose
    String _deviceType = "android";

    @SerializedName("device_token")
    @Expose
    String _deviceToken;

    @SerializedName("latitude")
    @Expose
    String latitude = "";

    @SerializedName("longitude")
    @Expose
    String longitude = "";

    @SerializedName("register_check")
    @Expose
    String registerCheck;

    @SerializedName("bio")
    @Expose
    String bio;


    public FacebookTokenLogin(Context context, String token, String deviceToken) {
        super(context, METHOD_NAME);

        _token = token;
        _deviceToken = deviceToken;
    }
}
