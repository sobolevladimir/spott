package com.spott.app.services.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("err-code")
    @Expose
    String errorCode;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("user")
    @Expose
    FacebookLoginResponse.User user;

    public boolean isSucceed(){
        return "0".equals(errorCode);
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }

    public FacebookLoginResponse.User getUser() {
        return user;
    }
}
