package com.spott.app.services.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResetPasswordResponse {

    @SerializedName("err-code")
    @Expose
    String errCode;

    @SerializedName("message")
    @Expose
    String message;

    public boolean isSuccessfulCode(){
        return errCode.equals("0");
    }

    public boolean isMessageCode(){
        return errCode.equals("300");
    }

    public boolean isTokenErrorCode(){
        return errCode.equals("700");
    }

    public String getErrCode() {
        return errCode;
    }

    public String getMessage() {
        return message;
    }
}
