package com.spott.app.services.api;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterRequest extends BaseSpottRequest {

    private static final String METHOD_NAME = "register_and_verify_app_user";

    @SerializedName("first_name")
    @Expose
    String firstName;
    @SerializedName("last_name")
    @Expose
    String lastName;
    @SerializedName("username")
    @Expose
    String username;
    @SerializedName("email")
    @Expose
    String email;
    @SerializedName("password")
    @Expose
    String password;
    @SerializedName("phone_no")
    @Expose
    String phoneNo;
    @SerializedName("device_type")
    @Expose
    String deviceType = "android";;
    @SerializedName("device_token")
    @Expose
    String deviceToken;
    @SerializedName("profile_url")
    @Expose
    String profileUrl;
    @SerializedName("latitude")
    @Expose
    String latitude;
    @SerializedName("longitude")
    @Expose
    String longitude;
    @SerializedName("gender")
    @Expose
    String gender;
    @SerializedName("dob")
    @Expose
    String dob;
    @SerializedName("image_data")
    @Expose
    String imageData;
    @SerializedName("bio")
    @Expose
    String bio = "";

    public RegisterRequest(Context context) {
        super(context, METHOD_NAME);
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

}
