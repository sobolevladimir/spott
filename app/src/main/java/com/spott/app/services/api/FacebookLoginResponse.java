package com.spott.app.services.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacebookLoginResponse {
    @SerializedName("err-code")
    @Expose
    public String errCode;
    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("user")
    @Expose
    public User user;

    public boolean isSuccessfulStatusCode() {
        return this.errCode.equals("0");
    }

    public boolean isChangePasswordNeeded() {
        return this.user.passwordChange.equals("1");
    }

    public class User {
        @SerializedName("uid")
        @Expose
        public String uid;
        @SerializedName("latitude")
        @Expose
        public String latitude;
        @SerializedName("longitude")
        @Expose
        public String longitude;

        @SerializedName("device_token")
        @Expose
        public String deviceToken;

        @SerializedName("login_token")
        @Expose
        public String loginToken;

        @SerializedName("username")
        @Expose
        public String username;

        @SerializedName("phone_no")
        @Expose
        public String phoneNumber;

        @SerializedName("email")
        @Expose
        public String email;

        @SerializedName("user_create_date")
        @Expose
        public String userCreateDate;


        @SerializedName("badge")
        @Expose
        public String badge;

        @SerializedName("facebook_id")
        @Expose
        public String facebookId;

        @SerializedName("password_change")
        @Expose
        public String passwordChange;

        @SerializedName("password")
        @Expose
        public String password;

        @SerializedName("profile")
        @Expose
        public Profile profile;

        @SerializedName("real_password")
        @Expose
        public String realPassword;
    }

    public class Profile {
        @SerializedName("first_name")
        @Expose
        public String firstName;

        @SerializedName("last_name")
        @Expose
        public String lastName;

        @SerializedName("gender")
        @Expose
        public String gender;

        @SerializedName("user_image")
        @Expose
        public String userImage;

        @SerializedName("quotes")
        @Expose
        public String quotes;
    }
}
