package com.spott.app.services.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.annotations.SerializedName;

/**
 * Created by oleg on 08.10.2017.
 */

public class BaseSpottRequest {

    @SerializedName("method")
    String MethodName;

    @SerializedName("login_token")
    String Token;

    public BaseSpottRequest(Context context, String methodName) {
        MethodName = methodName;

        SharedPreferences preferences = context.getSharedPreferences("UTC", Context.MODE_PRIVATE);
        Token = preferences.getString("login_token", "");
    }
}
