package com.spott.app.services.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PATCH;
import retrofit2.http.POST;

/**
 * Created by oleg on 08.10.2017.
 */

public interface SpottApi {

    String PATH = "./";

    @POST(PATH)
    Call<GroupDetailResponse> getGroupDetail(@Body GroupDetailRequest request);

    @POST(PATH)
    Call<GroupMembersResponse> getGroupMembers(@Body GroupMembersRequest request);

    @POST(PATH)
    Call<FacebookLoginResponse> loginWithFacebook(@Body FacebookLoginRequest request);

    @POST(PATH)
    Call<FacebookLoginResponse> loginWithFacebook(@Body FacebookTokenLogin request);

    @POST(PATH)
    Call<ResetPasswordResponse> resetPassword(@Body ResetPasswordRequest request);

    @POST(PATH)
    Call<RegisterResponse> register(@Body RegisterRequest request);
}
