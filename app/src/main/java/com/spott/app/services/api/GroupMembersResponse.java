package com.spott.app.services.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oleg on 08.10.2017.
 */

public class GroupMembersResponse {

    @SerializedName("err-code")
    @Expose
    public String errCode;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("you_like")
    @Expose
    public String youLike;
    @SerializedName("total_like")
    @Expose
    public String totalLike;
    @SerializedName("total_comment")
    @Expose
    public String totalComment;
    @SerializedName("list")
    @Expose
    public List<Member> list = new ArrayList<>();

    public class Member {
        @SerializedName("member_id")
        @Expose
        public String memberId;
        @SerializedName("group_id")
        @Expose
        public String groupId;
        @SerializedName("uid")
        @Expose
        public String uid;
        @SerializedName("member_status")
        @Expose
        public String memberStatus;
        @SerializedName("mute_group")
        @Expose
        public String muteGroup;
        @SerializedName("is_fav")
        @Expose
        public String isFav;
        @SerializedName("member_create_date")
        @Expose
        public String memberCreateDate;
        @SerializedName("profile_id")
        @Expose
        public String profileId;
        @SerializedName("phone")
        @Expose
        public Object phone;
        @SerializedName("first_name")
        @Expose
        public String firstName;
        @SerializedName("original_first_name")
        @Expose
        public Object originalFirstName;
        @SerializedName("last_name")
        @Expose
        public String lastName;
        @SerializedName("original_last_name")
        @Expose
        public Object originalLastName;
        @SerializedName("user_image")
        @Expose
        public String userImage;
        @SerializedName("user_thumbnail")
        @Expose
        public String userThumbnail;
        @SerializedName("hobbies")
        @Expose
        public Object hobbies;
        @SerializedName("dob")
        @Expose
        public Object dob;
        @SerializedName("quotes")
        @Expose
        public String quotes;
        @SerializedName("profile_create_date")
        @Expose
        public String profileCreateDate;
        @SerializedName("profile_modify_date")
        @Expose
        public String profileModifyDate;
        @SerializedName("gender")
        @Expose
        public Object gender;
        @SerializedName("bio")
        @Expose
        public String bio;
        @SerializedName("year_of_passing")
        @Expose
        public Object yearOfPassing;
        @SerializedName("batch")
        @Expose
        public Object batch;
        @SerializedName("course")
        @Expose
        public Object course;
        @SerializedName("job_company")
        @Expose
        public Object jobCompany;
        @SerializedName("designation")
        @Expose
        public Object designation;
        @SerializedName("total_pelcians")
        @Expose
        public String totalPelcians;
    }
}
