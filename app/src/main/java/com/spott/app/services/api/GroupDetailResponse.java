package com.spott.app.services.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupDetailResponse {

    @SerializedName("err-code")
    @Expose
    public String errCode;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("detail")
    @Expose
    public Detail detail;
    @SerializedName("total_comment")
    @Expose
    public String totalComment;
    @SerializedName("unread")
    @Expose
    public String unread;

    public class Detail {

        @SerializedName("group_create_date")
        @Expose
        public String groupCreateDate;
        @SerializedName("group_status")
        @Expose
        public String groupStatus;
        @SerializedName("group_id")
        @Expose
        public String groupId;
        @SerializedName("group_name")
        @Expose
        public String groupName;
        @SerializedName("group_detail")
        @Expose
        public String groupDetail;
        @SerializedName("group_image")
        @Expose
        public String groupImage;
        @SerializedName("group_type")
        @Expose
        public String groupType;
        @SerializedName("group_tags")
        @Expose
        public String groupTags;
        @SerializedName("owner_id")
        @Expose
        public String ownerId;
        @SerializedName("owner_type")
        @Expose
        public String ownerType;
        @SerializedName("you_like")
        @Expose
        public String youLike;
        @SerializedName("your_like")
        @Expose
        public Object yourLike;
        @SerializedName("total_likes")
        @Expose
        public String totalLikes;
        @SerializedName("you_group")
        @Expose
        public YouGroup youGroup;
        @SerializedName("moods")
        @Expose
        public List<Object> moods = null;

        public class YouGroup {

            @SerializedName("member_id")
            @Expose
            public String memberId;
            @SerializedName("group_id")
            @Expose
            public String groupId;
            @SerializedName("uid")
            @Expose
            public String uid;
            @SerializedName("member_status")
            @Expose
            public String memberStatus;
            @SerializedName("mute_group")
            @Expose
            public String muteGroup;
            @SerializedName("is_fav")
            @Expose
            public String isFav;
            @SerializedName("member_create_date")
            @Expose
            public String memberCreateDate;

        }
    }
}