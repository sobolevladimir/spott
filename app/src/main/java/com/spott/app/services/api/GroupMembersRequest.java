package com.spott.app.services.api;

import android.content.Context;

import com.google.gson.annotations.SerializedName;

/**
 * Created by oleg on 08.10.2017.
 */

public class GroupMembersRequest extends BaseSpottRequest {

    @SerializedName("group_id")
    public String GroupId;

    public GroupMembersRequest(Context context, String groupId) {
        super(context, "group_member");
        GroupId = groupId;
    }
}
