package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.LoaderClass;

/**
 * Created by Adimn on 11/19/2016.
 */
public class JoinActivity extends Activity implements View.OnClickListener {


    ArrayList<HashMap<String, String>> groupList;
    Context context;
    ListView group_list;
    ImageView iv_red_icon, iv_back_icon, iv_heart, iv_msg, iv_groupImage;
    TextView tv_title_group, tv_subheading, tv_count, tv_msg_count, tv_send, tv_num,tv_no_comment;
    EditText et_leave_comment;
    Activity activity;
    private BroadcastReceiver broadcastReceiver;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout ll_groups;
    private ArrayList<Bitmap> imagesArray = new ArrayList<>();
    String group_id = "", groupname = "", grpdetail = "", grpimg = "", member_id = "", grptype = "", grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    SharedPreferences preferences;
    ArrayList<HashMap<String, String>> viewlist;
    ImageView iv_menu_icon;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_number;
    static ArrayList<HashMap<String, String>> joingroupList;
    ArrayList<HashMap<String, String>> memberlist;
    HorizontalAdapter horizontalAdapter;
    RecyclerView rvImages;
    String uid = "", post_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_activity);
        this.activity = (Activity) JoinActivity.this;
        group_list = (ListView) findViewById(R.id.group_list);
        context = getApplicationContext();
        groupList = new ArrayList<HashMap<String, String>>();
        viewlist = new ArrayList<HashMap<String, String>>();
        memberlist = new ArrayList<HashMap<String, String>>();
        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        owner_id = intent.getStringExtra("owner_id");
        member_id = intent.getStringExtra("member_id");
        owner_type = intent.getStringExtra("owner_type");
        you_grp = intent.getStringExtra("you_group");
        uid = intent.getStringExtra("imp_id");
        post_id = intent.getStringExtra("post_id");
        initialise();
        clickables();
        setting();
        horizontalAdapter = new HorizontalAdapter(JoinActivity.this, memberlist);
        rvImages.setAdapter(horizontalAdapter);
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new ViewGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new ViewGroupData().execute();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            new GroupmemberData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new GroupmemberData().execute();
        }

    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);
    }


    private void initialise() {

        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        iv_heart = (ImageView) findViewById(R.id.iv_heart);
        iv_msg = (ImageView) findViewById(R.id.iv_msg);
        iv_groupImage = (ImageView) findViewById(R.id.iv_groupImage);
        tv_title_group = (TextView) findViewById(R.id.tv_title_group);
        tv_no_comment = (TextView) findViewById(R.id.tv_no_comment);
        tv_subheading = (TextView) findViewById(R.id.tv_subheading);
        tv_count = (TextView) findViewById(R.id.tv_count);
        tv_msg_count = (TextView) findViewById(R.id.tv_msg_count);
        tv_send = (TextView) findViewById(R.id.tv_send);
        et_leave_comment = (EditText) findViewById(R.id.et_leave_comment);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        tv_number = (TextView) findViewById(R.id.tv_number);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        rvImages = (RecyclerView) findViewById(R.id.rvImages);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(JoinActivity.this, LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(horizontalLayoutManagaer);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }



    private void clickables() {

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JoinActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JoinActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });
        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GroupMemberOptions.class);
                intent.putExtra("group_id", group_id);
                intent.putExtra("group_name", groupname);
                intent.putExtra("group_detail", grpdetail);
                intent.putExtra("options", "");
                startActivity(intent);
            }
        });


    }

    public void setListViewHeightBasedOnItems(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        System.out.println("Height is:" + params.height);
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:
                Intent intent = new Intent(JoinActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(JoinActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(JoinActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(JoinActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }

    }

    public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
        private ArrayList<HashMap<String, String>> memberlist;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView ivImage, ivImage1;

            public MyViewHolder(View view) {
                super(view);
                ivImage = (ImageView) view.findViewById(R.id.ivImage);
                ivImage1 = (ImageView) view.findViewById(R.id.ivImage1);
            }
        }

        public HorizontalAdapter(JoinActivity pendingActivity, ArrayList<HashMap<String, String>> memberlist) {
            this.memberlist = memberlist;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizontal_xyz, parent, false);
            MyViewHolder holder = new MyViewHolder(itemView);

            return holder;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            if (memberlist.get(position).get("uid").equals(preferences.getString("owner_id", ""))) {

                holder.ivImage1.setVisibility(View.VISIBLE);
                holder.ivImage.setVisibility(View.VISIBLE);
            }
            if (!memberlist.get(position).get("user_image").toString().equals("")) {
                holder.ivImage.setVisibility(View.VISIBLE);
                holder.ivImage1.setVisibility(View.GONE);
                Picasso.with(getApplicationContext()).load(memberlist.get(position).get("user_image")).into(holder.ivImage);
            }

        }

        @Override
        public int getItemCount() {
            return memberlist.size();
        }
    }

    public class GroupmemberData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_member");

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");
                String you_like = resultObj.getString("you_like");
                String total_like = resultObj.getString("total_like");
                String total_comment = resultObj.getString("total_comment");

                tv_count.setText(total_like);
                tv_msg_count.setText(total_comment);
                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");
                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();
                        //  JSONObject user_dataObj = jArray.getJSONObject(i);

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String uid1 = "";
                        String member_id = user_dataObj.getString("member_id");
                        String group_id = user_dataObj.getString("group_id");
                        uid1 = user_dataObj.getString("uid");
                        String member_status = user_dataObj.getString("member_status");
                        String mute_group = user_dataObj.getString("mute_group");
                        String member_create_date = user_dataObj.getString("member_create_date");
                        String profile_id = user_dataObj.getString("profile_id");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");

                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String hobbies = user_dataObj.getString("hobbies");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String batch = user_dataObj.getString("batch");
                        String course = user_dataObj.getString("course");
                        String total_pelcians = user_dataObj.getString("total_pelcians");
                        hmap.put("profile_id", profile_id);
                        hmap.put("uid", uid1);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("course", course);
                        hmap.put("total_pelcians", total_pelcians);

                        hmap.put("member_create_date", member_create_date);
                        hmap.put("mute_group", mute_group);

                        hmap.put("member_status", member_status);
                        hmap.put("group_id", group_id);

                        hmap.put("member_id", member_id);

                        hmap.put("batch", batch);
                        hmap.put("year_of_passing", year_of_passing);


                        memberlist.add(hmap);

                    }
                    horizontalAdapter = new HorizontalAdapter(JoinActivity.this, memberlist);
                    rvImages.setAdapter(horizontalAdapter);


                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> groupList;

        public GroupAdapter(Context context, ArrayList<HashMap<String, String>> groupList) {
            this.context = context;
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.group_page_adapter, null);

            ViewHolder holder = new ViewHolder();

            holder.iv_group_profile = (ImageView) convertView.findViewById(R.id.iv_group_profile);

            holder.tv_txt_msg = (TextView) convertView.findViewById(R.id.tv_txt_msg);

            holder.tv_likes = (TextView) convertView.findViewById(R.id.tv_likes);

            return convertView;
        }

        class ViewHolder {
            ImageView iv_group_profile;
            TextView tv_txt_msg, tv_likes;
        }
    }


    public class ViewGroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(JoinActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_detail");

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

//                    JSONArray jArray = resultObj.getJSONArray("list");
//
//                    for (int i = 0; i < jArray.length(); i++) {
//
//                        HashMap<String, String> hmap = new HashMap<>();

                    JSONObject user_dataObj = resultObj.getJSONObject("detail");
                    String uid1 = "";
                    String group_id = user_dataObj.getString("group_id");
                    String group_name = user_dataObj.getString("group_name");
                    String group_detail = user_dataObj.getString("group_detail");
                    String group_image = user_dataObj.getString("group_image");
                    String group_type = user_dataObj.getString("group_type");
                    String group_tags = user_dataObj.getString("group_tags");
                    String group_create_date = user_dataObj.getString("group_create_date");
                    String group_status = user_dataObj.getString("group_status");
                    String owner_id = user_dataObj.getString("owner_id");
                    String owner_type = user_dataObj.getString("owner_type");
                    String you_group = user_dataObj.getString("you_group");


                    if (!you_group.equals("")) {
                        JSONObject user_dataObj1 = new JSONObject(you_group);
                        String member_id = user_dataObj1.getString("member_id");
                        String group_id1 = user_dataObj1.getString("group_id");
                        uid1 = user_dataObj1.getString("uid");
                        String member_status = user_dataObj1.getString("member_status");
                        String mute_group = user_dataObj1.getString("mute_group");
                        String member_create_date = user_dataObj1.getString("member_create_date");


                        tv_title_group.setText(groupname);
                        tv_subheading.setText(grpdetail);
                        Picasso.with(getApplicationContext()).load(grpimg).into(iv_groupImage);
                        preferences.edit().putString("owner_id", owner_id).commit();

                    }

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(JoinActivity.this);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Join Screen");

        FlurryAgent.onStartSession(JoinActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(JoinActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}

