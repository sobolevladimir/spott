package com.spott.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;
import com.spott.app.common.CommonUtilities;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Adimn on 10/6/2016.
 */
public class PendingOptionsActivity extends Activity implements View.OnClickListener, AbsListView.OnScrollListener {


    ImageView iv_back_icon, iv_unmute;
    TextView tv_back, tv_title_group, tv_mute_group, tv_edit_group, tv_delete, tv_invite, tv_num;
    ListView group_list_members;
    LayoutInflater layoutInflater = null;
    ArrayList<HashMap<String, String>> groupList;
    Context context;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout ll_groups, rl_leave, rl_leave1;
    LinearLayout rl_mute, rl_unmute,tv_pending;
    Activity activity;
    String post_id = "", you_like = "", mute_group = "";
    SharedPreferences preferences;
    int count = 0;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "";
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    GroupMemberAdapter adapter;
    BroadcastReceiver broadcastReceiver;
    Button ok_btn, cancel_btn;
    PopupWindow popup_window;
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_number;
    String group_id = "", groupname = "", grpdetail = "", grpimg = "", grptype = "", grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    ImageView iv_menu_icon, iv_red_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_options);
        this.activity = (Activity) PendingOptionsActivity.this;
        group_list_members = (ListView) findViewById(R.id.group_list_members);
        context = getApplicationContext();
        groupList = new ArrayList<HashMap<String, String>>();
        adapter = new GroupMemberAdapter(context, groupList);
        group_list_members.setAdapter(adapter);
        initialise();
        clickables();
        Intent intent = getIntent();
        post_id = intent.getStringExtra("post_id");
        you_like = intent.getStringExtra("you_like");
        setting();
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);

        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        owner_id = intent.getStringExtra("owner_id");
        owner_type = intent.getStringExtra("owner_type");
        you_grp = intent.getStringExtra("you_group");

        tv_title_group.setText(groupname);
    }

    private void initialise() {
        connDec = new ConnectionDetector(PendingOptionsActivity.this);
        alert = new Alert_Dialog_Manager(context);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_unmute = (ImageView) findViewById(R.id.iv_unmute);
        tv_title_group = (TextView) findViewById(R.id.tv_title_group);
        tv_mute_group = (TextView) findViewById(R.id.tv_mute_group);
         tv_pending = (LinearLayout) findViewById(R.id.tv_pending);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_edit_group = (TextView) findViewById(R.id.tv_edit_group);
        tv_delete = (TextView) findViewById(R.id.tv_delete);
        tv_invite = (TextView) findViewById(R.id.tv_invite);
        ok_btn = (Button) findViewById(R.id.ok_btn);
        cancel_btn = (Button) findViewById(R.id.cancel_btn);
        tv_number = (TextView) findViewById(R.id.tv_num);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        rl_mute = (LinearLayout) findViewById(R.id.rl_mute);
        rl_unmute = (LinearLayout) findViewById(R.id.rl_unmute);
        rl_leave = (RelativeLayout) findViewById(R.id.rl_leave);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.hasExtra("feed_count"))
                {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if(intent.hasExtra("message_count"))
                {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver,intentFilter);
    }



    private void clickables() {

        iv_back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PendingOptionsActivity.this, CrewActivity.class);
                startActivity(intent);
            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PendingOptionsActivity.this, CrewActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = groupList.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {

                }
            } else {
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.ll_feed:
                Intent intent = new Intent(PendingOptionsActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(PendingOptionsActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_groups:
                Intent intent2 = new Intent(PendingOptionsActivity.this, JoinGroupActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent2);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(PendingOptionsActivity.this, MyDirectoryActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }
    }

    private void customShareDetailsPopup(Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.popup_design);
        Button ok_btn, cancel_btn;
        ok_btn = (Button) dialog.findViewById(R.id.ok_btn);
        cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new DeleteServiceData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new DeleteServiceData().execute();
                    }
                }
            }
        });
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    private class GroupMemberAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> groupList;

        public GroupMemberAdapter(Context context, ArrayList<HashMap<String, String>> groupList) {
            this.context = context;
            this.groupList = groupList;
        }

        @Override
        public int getCount() {
            return groupList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group_member_options_adapter, null);
            ViewHolder holder = new ViewHolder();
            holder.iv_group_member_profile = (ImageView) convertView.findViewById(R.id.iv_group_member_profile);
            holder.tv_txt_memberName = (TextView) convertView.findViewById(R.id.tv_txt_memberName);
            holder.tv_likes = (TextView) convertView.findViewById(R.id.tv_likes);
            holder.tv_txt_msg = (TextView) convertView.findViewById(R.id.tv_txt_msg);
            holder.tv_txt_year = (TextView) convertView.findViewById(R.id.tv_txt_year);
            holder.tv_txt_memberName.setText(groupList.get(i).get("first_name") + " " + groupList.get(i).get("last_name"));

            if (!groupList.get(i).get("batch").toString().equals("")) {

                holder.tv_txt_msg.setText(groupList.get(i).get("batch").toString());
            }
            if (!groupList.get(i).get("user_image").toString().equals("")) {

                Picasso.with(getApplicationContext()).load(groupList.get(i).get("user_image")).into(holder.iv_group_member_profile);
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_group_member_profile;
            TextView tv_txt_memberName, tv_likes, tv_txt_msg, tv_txt_year;
        }
    }


    public class DeleteServiceData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            pdDialog = new Dialog(PendingOptionsActivity.this);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            // pdDialog.show();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "delete_group");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            // pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(PendingOptionsActivity.this, MyCrewsActivity.class);
                    startActivity(intent);

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Pending Detail Screen");

        FlurryAgent.onStartSession(PendingOptionsActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(PendingOptionsActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}

