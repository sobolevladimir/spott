package com.spott.app.common;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.spott.app.LoginScreenActivity;
import com.spott.app.R;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * Created by Acer on 9/20/2016.
 */
public class CommonUtilities {

    Context context;

    static AlertDialog alertDialog;

    private static Bitmap theBitmap;

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 234;
    public static String LOGIN_URL1 = "http://23.20.26.147";
//    public static String LOGIN_URL1 = "http://54.237.104.144";
    //public static String LOGIN_URL1 = "http://138.197.112.87";
    public static String LOGIN_URL = "http://beta-utcapp.simplyintense.com";

    public CommonUtilities(Context context) {
        this.context = context;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static boolean checkPermission_location(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;

        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            /*if ( ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission( context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                return  false;

                }
            else
            {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                }
                return false;
            } else {
                return true;
            }*/
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.ACCESS_FINE_LOCATION)) {

                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_FINE_LOCATION);

                } else {

                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_FINE_LOCATION);


                }

                return false;
            } else {
                return true;
            }

        }

        return true;
    }

    public static Bitmap getDecoadedBitmap(final String url, final ImageView imageView, final Activity activity) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                   /* theBitmap = Glide.
                            with(activity).
                            load(url).
                            asBitmap().
                            into(-1, -1).
                            get();*/
                    Glide.with(activity)
                            .load(url)
                            .asBitmap()
                            .into(new SimpleTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                    double height = resource.getHeight() / 1;
                                    double width = resource.getWidth() / 1;
                                    //Bitmap b = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
                                    imageView.setImageBitmap(Bitmap.createScaledBitmap(resource, (int) width, (int) height, false));
                                    //imageView.setImageBitmap(resource);
                                    //imageView.setImageBitmap(resource);
                                }
                            });
                /*} catch (final ExecutionException e) {
                    Log.e("getDecoadedBitmap", e.getMessage());
                } catch (final InterruptedException e) {
                    Log.e("getDecoadedBitmap", e.getMessage());*/
                } catch (Exception e) {

                }
            }
        });

        return theBitmap;
    }


    public static void showHideEmoLay(LinearLayout emoLay, Activity activity) {
        emoLay.setVisibility(View.GONE);
        try {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (emoLay.getVisibility() == View.VISIBLE) {
            emoLay.setVisibility(View.GONE);
        } else {
            emoLay.setVisibility(View.VISIBLE);
        }
    }


    public static String encodeUTF8(String system_string) {
        String encoaded_string = StringEscapeUtils.escapeJava(system_string);
        return encoaded_string;
    }

    public static String decodeUTF8(String utf8_string) {
        String decoaded_string = StringEscapeUtils.unescapeJava(utf8_string);
        return decoaded_string;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public static void showStarsPopup(Activity activity) {


        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.popup_earn_stars, null);

        TextView tv_ok = (TextView) dialogView.findViewById(R.id.tv_ok);

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        builder.setView(dialogView);

        alertDialog = builder.create();
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

    }


    public static void showSessionExpirePopup(final Activity activity) {
        AlertDialog.Builder alert = new AlertDialog.Builder(activity);

        alert.setCancelable(false);

        alert.setTitle("Session Expired");

        alert.setMessage("Session Expired");


        alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();

                activity.startActivity(new Intent(activity, LoginScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));

            }
        });
        alert.show();
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public static void appendCode(final EditText editText) {
        editText.setText("@ttutc.com");

        Selection.setSelection(editText.getText(), 0);

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().endsWith("@ttutc.com")) {
                    editText.setText("@ttutc.com");

                    Selection.setSelection(editText.getText(), 0);

                }

            }

        });
    }

    ///////////////fonts style change//////////////////////////////////////////////////////////////////

    public static void TV_Bold_os(Context context, TextView textView) {

        textView.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));

    }


    public static String getDaysMonths(long diff) {
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        long months = days / 30;

        if (months > 0) {
            if (months > 1)
                return (months + "mths");

            else
                return (months + "mth");
        }
        if (days > 0)
            return (days + "d");
        else if (hours > 0)
            return (hours + "h");
        else if (minutes >= 0 && minutes <= 60)
            return (minutes + "m");
        else
            return (2 + "s");
    }


    ////////////////////////////////////////////Hide Show keyboard///////////////////////////////////////////////

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void showSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

}
