package com.spott.app.common;

import android.content.Context;
import android.util.AttributeSet;

import com.spott.app.R;

/**
 * Created by Adimn on 10/7/2016.
 */
public class DiamondImageView extends ShaderImageView {

        public DiamondImageView(Context context) {
            super(context);
        }

        public DiamondImageView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public DiamondImageView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        public ShaderHelper createImageViewHelper() {
            return new SvgShader(R.raw.imgview_diamond);
        }
    }


