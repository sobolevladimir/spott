package com.spott.app.common;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.view.Display;
import android.view.ViewGroup;
import android.view.Window;

import com.spott.app.R;
import com.pnikosis.materialishprogress.ProgressWheel;

/**
 * Created by Adimn on 10/26/2016.
 */
public class LoaderClass {

    private static Dialog dialog;

    public static  void ShowProgressWheel(Activity context)
    {
        System.out.println("Show Activity from which dialog called "+context.getClass().getSimpleName());

        dialog = new Dialog(context);

        dialog.setCancelable(false);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.inflate_progress_bar);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Display display = ((Activity) context).getWindowManager().getDefaultDisplay();

        ProgressWheel wheel = (ProgressWheel)dialog.findViewById(R.id.progreeWheel);

        wheel.setBarColor(context.getResources().getColor(R.color.red));
        wheel.setRimWidth(wheel.getBarWidth()*3);

        Point size = new Point();

        display.getSize(size);

        int width = size.x;

        dialog.getWindow().setLayout(width - 60, ViewGroup.LayoutParams.WRAP_CONTENT);// set width of alert dialog box nad get width dynamically

        dialog.show();
    }

    public static void HideProgressWheel(Activity context)
    {
        System.out.println("Hide Activity from which dialog called "+context.getClass().getSimpleName());

        try
        {
            if (dialog != null)
                dialog.dismiss();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
