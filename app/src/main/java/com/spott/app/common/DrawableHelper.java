package com.spott.app.common;

import android.content.Context;
import android.view.View;

import com.spott.app.R;

/**
 * Created by oleg on 16.09.2017.
 */

public class DrawableHelper {

    public static void setBottomNavigationBackground(View view) {
        view.setBackgroundResource(R.drawable.background_with_symbols);
    }
}
