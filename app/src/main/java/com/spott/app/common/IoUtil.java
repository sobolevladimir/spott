package com.spott.app.common;

import java.io.InputStream;

/**
 * Created by Adimn on 10/7/2016.
 */
    @SuppressWarnings("FinalStaticMethod")
    public class IoUtil {
        public static final void closeQuitely(InputStream is) {
            if(is != null) {
                try {
                    is.close();
                } catch (Throwable ignored) {
                    //ignored
                }
            }
        }
    }

