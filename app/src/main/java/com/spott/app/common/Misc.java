package com.spott.app.common;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by interworld on 18/12/15.
 */
public class Misc {

    public static boolean isValidEmail(final String email) {

        Pattern pattern;
        Matcher matcher;

        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);

        return matcher.matches();

    }

    public static boolean isValidEmailAddress(String email)
    {
        if (email != null)
        {
            Pattern p = Pattern.compile("^[A-Za-z].*?@ttutc\\.com$");
            Matcher m = p.matcher(email);
            return m.find();
        }
        return false;
    }
    public static boolean isValidEmailAddress1(String email)
    {
        if (email != null)
        {
            Pattern p = Pattern.compile("^[A-Za-z].*?@dikonia\\.com$");
            Matcher m = p.matcher(email);
            return m.find();
        }
        return false;
    }
    public static boolean isValidEmailAddress2(String email)
    {
        if (email != null)
        {
            Pattern p = Pattern.compile("^[A-Za-z].*?@simplyintense\\.com$");
            Matcher m = p.matcher(email);
            return m.find();
        }
        return false;
    }
    public static boolean isAlphaNumeric(String s) {

        String pattern = "(^(?=.*\\w)(?=.*\\d{1,}).{8,})";

        if (s.matches(pattern)) {
            return true;
        }
        return false;
    }

    public static String remveSChar(String text) {


        StringBuilder sb = null;

        try {
            int pos = 0;

            boolean ssss = false;
            sb = new StringBuilder(text);
            while (pos < sb.length()) {

                if (sb.charAt(pos) == 'S' || sb.charAt(pos) == 's') {

                    if(pos==8)
                        ssss=false;

                    if(ssss){
                        sb.deleteCharAt(pos);
                    }

                    ssss = true;
                }
                pos++;
            }
            System.out.println(sb.toString());
            return sb.toString();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();

            return sb.toString();

        }
    }
}


