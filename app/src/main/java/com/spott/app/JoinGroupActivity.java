package com.spott.app;

/**
 * Created by Acer on 9/19/2016.
 */

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.spott.app.common.DrawableHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.spott.app.chip.FilteredArrayAdapter;
import com.spott.app.chip.TokenCompleteTextView;
import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Acer on 9/8/2016.
 */
public class JoinGroupActivity extends Activity implements View.OnClickListener,
        AbsListView.OnScrollListener, TokenCompleteTextView.TokenListener<Person> {

    ImageView iv_back_icon, iv_add_icon;
    LayoutInflater layoutInflater = null;
    EditText iv_serach_view;
    ArrayList<HashMap<String, String>> mygroupfavlist;
    Activity activity;
    PullToRefreshListView pulllist;
    FrameLayout fl_view_pager;
    ViewPager viewpager;
    GroupAdapter1 gAdapter;
    NavigationFragmentAdapter nadapter;
    ArrayList<HashMap<String, String>> joingroupList1;
    Context context;
    PullToRefreshListView listview;
    TextView  tv_profile_name, tv_profile_username, tv_subjects, tv_occupation;
    JoinGroupAdapter mAdapter;
    String feed_count = "";
    String encoded = "";
    DiamondImageView img_profile;
    JoinGroupPagingAdapter groupAdapter;
    LinearLayout ll_feed, ll_profile, ll_directory;
    RelativeLayout ll_groups;
    SharedPreferences preferences;
    ViewPager pager;
    private BroadcastReceiver broadcastReceiver;
    String firstname = "", lastname = "", star = "", group = "", post = "", method = "";
    RelativeLayout rlMainFront;
    int count = 0,currentPage=0;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "";
    int height, width;
    ConnectionDetector connDec;
    boolean drawer_open = false;
    String image;
    Alert_Dialog_Manager alert;
    String[] courses, skills, inerests;
    String course = "", skill = "", interst = "";
    String finalInterests = "";
    View shadowView = null, view = null;
    ArrayList<HashMap<String, String>> dataList;
    ArrayList<HashMap<String, String>> requestList;
    ArrayList<HashMap<String, String>> searchlist;
    ArrayList<Person> srch_help_arrayList = new ArrayList<Person>();
    boolean showDropDown = true;
    boolean already_queried = false;
    String previousText = "", login = "";
    Person[] people;
    long last_text_edit = 0;
    long idle_min = 1500; // 4 seconds after user stops typing
    ArrayAdapter<Person> adapter;
    Handler h = new Handler();
    static List<Person> persons;
    ArrayList<String> name_array = new ArrayList<>();
    TextView tv_fed, tv_profile_txt, tv_my_group, tv_staf_directory, tv_num, tv_msg_grp;
    ImageView iv_menu_icon, iv_red_icon;
    String grpType = "", groupid = "", groupType = "", search = "";
    String group_id = "", groupname = "", grpdetail = "", grpimg = "", grptype = "", grptag = "", groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join_group_activity);
        this.activity = (Activity) JoinGroupActivity.this;
        listview = (PullToRefreshListView) findViewById(R.id.listing_groups);
        context = JoinGroupActivity.this;
        nadapter = new NavigationFragmentAdapter();
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(nadapter);
        joingroupList1 = new ArrayList<>();
        joingroupList1.addAll(((UTCAPP) getApplication()).joingroupList);
        dataList = new ArrayList<HashMap<String, String>>();
        requestList = new ArrayList<HashMap<String, String>>();
        searchlist = new ArrayList<HashMap<String, String>>();
        mAdapter = new JoinGroupAdapter(JoinGroupActivity.this, joingroupList1);
        listview.setAdapter(mAdapter);
        groupAdapter = new JoinGroupPagingAdapter(JoinGroupActivity.this, dataList);
        viewpager = (ViewPager)findViewById(R.id.viewpager);
        viewpager.setAdapter(groupAdapter);
        layoutInflater = LayoutInflater.from(this);
        initialise();

        grpType = preferences.getString("group_type", "");
        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        owner_id = intent.getStringExtra("owner_id");
        owner_type = intent.getStringExtra("owner_type");
        ViewPager pager;
        you_grp = intent.getStringExtra("you_group");
        listview.setOnScrollListener(this);
        skill = preferences.getString("skills", "");
        interst = preferences.getString("interests", "");
        course = preferences.getString("course", "");
        System.out.println("skills=" + skill);
        System.out.println("interst=" + interst);
        System.out.println("course=" + course);
        courses = course.split(",");
        inerests = interst.split(",");
        skills = skill.split(",");
        arrayCheck();

        login = preferences.getString("login_token", "");


        setting();
        encoded = preferences.getString("user_image", "");
        ll_feed.setOnClickListener(this);
        ll_profile.setOnClickListener(this);
        ll_groups.setOnClickListener(this);
        ll_directory.setOnClickListener(this);
        iv_menu_icon.setOnClickListener(this);
        clickables();
        listview.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                {
                    if (!isLoading) {
                        if (has_more.equalsIgnoreCase("true")) {
                            isLoading = true;

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                new JoinGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                            } else {
                                new JoinGroupData().execute("loadmore");
                            }
                        } else {
                            Toast.makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
        listview.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                count = 0;
                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    new JoinGroupData().execute(search);
                }
                //   joingroupList1.clear();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new JoinGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new JoinGroupData().execute();
            }
        }

        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new RecommendGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new RecommendGroupData().execute();
            }
        }
    }

    private void setting() {

        ll_feed.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_fed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myfeed, 0, 0);
        tv_fed.setTextColor(Color.parseColor("#F15A51"));

        ll_profile.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_profile_txt.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_myprofile, 0, 0);
        tv_profile_txt.setTextColor(Color.parseColor("#F15A51"));

        ll_groups.setBackgroundColor(Color.parseColor("#139FDA"));
        tv_my_group.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.sel_btm_mygroups, 0, 0);
        tv_my_group.setTextColor(Color.parseColor("#ffffff"));

        ll_directory.setBackgroundColor(Color.parseColor("#ffffff"));
        tv_staf_directory.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.btm_staff, 0, 0);
        tv_staf_directory.setTextColor(Color.parseColor("#F15A51"));

        DrawableHelper.setBottomNavigationBackground(ll_groups);
    }


    private void arrayCheck() {

        for (int i = 0; i < courses.length; i++) {
            courses[i] = courses[i].trim();

            if (finalInterests.equals("")) {
                finalInterests = finalInterests + courses[i];
            } else {
                finalInterests = finalInterests + "," + courses[i];
            }
        }

        for (int i = 0; i < inerests.length; i++) {
            inerests[i] = inerests[i].trim();

            finalInterests = finalInterests + "," + inerests[i];
        }

        for (int i = 0; i < skills.length; i++) {
            skills[i] = skills[i].trim();

            finalInterests = finalInterests + "," + skills[i];
        }

        System.out.println("FINAL INTERESTS=" + finalInterests);
    }


    private void initialise() {
        connDec = new ConnectionDetector(JoinGroupActivity.this);
        alert = new Alert_Dialog_Manager(context);
        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        iv_back_icon = (ImageView) findViewById(R.id.iv_back_icon);
        rlMainFront = (RelativeLayout) findViewById(R.id.rlMainFront);
        iv_add_icon = (ImageView) findViewById(R.id.iv_add_icon);
        iv_serach_view = (EditText) findViewById(R.id.iv_serach_view);
        ll_feed = (LinearLayout) findViewById(R.id.ll_feed);
        ll_profile = (LinearLayout) findViewById(R.id.ll_profile);
        ll_directory = (LinearLayout) findViewById(R.id.ll_calendar);
        ll_groups = (RelativeLayout) findViewById(R.id.ll_groups);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
        iv_menu_icon = (ImageView) findViewById(R.id.iv_menu_icon);
        tv_my_group = (TextView) findViewById(R.id.tv_my_group);
        tv_num = (TextView) findViewById(R.id.tv_num);
        tv_fed = (TextView) findViewById(R.id.tv_fed);
        tv_profile_txt = (TextView) findViewById(R.id.tv_profile_txt);
        tv_staf_directory = (TextView) findViewById(R.id.tv_staf_directory);
        tv_msg_grp = (TextView) findViewById(R.id.tv_msg_grp);
        fl_view_pager = (FrameLayout) findViewById(R.id.fl_view_pager);
        view = (View) findViewById(R.id.vw);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        height = displaymetrics.heightPixels;
        width = displaymetrics.widthPixels;
        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MAIN");

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("feed_count")) {
                    /*String new_feed = intent.getStringExtra("feed_count");
                    Integer i = Integer.parseInt(new_feed);

                    if (i == 0) {

                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else if (i < 99) {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText(i + "");
                    } else {
                        iv_red_icon.setVisibility(View.VISIBLE);
                        iv_red_icon.setText("99+");
                    }*/
                }
                if (intent.hasExtra("message_count")) {
                    String new_message = intent.getStringExtra("message_count");
                    Integer i = Integer.parseInt(new_message);
                    if (i == 0) {
                        tv_num.setVisibility(View.GONE);
                    } else if (i < 99) {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText(new_message);
                    } else {
                        tv_num.setVisibility(View.VISIBLE);
                        tv_num.setText("99+");
                    }
                }
            }
        };

        this.registerReceiver(broadcastReceiver, intentFilter);
    }

    private void updateAdapter() {
        adapter = new FilteredArrayAdapter<Person>(this, R.layout.person_layout, srch_help_arrayList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {

                    LayoutInflater l = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
                    convertView = l.inflate(R.layout.person_layout, parent, false);
                }

                final Person p = getItem(position);

                ((TextView) convertView.findViewById(R.id.name)).setText(p.getName());

                ((TextView) convertView.findViewById(R.id.email)).setText(p.getEmail());

                return convertView;
            }

            @Override
            protected boolean keepObject(Person person, String mask) {
                mask = mask.toLowerCase();
                return person.getName().toLowerCase().startsWith(mask) || person.getEmail().toLowerCase().startsWith(mask);
            }
        };

    }
    public void StopDrawerAnim() {

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 1f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 1f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", 1f);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 1f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 1f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", 1f);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();

        view.setVisibility(View.GONE);

        drawer_open = false;
    }

    public void startDrawerAnim() {
        view.setVisibility(View.VISIBLE);

        ObjectAnimator animX = ObjectAnimator.ofFloat(rlMainFront, "scaleX", 0.9f);
        ObjectAnimator animY = ObjectAnimator.ofFloat(rlMainFront, "scaleY", 0.8f);
        ObjectAnimator animXY = ObjectAnimator.ofFloat(rlMainFront, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY, animXY);
        animSetXY.start();

        ObjectAnimator animX1 = ObjectAnimator.ofFloat(view, "scaleX", 0.9f);
        ObjectAnimator animY1 = ObjectAnimator.ofFloat(view, "scaleY", 0.8f);
        ObjectAnimator animXY1 = ObjectAnimator.ofFloat(view, "translationX", (width / 10) * 8);
        AnimatorSet animSetXY1 = new AnimatorSet();
        animSetXY1.playTogether(animX1, animY1, animXY1);
        animSetXY1.start();
    }


    private void clickables() {

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StopDrawerAnim();
            }
        });

        iv_menu_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (drawer_open == false) {

                    startDrawerAnim();
                    drawer_open = true;
                    pager.setAdapter(nadapter);
                    pager.setCurrentItem(((UTCAPP) getApplication()).pagerPos);
                    mAdapter.notifyDataSetChanged();
//                    if (!(preferences.getString("user_image", "").equals(""))) {
//                        Picasso.with(getApplicationContext()).load(preferences.getString("user_image", "")).into(img_profile);
//                        img_profile.setBorderColor(Color.parseColor("#F15A51"));
//                        img_profile.setBorderWidth(2);
//                    }

                } else {
                    StopDrawerAnim();
                }

            }
        });

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JoinGroupActivity.this, UpdatesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            }
        });


        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JoinGroupActivity.this, MyCrewsActivity.class);
                startActivity(intent);
            }
        });

        iv_add_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JoinGroupActivity.this, AddGroupActivity.class);
                startActivity(intent);
            }
        });

        iv_serach_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_serach_view.getText().toString().trim();
                //callAPI();
                // new search_help().execute();
            }
        });

        iv_serach_view.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

//                if (iv_serach_view.getText().toString().equals("")) {
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        new JoinGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
//                    } else {
//                        new JoinGroupData().execute();
//                    }
//                    //fl_view_pager.setVisibility(View.VISIBLE);
//                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                last_text_edit = System.currentTimeMillis();

                h.postDelayed(input_finish_checker, idle_min);

            }
        });

        iv_serach_view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {

                    //performSearch();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    imm.hideSoftInputFromWindow(iv_serach_view.getWindowToken(), 0);

                    iv_serach_view.setCursorVisible(false);

                    //  setSearchLayout();

                    callAPI();


                    return true;
                }


                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                imm.hideSoftInputFromWindow(iv_serach_view.getWindowToken(), 0);

                iv_serach_view.setCursorVisible(false);

                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.vw:

                StopDrawerAnim();

                break;

            case R.id.ll_feed:
                Intent intent = new Intent(JoinGroupActivity.this, MyFeedActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);

                break;

            case R.id.ll_profile:
                Intent intent1 = new Intent(JoinGroupActivity.this, MyProfileActivity.class);
                intent1.putExtra("uid", preferences.getString("uid", ""));
                intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent1);

                break;

            case R.id.ll_calendar:
                Intent intent3 = new Intent(JoinGroupActivity.this, CalendarActivity.class);
                intent3.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent3);

                break;
        }

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = joingroupList1.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    //new JoinGroupData().execute();
                    // joingroupList1.clear();
                }
            } else {
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }

    @Override
    public void onTokenAdded(Person p) {
        //    ((TextView)findViewById(R.id.lastEvent)).setText("Added: " + token);

        {
            //final Person p = (Person) parent.getAdapter().getItem(position);

            try {
                if (name_array.size() > 0) {
                    if (!name_array.contains(p.getName())) {
                        if (!p.getName().equals("")) {
                            name_array.add(p.getName());
                        }
                    }
                } else {
                    if (!p.getName().equals("")) {
                        name_array.add(p.getName());
                    }
                    showDropDown = false;
                }

                System.out.println("JSON name array try : " + name_array + " " + p.getName());
            } catch (Exception e) {

                if (!p.getName().equals("")) {
                    name_array.add(p.getName());
                }

                showDropDown = false;

                e.printStackTrace();

                System.out.println("JSON name array catch : " + name_array + " " + p.getName());
            }

        }

        updateTokenConfirmation();

    }

    @Override
    public void onTokenRemoved(Person token) {
        name_array.remove(token.getName());

        updateTokenConfirmation();

        try {
            if (name_array.size() == 0) {
                callAPI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callAPI() {
        if (!connDec.isConnectingToInternet()) {
            alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                srch_help_arrayList.clear();
            } else {
                new search_help().execute();
                srch_help_arrayList.clear();
            }
        }

    }

    private void updateTokenConfirmation() {
        StringBuilder sb = new StringBuilder("Current tokens:\n");
    }

    private class JoinGroupAdapter extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> joingroupList1;

        public JoinGroupAdapter(Context context, ArrayList<HashMap<String, String>> joingroupList1) {
            this.context = context;
            this.joingroupList1 = joingroupList1;

            System.out.println("JOIN LIST=" + this.joingroupList1);
        }

        @Override
        public int getCount() {
            return joingroupList1.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_join_grp_adapter, null);
            final ViewHolder holder = new ViewHolder();
            holder.rl_join = (RelativeLayout) convertView.findViewById(R.id.rl_join);
            holder.iv_list_img = (DiamondImageView) convertView.findViewById(R.id.iv_list_img);
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
            holder.tv_joinbtn = (TextView) convertView.findViewById(R.id.tv_join);
            holder.tv_pendingbtn = (TextView) convertView.findViewById(R.id.tv_pendingbtn);
            holder.tv_viewbtn = (TextView) convertView.findViewById(R.id.tv_viewbtn);
            holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(joingroupList1.get(i).get("group_name")));

            System.out.println("POSITON " + joingroupList1.get(i));

            if (joingroupList1 == null || joingroupList1.size() == 0) {

                tv_msg_grp.setVisibility(View.VISIBLE);
            } else {
                tv_msg_grp.setVisibility(View.GONE);

                if (joingroupList1.get(i).containsKey("member_status")) {
                    if (!(joingroupList1.get(i).get("member_status").equals(""))) {
                        System.out.println("DATA " + joingroupList1.get(i).get("member_status"));

                        if (joingroupList1.get(i).get("member_status").equals("Active")) {
                            holder.tv_viewbtn.setVisibility(View.VISIBLE);
                            holder.tv_joinbtn.setVisibility(View.GONE);
                            holder.tv_pendingbtn.setVisibility(View.GONE);
                            holder.rl_join.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, CrewActivity.class);

                                    intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));

                                    intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));

                                    intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));

                                    intent.putExtra("group_image", joingroupList1.get(i).get("group_image"));

                                    intent.putExtra("group_type", joingroupList1.get(i).get("group_type"));

                                    intent.putExtra("group_tags", joingroupList1.get(i).get("group_tags"));

                                    intent.putExtra("group_create_date", joingroupList1.get(i).get("group_create_date"));

                                    intent.putExtra("group_status", joingroupList1.get(i).get("group_status"));

                                    intent.putExtra("owner_id", joingroupList1.get(i).get("owner_id"));

                                    intent.putExtra("owner_type", joingroupList1.get(i).get("owner_type"));

                                    intent.putExtra("you_group", joingroupList1.get(i).get("you_group"));

                                    intent.putExtra("member_status", joingroupList1.get(i).get("member_status"));

                                    startActivity(intent);
                                }
                            });
                        } else if (joingroupList1.get(i).get("member_status").equals("Request")) {
                            holder.tv_viewbtn.setVisibility(View.GONE);
                            holder.tv_joinbtn.setVisibility(View.GONE);
                            holder.tv_pendingbtn.setVisibility(View.VISIBLE);
                            holder.rl_join.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(context, CrewActivity.class);

                                    intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));

                                    intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));

                                    intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));

                                    intent.putExtra("group_image", joingroupList1.get(i).get("group_image"));

                                    intent.putExtra("group_type", joingroupList1.get(i).get("group_type"));

                                    intent.putExtra("group_tags", joingroupList1.get(i).get("group_tags"));

                                    intent.putExtra("group_create_date", joingroupList1.get(i).get("group_create_date"));

                                    intent.putExtra("group_status", joingroupList1.get(i).get("group_status"));

                                    intent.putExtra("owner_id", joingroupList1.get(i).get("owner_id"));

                                    intent.putExtra("owner_type", joingroupList1.get(i).get("owner_type"));

                                    intent.putExtra("you_group", joingroupList1.get(i).get("you_group"));

                                    intent.putExtra("member_status", joingroupList1.get(i).get("member_status"));

                                    if (!joingroupList1.get(i).get("you_group").equals("") &&
                                            joingroupList1.get(i).get("member_status").equalsIgnoreCase("Active")) {
                                        startActivity(intent);
                                    } else {
                                        intent.putExtra("options_next", "");

                                        startActivity(intent);

                                    }
                                }
                            });
                        }

                    }
                } else if ((joingroupList1.get(i).get("you_group").equals(""))) {

                    holder.tv_pendingbtn.setVisibility(View.GONE);
                    holder.tv_joinbtn.setVisibility(View.VISIBLE);
                    holder.tv_viewbtn.setVisibility(View.GONE);
                    holder.rl_join.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, GroupMemberOptions.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

//                            intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));
//
//                            intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));
//
//                            intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));
//
//                            intent.putExtra("group_image", joingroupList1.get(i).get("group_image"));
//
//                            intent.putExtra("group_type", joingroupList1.get(i).get("group_type"));
//
//                            intent.putExtra("group_tags", joingroupList1.get(i).get("group_tags"));
//
//                            intent.putExtra("group_create_date", joingroupList1.get(i).get("group_create_date"));
//
//                            intent.putExtra("group_status", joingroupList1.get(i).get("group_status"));
//
//                            intent.putExtra("owner_id", joingroupList1.get(i).get("owner_id"));
//
//                            intent.putExtra("owner_type", joingroupList1.get(i).get("owner_type"));
//
//                            intent.putExtra("you_group", joingroupList1.get(i).get("you_group"));
//
//                            intent.putExtra("member_status", joingroupList1.get(i).get("member_status"));

                            intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));
                            intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));
                            intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));
                            intent.putExtra("group_image", joingroupList1.get(i).get("group_image"));
                            intent.putExtra("group_type", joingroupList1.get(i).get("group_type"));
                            intent.putExtra("group_tags", joingroupList1.get(i).get("group_tags"));
                            intent.putExtra("group_create_date", joingroupList1.get(i).get("group_create_date"));
                            intent.putExtra("group_status", joingroupList1.get(i).get("group_status"));
                            intent.putExtra("owner_id", joingroupList1.get(i).get("owner_id"));
                            intent.putExtra("owner_type", joingroupList1.get(i).get("owner_type"));
                            intent.putExtra("you_group", joingroupList1.get(i).get("you_group"));
                            intent.putExtra("member_id", preferences.getString("member_id", "").toString());
                            intent.putExtra("imp_id", preferences.getString("uid", ""));
                            intent.putExtra("key", "");
                            intent.putExtra("is_fav", false);
                            intent.putExtra("mute_group", false);
                            //intent.putExtra("options_next", "");
                            intent.putExtra("member_status", joingroupList1.get(i).get("member_status"));

                            if (!joingroupList1.get(i).get("you_group").equals("") &&
                                    joingroupList1.get(i).get("member_status").equalsIgnoreCase("Active")) {
                                startActivity(intent);
                            } else {
                                intent.putExtra("options_next", "");

                                startActivity(intent);

                            }

                        }
                    });
                }
            }

            if (!joingroupList1.get(i).get("group_image").toString().equals("")) {

                Picasso.with(getApplicationContext()).load((String) joingroupList1.get(i).get("group_image")).into(holder.iv_list_img);
                holder.iv_list_img.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_list_img.setBorderWidth(2);
            }

            holder.tv_joinbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    groupid = joingroupList1.get(i).get("group_id");
                    groupType = joingroupList1.get(i).get("group_type");
                    if (!(joingroupList1.get(i).get("you_group").equals("") || joingroupList1.get(i).get("you_group") == null)) {
                        if (joingroupList1.get(i).get("member_status").equals("Active")) {
                            holder.tv_viewbtn.setVisibility(View.VISIBLE);
                            holder.tv_joinbtn.setVisibility(View.GONE);
                            holder.tv_pendingbtn.setVisibility(View.GONE);
                            joingroupList1.get(i).put("member_status", "Active");
                        } else if (joingroupList1.get(i).get("member_status").equals("Request")) {
                            holder.tv_viewbtn.setVisibility(View.GONE);
                            holder.tv_joinbtn.setVisibility(View.GONE);
                            holder.tv_pendingbtn.setVisibility(View.VISIBLE);
                            joingroupList1.get(i).put("member_status", "Request");
                        }


                    } else if ((joingroupList1.get(i).get("you_group").equals("") || joingroupList1.get(i).get("you_group") == null)) {
                        if (joingroupList1.get(i).get("group_type").equals("Public")) {
                            holder.tv_pendingbtn.setVisibility(View.GONE);
                            holder.tv_joinbtn.setVisibility(View.GONE);
                            holder.tv_viewbtn.setVisibility(View.VISIBLE);
                            joingroupList1.get(i).put("member_status", "Active");
                        } else if (joingroupList1.get(i).get("group_type").equals("Private")) {
                            holder.tv_pendingbtn.setVisibility(View.VISIBLE);
                            holder.tv_joinbtn.setVisibility(View.GONE);
                            holder.tv_viewbtn.setVisibility(View.GONE);
                            joingroupList1.get(i).put("member_status", "Request");
                        }
                    }
                    if (!connDec.isConnectingToInternet()) {
                        //    alert.showNetAlert();
                    } else {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            new GroupRequestData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        } else {
                            new GroupRequestData().execute();
                        }
                    }
                    //}
                }
            });

            holder.tv_pendingbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*Intent intent = new Intent(context, PendingActivity.class);

                    intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));

                    intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));

                    intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));

                    startActivity(intent);*/

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));

                    intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));

                    intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));

                    intent.putExtra("group_image", joingroupList1.get(i).get("group_image"));

                    intent.putExtra("group_type", joingroupList1.get(i).get("group_type"));

                    intent.putExtra("group_tags", joingroupList1.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", joingroupList1.get(i).get("group_create_date"));

                    intent.putExtra("group_status", joingroupList1.get(i).get("group_status"));

                    intent.putExtra("owner_id", joingroupList1.get(i).get("owner_id"));

                    intent.putExtra("owner_type", joingroupList1.get(i).get("owner_type"));

                    intent.putExtra("you_group", joingroupList1.get(i).get("you_group"));

                    intent.putExtra("member_status", joingroupList1.get(i).get("member_status"));

                    if (!joingroupList1.get(i).get("you_group").equals("") &&
                            joingroupList1.get(i).get("member_status").equalsIgnoreCase("Active")) {
                        startActivity(intent);
                    } else {
                        intent.putExtra("options_next", "");

                        startActivity(intent);

                    }

                }
            });

            holder.tv_viewbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.putExtra("group_id", joingroupList1.get(i).get("group_id"));

                    intent.putExtra("group_name", joingroupList1.get(i).get("group_name"));

                    intent.putExtra("group_detail", joingroupList1.get(i).get("group_detail"));

                    intent.putExtra("group_image", joingroupList1.get(i).get("group_image"));

                    intent.putExtra("group_type", joingroupList1.get(i).get("group_type"));

                    intent.putExtra("group_tags", joingroupList1.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", joingroupList1.get(i).get("group_create_date"));

                    intent.putExtra("group_status", joingroupList1.get(i).get("group_status"));

                    intent.putExtra("owner_id", joingroupList1.get(i).get("owner_id"));

                    intent.putExtra("owner_type", joingroupList1.get(i).get("owner_type"));

                    intent.putExtra("member_status", joingroupList1.get(i).get("member_status"));

                    intent.putExtra("you_group", joingroupList1.get(i).get("you_group"));

                    startActivity(intent);

                }
            });
            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_list_img;
            TextView tv_items_grp, tv_joinbtn, tv_pendingbtn, tv_viewbtn;
            RelativeLayout rl_join;
        }
    }

    private class JoinGroupPagingAdapter extends PagerAdapter {


        ImageView iv_group_img;
        TextView tv_grp_name, tv_join, tv_viewbtnn, tv_pendingbtnn,tv_grp_detail;
        ArrayList<HashMap<String, String>> dataList;
        Context context;
        LayoutInflater inflater;
        RelativeLayout rl_recommend;

        public JoinGroupPagingAdapter(Context context, ArrayList<HashMap<String, String>> dataList) {

            this.dataList = dataList;
            this.context = context;
            System.out.println("dataList=" + this.dataList);
        }

        @Override
        public int getCount() {
            return dataList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewGroup) container).removeView((View) object);
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {

            try {
                view = layoutInflater.inflate(R.layout.join_pager_adapter, container, false);

                iv_group_img = (ImageView) view.findViewById(R.id.iv_group_img);
                tv_grp_name = (TextView) view.findViewById(R.id.tv_grp_name);
                tv_grp_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_join = (TextView) view.findViewById(R.id.tv_join);
                tv_pendingbtnn = (TextView) view.findViewById(R.id.tv_pendingbtnn);
                tv_viewbtnn = (TextView) view.findViewById(R.id.tv_view);
                rl_recommend = (RelativeLayout) view.findViewById(R.id.rl_recommend);
                tv_grp_detail=(TextView)view.findViewById(R.id.tv_grp_detail);

                tv_grp_detail.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));

                iv_group_img = (ImageView) view.findViewById(R.id.iv_group_img);

                if (!dataList.get(position).get("group_image").toString().equals("")) {

                    Picasso.with(getApplicationContext()).load(dataList.get(position).get("group_image")).into(iv_group_img);

                }

                tv_grp_name.setText(CommonUtilities.decodeUTF8(dataList.get(position).get("group_name").toString()));

                tv_grp_detail.setText(CommonUtilities.decodeUTF8(dataList.get(position).get("group_detail").toString()));

                rl_recommend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if ((dataList.get(position).get("you_group").equals(""))) {
                            Intent intent = new Intent(activity, CrewActivity.class);
                            intent.putExtra("group_id", dataList.get(position).get("group_id"));

                            intent.putExtra("group_name", dataList.get(position).get("group_name"));

                            intent.putExtra("group_detail", dataList.get(position).get("group_detail"));

                            intent.putExtra("group_image", dataList.get(position).get("group_image"));

                            intent.putExtra("group_type", dataList.get(position).get("group_type"));

                            intent.putExtra("group_tags", dataList.get(position).get("group_tags"));

                            intent.putExtra("group_create_date", dataList.get(position).get("group_create_date"));

                            intent.putExtra("group_status", dataList.get(position).get("group_status"));

                            intent.putExtra("owner_id", dataList.get(position).get("owner_id"));

                            intent.putExtra("owner_type", dataList.get(position).get("owner_type"));

                            intent.putExtra("options_next", "");

                            startActivity(intent);
                        }
                        else if ((!dataList.get(position).get("you_group").equals("")))
                        {
                            Intent intent = new Intent(activity, CrewActivity.class);
                            intent.putExtra("group_id", dataList.get(position).get("group_id"));

                            intent.putExtra("group_name", dataList.get(position).get("group_name"));

                            intent.putExtra("group_detail", dataList.get(position).get("group_detail"));

                            intent.putExtra("group_image", dataList.get(position).get("group_image"));

                            intent.putExtra("group_type", dataList.get(position).get("group_type"));

                            intent.putExtra("group_tags", dataList.get(position).get("group_tags"));

                            intent.putExtra("group_create_date", dataList.get(position).get("group_create_date"));

                            intent.putExtra("group_status", dataList.get(position).get("group_status"));

                            intent.putExtra("owner_id", dataList.get(position).get("owner_id"));

                            intent.putExtra("owner_type", dataList.get(position).get("owner_type"));

                            startActivity(intent);
                        }
                    }

                });

                container.addView(view, 0);

            } catch (InflateException e) {

            }

            return view;
        }
    }

    public class JoinGroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(JoinGroupActivity.this);

        }

        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();
                if (strings.length > 0) {
                    if (strings[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }
                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                if (!isLoadMore) {

                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", joingroupList1.size() + "");
                }


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }




        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            listview.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    if (!isLoadMore) {
                        joingroupList1.clear();
                    }

                    ((UTCAPP) (getApplication())).joingroupList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);
                        String member_status = "";
                        String group_id = user_dataObj.getString("group_id");
                        String group_name = CommonUtilities.decodeUTF8(user_dataObj.getString("group_name"));
                        String group_detail = user_dataObj.getString("group_detail");
                        String group_image = user_dataObj.getString("group_image");
                        String group_type = user_dataObj.getString("group_type");
                        String group_tags = user_dataObj.getString("group_tags");
                        String group_create_date = user_dataObj.getString("group_create_date");
                        String group_status = user_dataObj.getString("group_status");
                        String owner_id = user_dataObj.getString("owner_id");
                        String owner_type = user_dataObj.getString("owner_type");
                        String you_group1 = user_dataObj.getString("you_group");

                        String memberId = "";
                        String groupId = "";
                        String uid = "";
                        String memberStatus = "";
                        String mute_group = "";
                        String is_fav = "";
                        String member_create_date = "";

                        if (user_dataObj.get("you_group") instanceof JSONObject) {
                            JSONObject you_group = user_dataObj.getJSONObject("you_group");

                            memberId = you_group.getString("member_id");
                            groupId = you_group.getString("group_id");
                            uid = you_group.getString("uid");
                            memberStatus = you_group.getString("member_status");
                            mute_group = you_group.getString("mute_group");
                            is_fav = you_group.getString("is_fav");
                            member_create_date = you_group.getString("member_create_date");

                            updateHash(hmap, memberId, groupId, uid, memberStatus, mute_group, is_fav, member_create_date, you_group1);
                        }

                        hmap.put("group_id", group_id);
                        hmap.put("group_name", group_name);
                        hmap.put("group_detail", group_detail);
                        hmap.put("group_image", group_image);
                        hmap.put("group_type", group_type);
                        hmap.put("group_tags", group_tags);
                        hmap.put("group_create_date", group_create_date);
                        hmap.put("group_status", group_status);
                        hmap.put("owner_id", owner_id);
                        hmap.put("owner_type", owner_type);
                        hmap.put("you_group", you_group1);
                        preferences.edit().putString("group_id", group_id).commit();

                        preferences.edit().putString("group_type", group_type).commit();

                        joingroupList1.add(hmap);

                       /* ((UTCAPP) (getApplication())).joingroupList.add(hmap);

                        joingroupList1.addAll(((UTCAPP) (getApplication())).joingroupList);*/

                    }
                    System.out.println("LIST SIZE " + joingroupList1.size());

                    mAdapter = new JoinGroupAdapter(JoinGroupActivity.this, joingroupList1);

                    listview.setAdapter(mAdapter);

                    mAdapter.notifyDataSetChanged();

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(JoinGroupActivity.this);
            isLoading = false;
        }

        private void updateHash(HashMap<String, String> hmap, String memberId, String groupId, String uid, String memberStatus, String mute_group, String is_fav, String member_create_date, String you_group1) {
            hmap.put("member_id", memberId);
            hmap.put("group_id", groupId);
            hmap.put("uid", uid);
            hmap.put("member_status", memberStatus);
            hmap.put("mute_group", mute_group);
            hmap.put("is_fav", is_fav);
            hmap.put("member_create_date", member_create_date);
            hmap.put("you_group", you_group1);
            preferences.edit().putString("you_group", you_group1).commit();
        }

    }

    public class RecommendGroupData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdDialog = new Dialog(JoinGroupActivity.this);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            //  pdDialog.show();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "recommend_groups");

                jo.put("login_token", login);

                jo.put("user_interest", interst);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Recomm data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            //  pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    dataList.clear();

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String group_id = user_dataObj.getString("group_id");
                        String group_name = user_dataObj.getString("group_name");
                        String group_detail = user_dataObj.getString("group_detail");
                        String group_image = user_dataObj.getString("group_image");
                        String group_type = user_dataObj.getString("group_type");
                        String group_tags = user_dataObj.getString("group_tags");
                        String group_create_date = user_dataObj.getString("group_create_date");
                        String group_status = user_dataObj.getString("group_status");
                        String owner_id = user_dataObj.getString("owner_id");
                        String owner_type = user_dataObj.getString("owner_type");
                        String you_group = user_dataObj.getString("you_group");


                        if (!you_group.equals("")) {
                            JSONObject user_dataObj1 = new JSONObject(you_group);
                            String member_id = user_dataObj1.getString("member_id");
                            String group_id1 = user_dataObj1.getString("group_id");
                            String uid1 = user_dataObj1.getString("uid");
                            String member_status = user_dataObj1.getString("member_status");
                            String mute_group = user_dataObj1.getString("mute_group");
                            String member_create_date = user_dataObj1.getString("member_create_date");
                        }

                        hmap.put("group_id", group_id);
                        hmap.put("group_name", group_name);
                        hmap.put("group_detail", group_detail);
                        hmap.put("group_image", group_image);
                        hmap.put("group_type", group_type);
                        hmap.put("group_tags", group_tags);
                        hmap.put("group_create_date", group_create_date);
                        hmap.put("group_status", group_status);
                        hmap.put("owner_id", owner_id);
                        hmap.put("owner_type", owner_type);
                        hmap.put("you_group", you_group);
                        dataList.add(hmap);

                    }

                    if(dataList.size()!=0)
                    {
                        fl_view_pager.setVisibility(View.VISIBLE);

                        groupAdapter = new JoinGroupPagingAdapter(JoinGroupActivity.this, dataList);

                        viewpager.setAdapter(groupAdapter);

                        viewpager.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                playPager(dataList.size());

                            }
                        }, 500);
                    }

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void playPager(final int NUM_PAGES) {
        final Handler handler = new Handler();

        final Runnable update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewpager.setCurrentItem(currentPage++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 100, 3000);
    }

    public class GroupRequestData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdDialog = new Dialog(JoinGroupActivity.this);
            pdDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pdDialog.setContentView(R.layout.custom_progress_dialog);
            pdDialog.setTitle("Please Wait...");
            pdDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            // pdDialog.show();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_request");

                jo.put("login_token", login);

                jo.put("group_id", groupid);

                jo.put("group_type", groupType);

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Group request data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            // pdDialog.dismiss();

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    String you_group = preferences.getString("you_group", "");

                } else if (errCode.equals("700")) {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    public class search_help extends AsyncTask<String, String, String> {


        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(JoinGroupActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();
                if (params.length > 0) {
                    if (params[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }
                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "group_search");

                jo.put("login_token", login);

                jo.put("search_text", params[0]);         // pass empty string

                //  jo.put("search_for", MainActivity.str_search_for);

                if (!isLoadMore) {
                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", srch_help_arrayList.size() + "");
                }


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            srch_help_arrayList = new ArrayList<Person>();


            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        joingroupList1 = new ArrayList<>();
//                        if(!isLoadMore) {
//                            srch_help_arrayList.clear();
//                        }
                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);

                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group1 = helpObject.getString("you_group");

                            String memberId = "";
                            String groupId = "";
                            String uid = "";
                            String memberStatus = "";
                            String mute_group = "";
                            String is_fav = "";
                            String member_create_date = "";

                            if (helpObject.get("you_group") instanceof JSONObject) {
                                JSONObject you_group = helpObject.getJSONObject("you_group");

                                memberId = you_group.getString("member_id");
                                groupId = you_group.getString("group_id");
                                uid = you_group.getString("uid");
                                memberStatus = you_group.getString("member_status");
                                mute_group = you_group.getString("mute_group");
                                is_fav = you_group.getString("is_fav");
                                member_create_date = you_group.getString("member_create_date");

                                updateHash(hmap, memberId, groupId, uid, memberStatus, mute_group, is_fav, member_create_date, you_group1);
                            }

                            //srch_help_arrayList.add(new Person(group_name, group_name));

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group1);

                            joingroupList1.add(hmap);
                        }

                        fl_view_pager.setVisibility(View.GONE);

                        mAdapter = new JoinGroupAdapter(JoinGroupActivity.this, joingroupList1);

                        listview.setAdapter(mAdapter);

                        mAdapter.notifyDataSetChanged();

                        showDropDown = true;


                        showDrop();

                    }
                } else if (errCode.equals("300")) {
                    joingroupList1.clear();
                    mAdapter.notifyDataSetChanged();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(JoinGroupActivity.this);
            isLoading = false;
        }
    }

    private void updateHash(HashMap<String, String> hmap, String memberId, String groupId, String uid, String memberStatus, String mute_group, String is_fav, String member_create_date, String you_group1) {
        hmap.put("member_id", memberId);
        hmap.put("group_id", groupId);
        hmap.put("uid", uid);
        hmap.put("member_status", memberStatus);
        hmap.put("mute_group", mute_group);
        hmap.put("is_fav", is_fav);
        hmap.put("member_create_date", member_create_date);
        hmap.put("you_group", you_group1);
        preferences.edit().putString("you_group", you_group1).commit();
    }

    private Runnable input_finish_checker = new Runnable() {
        public void run() {
            if (System.currentTimeMillis() > (last_text_edit + idle_min - 500)) {
                String textCalled = iv_serach_view.getText().toString().trim();

                if (!already_queried) {
                    if (!textCalled.equals("")) {
                        if (!previousText.equals(iv_serach_view.getText().toString().trim())) {
                            previousText = textCalled;

                            String search = previousText.replace(",", "");

                            search = search.trim();
                            if (!connDec.isConnectingToInternet()) {
                                //    alert.showNetAlert();
                            } else {
                                if (!search.equals("")) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                        new search_help().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, search);
                                    } else {
                                        new search_help().execute(search);
                                    }
                                }
                            }
                        }
                    } else {
                        if (isLoading == false) {
                            isLoading = true;
                            if (!connDec.isConnectingToInternet()) {
                                alert.showNetAlert();
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new JoinGroupData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {
                                    new JoinGroupData().execute();
                                }
                            }

                            showDrop();
                        }
                    }
                }
            }
        }
    };

    private void showDrop() {
        try {
            if (showDropDown == true) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!JoinGroupActivity.this.isFinishing()) {
                        }
                    }
                }, 500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public class GroupFavData extends AsyncTask<String, String, String> {

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "fav_groups_list");

                jo.put("login_token", preferences.getString("login_token", ""));

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            pulllist.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONArray help_array = resultObj.getJSONArray("list");
                    {

                        if (mygroupfavlist != null)
                            mygroupfavlist.clear();
                        else
                            mygroupfavlist = new ArrayList<>();


                        for (int i = 0; i < help_array.length(); i++) {

                            HashMap<String, String> hmap = new HashMap<>();

                            JSONObject helpObject = help_array.getJSONObject(i);
                            String group_id = helpObject.getString("group_id");
                            String group_name = helpObject.getString("group_name");
                            String group_detail = helpObject.getString("group_detail");
                            String group_image = helpObject.getString("group_image");
                            String group_type = helpObject.getString("group_type");
                            String group_tags = helpObject.getString("group_tags");
                            String group_create_date = helpObject.getString("group_create_date");
                            String group_status = helpObject.getString("group_status");
                            String owner_id = helpObject.getString("owner_id");
                            String owner_type = helpObject.getString("owner_type");
                            String you_group = helpObject.getString("you_group");
                            String unread = helpObject.getString("unread");
                            if (!you_group.equals("")) {
                                JSONObject user_dataObj1 = new JSONObject(you_group);
                                String member_id = user_dataObj1.getString("member_id");
                                String group_id1 = user_dataObj1.getString("group_id");
                                String uid1 = user_dataObj1.getString("uid");
                                String member_status = user_dataObj1.getString("member_status");
                                String mute_group = user_dataObj1.getString("mute_group");
                                String member_create_date = user_dataObj1.getString("member_create_date");

                            }

                            hmap.put("group_id", group_id);
                            hmap.put("group_name", group_name);
                            hmap.put("group_detail", group_detail);
                            hmap.put("group_image", group_image);
                            hmap.put("group_type", group_type);
                            hmap.put("group_tags", group_tags);
                            hmap.put("group_create_date", group_create_date);
                            hmap.put("group_status", group_status);
                            hmap.put("owner_id", owner_id);
                            hmap.put("owner_type", owner_type);
                            hmap.put("you_group", you_group);
                            hmap.put("unread", unread);
                            preferences.edit().putString("group_name", group_name).commit();

                            preferences.edit().putString("group_type", group_type).commit();

                            preferences.edit().putString("group_detail", group_detail).commit();

                            preferences.edit().putString("group_image", encoded).commit();

                            preferences.edit().putString("group_tags", group_tags).commit();

                            mygroupfavlist.add(hmap);
                        }
                        gAdapter = new GroupAdapter1(JoinGroupActivity.this, mygroupfavlist);

                        pulllist.setAdapter(gAdapter);

                        gAdapter.notifyDataSetChanged();
                    }
                } else if (errCode.equals("700")) {
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private class GroupAdapter1 extends BaseAdapter {
        Context context;
        ArrayList<HashMap<String, String>> mygroupfavlist;

        public GroupAdapter1(Context context, ArrayList<HashMap<String, String>> mygroupfavlist) {
            this.context = context;
            this.mygroupfavlist = mygroupfavlist;
        }

        @Override
        public int getCount() {
            return mygroupfavlist.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_group_adapter, null);
            GroupAdapter1.ViewHolder holder = new GroupAdapter1.ViewHolder();
            holder.tv_items_grp = (TextView) convertView.findViewById(R.id.tv_items_grp);
            holder.tv_items_grp.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tv_items_grp1 = (TextView) convertView.findViewById(R.id.tv_items_grp1);
            holder.tv_items_grp1.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_view_btngrp = (TextView) convertView.findViewById(R.id.tv_view_btngrp);
            holder.tv_white_angle = (TextView) convertView.findViewById(R.id.tv_white_angle);
            holder.rll = (RelativeLayout) convertView.findViewById(R.id.rll);
            holder.rll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);

                    finish();
                }
            });
            holder.tv_view_btngrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, CrewActivity.class);

                    intent.putExtra("group_id", mygroupfavlist.get(i).get("group_id"));

                    intent.putExtra("group_name", mygroupfavlist.get(i).get("group_name"));

                    intent.putExtra("group_detail", mygroupfavlist.get(i).get("group_detail"));

                    intent.putExtra("group_image", mygroupfavlist.get(i).get("group_image"));

                    intent.putExtra("group_type", mygroupfavlist.get(i).get("group_type"));

                    intent.putExtra("group_tags", mygroupfavlist.get(i).get("group_tags"));

                    intent.putExtra("group_create_date", mygroupfavlist.get(i).get("group_create_date"));

                    intent.putExtra("group_status", mygroupfavlist.get(i).get("group_status"));

                    intent.putExtra("owner_id", mygroupfavlist.get(i).get("owner_id"));

                    intent.putExtra("owner_type", mygroupfavlist.get(i).get("owner_type"));

                    intent.putExtra("you_group", mygroupfavlist.get(i).get("you_group"));

                    startActivity(intent);

                    finish();
                }
            });
            if (mygroupfavlist.get(i).get("unread").equals("0")) {
                holder.tv_items_grp1.setVisibility(View.GONE);
                holder.tv_items_grp.setVisibility(View.VISIBLE);
                holder.tv_items_grp.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.GONE);
            } else {

                holder.tv_items_grp1.setVisibility(View.VISIBLE);
                holder.tv_items_grp1.setText(CommonUtilities.decodeUTF8(mygroupfavlist.get(i).get("group_name")));
                holder.tv_white_angle.setVisibility(View.VISIBLE);
                holder.tv_white_angle.setText(mygroupfavlist.get(i).get("unread"));
            }

            return convertView;
        }

        class ViewHolder {
            ImageView iv_list_img;
            TextView tv_items_grp, tv_view_btngrp, tv_white_angle, tv_items_grp1;
            RelativeLayout rll;
        }
    }

    private class NavigationFragmentAdapter extends PagerAdapter {


        TextView tv_browsing;
        ImageView iv_group_add_icon;
        RelativeLayout rl_slider_menu, rl_profile_info, rl_menu, rlMainFront;
        TextView white_box, orange_box, white_box1, tv_post, tv_groupss, tv_notification_count, tv_update, tv_feed, tv_profile, tv_groups, tv_staff_dir, tv_log_out;


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }

        @Override
        public void notifyDataSetChanged() {
            if (feed_count.equals("0"))
                tv_notification_count.setVisibility(View.GONE);

            else {
                tv_notification_count.setVisibility(View.VISIBLE);

                tv_notification_count.setText(feed_count);
            }
        }

        @Override
        public Object instantiateItem(final ViewGroup container, final int position) {
            View view = null;

            if (position == 0) {
                view = layoutInflater.inflate(R.layout.group_slider_menu, container, false);
                pulllist = (PullToRefreshListView) view.findViewById(R.id.pulllist);
                iv_group_add_icon = (ImageView) view.findViewById(R.id.iv_group_add_icon);
                tv_browsing = (TextView) view.findViewById(R.id.tv_browsing);

                if (!connDec.isConnectingToInternet()) {
                    //alert.showNetAlert();
                } else {
                    new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }
                iv_group_add_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(JoinGroupActivity.this, AddGroupActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

                pulllist.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

                    @Override
                    public void onLastItemVisible() {

                    }

                });
                pulllist.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                    @Override
                    public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                        count = 0;

                        if (!connDec.isConnectingToInternet()) {
                            //alert.showNetAlert();
                        } else {
                            new GroupFavData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                            mygroupfavlist.clear();
                        }

                    }
                });

                tv_browsing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(JoinGroupActivity.this, JoinGroupActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                container.addView(view, 0);

            } else if (position == 1) {
                view = layoutInflater.inflate(R.layout.slider_menu, container, false);
                img_profile = (DiamondImageView) view.findViewById(R.id.img_profile);
                white_box = (TextView) view.findViewById(R.id.white_box);
                orange_box = (TextView) view.findViewById(R.id.orange_box);
                white_box1 = (TextView) view.findViewById(R.id.white_box1);
                tv_profile_name = (TextView) view.findViewById(R.id.tv_profile_name);
                tv_profile_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Bold.ttf"));
                tv_profile_username = (TextView) view.findViewById(R.id.tv_profile_username);
                tv_profile_username.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Light.ttf"));
                tv_feed = (TextView) view.findViewById(R.id.tv_feed);
                tv_profile = (TextView) view.findViewById(R.id.tv_profile);
                tv_groups = (TextView) view.findViewById(R.id.tv_groups);
                tv_staff_dir = (TextView) view.findViewById(R.id.tv_staff_dir);
                tv_update = (TextView) view.findViewById(R.id.tv_update);
                tv_notification_count = (TextView) view.findViewById(R.id.tv_notification_count);
                tv_log_out = (TextView) view.findViewById(R.id.tv_logout);
                tv_post = (TextView) view.findViewById(R.id.tv_post);
                tv_groupss = (TextView) view.findViewById(R.id.tv_groupss);
                rlMainFront = (RelativeLayout) view.findViewById(R.id.rlMainFront);
                rl_slider_menu = (RelativeLayout) view.findViewById(R.id.rl_slider_menu);
                rl_profile_info = (RelativeLayout) view.findViewById(R.id.rl_profile_info);
                rl_menu = (RelativeLayout) view.findViewById(R.id.rl_menu);
                tv_profile_name.setText(preferences.getString("first_name", "") + " " + preferences.getString("last_name", ""));
                white_box1.setText(post);
                white_box.setText(group);
                orange_box.setText(preferences.getString("pelican", "0"));
                tv_profile_username.setText("@" + CommonUtilities.decodeUTF8(preferences.getString("username", "")));
                if (feed_count.equals("0"))
                    tv_notification_count.setVisibility(View.GONE);

                else {
                    tv_notification_count.setVisibility(View.VISIBLE);

                    tv_notification_count.setText(feed_count);
                }
                if (encoded.equalsIgnoreCase("")||encoded.equalsIgnoreCase("null")) {
                    img_profile.setImageResource(R.drawable.avatar);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                } else {
                    Picasso.with(getApplicationContext()).load(encoded).into(img_profile);
                    img_profile.setBorderColor(Color.parseColor("#F15A51"));
                    img_profile.setBorderWidth(2);
                }

                white_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(activity, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                        finish();

                    }
                });
                tv_groupss.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent2 = new Intent(activity, MyCrewsActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent2);
                        finish();
                    }
                });
                orange_box.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonUtilities.showStarsPopup(activity);
                    }
                });

                tv_feed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        Intent intent = new Intent(JoinGroupActivity.this, MyFeedActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();

                    }
                });

                tv_notification_count.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(JoinGroupActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                });
                tv_groups.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(JoinGroupActivity.this, JoinGroupActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }
                });
                tv_update.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(JoinGroupActivity.this, UpdatesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }
                });
                tv_staff_dir.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();
                        Intent intent = new Intent(JoinGroupActivity.this, MyDirectoryActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                        finish();
                    }
                });
                tv_log_out.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        StopDrawerAnim();

                        customShareLogOutPopup();
                    }
                });

                container.addView(view, 0);
            }
            //   ((ViewPager) container).addView(view, 0);

            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }


    private void customShareLogOutPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.leave_popup);
        final TextView tv_title_logout, ok_button, tv_title_leave, cancel_button;
        ok_button = (TextView) dialog.findViewById(R.id.ok_button);
        cancel_button = (TextView) dialog.findViewById(R.id.cancel_button);
        tv_title_logout = (TextView) dialog.findViewById(R.id.tv_title_logout);
        tv_title_leave = (TextView) dialog.findViewById(R.id.tv_title_leave);
        tv_title_leave.setVisibility(View.GONE);
        tv_title_logout.setVisibility(View.VISIBLE);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
               /* Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/

            }
        });


        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                /*Intent i = new Intent(MyFeedActivity.this, SplashActivity.class);
                startActivity(i);
                finishAffinity();*/
            }
        });

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Join Group Screen");

        FlurryAgent.onStartSession(JoinGroupActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(JoinGroupActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
    
    
}

