package com.spott.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.spott.app.services.api.RegisterRequest;
import com.spott.app.services.api.RegisterResponse;
import com.spott.app.services.api.RetrofitBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

import com.spott.app.cropiimage.CropImage;

import io.github.rockerhieu.emojicon.EmojiconGridFragment;
import io.github.rockerhieu.emojicon.EmojiconsFragment;
import io.github.rockerhieu.emojicon.emoji.Emojicon;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterScreenActivity extends FragmentActivity implements  EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    EditText et_first_name, et_last_name, et_mail, et_passwrd, et_phn, et_birth;
    TextView btn_register;
    String firststr = "", laststr = "", emailstr = "", phoneStr = "", passwordStr = "", birth = "";
    SharedPreferences preferences;
    DiamondImageView iv_signup_img;
    private ImageView iv_emojibtn_first_name,iv_keyboard_first_name,iv_emojibtn_last_name,iv_keyboard_last_name;
    private File mFileTemp;
    RelativeLayout rl_back, rl_emo_buttons_first_name,rl_emo_buttons_last_name;
    public static final int REQUEST_CODE_GALLERY = 0x1,
            REQUEST_CODE_TAKE_PICTURE = 0x2, REQUEST_CODE_CROP_IMAGE = 0x3;
    public static String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    String encoded;
    RadioButton first, second;
    String Radiobtn = "", token = "";
    public static int api_level = 0;
    ConnectionDetector connDec;
    private LinearLayout emoLay;
    Alert_Dialog_Manager alert;
    private Activity activity;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        context = RegisterScreenActivity.this;
        initialise();
        clickables();
        iv_signup_img.setVisibility(View.VISIBLE);
        iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
        iv_signup_img.setBorderWidth(2);
        token = preferences.getString("TOKEN", token);
        System.out.println("LOGIN TOKEN" + token);
    }

    private void initialise() {
        activity=RegisterScreenActivity.this;
        connDec = new ConnectionDetector(RegisterScreenActivity.this);
        alert = new Alert_Dialog_Manager(context);
        et_first_name = (EditText) findViewById(R.id.et_first_name);
        et_first_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_last_name = (EditText) findViewById(R.id.et_last_name);
        et_last_name.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
        et_mail = (EditText) findViewById(R.id.et_mail);
        et_passwrd = (EditText) findViewById(R.id.et_passwrd);
        et_phn = (EditText) findViewById(R.id.et_phn);
        et_birth = (EditText) findViewById(R.id.et_birth);
        et_phn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int phoneLength = charSequence.length();
                if (i2 == 1){
                    if (phoneLength==1){
                        et_phn.setText("(+"+charSequence);
                        et_phn.setSelection(phoneLength+2);
                    }else if (phoneLength==5){
                        et_phn.setText(charSequence+")");
                        et_phn.setSelection(phoneLength+1);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_birth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int textLength = charSequence.length();

                if (i2 ==1){
                    if (textLength==4||textLength==7){
                        et_birth.setText(charSequence+"-");
                        et_birth.setSelection(textLength+1);

                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btn_register = (TextView) findViewById(R.id.btn_register);
        first = (RadioButton) findViewById(R.id.first);
        second = (RadioButton) findViewById(R.id.second);
        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        rl_emo_buttons_first_name = (RelativeLayout) findViewById(R.id.rl_emo_buttons_first_name);
        rl_emo_buttons_last_name = (RelativeLayout) findViewById(R.id.rl_emo_buttons_last_name);
        iv_emojibtn_first_name=(ImageView)findViewById(R.id.iv_emojibtn_first_name);
        iv_keyboard_first_name=(ImageView)findViewById(R.id.iv_keyboard_first_name);
        iv_emojibtn_last_name=(ImageView)findViewById(R.id.iv_emojibtn_last_name);
        iv_keyboard_last_name=(ImageView)findViewById(R.id.iv_keyboard_last_name);
        emoLay = (LinearLayout) findViewById(R.id.emoLay);
        iv_signup_img = (DiamondImageView) findViewById(R.id.iv_signup_img);
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);
    }

    private void clickables() {

        iv_emojibtn_first_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_first_name.setVisibility(View.VISIBLE);
                iv_emojibtn_first_name.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);


            }
        });

        iv_keyboard_first_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard_first_name.setVisibility(View.GONE);
                iv_emojibtn_first_name.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_first_name);
            }
        });

        et_first_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons_first_name.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons_first_name.setVisibility(View.GONE);
                    iv_emojibtn_first_name.setVisibility(View.VISIBLE);
                    iv_keyboard_first_name.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_first_name.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_first_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_first_name.setVisibility(View.VISIBLE);
                iv_keyboard_first_name.setVisibility(View.GONE);
            }
        });



        iv_emojibtn_last_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_keyboard_last_name.setVisibility(View.VISIBLE);
                iv_emojibtn_last_name.setVisibility(View.GONE);
                try {
                    CommonUtilities.hideSoftKeyboard(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        emoLay.setVisibility(View.VISIBLE);
                    }
                },200);


            }
        });

        iv_keyboard_last_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv_keyboard_last_name.setVisibility(View.GONE);
                iv_emojibtn_last_name.setVisibility(View.VISIBLE);
                emoLay.setVisibility(View.GONE);
                CommonUtilities.showSoftKeyboard(activity,et_last_name);
            }
        });

        et_last_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    rl_emo_buttons_last_name.setVisibility(View.VISIBLE);
                }
                else {
                    rl_emo_buttons_last_name.setVisibility(View.GONE);
                    iv_emojibtn_last_name.setVisibility(View.VISIBLE);
                    iv_keyboard_last_name.setVisibility(View.GONE);
                    emoLay.setVisibility(View.GONE);
                }
            }
        });


        et_last_name.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                emoLay.setVisibility(View.GONE);

                return false;
            }
        });

        et_last_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_emojibtn_last_name.setVisibility(View.VISIBLE);
                iv_keyboard_last_name.setVisibility(View.GONE);
            }
        });

        iv_signup_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showCustomMessagephoto("UTC", "Profile Pic");
            }
        });

        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                second.setChecked(false);
                Radiobtn = "Male";


            }
        });

        second.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                first.setChecked(false);
                Radiobtn = "Female";

            }
        });
        rl_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firststr = CommonUtilities.encodeUTF8(et_first_name.getText().toString().trim());
                laststr = CommonUtilities.encodeUTF8(et_last_name.getText().toString().trim());
                emailstr = et_mail.getText().toString().trim();
                phoneStr = et_phn.getText().toString().trim();
                passwordStr = et_passwrd.getText().toString();
                birth = et_birth.getText().toString();
                String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                String passwordPattern ="^(?=.*[A-Za-z])[A-Za-z\\d]{6,}$";
                if (!passwordStr.matches(passwordPattern)){
                    Toast.makeText(RegisterScreenActivity.this,"Password should be no less than 6 characters and contain at least one letter of Latin alphabet.",Toast.LENGTH_LONG).show();
                    return;
                }
                if (!(firststr.length() > 0) || !(laststr.length() > 0) || phoneStr.equals("")) {

                    Toast.makeText(getApplicationContext(), R.string.firstname, Toast.LENGTH_SHORT).show();
                } else if (!(emailstr.matches(emailPattern) && emailstr.length() > 0)) {

                    Toast.makeText(getApplicationContext(), R.string.email, Toast.LENGTH_SHORT).show();
                } else {
                    if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                 } else {
                        RegisterRequest request = new RegisterRequest(RegisterScreenActivity.this);
                        request.setFirstName(firststr);
                        request.setLastName(laststr);
                        request.setEmail(emailstr);
                        request.setPassword(passwordStr);
                        request.setPhoneNo(phoneStr);
                        request.setDeviceToken(token);
                        request.setDob(birth);
                        request.setImageData(encoded);

                        RetrofitBuilder.getSpottApi().register(request).enqueue(
                                new Callback<RegisterResponse>() {
                                    @Override
                                    public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                                        processResponse(response.body());
                                    }

                                    @Override
                                    public void onFailure(Call<RegisterResponse> call, Throwable t) {

                                    }
                                }
                        );
                    }
                }
            }
        });

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(),
                    TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
    }

    @Override
    public void onBackPressed() {

        if (emoLay.getVisibility() == View.VISIBLE)
            emoLay.setVisibility(View.GONE);
        else
            finish();
    }



    private void showCustomMessagephoto(String string, String string2) {
        final Dialog dialogphoto = new Dialog(RegisterScreenActivity.this);
        dialogphoto.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogphoto.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogphoto.setContentView(R.layout.upload_profile);
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setText("Photo Library");
        ((Button) dialogphoto.findViewById(R.id.photo_library))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        api_level = Build.VERSION.SDK_INT;
                        if (api_level >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(RegisterScreenActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(RegisterScreenActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequest2();
                            } else {
                                OpenGallery();
                            }
                        } else {
                            OpenGallery();
                        }
                    }
                });
        ((Button) dialogphoto.findViewById(R.id.camera)).setText("Camera");
        ((Button) dialogphoto.findViewById(R.id.camera))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                        api_level = Build.VERSION.SDK_INT;
                        if (api_level >= 23) {
                            int permission1 = ContextCompat.checkSelfPermission(RegisterScreenActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE);

                            int permission2 = ContextCompat.checkSelfPermission(RegisterScreenActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
                                makeRequest1();
                            } else {
                                takePicture();
                            }
                        } else {
                            takePicture();
                        }

                    }
                });
        ((Button) dialogphoto.findViewById(R.id.Cancel)).setText("Cancel");
        ((Button) dialogphoto.findViewById(R.id.Cancel))
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogphoto.dismiss();
                    }
                });
        dialogphoto.show();

    }

    public void makeRequest1() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 1);
    }

    @SuppressLint("Override")
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(RegisterScreenActivity.this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();

                } else {
                    takePicture();
                }
                break;

            case 2:

                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("", "Permission has been denied by user");

                    Toast.makeText(RegisterScreenActivity.this, "Permission has been denied by user", Toast.LENGTH_SHORT).show();

                } else {
                    OpenGallery();
                }
                break;
        }
    }

    public void makeRequest2() {
        ActivityCompat.requestPermissions(this, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"}, 2);
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        if (et_first_name.hasFocus()) {
            EmojiconsFragment.input(et_first_name, emojicon);
        } else if (et_last_name.hasFocus()) {
            EmojiconsFragment.input(et_last_name, emojicon);
        }
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        if (et_first_name.hasFocus()) {
            EmojiconsFragment.backspace(et_first_name);
        } else if (et_last_name.hasFocus()) {
            EmojiconsFragment.backspace(et_last_name);
        }
    }

    public class RegisterData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(RegisterScreenActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "register_app_user");

                jo.put("email", emailstr);

                jo.put("first_name", firststr);

                jo.put("last_name", laststr);

                jo.put("username", firststr);

                jo.put("password", "");

                jo.put("gender", "");

                jo.put("device_type", "android");

                jo.put("device_token", token);

                jo.put("profile_url", "");

                jo.put("image", encoded);

                jo.put("phone_no", ""+phoneStr);

                jo.put("latitude", "");

                jo.put("longitude", "");


                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                if (errCode.equals("0")) {

                    JSONObject user_dataObj = resultObj.getJSONObject("user");

                    String uid = user_dataObj.getString("uid");

                    emailstr = user_dataObj.getString("email");
                    String latitude = user_dataObj.getString("latitude");
                    String longitude = user_dataObj.getString("longitude");
                    String device_token = user_dataObj.getString("device_token");
                    String login_token = user_dataObj.getString("login_token");
                    String user_create_date = user_dataObj.getString("user_create_date");
                    String badge = user_dataObj.getString("badge");
                    String facebook_id = user_dataObj.getString("facebook_id");
                    String survey_alert = user_dataObj.getString("survey_alert");
                    String password_token = user_dataObj.getString("password_token");
                    String register_check = user_dataObj.getString("register_check");
                    String otp = user_dataObj.getString("otp");
                    String verified = user_dataObj.getString("verified");
                    String password_change = user_dataObj.getString("password_change");
                    phoneStr = user_dataObj.getString("phone_no");

                    preferences.edit().putString("device_token", device_token).commit();

                    preferences.edit().putString("email", emailstr).commit();

                    preferences.edit().putString("latitude", latitude).commit();

                    preferences.edit().putString("longitude", longitude).commit();

                    preferences.edit().putString("phone_no", phoneStr).commit();

                    JSONObject user_dataObject = user_dataObj.getJSONObject("profile");
                    String profile_id = user_dataObject.getString("profile_id");
                    String first_name = user_dataObject.getString("first_name");
                    String last_name = user_dataObject.getString("last_name");
                    String user_image = user_dataObject.getString("user_image");
                    String user_thumbnail = user_dataObject.getString("user_thumbnail");
                    String hobbies = user_dataObject.getString("hobbies");
                    String phone = user_dataObject.getString("phone");
                    String dob = user_dataObject.getString("dob");
                    String quotes = user_dataObject.getString("quotes");
                    String profile_create_date = user_dataObject.getString("profile_create_date");
                    String profile_modify_date = user_dataObject.getString("profile_modify_date");
                    String gender = user_dataObject.getString("gender");

                    preferences.edit().putString("first_name", first_name).commit();

                    preferences.edit().putString("last_name", last_name).commit();

                    preferences.edit().putString("user_image", user_image).commit();

                    preferences.edit().putString("quotes", quotes).commit();

                    preferences.edit().putString("gender", gender).commit();

                    System.out.println("QUOTES=" + preferences.getString("quotes", ""));

                    Picasso.with(getApplicationContext()).load(encoded).into(iv_signup_img);
                    iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
                    iv_signup_img.setBorderWidth(2);

                    Intent intent = new Intent(RegisterScreenActivity.this, RegisterConfirmationScreen.class);

                    System.out.println("EMAIL GOING "+ emailstr);

                    intent.putExtra("email", emailstr);
                    intent.putExtra("user_image", user_image);
                    intent.putExtra("device_token", device_token);

                    startActivity(intent);

                    finish();

                } else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterScreenActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage(message).setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    runOnUiThread(new Runnable() {
                        public void run() {
                            alertDialog.show();
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(RegisterScreenActivity.this);
        }
    }

    ////////////////////////

    private String base64frombitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();

        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

    private void OpenGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_CODE_GALLERY);
    }

    static ArrayList<Bitmap> bitmaps_arrayList = new ArrayList<Bitmap>();

    File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath() + "/.temp/");
        if (!tempDir.exists()) {
            tempDir.mkdir();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (resultCode != Activity.RESULT_OK) {
                return;
            }

            Bitmap bitmap;

            byte[] b;
            switch (requestCode) {
                case REQUEST_CODE_GALLERY:

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(data.getData());

                        FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);

                        copyStream(inputStream, fileOutputStream);

                        fileOutputStream.close();

                        inputStream.close();

                        startCropImage();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case REQUEST_CODE_TAKE_PICTURE:
                    //System.out.println("onact camera");
                    startCropImage();
                    break;
                case REQUEST_CODE_CROP_IMAGE:

                    String path = data.getStringExtra(CropImage.IMAGE_PATH);
                    if (path == null) {

                        return;
                    }

                    bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, baos);

                    b = baos.toByteArray();

                    //String baseimg = Base64.encodeToString(b, Base64.DEFAULT);

                    System.out.println("bitmap in return : " + bitmap);

                    bitmaps_arrayList.add(bitmap);

                    if (mFileTemp.exists()) {
                        mFileTemp.delete();
                    }

                    base64frombitmap(bitmap);

                    int bwidth = bitmap.getWidth();

                    int bheight = bitmap.getHeight();

                    int swidth = iv_signup_img.getWidth();

                    int sheight = iv_signup_img.getHeight();

                    int new_width = swidth;

//                    int new_height = (int) Math.floor((double) bheight * ((double) new_width / (double) bwidth));
//
//                    Bitmap newbitMap = Bitmap.createScaledBitmap(bitmap, new_width, new_height, true);
//
//                    iv_signup_img.setImageBitmap(newbitMap);

                    iv_signup_img.setImageBitmap(bitmap);
                    iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
                    iv_signup_img.setBorderWidth(2);

            }
        } else {
            if (requestCode == REQUEST_CODE_TAKE_PICTURE && resultCode == Activity.RESULT_OK) {
                startCropImage();
            }
        }
    }

    private void startCropImage() {

        //System.out.println("cropimage");

        Intent intent = new Intent(RegisterScreenActivity.this, CropImage.class);

        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());

        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 1);

        intent.putExtra(CropImage.ASPECT_Y, 1);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //System.out.println("takepict inner");
        try {
            Uri mImageCaptureUri = null;

            String state = Environment.getExternalStorageState();

            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            } else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }

            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

            intent.putExtra("return-data", true);

            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyStream(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[1024];

        int bytesRead;

        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Register Screen");

        FlurryAgent.onStartSession(RegisterScreenActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(RegisterScreenActivity.this);
    }

    Tracker getAnalyticTracker() {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }

    private void processResponse(RegisterResponse response) {
        if (response.isSucceed()) {
            emailstr = response.getUser().email;
            phoneStr = response.getUser().phoneNumber;

            preferences.edit()
                    .putString("device_token", response.getUser().deviceToken)
                    .putString("login_token", response.getUser().loginToken)
                    .putString("email", emailstr)
                    .putString("latitude", response.getUser().latitude)
                    .putString("longitude", response.getUser().longitude)
                    .putString("phone_no", phoneStr)
                    .putString("first_name", response.getUser().profile.firstName)
                    .putString("last_name", response.getUser().profile.lastName)
                    .putString("user_image", response.getUser().profile.userImage)
                    .putString("gender", response.getUser().profile.gender)
                    .putString("quotes", response.getUser().profile.quotes)
                    .putString("uid",response.getUser().uid)
                    .apply();


            Picasso.with(getApplicationContext()).load(encoded).into(iv_signup_img);
            iv_signup_img.setBorderColor(Color.parseColor("#F15A51"));
            iv_signup_img.setBorderWidth(2);

            Intent intent = new Intent(RegisterScreenActivity.this, MyFeedActivity.class);
            intent.putExtra("first_name", response.getUser().profile.firstName);
            intent.putExtra("last_name", response.getUser().profile.lastName);
            intent.putExtra("user_image", "");
            intent.putExtra("username", response.getUser().username);
            intent.putExtra("gender", response.getUser().profile.gender);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        } else {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterScreenActivity.this);

            alertDialogBuilder.setTitle(R.string.app_name);

            alertDialogBuilder.setMessage(response.getMessage()).setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            final AlertDialog alertDialog = alertDialogBuilder.create();

            runOnUiThread(new Runnable() {
                public void run() {
                    alertDialog.show();
                }
            });
        }
        LoaderClass.HideProgressWheel(RegisterScreenActivity.this);
    }
}
