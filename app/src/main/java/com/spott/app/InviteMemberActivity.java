package com.spott.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.Tracker;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import com.spott.app.common.CommonUtilities;
import com.spott.app.common.DiamondImageView;
import com.spott.app.common.LoaderClass;

/**
 * Created by Adimn on 10/7/2016.
 */
public class InviteMemberActivity extends Activity implements AbsListView.OnScrollListener {

    PullToRefreshListView list;
    ListInflater listInflater;
    Context context;
    TextView invite_btn;
    ArrayList<HashMap<String, String>> directoryList;
    ArrayList<HashMap<String, String>> selctedArraylist = new ArrayList<>();

    ArrayList<String> uidArray = new ArrayList<>();

    int count = 0;
    int currentScrollState, currentFirstVisibleItem = 0, currentVisibleItemCount = 0;
    String has_more = "";
    ConnectionDetector connDec;
    Alert_Dialog_Manager alert;
    SharedPreferences preferences;
    JSONArray jArray;
    String chipArr = "";
    ArrayList<HashMap<String, String>> invitelist;
    String group_id = "", groupname = "", grpdetail = "", grpimg = "", grptype = "", grptag = "",
            groupcreatdate = "", grpstatus = "", owner_id = "", owner_type = "", you_grp = "";
    ImageView iv_red_icon;
    private boolean isLoading = false;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite_member);
        this.activity = (Activity) InviteMemberActivity.this;
        context=InviteMemberActivity.this;
        list = (PullToRefreshListView) findViewById(R.id.list);
        invite_btn = (TextView) findViewById(R.id.invite_btn);
        directoryList = new ArrayList<HashMap<String, String>>();
        preferences = getSharedPreferences("UTC", MODE_PRIVATE);

        setAdapter();

        Intent intent = getIntent();
        group_id = intent.getStringExtra("group_id");
        groupname = intent.getStringExtra("group_name");
        grpdetail = intent.getStringExtra("group_detail");
        grpimg = intent.getStringExtra("group_image");
        grptype = intent.getStringExtra("group_type");
        grptag = intent.getStringExtra("group_tags");
        groupcreatdate = intent.getStringExtra("group_create_date");
        grpstatus = intent.getStringExtra("group_status");
        owner_id = intent.getStringExtra("owner_id");
        owner_type = intent.getStringExtra("owner_type");
        you_grp = intent.getStringExtra("you_group");

        initialise();
        clickables();
        if (!connDec.isConnectingToInternet()) {
                alert.showNetAlert();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                new DirectoryListData().execute();
            }
        }
        invitelist = new ArrayList<HashMap<String, String>>();
        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
        if (getIntent().hasExtra("List")) {
            arrayList = (ArrayList<HashMap<String, String>>) getIntent()
                    .getSerializableExtra("List");

            jArray = new JSONArray();
            for (int i = 0; i < arrayList.size(); i++) {
                jArray.put(arrayList.get(i).get("uid"));
                if (chipArr.equals("")) {
                    chipArr = arrayList.get(i).get("first_name") + " " + arrayList.get(i).get("last_name");
                } else {
                    chipArr = chipArr + " , " + arrayList.get(i).get("first_name") + " "
                            + arrayList.get(i).get("last_name");
                }
            }
            System.out.println("SELECTED ARRAY " + jArray);
        }


        list.setOnLastItemVisibleListener(new PullToRefreshBase.OnLastItemVisibleListener() {

            @Override
            public void onLastItemVisible() {
                {
                    if (!connDec.isConnectingToInternet()) {
                            alert.showNetAlert();
                    } else {
                        if (!isLoading) {
                            if (has_more.equalsIgnoreCase("true")) {

                                isLoading = true;

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                                    new DirectoryListData()
                                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "loadmore");
                                } else {
                                    new DirectoryListData().execute("loadmore");
                                }
                            } else {
                                Toast
                                        .makeText(activity, "No more items to load.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        });
        list.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {

                count = 0;
                if (!connDec.isConnectingToInternet()) {
                       alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new DirectoryListData().execute();
                    }

                    directoryList.clear();

                }
            }
        });


    }

    private void clickables() {

        iv_red_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        invite_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                for (int i = 0; i < directoryList.size(); i++) {

                    if (directoryList.get(i).get("selected").equals("1")) {
                        selctedArraylist.add(directoryList.get(i));

                        uidArray.add(directoryList.get(i).get("uid"));

                    }
                }
                if (!connDec.isConnectingToInternet()) {
                        alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new InviteMemberData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new InviteMemberData().execute();
                    }

                }
                System.out.println("SELECTED LIST " + selctedArraylist);

                finish();

            }
        });
    }

    private void initialise() {

        iv_red_icon = (ImageView) findViewById(R.id.iv_red_icon);
        connDec = new ConnectionDetector(InviteMemberActivity.this);
        alert = new Alert_Dialog_Manager(context);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        this.currentScrollState = scrollState;
        this.isScrollCompleted();
    }

    private void isScrollCompleted() {
        if (this.currentVisibleItemCount > 0 && this.currentScrollState == SCROLL_STATE_IDLE) {
            /*** In this way I detect if there's been a scroll which has completed ***/
            /*** do the work for load more date! ***/
            if (has_more.equals("yes")) {
                count = directoryList.size();

                if (!connDec.isConnectingToInternet()) {
                    alert.showNetAlert();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        new DirectoryListData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new DirectoryListData().execute();
                    }
                }
            } else {
            }
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        this.currentFirstVisibleItem = firstVisibleItem;
        this.currentVisibleItemCount = visibleItemCount;
    }


    private class ListInflater extends BaseAdapter {

        Context context;
        LayoutInflater inflater;
        ArrayList<HashMap<String, String>> directoryList;

        public ListInflater(Context context, ArrayList<HashMap<String, String>> directoryList) {
            this.context = context;
            this.directoryList = directoryList;
        }

        @Override
        public int getCount() {
            return directoryList.size();
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int i, View convertView, ViewGroup viewGroup) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_selected_member, null);
            ViewHolder holder = new ViewHolder();
            holder.iv_profile_alumni = (DiamondImageView) convertView.findViewById(R.id.iv_profile_alumni);
            holder.tv_name_alumni = (TextView) convertView.findViewById(R.id.tv_name_alumni);
            holder.tv_name_alumni.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Semibold.ttf"));
            holder.tv_date_alumni = (TextView) convertView.findViewById(R.id.tv_date_alumni);
            holder.tv_date_alumni.setTypeface(Typeface.createFromAsset(context.getAssets(), "com/spott/app/fonts/OpenSans_Regular.ttf"));
            holder.tick_icon = (ImageView) convertView.findViewById(R.id.tick_icon);
            String image = (String) directoryList.get(i).get("user_image");
            String genderr = (String) directoryList.get(i).get("gender");
            holder.tick_icon.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (directoryList.get(i).get("selected").equals("0")) {
                        directoryList.get(i).put("selected", "1");
                    } else {
                        directoryList.get(i).put("selected", "0");
                    }
                    notifyDataSetChanged();
                }
            });

            holder.tv_name_alumni.setText(CommonUtilities.decodeUTF8(directoryList.get(i).get("first_name")) + " "
                    + CommonUtilities.decodeUTF8(directoryList.get(i).get("last_name")));
            holder.tv_date_alumni.setText(CommonUtilities.decodeUTF8(directoryList.get(i).get("job_company")));

            if (directoryList.get(i).get("user_image").toString().equals("") || directoryList.get(i).get("user_image").toString() == null) {
                holder.iv_profile_alumni.setImageResource(R.drawable.app);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(2);
            }
            if (directoryList.get(i).get("gender").toString().equals("") || directoryList.get(i).get("gender").toString() == null) {
                holder.iv_profile_alumni.setImageResource(R.drawable.app);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(2);
            }
            if (!directoryList.get(i).get("user_image").toString().equals("")) {

                Picasso.with(context).load(directoryList.get(i).get("user_image").toString()).into(holder.iv_profile_alumni);
                holder.iv_profile_alumni.setBorderColor(Color.parseColor("#F15A51"));
                holder.iv_profile_alumni.setBorderWidth(2);
            }

            if (directoryList.get(i).get("selected").equals("0")) {
                holder.tick_icon.setImageResource(R.drawable.check_box_unsel);
            } else {
                holder.tick_icon.setImageResource(R.drawable.check_box_sel);
            }

            return convertView;
        }

        class ViewHolder {
            DiamondImageView iv_profile_alumni;
            ImageView tick_icon;
            TextView tv_name_alumni, tv_date_alumni;
        }

    }


    public class DirectoryListData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";

        JSONArray jArray;
        JSONObject jObject, jObjResp;

        String message = "";
        boolean isLoadMore = false;

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            LoaderClass.ShowProgressWheel(InviteMemberActivity.this);

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();
                if (strings.length > 0) {
                    if (strings[0].equals("loadmore")) {
                        isLoadMore = true;
                    }
                }
                //Create JSONObject here

                JSONObject jo = new JSONObject();

                jo.put("method", "alumni_list_member");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", group_id);

                if (!isLoadMore) {
                    jo.put("post_value", "0");
                } else {
                    jo.put("post_value", directoryList.size() + "");
                }

                System.out.println("JSON DATA = " + jo);

                // Send POST output.

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Alumni data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            list.onRefreshComplete();
            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");

                has_more = resultObj.getString("has_more");

                if (errCode.equals("0")) {

                    JSONArray jArray = resultObj.getJSONArray("list");

                    if (!isLoadMore) {
                        directoryList.clear();
                    }

                    for (int i = 0; i < jArray.length(); i++) {

                        HashMap<String, String> hmap = new HashMap<>();

                        JSONObject user_dataObj = jArray.getJSONObject(i);

                        String profile_id =user_dataObj.getString("profile_id");
                        String uid = user_dataObj.getString("uid");
                        String first_name = user_dataObj.getString("first_name");
                        String last_name = user_dataObj.getString("last_name");
                        String user_image = user_dataObj.getString("user_image");
                        String user_thumbnail = user_dataObj.getString("user_thumbnail");
                        String hobbies = user_dataObj.getString("hobbies");
                        String phone = user_dataObj.getString("phone");
                        String dob = user_dataObj.getString("dob");
                        String quotes = user_dataObj.getString("quotes");
                        String profile_create_date = user_dataObj.getString("profile_create_date");
                        String profile_modify_date = user_dataObj.getString("profile_modify_date");
                        String gender = user_dataObj.getString("gender");
                        String batch = user_dataObj.getString("batch");
                        String year_of_passing = user_dataObj.getString("year_of_passing");
                        String job_company = user_dataObj.getString("job_company");
                        String designation = user_dataObj.getString("designation");

                        hmap.put("profile_id", profile_id);
                        hmap.put("uid", uid);
                        hmap.put("first_name", first_name);
                        hmap.put("last_name", last_name);
                        hmap.put("user_image", user_image);
                        hmap.put("user_thumbnail", user_thumbnail);
                        hmap.put("hobbies", hobbies);
                        hmap.put("phone", phone);
                        hmap.put("dob", dob);
                        hmap.put("quotes", quotes);
                        hmap.put("profile_create_date", profile_create_date);
                        hmap.put("profile_modify_date", profile_modify_date);
                        hmap.put("gender", gender);
                        hmap.put("batch", batch);
                        hmap.put("year_of_passing", year_of_passing);
                        hmap.put("job_company", job_company);
                        hmap.put("designation", designation);
                        hmap.put("selected", "0");

                        preferences.edit().putString("profile_id", profile_id).commit();


                        directoryList.add(hmap);

                    }
                    if (getIntent().hasExtra("list")) {
                        ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("list");

                        for (int z = 0; z < list.size(); z++) {
                            String getPid = list.get(z).get("profile_id");

                            for (int a = 0; a < directoryList.size(); a++) {
                                String getDirectoryPid = directoryList.get(a).get("profile_id");

                                if (getDirectoryPid.equalsIgnoreCase(getPid)) {
                                    directoryList.get(a).put("selected", "1");//, list.get(z));
                                }
                            }
                        }
                    }
                    setAdapter();

                } else if (errCode.equals("700")) {
                    CommonUtilities.showSessionExpirePopup(activity);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            LoaderClass.HideProgressWheel(InviteMemberActivity.this);
            isLoading = false;
        }
    }

    private void setAdapter()
    {
        if(listInflater==null)
        {
            listInflater = new ListInflater(InviteMemberActivity.this, directoryList);

            list.setAdapter(listInflater);
        }
        else
        {
            listInflater.notifyDataSetChanged();
        }
    }

    public class InviteMemberData extends AsyncTask<String, String, String> {

        Dialog pdDialog;

        String getData = "";
        String message = "";
        JSONArray jArray;
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected String doInBackground(String... strings) {
            URL url;

            try {
                URLConnection urlConn;

                DataOutputStream printout;

                url = new URL(CommonUtilities.LOGIN_URL1);

                urlConn = url.openConnection();

                urlConn.setDoInput(true);

                urlConn.setDoOutput(true);

                urlConn.setUseCaches(false);

                urlConn.setRequestProperty("Content-Type", "application/json");

                urlConn.connect();

                JSONObject jo = new JSONObject();

                jo.put("method", "invite_member");

                jo.put("login_token", preferences.getString("login_token", ""));

                jo.put("group_id", getIntent().getStringExtra("group_id"));

                jo.put("member", new JSONArray(uidArray));

                System.out.println("JSON DATA = " + jo);

                printout = new DataOutputStream(urlConn.getOutputStream());

                String str = jo.toString();

                byte[] data = str.getBytes("UTF-8");

                printout.write(data);

                printout.flush();

                printout.close();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));

                String line;

                StringBuilder content = new StringBuilder();

                while ((line = bufferedReader.readLine()) != null) {
                    content.append(line + "\n");
                }
                bufferedReader.close();

                getData = content.toString();

                System.out.println("Input data is : " + getData);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return getData;
        }


        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);

            try {
                JSONObject resultObj = new JSONObject(result);

                String errCode = resultObj.getString("err-code");

                String message = resultObj.getString("message");


                if (errCode.equals("0")) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getApplicationContext());

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Staff Invited Successfully.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    startActivity(new Intent(InviteMemberActivity.this, GroupMemberOptions.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                                    finish();
                                }
                            });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    activity.runOnUiThread(new Runnable() {
                        public void run() {

                           // alertDialog.show();

                        }
                    });


                } else if (errCode.equals("700")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(InviteMemberActivity.this);

                    alertDialogBuilder.setTitle(R.string.app_name);

                    alertDialogBuilder.setMessage("Your Session has been expired.Please login again.").setCancelable(false)

                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    preferences.edit().clear().commit();((UTCAPP) getApplication()).joingroupList.clear();

                            startActivity(new Intent(InviteMemberActivity.this, LoginScreenActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                            finish();
                        }
                    });

                    final AlertDialog alertDialog = alertDialogBuilder.create();

                    activity.runOnUiThread(new Runnable() {
                        public void run() {

                            alertDialog.show();

                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
           // LoaderClass.HideProgressWheel(InviteMemberActivity.this);
        }
    }
    @Override
    protected void onStart() {
        super.onStart();

        getAnalyticTracker().setScreenName("Invite Member Screen");

        FlurryAgent.onStartSession(InviteMemberActivity.this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        FlurryAgent.onEndSession(InviteMemberActivity.this);
    }

    Tracker getAnalyticTracker()
    {
        UTCAPP myApplication = (UTCAPP) getApplication();

        return myApplication.getDefaultTracker();
    }
}


